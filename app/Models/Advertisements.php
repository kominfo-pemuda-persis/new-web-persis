<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advertisements extends Model
{
    use HasFactory, HasUuids;
    use SoftDeletes;

    protected $fillable = [
        'title',
        'desc',
        'image',
        'width',
        'height',
        'url_ads',
        'clicked',
        'client_id',
        'ads_start',
        'ads_end',
        'created_by'
    ];

    protected $casts = [
        'ads_start' => 'datetime',
        'ads_end' => 'datetime',
    ];
    protected $dates = ['deleted_at'];

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
}
