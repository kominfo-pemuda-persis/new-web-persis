<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;

class SetIklanList extends Model
{
    use HasUuids;

    public $timestamps = false;
    
    protected $table = 'set_iklan_list';
    protected $fillable = ['ads_position', 'ads_id', 'order'];

    public function advertisement()
    {
        return $this->belongsTo(Advertisements::class, 'ads_id');
    }
}
