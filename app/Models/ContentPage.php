<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ContentPage extends Model
{
    use HasFactory, HasUuids;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('page_order', 'desc');
        });
    }

    protected $fillable = ['content_id', 'content', 'embeded_string', 'photo', 'photo_desc', 'photo_caption', 'related_content', 'advertisement_id', 'advertisement_position', 'page_order'];

    public function relateds()
    {
        return $this->belongsTo(Content::class, 'related_content');
    }

    public function advertisement()
    {
        return $this->belongsTo(Advertisements::class, 'advertisement_id');
    }
}
