<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebInfo extends Model
{
    use HasFactory;
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = null;

    protected $fillable = [
        'web_home',
        'web_title',
        'web_icon',
        'web_logo_top',
        'web_info_top',
        'web_logo_footer',
        'web_footer_info',
        'url_socmed_fb',
        'url_socmed_ig',
        'url_socmed_x',
        'url_socmed_yt',
        'url_mail'
    ];
}
