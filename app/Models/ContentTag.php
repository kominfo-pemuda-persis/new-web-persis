<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContentTag extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = [
        'content_id',
        'tag_id'
    ];

    public $timestamps = false;
}
