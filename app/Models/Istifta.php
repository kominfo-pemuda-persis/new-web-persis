<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Istifta extends Model
{
    use HasUuids;

    protected $table = 'istifta';
    protected $fillable = [
        'full_name',
        'show_name',
        'email',
        'send_to_email',
        'topik_pertanyaan',
        'pertanyaan',
        'content_id'
    ];

    public function content(): BelongsTo
    {
        return $this->belongsTo(Content::class, 'content_id');
    }
}
