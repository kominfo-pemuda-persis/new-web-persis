<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Reporter extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = ['reporter_name', 'added_by'];
    public $timestamps = false;

    public function contents(): BelongsToMany
    {
        return $this->belongsToMany(Content::class, 'content_tags', 'tag_id', 'content_id');
    }
}
