<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ContentCategoriesSub extends Model
{
    use HasFactory, HasUuids;

    public $timestamps = false;

    protected $fillable = [
        'category_sub_name',
        'slug',
        'desc',
        'content_categories_id',
        'status',
        'created_by'
    ];

    public function content_categories(): BelongsTo
    {
        return $this->belongsTo(ContentCategories::class, 'content_categories_id');
    }

    public function created_by(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function contents(): HasMany
    {
        return $this->hasMany(Content::class, 'content_category_sub_id');
    }
}
