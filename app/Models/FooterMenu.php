<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FooterMenu extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = [
        'menu_name',
        'desc',
        'slug',
        'parent',
        'status',
        'can_navigate',
        'order',
        'created_by',
        'type',
        'pages_id'
    ];

    public function children()
    {
        return $this->hasMany(FooterMenu::class, 'parent', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(FooterMenu::class, 'parent', 'id');
    }

    public function halaman_tunggal()
    {
        return $this->belongsTo(Page::class, 'pages_id', 'id');
    }
}
