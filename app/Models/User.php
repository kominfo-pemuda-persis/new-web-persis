<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, HasUuids, Notifiable;

    protected $fillable = [
        'full_name',
        'username',
        'email',
        'phone',
        'photo',
        'role',
        'password',
        'token',
        'status'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function contents(): HasMany
    {
        return $this->hasMany(Content::class, 'created_by', 'id');
    }

    public function getContentsCountAttribute()
    {
        return $this->hasMany(Content::class, 'created_by', 'id')->count();
    }

    public function role_name(): BelongsTo
    {
        return $this->belongsTo(Role::class, 'role');
    }
}
