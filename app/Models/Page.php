<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Page extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = [
        'title',
        'featured_image',
        'desc_seo',
        'content',
        'embeded_string',
        'slug',
        'created_by',
        'visited',
        'menu_id',
        'photos',
        'status'
    ];

    protected $casts = [
        'visited' => 'integer',
    ];

    public function creators(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
