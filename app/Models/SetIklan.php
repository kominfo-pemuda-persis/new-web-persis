<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Model;
use App\Models\Advertisements;

class SetIklan extends Model
{
    use HasFactory, HasUuids;

    protected $table = 'set_iklan';
    protected $fillable = ['id', 'title'];

    public function advertisement(): HasManyThrough
    {
        return $this->hasManyThrough(
            Advertisements::class,
            SetIklanList::class,
            'ads_position',
            'id',
            'id',
            'ads_id'
        );
    }
}
