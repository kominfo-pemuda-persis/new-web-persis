<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;

class Content extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = ['title', 'featured_image', 'desc_featured_image', 'desc_seo', 'slug', 'content_type', 'content_category_sub_id', 'is_headline', 'display', 'visited', 'status', 'scheduled_date', 'created_by', 'published_at', 'reporter_id'];

    protected $casts = [
        'scheduled_date' => 'datetime',
        // 'published_at' => 'datetime',
    ];

    public function content_category_sub(): BelongsTo
    {
        return $this->belongsTo(ContentCategoriesSub::class, 'content_category_sub_id');
    }

    public function content_category(): HasOneThrough
    {
        return $this->hasOneThrough(ContentCategories::class, ContentCategoriesSub::class, 'id', 'id', 'content_category_sub_id', 'content_categories_id');
    }

    public function creators(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'content_tags', 'content_id', 'tag_id');
    }

    public function content_pages(): HasMany
    {
        return $this->hasMany(ContentPage::class);
    }

    public function content_tags(): HasMany
    {
        return $this->hasMany(ContentTag::class);
    }

    public function reporters(): BelongsTo
    {
        return $this->belongsTo(Reporter::class, 'reporter_id');
    }


    public function istifta(): HasOne
    {
        return $this->hasOne(Istifta::class, 'content_id');
    }
}
