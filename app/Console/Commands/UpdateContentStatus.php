<?php

namespace App\Console\Commands;

use App\Models\Content;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateContentStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-content-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish Content!';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $now = Carbon::now();
        $this->info($now);
        $query = Content::where('published_at', '<=', $now)
            ->where('status', 3)
            ->update(['status' => 1]);
        $this->info($query . ' Content status updated.');
    }
}
