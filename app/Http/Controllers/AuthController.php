<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    public function showLoginForm()
    {
        return view('cms.auth.login');
    }

    public function index()
    {
        if (Auth::check() || Auth::viaRemember()) {
            return redirect()->route('cms.index');
        } else {
            return $this->showLoginForm();
        }
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'status' => 1], true)) {
            return response()->json([
                'status' => true,
            ]);
        } elseif (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'status' => 0])) {
            Session::flush();
            Auth::logout();

            return response()->json([
                'status' => false,
                'message' => 'Akun dalam status tidak aktif, Mohon hubungi Administrator!',
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Identitas tersebut tidak cocok dengan data kami.',
            ]);
        }
    }

    public function logout(Request $request): RedirectResponse
    {
        Session::flush();
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/cms';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
