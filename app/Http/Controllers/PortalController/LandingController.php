<?php

namespace App\Http\Controllers\PortalController;

use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\ContentCategories;
use App\Models\ContentCategoriesSub;
use App\Models\ContentPage;
use App\Models\Page;
use App\Models\Tag;
use App\Models\SetIklan;
use App\Models\WebInfo;
use App\Models\Menu;
use App\Models\FooterMenu;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LandingController extends Controller
{
    public function index(Request $request)
    {
        // Konten Headline
        $is_headline = $this->getHeadline();
        $main_headline = $is_headline->first();
        $headline_content = $is_headline->skip(1);

        //Konten Terkini
        $kategori_konten = ContentCategories::where('category_name', 'Berita')->value('id');
        $sub_kategori_konten = ContentCategoriesSub::where('content_categories_id', $kategori_konten)->pluck('id');
        // $latest_content = Content::whereNot('is_headline', 1)->whereIn('content_category_sub_id', $sub_kategori_konten)->get();
        $latest_content = Content::where('status', 1)->orderByDesc('published_at')->skip(5)->limit(10)->get();
        $content_count = Content::where('status', 1)->count();
        if ($request->ajax()) {
            $offset = $request->query('offset', 0);
            $limit = 10;

            $contents = Content::with('content_category_sub', 'content_category_sub.content_categories')->where('status', 1)->orderByDesc('published_at')->offset($offset)->limit($limit)->get();

            return response()->json([
                'contents' => $contents,
            ]);
        }

        //Konten Video Terbaru
        $latest_video_content = Content::where('content_type', 'video')->where('status', 1)->orderByDesc('published_at')->limit(3)->get();

        //Konten Gambar Terbaru
        $latest_image_content = Content::where('content_type', 'gambar')->where('status', 1)->orderByDesc('published_at')->limit(8)->get();

        //Widget Pojok Ketua
        $tagTitle = 'Pojok Ketua';
        $ketua_content = $this->getDataWidgetPojokKetua($tagTitle);

        //Widget Tag Terpopuler
        $popular_tags = $this->getPopularContent();

        //Widget Tag Istifta
        $istifta = $this->getIstiftaContent();

        $iklanData = $this->getSetIklanData();

        $webInfos = WebInfo::first();

        $footerMenuGrouped = $this->getFooterMenus();

        $navMenuGrouped = $this->getNavMenus();

        $iklanPopUp = SetIklan::with('advertisement')->where('title', 'Iklan Pop-up')->firstorfail();

        return view('portal.index', array_merge(compact('main_headline', 'headline_content', 'latest_content', 'content_count', 'latest_image_content', 'latest_video_content', 'ketua_content', 'popular_tags', 'webInfos', 'footerMenuGrouped', 'navMenuGrouped', 'iklanPopUp', 'istifta'), $iklanData));
    }

    public function detail_content($caterogy, $slug)
    {
        $content = Content::where('slug', $slug)->where('status', 1)->firstOrFail();

        if ($content->content_type == 'teks') {
            $content_pages = ContentPage::where('content_id', $content->id)
                ->orderBy('page_order', 'asc')
                ->paginate(1);
        } else {
            $content_pages = ContentPage::where('content_id', $content->id)->get();
        }

        $tags = $content->tags;

        $content->increment('visited');

        //Konten Relevan
        $kontenID = $content->id;
        $content_relevant = $this->getRelevantContent($kontenID);

        //Widget Pojok Ketua
        $tagTitle = 'Pojok Ketua';
        $ketua_content = $this->getDataWidgetPojokKetua($tagTitle);

        //Widget Tag Terpopuler
        $popular_tags = $this->getPopularContent();

        $iklanData = $this->getSetIklanData();

        $webInfos = WebInfo::first();

        $footerMenuGrouped = $this->getFooterMenus();

        $navMenuGrouped = $this->getNavMenus();

        return view('portal.detail-content', array_merge(compact('content', 'content_pages', 'tags', 'content_relevant', 'ketua_content', 'popular_tags', 'webInfos', 'footerMenuGrouped', 'navMenuGrouped'), $iklanData));
    }

    public function search_content(Request $request)
    {
        $query = $request->input('q');

        $contents = Content::where(function ($queryBuilder) use ($query) {
            $queryBuilder->where('title', 'ILIKE', "%{$query}%")->orWhere('desc_seo', 'ILIKE', "%{$query}%");
        })
            ->where('status', 1)
            ->paginate(10);

        //Widget Pojok Ketua
        $tagTitle = 'Pojok Ketua';
        $ketua_content = $this->getDataWidgetPojokKetua($tagTitle);

        //Widget Tag Terpopuler
        $popular_tags = $this->getPopularContent();

        $iklanData = $this->getSetIklanData();

        $webInfos = WebInfo::first();

        $footerMenuGrouped = $this->getFooterMenus();

        $navMenuGrouped = $this->getNavMenus();

        return view('portal.result', array_merge(compact('contents', 'query', 'ketua_content', 'popular_tags', 'webInfos', 'footerMenuGrouped', 'navMenuGrouped'), $iklanData));
    }

    public function contents_category($categorySlug)
    {
        $category = ContentCategories::where('slug', $categorySlug)->firstOrFail();
        $contents = $this->getContentByCategoryParent($category->id);

        //Widget Pojok Ketua
        $tagTitle = 'Pojok Ketua';
        $ketua_content = $this->getDataWidgetPojokKetua($tagTitle);

        //Widget Tag Terpopuler
        $popular_tags = $this->getPopularContent();

        $iklanData = $this->getSetIklanData();

        $webInfos = WebInfo::first();

        $footerMenuGrouped = $this->getFooterMenus();

        $navMenuGrouped = $this->getNavMenus();

        return view('portal.result', array_merge(compact('category', 'contents', 'ketua_content', 'popular_tags', 'webInfos', 'footerMenuGrouped', 'navMenuGrouped'), $iklanData));
    }

    public function content_sub_category($caterogy, $categorySub)
    {
        $category_sub = ContentCategoriesSub::where('slug', $categorySub)->firstOrFail();
        $contents = $this->getContentByCategorySub($category_sub->id);

        //Widget Pojok Ketua
        $tagTitle = 'Pojok Ketua';
        $ketua_content = $this->getDataWidgetPojokKetua($tagTitle);

        //Widget Tag Terpopuler
        $popular_tags = $this->getPopularContent();

        $iklanData = $this->getSetIklanData();

        $webInfos = WebInfo::first();

        $footerMenuGrouped = $this->getFooterMenus();

        $navMenuGrouped = $this->getNavMenus();

        return view('portal.result', array_merge(compact('category_sub', 'contents', 'ketua_content', 'popular_tags', 'webInfos', 'footerMenuGrouped', 'navMenuGrouped'), $iklanData));
    }

    public function contents_tag($slug)
    {
        $tag_content = Tag::where('slug', $slug)->firstOrFail();

        $contents = Content::whereHas('tags', function ($query) use ($slug) {
            $query->where('slug', $slug);
        })->paginate(10);

        //Widget Pojok Ketua
        $tagTitle = 'Pojok Ketua';
        $ketua_content = $this->getDataWidgetPojokKetua($tagTitle);

        //Widget Tag Terpopuler
        $popular_tags = $this->getPopularContent();

        $iklanData = $this->getSetIklanData();

        $webInfos = WebInfo::first();

        $footerMenuGrouped = $this->getFooterMenus();

        $navMenuGrouped = $this->getNavMenus();

        return view('portal.result', array_merge(compact('tag_content', 'contents', 'ketua_content', 'popular_tags', 'webInfos', 'footerMenuGrouped', 'navMenuGrouped'), $iklanData));
    }

    public function detail_pages($slug)
    {
        $page = Page::where('slug', $slug)->firstOrFail();

        $page->increment('visited');

        //Widget Pojok Ketua
        $tagTitle = 'Pojok Ketua';
        $ketua_content = $this->getDataWidgetPojokKetua($tagTitle);

        //Widget Tag Terpopuler
        $popular_tags = $this->getPopularContent();

        $iklanData = $this->getSetIklanData();

        $webInfos = WebInfo::first();

        $footerMenuGrouped = $this->getFooterMenus();

        $navMenuGrouped = $this->getNavMenus();

        return view('portal.static-page', array_merge(compact('page', 'ketua_content', 'popular_tags', 'webInfos', 'footerMenuGrouped', 'navMenuGrouped'), $iklanData));
    }

    public function getHeadline()
    {
        // return Content::where('is_headline', 1)->with('content_category_sub')->orderByDesc('published_at')->get();

        return Content::where('status', 1)->with('content_category_sub')->orderByDesc('published_at')->take(5)->get();
    }

    public function getDataWidgetPojokKetua($tagTitle)
    {
        return Content::whereHas('tags', function ($query) use ($tagTitle) {
            $query->where('title', $tagTitle);
        })
            ->where('status', 1)
            ->orderByDesc('published_at')
            ->limit(2)
            ->get();
    }

    // getPopularTags akan mengambi data tag 5 terbanyak dalam jangka waktu 1 bulan kebelakang dari tanggal saat ini
    public function getPopularTags()
    {
        return Tag::select('tags.title', 'tags.slug', DB::raw('COUNT(content_tags.id) as jumlah'))->join('content_tags', 'tags.id', '=', 'content_tags.tag_id')->join('contents', 'content_tags.content_id', '=', 'contents.id')->where('contents.published_at', '>=', now()->subMonth())->where('contents.status', 1)->groupBy('tags.title', 'tags.slug')->orderByDesc('jumlah')->limit(5)->get();
    }

    public function getPopularContent()
    {
        return Content::whereDate('published_at', '>=', Carbon::now()->subDays(7))->where('status', 1)->orderByDesc('visited')->with('content_category_sub')->limit(5)->get();
    }

    public function getIstiftaContent(){
        return Content::where('content_type', 'tanya-jawab')->whereDate('published_at', '>=', Carbon::now()->subDays(7))->where('status', 1)->orderByDesc('visited')->with('content_category_sub')->limit(5)->get();
    }

    public function getRelevantContent($kontenId)
    {
        return Content::leftJoin('content_categories_subs', 'contents.content_category_sub_id', '=', 'content_categories_subs.id')
            ->leftJoin('content_categories', 'content_categories_subs.content_categories_id', '=', 'content_categories.id')
            ->where('contents.status', 1)
            ->where('contents.id', '!=', $kontenId)
            ->where(function ($query) use ($kontenId) {
                $query->whereNull('content_categories.id')->orWhere('content_categories.id', DB::raw("(SELECT content_categories_subs.content_categories_id FROM content_categories_subs WHERE id = (SELECT contents.content_category_sub_id FROM contents WHERE id = '{$kontenId}'))"));
            })
            ->orderByDesc('published_at')
            ->limit(3)
            ->get(['contents.*']);
    }

    public function getContentByCategoryParent($category_id)
    {
        return Content::leftJoin('content_categories_subs', 'contents.content_category_sub_id', '=', 'content_categories_subs.id')->leftJoin('content_categories', 'content_categories_subs.content_categories_id', '=', 'content_categories.id')->where('contents.status', 1)->where('content_categories.id', $category_id)->select('contents.*')->orderByDesc('published_at')->paginate(10);
    }

    public function getContentByCategorySub($category_sub_id)
    {
        return Content::where('content_category_sub_id', $category_sub_id)->where('contents.status', 1)->orderByDesc('published_at')->paginate(10);
    }

    public function getSetIklanData()
    {
        // Definisikan iklan yang akan diambil
        $iklanTitles = [
            'iklanHeader' => 'Iklan Header',
            'iklanFooter' => 'Iklan Footer',
            'iklanSamping1' => 'Iklan Samping 1',
            'iklanSamping2' => 'Iklan Samping 2',
            'iklanStickyKiri' => 'Iklan Sticky Kiri',
            'iklanStickyKanan' => 'Iklan Sticky Kanan',
            'iklanPopUp' => 'Iklan Pop-up',
        ];

        // Ambil semua data iklan berdasarkan title
        $iklans = SetIklan::with([
            'advertisement' => function ($query) {
                $query->orderBy('ads_position')->orderBy('order');
            },
        ])
            ->whereIn('title', array_values($iklanTitles))
            ->get()
            ->keyBy('title');

        // Buat array untuk menyimpan hasil iklan
        $dataIklan = [];

        // Loop untuk mengisi hasil berdasarkan title
        foreach ($iklanTitles as $varName => $title) {
            $dataIklan[$varName] = $iklans->get($title);
        }

        // Ekstrak variabel secara dinamis
        return $dataIklan;
    }

    public function getFooterMenus()
    {
        $menus = FooterMenu::where('status', 1)->orderBy('order', 'asc')->get();

        $groupedMenus = [];
        foreach ($menus as $menu) {
            if ($menu->can_navigate == 0) {
                // Parent menu
                $groupedMenus[$menu->id] = [
                    'title' => $menu->menu_name,
                    'items' => [],
                ];
            }
        }

        foreach ($menus as $menu) {
            if ($menu->can_navigate == 1 && isset($groupedMenus[$menu->parent])) {
                // Child menu
                $groupedMenus[$menu->parent]['items'][] = $menu;
            }
        }

        return $groupedMenus;
    }

    public function getNavMenus()
    {
        $navMenus = Menu::where('status', 1)->orderBy('order', 'asc')->get();

        $groupedNavMenus = [];
        foreach ($navMenus as $navMenu) {
            if (is_null($navMenu->parent)) {
                // Parent menu
                $groupedNavMenus[$navMenu->id] = [
                    'title' => $navMenu->menu_name,
                    'slug' => $navMenu->slug,
                    'items' => [],
                ];
            }
        }

        foreach ($navMenus as $navMenu) {
            if (!is_null($navMenu->parent) && isset($groupedNavMenus[$navMenu->parent])) {
                // Child menu
                $groupedNavMenus[$navMenu->parent]['items'][] = [
                    'title' => $navMenu->menu_name,
                    'slug' => $navMenu->slug,
                ];
            }
        }

        return $groupedNavMenus;
    }
}
