<?php

namespace App\Http\Controllers\CMSController;

use App\Http\Controllers\Controller;
use App\Models\ContentCategories;
use App\Models\ContentCategoriesSub;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

use DataTables;

class KelolaKategoriController extends Controller
{
    public function index(Request $request)
    {
        if (request()->ajax()) {
            if ($request->type_form === "kategori_table") {
                $query = ContentCategories::with('content_categories_sub')->get();
                $data = [];
                $count = 0;
                foreach ($query as $parent) {
                    if ($parent->content_categories_sub->isNotEmpty()) {
                        foreach ($parent->content_categories_sub as $child) {
                            $data[$count]["content_categories_id"] = $parent->id;
                            $data[$count]["content_categories_name"] = $parent->category_name;
                            $data[$count]["content_categories_status"] = $parent->status;
                            $data[$count]["content_categories_sub_id"] = $child->id;
                            $data[$count]["content_categories_sub_name"] = $child->category_sub_name;
                            $data[$count]["content_categories_sub_status"] = $child->status;
                            $count += 1;
                        }
                    } else {
                        $data[$count]["content_categories_id"] = $parent->id;
                        $data[$count]["content_categories_name"] = $parent->category_name;
                        $data[$count]["content_categories_status"] = $parent->status;
                        $data[$count]["content_categories_sub_id"] = null;
                        $count += 1;
                    }
                }
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->rawColumns(['action'])
                    ->make(true);
            } else {
                $search = $request->search ? $request->search : '';

                $query = ContentCategories::select('id as id', 'category_name as text')
                    ->where('category_name', 'like', '%' . $search . '%')
                    ->simplePaginate(10);

                $morePages = true;
                if (empty($query->nextPageUrl())) {
                    $morePages = false;
                }
                $results = [
                    'results' => $query->items(),
                    'pagination' => [
                        'more' => $morePages,
                    ],
                ];

                return response()->json($results);
            }
        }
        return view('cms.kelola_kategori.index');
    }

    public function edit($id)
    {
        $query = ContentCategories::find($id);
        if (!$query) {
            $query = ContentCategoriesSub::with('content_categories')->find($id);
        }

        return response()->json($query);
    }

    public function store(Request $request)
    {
        if (!$request->id) {
            if (!$request->parent_kategori) {
                $rules = [
                    'nama_kategori' => [
                        'required',
                        'unique:\App\Models\ContentCategories,category_name'
                    ],
                ];
            } else {
                $rules = [
                    'nama_kategori' => [
                        'required',
                        Rule::unique('content_categories_subs', 'category_sub_name')->where(function ($query) use ($request) {
                            return $query->where('id', '!=', $request->parent_kategori);
                        }),
                    ],
                ];
            }
            $this->validate($request, $rules);
        } else {
            # code...
        }

        $input = [
            'slug' => Str::slug($request->nama_kategori),
            'desc' => $request->desc_kategori,
            'status' => ($request->status != null) ? $request->status : 1
        ];

        if (!$request->id) {
            $input['created_by'] = Auth::user()->id;
        }

        if ($request->parent_kategori) {
            $input['category_sub_name'] = $request->nama_kategori;
            $input['content_categories_id'] = $request->parent_kategori;

            $status = ContentCategoriesSub::updateOrCreate(['id' => $request->id], $input);
        } else {
            $input['category_name'] = $request->nama_kategori;

            if ($input['status'] != 1 && $request->id) {
                ContentCategoriesSub::where('content_categories_id', $request->id)->update(['status' => 0]);
            }

            $status = ContentCategories::updateOrCreate(['id' => $request->id], $input);
        }
        return redirect()->route('kelola-kategori.index')->with($status ? 'success' : 'error', $status ? 'Data berhasil disimpan.' : 'Data gagal disimpan.');
    }

    public function destroy($id)
    {
        $query = ContentCategories::find($id);
        if ($query) {
            $status = ContentCategories::where('id', $id)->firstorfail()->delete();
        } else {
            $status = ContentCategories::where('id', $id)->firstorfail()->delete();
        }

        return response()->json([
            'status' => $status
        ]);

        return redirect()->route('kelola-kategori.index')->with($status ? 'success' : 'error', $status ? 'Data berhasil dihapus.' : 'Data gagal dihapus.');
    }
}
