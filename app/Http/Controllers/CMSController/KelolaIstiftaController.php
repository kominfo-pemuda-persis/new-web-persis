<?php

namespace App\Http\Controllers\CMSController;

use App\Http\Controllers\Controller;
use App\Models\Advertisements;
use App\Models\Content;
use App\Models\ContentCategories;
use App\Models\ContentCategoriesSub;
use App\Models\Istifta;
use App\Models\Reporter;
use App\Models\Tag;
use App\Rules\ReCaptcha;
use Carbon\Carbon;
use Illuminate\Http\Request;

use DataTables;
use Illuminate\Support\Facades\Validator;

class KelolaIstiftaController extends Controller
{
    public function index(Request $request)
    {
        if (request()->ajax()) {
            if ($request->type_form === "istifta_table") {
                $query = Istifta::orderBy('updated_at', 'desc')
                    ->orderBy('created_at', 'desc');

                return DataTables::of($query)
                    ->addIndexColumn()
                    ->rawColumns(['action'])
                    ->order(function ($query) {
                        $query->orderBy('updated_at', 'desc')
                            ->orderBy('created_at', 'desc');
                    })
                    ->make(true);
            }
        }

        return view('cms.kelola_istifta.index');
    }

    public function create(Request $request, $id)
    {
        // dd($id);
        if (request()->ajax()) {
            $query = Istifta::find($id);
            return response()->json($query);
        }
        $is_headline = Content::where('is_headline', '1')->count('is_headline');

        $istifta = [
            'content_type' => 'tanya-jawab',
            'kategori' => ContentCategoriesSub::where('category_sub_name', 'Istifta')->select('id', 'category_sub_name')->get()[0],
            'istifta' => Istifta::find($id),
            'tags' => Tag::where('title', 'Istifta')->select('id', 'title')->get()[0]
        ];

        return view('cms.kelola_konten.create', compact('is_headline', 'istifta'));
    }

    public function store(Request $request)
    {
        // dd($request->all());

        $validator = Validator::make($request->all(), [
            'g-recaptcha-response' => ['required', new ReCaptcha(0.5)],
            'full_name' => 'required',
            'email' => 'required',
            'topik_pertanyaan' => 'required',
            'pertanyaan' => 'required',
        ], [], [
            // 'g-recaptcha-response.required' => 'Mohon selesaikan captcha.',
            'full_name.required' => 'Mohon isi nama lengkap.',
            'email.required' => 'Mohon isi alamat email.',
            'topik_pertanyaan.required' => 'Mohon pilih topik pertanyaan.',
            'pertanyaan.required' => 'Mohon isi pertanyaan Anda.',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()
            ], 422);
        }

        // $request->validate([
        //     'g-recaptcha-response' => 'required',
        //     'full_name' => 'required',
        //     'email' => 'required',
        //     'topik_pertanyaan' => 'required',
        //     'pertanyaan' => 'required',
        // ], [], [
        //     'g-recaptcha-response.required' => 'Mohon selesaikan captcha.',
        //     'full_name.required' => 'Mohon isi nama lengkap.',
        //     'email.required' => 'Mohon isi alamat email.',
        //     'topik_pertanyaan.required' => 'Mohon pilih topik pertanyaan.',
        //     'pertanyaan.required' => 'Mohon isi pertanyaan Anda.',
        // ]);

        $data = [
            'full_name' => $request->full_name,
            'email' => $request->email,
            'topik_pertanyaan' => $request->topik_pertanyaan,
            'pertanyaan' => $request->pertanyaan,
            'show_name' => $request->show_name ? 1 : 0,
            'send_to_email' => $request->send_to_email ? 1 : 0,
        ];

        try {
            Istifta::create($data);
            return response()->json([
                'success' => true,
                'message' => 'Pertanyaan berhasil dikirim'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Terjadi kesalahan. Silakan coba lagi.'
            ], 500);
        }

        // $status = Istifta::create($data);
        // return redirect()->back()->with($status ? 'success' : 'error', $status ? 'Pertanyaan berhasil dikirm.' : 'Pertanyaan gagal dikirimkan');
    }

    public function destroy(string $id)
    {
        $query = Istifta::findOrFail($id);
        $status = $query->delete();

        return response()->json([
            'status' => $status,
            'message' => "Data berhasil dihapus"
        ]);
    }
}
