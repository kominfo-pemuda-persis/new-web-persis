<?php

namespace App\Http\Controllers\CMSController;

use App\Models\ContentCategories;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class DashboardCMSController extends Controller
{
    public function index(Request $request)
    {
        if (request()->ajax()) {
            if ($request->type_form === "most_contributor") {
                $query = User::has('contents')->withCount('contents');

                return Datatables::of($query)
                    ->addIndexColumn()
                    ->rawColumns(['action'])
                    ->make(true);
            } else if ($request->type_form === "konten_terpopuler") {
                $query = Content::with('content_category', 'content_category_sub', 'creators')
                    ->orderBy('visited', 'desc')
                    ->orderBy('updated_at', 'desc')
                    ->take(5);

                return DataTables::of($query)
                    ->addIndexColumn()
                    ->rawColumns(['action'])
                    ->make(true);
            }
        }
        $berita_hari_ini = Content::whereDate('published_at', now()->format('Y-m-d'))->count();
        $total_berita = Content::count();
        $total_kontributor = Content::distinct('created_by')->count();

        return view('cms.dashboard', compact('berita_hari_ini', 'total_berita', 'total_kontributor'));
    }
}
