<?php

namespace App\Http\Controllers\CMSController;

use App\Http\Controllers\Controller;
use App\Http\Controllers\PortalController\LandingController;
use App\Models\Advertisements;
use App\Models\Content;
use App\Models\ContentCategories;
use App\Models\ContentCategoriesSub;
use App\Models\ContentPage;
use App\Models\ContentTag;
use App\Models\Reporter;
use App\Models\Tag;
use App\Models\User;
use App\Helpers\CustomHelpers;
use App\Models\Istifta;
use App\Models\WebInfo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

use DataTables;
use Illuminate\Support\Facades\DB;

class KelolaKontenController extends Controller
{
    public function index(Request $request)
    {
        $headline_count = Content::sum('is_headline');

        if (request()->ajax()) {
            if ($request->type_form === "konten_table") {
                $query = Content::with('content_category','content_category_sub', 'creators');
                // ->orderBy('is_headline', 'desc')
                // ->orderBy('updated_at', 'desc')
                // ->orderBy('created_at', 'desc');

                return DataTables::of($query)
                    ->addIndexColumn()
                    ->editColumn('title', function ($row) {
                        return strip_tags($row->title);
                    })
                    ->rawColumns(['action'])
                    // ->order(function ($query) {
                    //     $query->orderBy('contents.is_headline', 'desc')
                    //         ->orderBy('contents.updated_at', 'desc')
                    //         ->orderBy('contents.created_at', 'desc');
                    // })
                    ->make(true);
            } else if ($request->type_form === "filter_select") {
                $search = $request->search ? $request->search : '';
                if ($request->select_form === "filter_kategori") {
                    $query = ContentCategoriesSub::has('contents')
                        ->select('id as id', 'category_sub_name as text')
                        ->where('category_sub_name', 'like', '%' . $search . '%')
                        ->simplePaginate(10);
                } else if ($request->select_form === "filter_penulis") {
                    $query = User::has('contents')
                        ->select('id as id', 'full_name as text')
                        ->where('full_name', 'like', '%' . $search . '%')
                        ->simplePaginate(10);
                }

                $morePages = true;
                if (empty($query->nextPageUrl())) {
                    $morePages = false;
                }
                $results = [
                    'results' => $query->items(),
                    'pagination' => [
                        'more' => $morePages,
                    ],
                ];

                return response()->json($results);
            }
        }
        return view('cms.kelola_konten.index', compact('headline_count'));
    }

    public function preview($id)
    {
        $content = Content::find($id);

        if ($content->content_type == 'teks') {
            $content_pages = ContentPage::where('content_id', $content->id)
                ->orderBy('page_order', 'asc')
                ->paginate(1);
        } else {
            $content_pages = ContentPage::where('content_id', $content->id)->get();
        }

        $tags = $content->tags;

        //Konten Relevan
        $kontenID = $content->id;
        $content_relevant = (new LandingController)->getRelevantContent($kontenID);

        //Widget Pojok Ketua
        $tagTitle = 'Pojok Ketua';
        $ketua_content = (new LandingController)->getDataWidgetPojokKetua($tagTitle);

        //Widget Tag Terpopuler
        $popular_tags = (new LandingController)->getPopularContent();
        $iklanData = (new LandingController)->getSetIklanData();
        $footerMenuGrouped = (new LandingController)->getFooterMenus();
        $navMenuGrouped = (new LandingController)->getNavMenus();
        $webInfos = WebInfo::first();

        return view('portal.detail-content', array_merge(compact('content', 'content_pages', 'tags', 'content_relevant', 'ketua_content', 'popular_tags', 'webInfos', 'footerMenuGrouped', 'navMenuGrouped'), $iklanData));
    }

    public function create(Request $request)
    {
        $is_headline = Content::where('is_headline', '1')->count('is_headline');

        if (request()->ajax()) {
            if ($request->type_form === "filter_select") {
                $search = $request->search ? $request->search : '';
                $results = [];

                if ($request->select_form === "filter_kategori") {
                    $query = ContentCategories::with(['content_categories_sub' => function ($query) use ($search) {
                        if ($search) {
                            $query->where('category_sub_name', 'ILIKE', '%' . $search . '%');
                        }
                    }])->simplePaginate(10);

                    foreach ($query as $value) {
                        $children = $value->content_categories_sub->map(function ($content_categories_sub) {
                            return [
                                'id' => $content_categories_sub->id,
                                'text' => $content_categories_sub->category_sub_name
                            ];
                        })->toArray();

                        if (count($children) > 0) {
                            $results[] = [
                                'text' => $value->category_name,
                                'children' => $children
                            ];
                        }
                    }
                    return response()->json([
                        'results' => $results,
                        'pagination' => [
                            'more' => $query->hasMorePages()
                        ]
                    ]);
                } else if ($request->select_form === "filter_tag") {
                    $query = Tag::select('id as id', 'title as text')
                        ->where('title', 'ILIKE', '%' . $search . '%')
                        ->simplePaginate(10);
                } else if ($request->select_form === "filter_related_content") {
                    $oneYearAgo = Carbon::now(); // Tanggal hari ini
                    $oneYearAgo->subYear();
                    $query =
                        Content::select(DB::raw("id as id, REGEXP_REPLACE(title, '<[^>]*>', '', 'g') as text"))
                        ->where(
                            'title',
                            'ILIKE',
                            '%' . $search . '%'
                        )
                        ->where('published_at', '>=', $oneYearAgo)
                        ->simplePaginate(10);
                } else if ($request->select_form === "filter_advertisement") {
                    $query = Advertisements::select('id as id', 'title as text')
                        ->where('title', 'like', '%' . $search . '%')
                        ->simplePaginate(10);
                } else if ($request->select_form === "filter_reporter") {
                    $query = Reporter::select('id as id', 'reporter_name as text')
                        ->where('reporter_name', 'like', '%' . $search . '%')
                        ->simplePaginate(10);
                } else if ($request->select_form === "filter_istifta") {
                    $query = Istifta::whereNull('content_id')
                        ->select('id as id', 'pertanyaan as text')
                        ->simplePaginate(10);
                }

                $morePages = true;
                if (empty($query->nextPageUrl())) {
                    $morePages = false;
                }
                $results = [
                    'results' => $query->items(),
                    'pagination' => [
                        'more' => $morePages,
                    ],
                ];

                return response()->json($results);
            }
        }
        $kategori_istifta = ContentCategoriesSub::where('category_sub_name', 'Istifta')->select('id as id', 'category_sub_name as text')->first();
        return view('cms.kelola_konten.create', compact('is_headline', 'kategori_istifta'));
    }

    public function edit(Request $request, $id)
    {
        $data = Content::with([
            'content_category_sub',
            'creators',
            'tags',
            'content_pages' => function ($query) {
                $query->orderBy('page_order');
            },
            'reporters',
            'content_pages.relateds',
            'content_pages.advertisement',
            'istifta'
        ])->find($id);
        if ($data) {
            // Strip tags from the main title
            $data->title = strip_tags($data->title);

            // Iterate over content_pages and relateds to strip tags
            foreach ($data->content_pages as $page) {
                if (isset($page->relateds)) {
                    $page->relateds->title = strip_tags($page->relateds->title);
                }
            }
        }
        $is_headline = Content::where('is_headline', '1')->count('is_headline');

        if (request()->ajax()) {
            if ($request->type_form === "filter_select") {
                $search = $request->search ? $request->search : '';
                $results = [];

                if ($request->select_form === "filter_kategori") {
                    $query = ContentCategories::with(['content_categories_sub' => function ($query) use ($search) {
                        if ($search) {
                            $query->where('category_sub_name', 'ILIKE', '%' . $search . '%');
                        }
                    }])->simplePaginate(10);

                    foreach ($query as $value) {
                        $children = $value->content_categories_sub->map(function ($content_categories_sub) {
                            return [
                                'id' => $content_categories_sub->id,
                                'text' => $content_categories_sub->category_sub_name
                            ];
                        })->toArray();

                        if (count($children) > 0) {
                            $results[] = [
                                'text' => $value->category_name,
                                'children' => $children
                            ];
                        }
                    }
                    return response()->json([
                        'results' => $results,
                        'pagination' => [
                            'more' => $query->hasMorePages()
                        ]
                    ]);
                } else if ($request->select_form === "filter_tag") {
                    $query = Tag::select('id as id', 'title as text')
                        ->where('title', 'ILIKE', '%' . $search . '%')
                        ->simplePaginate(10);
                } else if ($request->select_form === "filter_related_content") {
                    $query = Content::select('id as id', 'title as text')
                        ->where('title', 'ILIKE', '%' . $search . '%')
                        ->simplePaginate(10);
                }

                $morePages = true;
                if (empty($query->nextPageUrl())) {
                    $morePages = false;
                }
                $results = [
                    'results' => $query->items(),
                    'pagination' => [
                        'more' => $morePages,
                    ],
                ];

                return response()->json($results);
            }
        }
        return view('cms.kelola_konten.create', compact('is_headline', 'data'));
    }

    public function store(Request $request)
    {
        // dd($request->all());

        $rules = [
            'title' => 'required',
            'desc_seo' => 'required',
            'content_type' => 'required',
            'content_category_id' => 'required',
            'reporter' => 'required',
        ];
        $messages = [
            'title.required' => 'Judul Konten Harus Diisi',
            'desc_seo.required' => 'Deskripsi SEO Harus Diisi',
            'content_type.required' => 'Jenis Konten Harus Dipilih',
            'content_category_id.required' => 'Kategori Konten Harus Dipilih',
            'reporter.required' => 'Reporter Konten Harus Dipilih',
        ];
        $this->validate($request, $rules, $messages);

        // dd($request->all());

        $content_data = [
            'title' => $request->title,
            'desc_seo' => $request->desc_seo,
            'slug' => $request->slug,
            'content_type' => $request->content_type,
            'content_category_sub_id' => $request->content_category_id,
            'is_headline' => (bool)$request->is_headline ?? 0,
            'status' => $request->button_status === 'publish_button' ? (Carbon::createFromFormat('d/m/Y H:i', $request->published_at ?? now()->format('d/m/Y H:i')) <= now() ? 1 : 3) : ($request->status ?? 0),
            'published_at' => $request->published_at ? Carbon::createFromFormat('d/m/Y H:i', $request->published_at ?? now()->format('d/m/Y H:i')) : now(),
            'created_by' => $request->created_by ?? Auth::user()->id,
            'reporter_id' => Str::isUuid($request->reporter) ? $request->reporter : Reporter::create(['reporter_name' => $request->reporter, 'added_by' => Auth::user()->id])->id
        ];

        switch ($request->content_type) {
            case 'teks':
            case 'tanya-jawab':
                $desc_konten_text_count = count($request->input('desc_konten_text', []));
                $rules = [
                    'featured_image' => 'sometimes|required',
                    'desc_featured_image' => 'required',
                    'desc_konten_text.*' => 'required|filled',
                    // 'content_text_image' => 'sometimes|required',
                    // 'content_text_image.*' => 'required',
                    // 'content_text_image_desc' => 'required|array|size:' . $desc_konten_text_count,
                    // 'content_text_image_desc.*' => 'required',
                    'related_content' => 'required|array|size:' . $desc_konten_text_count,
                    'related_content.*' => 'required',
                    // 'advertisement_id' => 'required|array|size:' . $desc_konten_text_count,
                    // 'advertisement_id.*' => 'required',
                    // 'advertisement_position' => 'required|array|size:' . $desc_konten_text_count,
                    // 'advertisement_position.*' => 'required',
                ];
                $messages = [
                    'featured_image.required' => 'Gambar Sampul Wajib Diisi',
                    'desc_featured_image.required' => 'Deskripsi Gambar Sampul Wajib Diisi',
                    'desc_konten_text.*' => 'Isi Konten Halaman Wajib Diisi',
                    // 'content_text_image.*' => 'Gambar Pada Halaman Konten Wajib Diisi',
                    // 'content_text_image_desc.*' => 'Deskripsi Gambar Pada Halaman Konten Wajib Diisi',
                    'related_content.*' => 'Rekomendasi Konten Wajib Diisi',
                    // 'advertisement_id.*' => 'Iklan pada Konten Wajib Dipilih',
                    // 'advertisement_position.*' => 'Posisi Iklan pada Konten Wajib Dipilih',
                ];
                $this->validate($request, $rules, $messages);

                $featured_image = $request->file('featured_image');
                $content_data['desc_featured_image'] = $request->desc_featured_image;
                $content_data['display'] = count($request->desc_konten_text) <= 1 ? 'all' : 'pages';

                foreach ($request->desc_konten_text as $key => $value) {
                    $content_page_data[$key] = [
                        'content' => $request->desc_konten_text[$key],
                        'photo_desc' => @$request->content_text_image_desc[$key],
                        'related_content' => $request->related_content[$key],
                        'advertisement_id' => @$request->advertisement_id[$key],
                        'advertisement_position' => @$request->advertisement_position[$key],
                        'page_order' => $key,
                    ];

                    $photo[$key] = @$request->file('content_text_image')[$key];
                }

                break;
            case 'gambar':
                $rules = [
                    'pilihGambar' => 'sometimes|required',
                    'pilihGambar.*' => 'required',
                    'desc_gambar' => 'required',
                    'desc_gambar.*' => 'required',
                    'caption' => 'required',
                    'caption.*' => 'required',
                ];
                $messages = [
                    'pilihGambar.*' => 'Gambar Pada Halaman Konten Wajib Diisi',
                    'desc_gambar.*' => 'Deskripsi Gambar Pada Halaman Konten Wajib Diisi',
                    'caption.*' => 'Caption Gambar Pada Halaman Konten Wajib Diisi',
                ];
                $this->validate($request, $rules, $messages);

                $featured_image = @$request->file('pilihGambar')[0];
                $content_data['display'] = 'pages';

                foreach ($request->desc_gambar as $key => $value) {
                    $content_page_data[$key] = [
                        'content' => $value,
                        'photo_desc' => $value,
                        'photo_caption' => $request->caption[$key],
                        'related_content' => null,
                        'page_order' => $key,
                    ];

                    $photo[$key] = @$request->file('pilihGambar')[$key];
                }

                break;
            case 'video':
                $rules = [
                    'featured_image' => 'sometimes|required',
                    'desc_kategori' => 'required',
                    'embed_video' => 'required'
                ];
                $messages = [
                    'featured_image.required' => 'Gambar Sampul Wajib Diisi',
                    'desc_kategori.required' => 'Deskripsi Konten Wajib Diisi',
                    'embed_video.required' => 'Embed Video Wajib Diisi'
                ];
                $this->validate($request, $rules, $messages);

                $featured_image = $request->file('featured_image');
                $content_data['display'] = 'all';

                $content_page_data[] = [
                    'content' => $request->desc_kategori,
                    'photo' => '-',
                    'photo_desc' => '-',
                    'related_content' => null,
                    'embeded_string' => $request->embed_video,
                    'page_order' => 0,
                ];

                break;
        }

        // dd($content_data, $content_page_data, $photo, $request->content_pages_id, $request->all());
        $status_content_data = Content::updateOrCreate(['id' => $request->content_id], $content_data);

        $image_type = 'konten/' . $status_content_data->id . '/';
        if ($featured_image) {
            if ($request->content_id) {
                $image_file = $image_type . basename($status_content_data->featured_image);
                Storage::disk('s3')->delete($image_file);
            }
            $featured_image = CustomHelpers::upload_minio($image_type, $featured_image, 'featured_image');

            $status_content_data->featured_image = $featured_image;
            $status_content_data->save();
        }

        if (array_filter($request->content_pages_id)) {
            $query = ContentPage::where('content_id', $request->content_id)
                ->whereNotIn('id', array_filter($request->content_pages_id));
            if ($query) $query->delete();
        }

        foreach ($content_page_data as $key => $value) {
            $value['content_id'] = $status_content_data->id;
            // dd($value);

            $status_content_pages = ContentPage::updateOrCreate(['id' => @$request->content_pages_id[$key]], $value);
            if ($photo[$key]) {
                if (@$request->content_pages_id[$key]) {
                    $image_file = $image_type . basename($status_content_pages->photo);
                    Storage::disk('s3')->delete($image_file);
                }
                $uploaded_photo = CustomHelpers::upload_minio($image_type, $photo[$key], 'photo-' . $key);

                $status_content_pages->photo = $uploaded_photo;
                $status_content_pages->save();
            }
        }

        $remove_tag = ContentTag::where('content_id', $status_content_data->id)->delete();
        foreach ($request->filter_tag as $value) {
            $tagId = Str::isUuid($value) ? $value : Tag::create([
                'title' => Str::title($value),
                'desc' => Str::title($value),
                'slug' => Str::slug($value),
                'created_by' => Auth::user()->id
            ])->id;

            $create_tag = ContentTag::create([
                'content_id' => $status_content_data->id,
                'tag_id' => $tagId
            ]);
        }

        if ($request->jenis_konten === "tanya-jawab" && $request->istifta_id) {
            $istifta = Istifta::find($request->istifta_id);
            $istifta->content_id = $status_content_data->id;

            $istifta->save();
        }

        return redirect()->route('kelola-konten.index')->with('success', 'Data berhasil disimpan.');
    }

    public function change_status($id, Request $request)
    {
        $query = Content::findorfail($id);
        if ($request->type === 'headline') {
            $query->is_headline = $request->status;
            if ($request->status) {
                $message = "Konten dijadikan Headline";
            } else {
                $message = "Konten dihapus dari Headline";
            }
        } else {
            $query->status = $request->status;

            if ($request->status === 2) {
                $message = "Konten telah diarsipkan";
            } else {
                $message = "Konten telah ditakedown";
            }
        }
        $status = $query->save();

        return response()->json([
            'status' => $status,
            'message' => $message
        ]);
    }

    public function destroy($id)
    {
        $istifta = Istifta::where('content_id', $id)->first();
        $istifta->content_id = null;
        $istifta->save();

        $query = Content::findorfail($id);
        $query->content_pages()->delete();
        $query->content_tags()->delete();
        $status = $query->delete();

        return response()->json([
            'status' => $status,
            'message' => "Data berhasil dihapus"
        ]);
    }

    public function getRelatedContent(Request $request)
    {
        $query = Content::where('id',  $request->id)->first();
        $response = array(
            "id" => $query->id,
            "name" => $query->title
        );
        return response()->json($response);
    }

    public function getAdvertisementContent(Request $request)
    {
        $query = Advertisements::where('id',  $request->id)->first();
        $response = array(
            "id" => $query->id,
            "name" => $query->title
        );
        return response()->json($response);
    }
}
