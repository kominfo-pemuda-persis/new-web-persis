<?php

namespace App\Http\Controllers\CMSController\KelolaPortal;

use App\Helpers\CustomHelpers;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\WebInfo;
use Illuminate\Support\Facades\Storage;

class KontenFooterController extends Controller
{
    public function index()
    {
        $data = WebInfo::first();

        return view('cms.kelola_portal.konten_footer.index', compact('data'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'web_footer_info' => 'required|string',
            'url_socmed_fb' => 'required|string|url|max:100',
            'url_socmed_ig' => 'required|string|url|max:100',
            'url_socmed_x' => 'required|string|url|max:100',
            'url_socmed_yt' => 'required|string|url|max:100',
            'web_logo_footer' => 'required_if:web_logo_footer,null|file|mimetypes:image/*',
        ], [
            'required' => ':attribute wajib diisi.',
            'required_if' => ':attribute wajib diisi.',
            'max' => ':attribute maksimal :max karakter.',
            'file' => ':attribute harus berupa file.',
            'mimetypes' => ':attribute harus gambar yang valid.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $data = WebInfo::first();

        if ($request->hasFile('web_logo_footer')) {
            if ($data['web_logo_footer']) {
                Storage::disk('s3')->delete(str_replace(env('AWS_ENDPOINT').'/'.env('AWS_BUCKET'), '', $data['web_logo_footer']));
            }

            $image_url = CustomHelpers::upload_minio('web_info/web_logo_footer', $request->file('web_logo_footer'), 'logo-navbar');
            $data->web_logo_footer = $image_url;
        }

        $data->web_footer_info = $request->web_footer_info;
        $data->url_socmed_fb = $request->url_socmed_fb;
        $data->url_socmed_ig = $request->url_socmed_ig;
        $data->url_socmed_x = $request->url_socmed_x;
        $data->url_socmed_yt = $request->url_socmed_yt;
        $data->save();

        return redirect()->route('kelola-portal.konten-footer.index')->with('success', 'Data berhasil diperbarui.');
    }
}
