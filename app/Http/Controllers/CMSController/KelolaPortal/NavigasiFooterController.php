<?php

namespace App\Http\Controllers\CMSController\KelolaPortal;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FooterMenu;
use App\Models\Page;

class NavigasiFooterController extends Controller
{
    public function index(Request $request)
    {
        if (request()->ajax()) {
            if ($request->get_data === "menus") {
                $search = $request->search ? $request->search : '';
                $results = [];

                $query = FooterMenu::select('id', 'menu_name as text')
                    ->where('menu_name', 'like', '%' . $search . '%')->whereNull('parent')->simplePaginate(10);

                foreach ($query as $value) {
                    $results[] = [
                        'id' => $value->id,
                        'text' => $value->category_name,
                    ];
                }

                $morePages = true;
                if (empty($query->nextPageUrl())) {
                    $morePages = false;
                }
                $results = [
                    'results' => $query->items(),
                    'pagination' => [
                        'more' => $morePages,
                    ],
                ];

                return response()->json($results);
            } else if ($request->get_data === "menu") {
                if ($request->id) {
                    $menu = FooterMenu::with(['children', 'parent', 'halaman_tunggal'])->find($request->id);
                    return response()->json([
                        'status' => 'success',
                        'data' => $menu
                    ]);
                }
            } else if ($request->get_data === "halaman_tunggal") {
                $search = $request->search ? $request->search : '';

                $query = Page::select('id as id', 'title as text')
                    ->where('title', 'like', '%' . $search . '%')
                    ->where('status', 1)
                    ->simplePaginate(10);

                $morePages = true;
                if (empty($query->nextPageUrl())) {
                    $morePages = false;
                }
                $results = [
                    'results' => $query->items(),
                    'pagination' => [
                        'more' => $morePages,
                    ],
                ];

                return response()->json($results);
            }
        }

        $menus = FooterMenu::with('children')->where('parent', null)->orderBy('order', 'asc')->get();

        return view('cms.kelola_portal.navigasi_footer.index', compact('menus'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'menu_name' => 'required|string|max:255',
            'jenis_menu' => 'required|string',
            'parent_menu' => 'nullable|required_if:jenis_menu,Child Menu|exists:footer_menus,id',
            'tautan' => 'nullable|required_if:sumber_data,Redirect Link',
            'halaman_khusus' => 'nullable|required_if:sumber_data,Halaman Khusus',
        ], [
            'tautan.required_if' => 'Tautan wajib diisi.',
            'halaman_khusus.required_if' => 'Tautan wajib diisi.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $msg = 'Data berhasil ditambah.';
        $order = 0;

        if (@$request->parent_menu) {
            $order = FooterMenu::where('parent', $request->parent_menu)->max('order');
        } else {
            $order = FooterMenu::where('parent', null)->max('order');
        }

        $data = [
            'menu_name' => $request->menu_name,
            'slug' => @$request->slug ?? (@$request->tautan ?? '#'),
            'can_navigate' => @$request->jenis_menu == 'Child Menu' ? 1 : 0,
            'status' => 1,
            'parent' => @$request->parent_menu ?? null,
            'type' => @$request->sumber_data ?? null,
            'pages_id' => @$request->halaman_khusus ?? null,
        ];

        if ($request->halaman_khusus) {
            $halaman_tunggal_slug = Page::find($request->halaman_khusus)->slug;
            $data['slug'] = route('portal.pages', $halaman_tunggal_slug);
        }

        if ($request->id) {
            FooterMenu::find($request->id)->update($data);

            $msg = 'Data berhasil diubah.';
        } else {
            $data['order'] = $order + 1;
            $data['created_by'] = auth()->user()->id;

            FooterMenu::create($data);
        }

        return redirect()->route('kelola-portal.navigasi-footer.index')->with('success', $msg);
    }

    public function update(Request $request)
    {
        $data_menu = @$request->data_menu;
        $data_submenu = @$request->data_submenu;

        $data_order_menu = @$request->data_order_menu;
        $data_order_submenu = @$request->data_order_submenu;

        if ($data_menu) {
            foreach ($data_menu as $menu) {
                FooterMenu::find($menu)->update([
                    'status' => 1,
                    'order' => @$data_order_menu[$menu]
                ]);

                if (@$data_submenu[$menu]) {
                    if (count($data_submenu[$menu]) > 0) {
                        foreach ($data_submenu[$menu] as $submenu) {
                            FooterMenu::find($submenu)->update([
                                'status' => 1,
                                'order' => $data_order_submenu[$submenu]
                            ]);
                        }
                    }
                }
            }
        }

        return redirect()->route('kelola-portal.navigasi-footer.index')->with('success', 'Data berhasil diperbarui.');
    }

    public function toggleActive($id)
    {
        $menu = FooterMenu::findOrFail($id);

        $menu->update([
            'status' => $menu->status == 1 ? 0 : 1
        ]);

        if (count($menu->children) > 0) {
            $menu->children()->update([
                'status' => $menu->status
            ]);
        }

        return redirect()->route('kelola-portal.navigasi-footer.index')->with('success', 'Data berhasil diubah.');
    }

    public function destroy($id)
    {
        $menu = FooterMenu::findOrFail($id);

        if ($menu->children->count() > 0) {
            $menu->children()->delete();
        }

        $menu->delete();

        return redirect()->route('kelola-portal.navigasi-footer.index')->with('success', 'Data berhasil dihapus.');
    }
}
