<?php

namespace App\Http\Controllers\CMSController\KelolaPortal;

use App\Helpers\CustomHelpers;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\WebInfo;
use Illuminate\Support\Facades\Storage;
use Cache;

class PengaturanPortalController extends Controller
{
    public function index()
    {
        $data = WebInfo::first();

        return view('cms.kelola_portal.pengaturan_portal.index', compact('data'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'web_title' => 'required|string|max:100',
                'web_info_top' => 'required|string',
                'web_logo_top' => 'required_if:web_logo_top,null|file|mimetypes:image/*',
                'web_icon' => 'required_if:web_icon,null|file|mimetypes:image/*',
            ],
            [
                'required' => ':attribute wajib diisi.',
                'required_if' => ':attribute wajib diisi.',
                'max' => ':attribute maksimal :max karakter.',
                'file' => ':attribute harus berupa file.',
                'mimetypes' => ':attribute harus gambar yang valid.',
            ],
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $data = WebInfo::first();

        if ($request->hasFile('web_logo_top')) {
            if ($data['web_logo_top']) {
                Storage::disk('s3')->delete(str_replace(env('AWS_ENDPOINT') . '/' . env('AWS_BUCKET'), '', $data['web_logo_top']));
            }

            $image_url = CustomHelpers::upload_minio('web_info/web_logo_top', $request->file('web_logo_top'), 'logo-navbar');
            $data->web_logo_top = $image_url;
        }

        if ($request->hasFile('web_icon')) {
            if ($data['web_icon']) {
                Storage::disk('s3')->delete(str_replace(env('AWS_ENDPOINT') . '/' . env('AWS_BUCKET'), '', $data['web_icon']));
            }

            $image_url = CustomHelpers::upload_minio('web_info/web_icon', $request->file('web_icon'), 'favicon');
            $data->web_icon = $image_url;
        }

        $data->web_title = $request->web_title;
        $data->web_info_top = $request->web_info_top;
        $data->save();

        Cache::forget('app_name');

        return redirect()->route('kelola-portal.pengaturan-portal.index')->with('success', 'Data berhasil diperbarui.');
    }
}
