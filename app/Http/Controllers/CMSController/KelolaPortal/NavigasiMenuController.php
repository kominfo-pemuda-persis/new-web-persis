<?php

namespace App\Http\Controllers\CMSController\KelolaPortal;

use App\Models\HalamanTunggal;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\ContentCategories;
use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\Page;

class NavigasiMenuController extends Controller
{
    public function index(Request $request)
    {
        if (request()->ajax()) {
            if ($request->get_data === "kategori") {
                $search = $request->search ? $request->search : '';
                $results = [];

                $query = ContentCategories::with(['content_categories_sub' => function ($query) use ($search) {
                    if ($search) {
                        $query->where('category_sub_name', 'ILIKE', '%' . $search . '%');
                    }
                }])->simplePaginate(10);

                foreach ($query as $value) {
                    $children = $value->content_categories_sub->map(function ($content_categories_sub) use ($value) {
                        return [
                            'id' => '/' . $value->slug . '/' . $content_categories_sub->slug,
                            'text' => $content_categories_sub->category_sub_name
                        ];
                    })->toArray();

                    if (count($children) > 0) {
                        $results[] = [
                            'id' => '/' . $value->slug,
                            'text' => $value->category_name,
                        ];

                        foreach ($children as $child) {
                            $results[] = $child;
                        }
                    }
                }

                return response()->json([
                    'results' => $results,
                    'pagination' => [
                        'more' => $query->hasMorePages()
                    ]
                ]);
            } else if ($request->get_data === "menus") {
                $search = $request->search ? $request->search : '';
                $results = [];

                $query = Menu::select('id', 'menu_name as text')
                    ->where('menu_name', 'like', '%' . $search . '%')->whereNull('parent')->simplePaginate(10);

                foreach ($query as $value) {
                    $results[] = [
                        'id' => $value->id,
                        'text' => $value->category_name,
                    ];
                }

                $morePages = true;
                if (empty($query->nextPageUrl())) {
                    $morePages = false;
                }
                $results = [
                    'results' => $query->items(),
                    'pagination' => [
                        'more' => $morePages,
                    ],
                ];

                return response()->json($results);
            } else if ($request->get_data === "menu") {
                if ($request->id) {
                    $menu = Menu::with(['children', 'parent'])->find($request->id);
                    return response()->json([
                        'status' => 'success',
                        'data' => $menu
                    ]);
                }
            } else if ($request->get_data === "halaman_tunggal") {
                $search = $request->search ? $request->search : '';

                $query = Page::select('id as id', 'title as text')
                    ->where('title', 'like', '%' . $search . '%')
                    ->where('status', 1)
                    ->simplePaginate(10);

                $morePages = true;
                if (empty($query->nextPageUrl())) {
                    $morePages = false;
                }
                $results = [
                    'results' => $query->items(),
                    'pagination' => [
                        'more' => $morePages,
                    ],
                ];

                return response()->json($results);
            }
        }

        $data_menu_aktif = Menu::with('children')->where('parent', null)->where('status', 1)->orderBy('order', 'asc')->get();
        $data_menu_non_aktif = Menu::with('children')->where('parent', null)->where('status', 0)->get();

        return view('cms.kelola_portal.navigasi_menu.index', [
            'data_menu_aktif' => $data_menu_aktif,
            'data_menu_non_aktif' => $data_menu_non_aktif
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sumber_data' => 'required|string',
            'menu_name' => 'required|string|max:255',
            'tipe_menu' => 'required|string',
            'slug' => 'nullable|required_if:sumber_data,Kategori|string',
            'tautan' => 'nullable|required_if:sumber_data,Redirect Link|url',
            'parent_menu' => 'nullable|required_if:tipe_menu,Child|exists:menus,id',
            'halaman_khusus' => 'nullable|required_if:sumber_data,Halaman Khusus',
        ], [
            'sumber_data.required' => 'Sumber data wajib diisi.',
            'menu_name.required' => 'Nama menu wajib diisi.',
            'slug.required_if' => 'Kategori wajib diisi.',
            'tautan.required_if' => 'Tautan wajib diisi.',
            'halaman_khusus.required_if' => 'Halaman Khusus wajib diisi.',
            'max' => ':attribute maksimal :max karakter.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $msg = 'Data berhasil ditambah.';
        $order = 0;

        if (@$request->parent_menu) {
            $order = Menu::where('parent', $request->parent_menu)->max('order');
        } else {
            $order = Menu::where('parent', null)->max('order');
        }

        $data = [
            'menu_name' => $request->menu_name,
            'slug' => @$request->slug ?? @$request->tautan,
            'can_navigate' => $request->sumber_data == 'Kategori' ? 0 : 1,
            'status' => 0,
            'parent' => @$request->parent_menu ?? null,
            'pages_id' => @$request->halaman_khusus ?? null,
        ];

        if ($request->halaman_khusus) {
            $halaman_tunggal_slug = Page::find($request->halaman_khusus)->slug;
            $data['slug'] = route('portal.pages', $halaman_tunggal_slug);
        }

        if ($request->id) {
            Menu::find($request->id)->update($data);

            $msg = 'Data berhasil diubah.';
        } else {
            $data['order'] = $order + 1;
            $data['created_by'] = auth()->user()->id;

            Menu::create($data);
        }

        return redirect()->route('kelola-portal.navigasi-menu.index')->with('success', $msg);
    }

    public function update(Request $request)
    {
        $data_menu_aktif = @$request->menu_aktif;
        $data_submenu_aktif = @$request->submenu_aktif;

        $data_menu_non_aktif = @$request->menu_non_aktif;

        $data_order_menu_aktif = @$request->order_menu_aktif;
        $data_order_submenu_aktif = @$request->order_submenu_aktif;

        if ($data_menu_aktif) {
            foreach ($data_menu_aktif as $menu) {
                Menu::find($menu)->update([
                    'status' => 1,
                    'order' => @$data_order_menu_aktif[$menu]
                ]);

                if (@$data_submenu_aktif[$menu]) {
                    if (count($data_submenu_aktif[$menu]) > 0) {
                        foreach ($data_submenu_aktif[$menu] as $submenu) {
                            Menu::find($submenu)->update([
                                'status' => 1,
                                'order' => $data_order_submenu_aktif[$submenu]
                            ]);
                        }
                    }
                }
            }
        }

        if ($data_menu_non_aktif) {
            foreach ($data_menu_non_aktif as $menu) {
                $menu = Menu::find($menu);

                $menu->update([
                    'status' => 0,
                    'order' => 0
                ]);

                if (count($menu->children) > 0) {
                    $menu->children()->update([
                        'status' => 0,
                        'order' => 0
                    ]);
                }
            }
        }

        return redirect()->route('kelola-portal.navigasi-menu.index')->with('success', 'Data berhasil diperbarui.');
    }

    public function toggleActive($id)
    {
        $menu = Menu::findOrFail($id);

        $menu->update([
            'status' => $menu->status == 1 ? 0 : 1
        ]);

        if (count($menu->children) > 0) {
            $menu->children()->update([
                'status' => $menu->status
            ]);
        }

        return redirect()->route('kelola-portal.navigasi-menu.index')->with('success', 'Data berhasil diubah.');
    }

    public function destroy($id)
    {
        $menu = Menu::findOrFail($id);

        if ($menu->children->count() > 0) {
            $menu->children()->delete();
        }

        $menu->delete();

        return redirect()->route('kelola-portal.navigasi-menu.index')->with('success', 'Data berhasil dihapus.');
    }
}
