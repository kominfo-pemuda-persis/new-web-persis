<?php

namespace App\Http\Controllers\CMSController;

use App\Helpers\CustomHelpers;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use DataTables;

class UserManagementController extends Controller
{
    public function index(Request $request)
    {
        if (request()->ajax()) {
            if ($request->type_form === "management_user_table") {
                $query = User::with('role_name')->select('id', 'full_name', 'username', 'email', 'phone', 'role', 'status')->orderBy('role');

                return Datatables::of($query)
                    ->addIndexColumn()
                    ->rawColumns(['action'])
                    ->make(true);
            } else {
                $search = $request->search ? $request->search : '';

                $query = Role::select('id as id', 'role_name as text')
                    ->whereNot('role_name', 'Pengguna')
                    ->where('role_name', 'like', '%' . $search . '%')
                    ->simplePaginate(10);

                $morePages = true;
                if (empty($query->nextPageUrl())) {
                    $morePages = false;
                }
                $results = [
                    'results' => $query->items(),
                    'pagination' => [
                        'more' => $morePages,
                    ],
                ];

                return response()->json($results);
            }
        }

        return view('cms.user_management.index');
    }

    public function edit($id)
    {
        $query = User::with('role_name')->find($id);

        return response()->json($query);
    }

    public function atur_profile_password($value)
    {
        $query = User::findorfail(Auth::user()->id);
        return view('cms.user_management.edit-profile', compact('query', 'value'));
    }

    public function store(Request $request)
    {
        $rules = [
            'username' => [
                'sometimes',
                'required',
                Rule::unique('users')->ignore($request->id),
            ],
            'email' => [
                'sometimes',
                'email',
                Rule::unique('users')->ignore($request->id),
            ],
            'full_name' => 'sometimes',
            'role' => 'sometimes',
            'status' => 'sometimes',
        ];
        $this->validate($request, $rules);

        $input = [
            'full_name' => $request->full_name ?? Auth::user()->full_name,
            'phone' => $request->phone ?? Auth::user()->phone,
            'role' => $request->role ?? Auth::user()->role,
            'status' => $request->status ?? Auth::user()->status
        ];

        if ($request->hasFile('photo')) {
            if ($request->id) {
                $image_file = 'profile_pic/' . basename(User::find($request->id)->photo);
                Storage::disk('s3')->delete($image_file);
            }
            $input['photo'] = $request->file('photo') ? CustomHelpers::upload_minio('profile_pic/', $request->file('photo'), Str::slug($request->email)) : User::find($request->id)->photo;
        }

        if ($request->type === 'password') {
            $url = route('user-management.atur-profile-password', ['value' => 'password']);
            $status = False;

            if (Hash::check($request->old_password, Auth::user()->getAuthPassword())) {
                $user = User::findorfail($request->id);
                $user->password = Hash::make($request->password);
                $status = $user->save();

                $url = route('cms.index');
            } else if (Hash::check($request->password, Auth::user()->getAuthPassword())) {
                $message = "Password Baru tidak boleh sama dengan password lama!";
            } else {
                $message = "Password Lama tidak sesuai!";
            }

            return redirect($url)
                ->with(
                    $status ? 'success' : 'error',
                    $status ? 'Password Baru Berhasil Disimpan.' : $message
                );
        }

        if (User::find($request->id)) {
            $input['password'] = $request->password ?? User::find($request->id)->password;
        } else {
            $input['password'] = Hash::make(($request->password != null) ? $request->password : 'jamiyyahIslamiyyah');
        }

        if (!$request->id) {
            $input['username'] = $request->username ?? Auth::user()->username;
            $input['email'] = $request->email ?? Auth::user()->email;

            $status = User::Create($input);

            $messageKey = $status ? 'success-create-user' : 'error';
            $messageValue = $status ? $input['full_name'] : 'Akun gagal dibuat.';

            return redirect()->route('user-management.index')
                ->with($messageKey, $messageValue);
        } else {
            $status = User::updateOrCreate(['id' => $request->id], $input);
            if ($request->type === 'profile') {
                return redirect()->route('user-management.atur-profile-password', ['profile'])->with($status ? 'success' : 'error', $status ? 'Akun berhasil diubah.' : 'Akun gagal diubah.');
            } else {
                return redirect()->route('user-management.index')->with($status ? 'success' : 'error', $status ? 'Akun berhasil disimpan.' : 'Akun gagal disimpan.');
            }
        }
    }

    public function destroy($id)
    {
        $status = User::where('id', $id)->firstorfail()->delete();

        return response()->json([
            'status' => $status
        ]);

        return redirect()->route('user-management.index')->with($status ? 'success' : 'error', $status ? 'Data berhasil dihapus.' : 'Data gagal dihapus.');
    }

    public function reset_password($id)
    {
        $query = User::find($id);
        $new_password = 'jamiyyahIslamiyyah';

        $encrypted_password = Hash::make($new_password);
        $query->password = $encrypted_password;

        $status = $query->save();
        if ($status) {
            return response()->json([
                'status' => $status,
                'message' => 'Anda berhasil mereset kata sandi akun <b>' . $query->full_name . '</b> menjadi kata sandi bawaan <b><p>' . $new_password . '</p></b>',
            ]);
        } else {
            return response()->json([
                'status' => $status,
                'message' => 'Penggantian Password Gagal ' . $status,
            ]);
        }

        return response()->json([
            'status' => $status,
            'message' => $new_password . $id,
        ]);
    }
}
