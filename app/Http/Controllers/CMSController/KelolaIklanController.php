<?php

namespace App\Http\Controllers\CMSController;

use App\Http\Controllers\Controller;
use App\Models\Advertisements;
use App\Models\Client;
use App\Models\SetIklan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

use Yajra\DataTables\Facades\DataTables;
use App\Helpers\CustomHelpers;
use App\Models\SetIklanList;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class KelolaIklanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $clients = Client::orderBy('name', 'asc')->get();
        if (request()->ajax()) {
            if ($request->type_form === 'iklan_table') {
                $query = Advertisements::with('client')->get();

                return DataTables::of($query)
                    ->addIndexColumn()
                    ->rawColumns(['action'])
                    ->make(true);
            } else {
                $search = $request->search ? $request->search : '';

                $query = Advertisements::select('id as id', 'title as text')
                    ->where('title', 'ILIKE', '%' . $search . '%')
                    ->simplePaginate(10);

                $morePages = true;
                if (empty($query->nextPageUrl())) {
                    $morePages = false;
                }
                $results = [
                    'results' => $query->items(),
                    'pagination' => [
                        'more' => $morePages,
                    ],
                ];

                return response()->json($results);
            }
        }
        return view('cms.kelola_iklan.index', compact('clients'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'desc' => 'required|string',
            'image' => 'required|image',
            'url_ads' => 'required|url',
            'client_id' => 'required',
            'name' => 'nullable|string|max:255',
        ]);

        $image_url = CustomHelpers::upload_minio('iklan/', $request->file('image'), Str::slug($request->title));

        // // Ambil UUID dan nama client dari request
        $client_id = $request->input('client_id');
        // $client = Client::find($clientId);
        // if (!$client) {
        //     return redirect()->back()->withErrors(['client_id' => 'Client ID tidak ditemukan.']);
        // }

        $newClientName = $request->input('newClientName'); // Retrieve new client name if any

        if (str_starts_with($client_id, 'new-')) {
            // Extract the name from client_id
            $newClientName = str_replace('new-', '', $client_id);

            // Validasi bahwa nama klien baru diisi
            if (empty($newClientName)) {
                return redirect()
                    ->back()
                    ->withErrors(['client_id' => 'Nama klien baru harus diisi.']);
            }

            // Buat client baru dengan UUID baru
            $client = new Client();
            $client->id = (string) Str::uuid(); // Generate UUID baru
            $client->name = $newClientName;
            $client->save();
            $client_id = $client->id;
        } else {
            // Validasi bahwa client_id ada di database
            $request->validate([
                'client_id' => 'exists:clients,id',
            ]);

            $client = Client::find($client_id);
            if (!$client) {
                return redirect()
                    ->back()
                    ->withErrors(['client_id' => 'Klien tidak ditemukan.']);
            }
        }

        $data = $request->all();
        $data['image'] = $image_url;
        $data['created_by'] = Auth::id();
        $data['client_id'] = $client->id;
        // Set default values for width, height, and clicked
        $data['width'] = 0;
        $data['height'] = 0;
        $data['clicked'] = 0;

        Advertisements::create($data);

        return redirect()->route('kelola-iklan.index')->with('success', 'Data berhasil disimpan.');
    }

    public function edit($id)
    {
        $advertisements = Advertisements::with('client')->findOrFail($id);
        return response()->json($advertisements);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'desc' => 'required|string',
            'image' => 'nullable|image',
            'url_ads' => 'required|url',
            'client_id' => 'required',
            'name' => 'nullable|string|max:255',
        ]);

        $advertisement = Advertisements::findOrFail($id);

        // Upload image if new image is provided
        if ($request->hasFile('image')) {
            Storage::disk('s3')->delete('iklan/' . basename($advertisement->image));
            $image_url = CustomHelpers::upload_minio('iklan/', $request->file('image'), Str::slug($request->title));
            $advertisement->image = $image_url;
        }

        // Ambil UUID dan nama client dari request
        $client_id = $request->input('client_id');
        $newClientName = $request->input('newClientName'); // Retrieve new client name if any

        if (str_starts_with($client_id, 'new-')) {
            // Extract the name from client_id
            $newClientName = str_replace('new-', '', $client_id);

            // Validasi bahwa nama klien baru diisi
            if (empty($newClientName)) {
                return redirect()
                    ->back()
                    ->withErrors(['client_id' => 'Nama klien baru harus diisi.']);
            }

            // Buat client baru dengan UUID baru
            $client = new Client();
            $client->id = (string) Str::uuid(); // Generate UUID baru
            $client->name = $newClientName;
            $client->save();
            $client_id = $client->id;
        } else {
            // Validasi bahwa client_id ada di database
            $request->validate([
                'client_id' => 'exists:clients,id',
            ]);

            $client = Client::find($client_id);
            if (!$client) {
                return redirect()
                    ->back()
                    ->withErrors(['client_id' => 'Klien tidak ditemukan.']);
            }
        }

        $advertisement->title = $request->input('title');
        $advertisement->desc = $request->input('desc');
        $advertisement->url_ads = $request->input('url_ads');
        $advertisement->client_id = $client->id;

        // Set default values for width, height, and clicked if they are not provided
        $advertisement->width = $request->input('width', 0);
        $advertisement->height = $request->input('height', 0);
        $advertisement->clicked = $request->input('clicked', 0);

        $advertisement->save();

        return redirect()->route('kelola-iklan.index')->with('success', 'Data berhasil diperbarui.');
    }

    public function destroy($id)
    {
        $iklan = Advertisements::findOrFail($id);
        $iklan->delete();

        return redirect()->route('kelola-kategori.index')->with('success', 'Data berhasil dihapus.');
    }

    public function indexSetIklan(Request $request)
    {
        if ($request->ajax()) {
            $search = $request->search ? $request->search : '';

            $query = Advertisements::select('id as id', 'title as text', 'image')
                ->where('title', 'ILIKE', '%' . $search . '%')
                ->simplePaginate(10);

            $morePages = true;
            if (empty($query->nextPageUrl())) {
                $morePages = false;
            }
            $results = [
                'results' => $query->items(),
                'pagination' => [
                    'more' => $morePages,
                ],
            ];

            return response()->json($results);
        }
        $advertisements = Advertisements::all();

        $setIklans = SetIklan::with('advertisement')
            ->whereIn('title', ['Iklan Header', 'Iklan Footer', 'Iklan Samping 1', 'Iklan Samping 2'])
            ->get()
            ->keyBy('title');

        $advertisements_position = SetIklan::with([
            'advertisement' => function ($query) {
                $query->orderBy('ads_position')->orderBy('order');
            },
        ])
            ->orderBy('order')
            ->get();

        return view('cms.kelola_iklan.set-ads', compact('advertisements', 'advertisements_position'));
    }

    public function setIklanPosition(Request $request)
    {
        $request->validate([
            'iklan-pop-up.*' => 'nullable|uuid',
            'iklan-header.*' => 'nullable|uuid',
            'iklan-footer.*' => 'nullable|uuid',
            'iklan-samping-1.*' => 'nullable|uuid',
            'iklan-samping-2.*' => 'nullable|uuid',
            'iklan-sticky-kiri.*' => 'nullable|uuid',
            'iklan-sticky-kanan.*' => 'nullable|uuid',
        ]);

        $ads = [
            'iklan-pop-up' => SetIklan::where('title', 'Iklan Pop-up')->firstOrFail()->id,
            'iklan-header' => SetIklan::where('title', 'Iklan Header')->firstOrFail()->id,
            'iklan-footer' => SetIklan::where('title', 'Iklan Footer')->firstOrFail()->id,
            'iklan-samping-1' => SetIklan::where('title', 'Iklan Samping 1')->firstOrFail()->id,
            'iklan-samping-2' => SetIklan::where('title', 'Iklan Samping 2')->firstOrFail()->id,
            'iklan-sticky-kiri' => SetIklan::where('title', 'Iklan Sticky Kiri')->firstOrFail()->id,
            'iklan-sticky-kanan' => SetIklan::where('title', 'Iklan Sticky Kanan')->firstOrFail()->id,
        ];

        foreach ($ads as $field => $id) {
            if ($request->filled($field)) {
                $old_iklan = SetIklanList::where('ads_position', $id)->delete();
                foreach ($request->$field as $key => $value) {
                    $input = [
                        'ads_id' => $value,
                        'ads_position' => $id,
                        'order' => $key,
                    ];
                    SetIklanList::updateOrCreate(['ads_position' => $id, 'order' => $key], $input);
                }
            }
        }

        return redirect()->back()->with('success', 'Posisi iklan berhasil diperbarui.');
    }
}
