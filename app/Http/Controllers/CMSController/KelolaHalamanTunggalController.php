<?php

namespace App\Http\Controllers\CMSController;

use App\Http\Controllers\Controller;
use App\Helpers\CustomHelpers;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

use DataTables;

class KelolaHalamanTunggalController extends Controller
{
    public function index(Request $request)
    {
        if (request()->ajax()) {
            if ($request->type_form === "halaman_tunggal_table") {
                $query = Page::with('creators')
                    ->orderBy('updated_at', 'desc')
                    ->orderBy('created_at', 'desc');

                return DataTables::of($query)
                    ->addIndexColumn()
                    ->rawColumns(['action'])
                    ->order(function ($query) {
                        $query->orderBy('updated_at', 'desc')
                            ->orderBy('created_at', 'desc');
                    })
                    ->make(true);
            }
        }

        return view('cms.kelola_halaman_tunggal.index');
    }

    public function create()
    {
        return view('cms.kelola_halaman_tunggal.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'slug' => ['required', Rule::unique('pages')->ignore($request->id)],
            'desc_seo' => 'required',
            'content' => 'required',
        ];
        $messages = [
            'title.required' => 'Judul Konten Harus Diisi',
            'slug.required' => 'Slug Harus Diisi/Slug Sudah ada mohon diganti',
            'desc_seo.required' => 'Deskripsi SEO Harus Diisi',
            'content.required' => 'Isi Konten Harus Diisi',
        ];
        $this->validate($request, $rules, $messages);

        $data = [
            'title' => $request->title,
            'slug' => Str::slug($request->slug),
            'created_by' => $request->created_by ?? Auth::user()->id,
            'desc_seo' => $request->desc_seo,
            'status' => $request->button_status === 'publish_button' ? 1 : 0,
            'embeded_string' => $request->embeded_string,
            'content' => $request->content,
        ];


        if (!$request->id) {
            $data['visited'] = 0;
        }

        $status_halaman_tunggal = Page::updateOrCreate(['id' => $request->id], $data);
        $featured_image = $request->file('featured_image');
        $image_type = 'halaman-tunggal/' . $status_halaman_tunggal->id . '/';
        if ($featured_image) {
            if ($request->content_id) {
                $image_file = $image_type . basename($status_halaman_tunggal->featured_image);
                Storage::disk('s3')->delete($image_file);
            }
            $featured_image = CustomHelpers::upload_minio($image_type, $featured_image, 'featured_image');

            $status_halaman_tunggal->featured_image = $featured_image;
            $status_halaman_tunggal->save();
        }

        return redirect()->route('kelola-halaman-tunggal.index')->with($status_halaman_tunggal ? 'success' : 'error', $status_halaman_tunggal ? 'Data berhasil disimpan.' : 'Data gagal disimpan.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    public function edit(string $id)
    {
        $data = Page::with('creators')->find($id);
        // dd($data);
        return view('cms.kelola_halaman_tunggal.create', compact('data'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $iklan = Page::findOrFail($id);
        $iklan->delete();

        return redirect()->route('kelola-kategori.index')->with('success', 'Data berhasil dihapus.');
    }
}
