<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EnsureUserHasRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, ...$role): Response
    {
        if (in_array(Auth::user()->role_name->role_name, $role)) {
            return $next($request);
        }

        return redirect()->route('cms.index')->with('error', 'Anda tidak diizinkan untuk melihat konten tersebut.');
    }
}
