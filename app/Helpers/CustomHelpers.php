<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CustomHelpers
{
    public static function upload_minio($image_type, $file, $slug)
    {
        $image_file = $file;
        $image_file_name = $image_type . $slug . '_' . Str::uuid()->toString() . '.' . $file->getClientOriginalExtension();
        $image_status = Storage::disk('s3')->put($image_file_name, file_get_contents($image_file));

        if (!$image_status) {
            return redirect()->back()->with($image_status ? 'success' : 'error', $image_status ? 'Data berhasil disimpan.' : 'Data gagal disimpan.');
        }
        $image = Storage::disk('s3')->url($image_file_name);

        return $image;
    }
}