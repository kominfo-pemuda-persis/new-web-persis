<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CMSController\DashboardCMSController;
use App\Http\Controllers\CMSController\KelolaKategoriController;
use App\Http\Controllers\CMSController\KelolaKontenController;
use App\Http\Controllers\CMSController\KelolaIklanController;
use App\Http\Controllers\CMSController\AturIklanController;
use App\Http\Controllers\CMSController\KelolaHalamanTunggalController;
use App\Http\Controllers\CMSController\KelolaIstiftaController;
use App\Http\Controllers\CMSController\UserManagementController;
use App\Http\Controllers\PortalController\LandingController;
use App\Http\Controllers\PortalController\ContentController;
use App\Http\Controllers\CMSController\KelolaPortal\PengaturanPortalController;
use App\Http\Controllers\CMSController\KelolaPortal\NavigasiMenuController;
use App\Http\Controllers\CMSController\KelolaPortal\KontenFooterController;
use App\Http\Controllers\CMSController\KelolaPortal\NavigasiFooterController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [LandingController::class, 'index'])->name('portal');

Route::get('/static-page', function () {
    return view('portal.static-page');
})->name('portal.static-page');

Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::post('/login', [AuthController::class, 'login']);
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
Route::group(['middleware' => ['auth']], function () {
    Route::prefix('cms')->group(function () {
        Route::get('/', [DashboardCMSController::class, 'index'])->name('cms.index');

        Route::middleware(['role:Editor,Admin Pusat,Admin Otonom'])->group(function () {
            Route::prefix('kelola-konten')->group(function () {
                Route::get('/', [KelolaKontenController::class, 'index'])->name('kelola-konten.index');
                Route::get('/tambah', [KelolaKontenController::class, 'create'])->name('kelola-konten.create');
                Route::get('/preview/{id}', [KelolaKontenController::class, 'preview'])->name('kelola-konten.preview');
                Route::get('/edit/{id}', [KelolaKontenController::class, 'edit'])->name('kelola-konten.edit');
                Route::post('/tambah', [KelolaKontenController::class, 'store'])->name('kelola-konten.store');
                Route::post('/ubah-status/{id}', [KelolaKontenController::class, 'change_status'])->name('kelola-konten.change-status');
                Route::delete('/{id}', [KelolaKontenController::class, 'destroy'])->name('kelola-konten.destroy');
                Route::get('related_content/{id}', [KelolaKontenController::class, 'getRelatedContent'])->name('kelola-konten.getRelatedContent');
                Route::get('advertisement/{id}', [KelolaKontenController::class, 'getAdvertisementContent'])->name('kelola-konten.getAdvertisementContent');
            });
            Route::prefix('kelola-halaman-tunggal')->group(function () {
                Route::get('/', [KelolaHalamanTunggalController::class, 'index'])->name('kelola-halaman-tunggal.index');
                Route::get('/tambah', [KelolaHalamanTunggalController::class, 'create'])->name('kelola-halaman-tunggal.create');
                Route::post('/tambah', [KelolaHalamanTunggalController::class, 'store'])->name('kelola-halaman-tunggal.store');
                Route::get('/edit/{id}', [KelolaHalamanTunggalController::class, 'edit'])->name('kelola-halaman-tunggal.edit');
                Route::delete('/{id}', [KelolaHalamanTunggalController::class, 'destroy'])->name('kelola-halaman-tunggal.destroy');
            });
            Route::prefix('kelola-istifta')->group(function () {
                Route::get('/', [KelolaIstiftaController::class, 'index'])->name('kelola-istifta.index');
                Route::get('/tambah/{id}', [KelolaIstiftaController::class, 'create'])->name('kelola-istifta.create');
                Route::delete('/{id}', [KelolaIstiftaController::class, 'destroy'])->name('kelola-istifta.destroy');
            });
        });

        Route::middleware(['role:Admin Pusat,Admin Otonom'])->group(function () {
            Route::prefix('kelola-kategori')->group(function () {
                Route::get('/', [KelolaKategoriController::class, 'index'])->name('kelola-kategori.index');
                Route::get('/{id}/edit', [KelolaKategoriController::class, 'edit'])->name('kelola-kategori.edit');
                Route::post('/tambah', [KelolaKategoriController::class, 'store'])->name('kelola-kategori.store');
                Route::delete('/{id}', [KelolaKategoriController::class, 'destroy'])->name('kelola-kategori.destroy');
            });
            Route::prefix('kelola-iklan')->group(function () {
                Route::get('/', [KelolaIklanController::class, 'index'])->name('kelola-iklan.index');
                Route::get('/set-ads', [KelolaIklanController::class, 'indexSetIklan'])->name('kelola-iklan.set_ads');
                Route::post('/set-iklan', [KelolaIklanController::class, 'setIklanPosition'])->name('set-iklan.updateIklan');
                Route::post('/tambah', [KelolaIklanController::class, 'store'])->name('kelola-iklan.store');
                Route::delete('/cms/kelola-iklan/{id}', [KelolaIklanController::class, 'destroy'])->name('kelola-iklan.destroy');
                Route::get('/cms/kelola-iklan/{id}', [KelolaIklanController::class, 'edit'])->name('kelola-iklan.edit');
                Route::post('/{id}', [KelolaIklanController::class, 'update'])->name('kelola-iklan.update');
            });
            Route::prefix('akun')->group(function () {
                Route::get('/', [UserManagementController::class, 'index'])->name('user-management.index');
                Route::delete('/{id}', [UserManagementController::class, 'destroy'])->name('user-management.destroy');
            });
            Route::prefix('kelola-portal')->group(function () {
                Route::prefix('pengaturan-portal')->group(function () {
                    Route::get('/', [PengaturanPortalController::class, 'index'])->name('kelola-portal.pengaturan-portal.index');
                    Route::post('/', [PengaturanPortalController::class, 'store'])->name('kelola-portal.pengaturan-portal.store');
                });
                Route::prefix('navigasi-menu')->group(function () {
                    Route::get('/', [NavigasiMenuController::class, 'index'])->name('kelola-portal.navigasi-menu.index');
                    Route::post('/', [NavigasiMenuController::class, 'store'])->name('kelola-portal.navigasi-menu.store');
                    Route::post('/update', [NavigasiMenuController::class, 'update'])->name('kelola-portal.navigasi-menu.update');
                    Route::post('/{id}/active', [NavigasiMenuController::class, 'toggleActive'])->name('kelola-portal.navigasi-menu.active');
                    Route::delete('/{id}', [NavigasiMenuController::class, 'destroy'])->name('kelola-portal.navigasi-menu.destroy');
                });
                Route::prefix('konten-footer')->group(function () {
                    Route::get('/', [KontenFooterController::class, 'index'])->name('kelola-portal.konten-footer.index');
                    Route::post('/', [KontenFooterController::class, 'store'])->name('kelola-portal.konten-footer.store');
                });
                Route::prefix('navigasi-footer')->group(function () {
                    Route::get('/', [NavigasiFooterController::class, 'index'])->name('kelola-portal.navigasi-footer.index');
                    Route::post('/', [NavigasiFooterController::class, 'store'])->name('kelola-portal.navigasi-footer.store');
                    Route::post('/update', [NavigasiFooterController::class, 'update'])->name('kelola-portal.navigasi-footer.update');
                    Route::post('/{id}/active', [NavigasiFooterController::class, 'toggleActive'])->name('kelola-portal.navigasi-footer.active');
                    Route::delete('/{id}', [NavigasiFooterController::class, 'destroy'])->name('kelola-portal.navigasi-footer.destroy');
                });
            });
        });

        Route::prefix('akun')->group(function () {
            Route::get('/{id}/edit', [UserManagementController::class, 'edit'])->name('user-management.edit');
            Route::post('/simpan', [UserManagementController::class, 'store'])->name('user-management.store');
            Route::post('/password/{id}/reset', [UserManagementController::class, 'reset_password'])->name('user-management.reset-password');
            Route::get('/atur/{value?}', [UserManagementController::class, 'atur_profile_password'])->name('user-management.atur-profile-password');
        });
    });
});

Route::post('/kirim-istifta', [KelolaIstiftaController::class, 'store'])->name('kelola-istifta.store');

Route::get('/search', [LandingController::class, 'search_content'])->name('portal.search');

Route::get('/tags/{slug}', [LandingController::class, 'contents_tag'])->name('portal.tags');

Route::get('/page/{slug}', [LandingController::class, 'detail_pages'])->name('portal.pages');

Route::get('/{category}/read/{slug}', [LandingController::class, 'detail_content'])->name('portal.detail-content');
Route::get('/{category}/{categorySub}', [LandingController::class, 'content_sub_category'])->name('portal.sub-category');
Route::get('/{category}', [LandingController::class, 'contents_category'])->name('portal.category');
