@extends('cms.layouts.master')
@section('title', 'Istifta')

@push('css')
    <style>
        table.dataTable.no-margin {
            margin-top: 0 !important;
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-gray-900 fw-bold flex-column justify-content-center my-0">
                        Istifta
                    </h1>
                    <!--end::Title-->

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-istifta.index') }}" class="text-muted text-hover-primary">
                                Istifta
                            </a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->

                <!--begin::Actions-->
                {{-- <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <a href="{{ route('kelola-istifta.create') }}" class="btn btn-lg btn-bd-primary fw-bold">
                        <i class="ki-duotone ki-plus-square">
                            <span class="path1"></span>
                            <span class="path2"></span>
                            <span class="path3"></span>
                        </i>
                        Tambah Baru
                    </a>
                </div> --}}
                <!--end::Actions-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->

        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <!--Insert Content Here-->
                <div class="row g-5 g-xl-10 py-6">
                    <!--begin::Col-->
                    <div class="col">
                        <div class="card">
                            <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                                <div class="card-title">
                                    <!--begin::Search-->
                                    <div class="d-flex align-items-center position-relative my-1">
                                        <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                                            </svg>
                                        </span>
                                        <input type="text" id="search" data-kt-filter="search" class="form-control form-control-solid w-250px ps-14" placeholder="Cari" autocomplete="off" />
                                    </div>
                                </div>
                            </div>

                            <div class="card-body p-0 d-flex flex-column">
                                <table class="table table-row-bordered align-middle no-margin fs-6" id="istifta_table">
                                    <thead class="bg-light-secondary">
                                        <tr class="fw-bold fs-5 text-capitalize">
                                            <th class="py-5 text-center">No</th>
                                            <th class="py-5 text-center">Nama Pengirim</th>
                                            <th class="py-5 text-center">Email Pengirim</th>
                                            <th class="py-5 text-center">Topik Pertanyaan</th>
                                            <th class="py-5 text-center">Pertanyaan</th>
                                            <th class="py-5 text-center">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody class="fw-semibold text-gray-600">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--end::Col-->
                </div>
            </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>
@endsection
@include('cms.kelola_konten.modal-preview')
@push('js')
    <script>
        let datatable;
        "use strict";

        var KategoriTables = function() {
            var table;

            var initDatatable = function() {
                const tableRows = table.querySelectorAll('tbody tr');

                datatable = $(table).DataTable({
                    processing: true,
                    searching: true,
                    ordering: false,
                    bInfo: false,
                    layout: {
                        topStart: null,
                        topEnd: null,
                        bottomStart: 'pageLength',
                    },
                    language: {
                        emptyTable: 'Tidak Ada Data Istifta'
                    },
                    ajax: {
                        url: "{{ route('kelola-istifta.index') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        },
                        data: function(d) {
                            d.type_form = "istifta_table";
                        },
                        error: function(req, err) {
                            language: {
                                emptyTable: 'Tidak Ada Data Kategori'
                            };
                            console.log('Table Error : ' + err);
                        }
                    },
                    dataType: 'JSON',
                    columns: [{
                            name: 'No.',
                            data: 'DT_RowIndex',
                            searchable: false,
                            className: 'ps-5 text-center',
                        },
                        {
                            name: 'full_name',
                            data: 'full_name',
                            defaultContent: '-',
                            className: 'text-center',
                            render: function(data, type, row) {
                                if (row.full_name) {
                                    return row.full_name
                                } else {
                                    return '<div class="text-center">-</div>'
                                }
                            },
                        },
                        {
                            name: 'email',
                            data: 'email',
                            defaultContent: '-',
                            className: 'text-center',
                            render: function(data, type, row) {
                                if (row.email) {
                                    return row.email
                                } else {
                                    return '<div class="text-center">-</div>'
                                }
                            },
                        },
                        {
                            name: 'topik_pertanyaan',
                            data: 'topik_pertanyaan',
                            className: 'text-center',
                            defaultContent: '-',
                            render: function(data, type, row) {
                                if (row.topik_pertanyaan) {
                                    return row.topik_pertanyaan
                                } else {
                                    return '<div class="text-center">-</div>'
                                }
                            },
                        },
                        {
                            name: 'pertanyaan',
                            data: 'pertanyaan',
                            defaultContent: '-',
                            className: 'text-center',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            searchable: false,
                            className: 'text-center',
                            render: function(data, type, row) {
                                // var url = (row.content_id) ? "kelola-konten/edit/" + row.content_id : "kelola-istifta/tambah/" + row.id
                                var button

                                if (row.content_id) {
                                    button =
                                        `<a href="{{ route('kelola-konten.edit', ['id' => '__ID__']) }}" data-toggle="tooltip" data-original-title="Edit" class="btn btn-icon btn-light-warning me-1"><i class="ki-duotone ki-notepad-edit"><span class="path1"></span><span class="path2"></span></i></a>`
                                        .replace('__ID__', row.content_id);
                                    button +=
                                        `<a href="#" data-toggle="tooltip" data-original-title="Preview" data-id="${row.content_id}" data-type="pratinjau" data-bs-toggle="modal" data-bs-target="#preview_konten" data-id="${row.id}" class="btn btn-icon btn-light-primary pratinjauButton"><i class="ki-duotone ki-eye"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i></a>`

                                } else {
                                    button =
                                        `<a href="{{ route('kelola-istifta.create', ['id' => '__ID__']) }}" data-toggle="tooltip" data-original-title="Create" class="btn btn-icon btn-light-success me-1"><i class="ki-duotone ki-notepad-edit"><span class="path1"></span><span class="path2"></span></i></a>`
                                        .replace('__ID__', row.id);
                                    button +=
                                        `<button data-toggle="tooltip" data-original-title="Delete" data-id="${row.id}" data-type="delete" data-title_pertanyaan="${row.pertanyaan}" data-pengirim="${row.full_name}" class="btn btn-icon btn-light-danger deleteButton"><i class="ki-duotone ki-trash"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i></button>`
                                }

                                return button
                            }
                        },
                    ],
                });
            }

            var handleSearchDatatable = () => {
                const filterSearch = document.querySelector('[data-kt-filter="search"]');
                filterSearch.addEventListener('keyup', function(e) {
                    datatable.search(e.target.value).draw();
                });
            }

            return {
                init: function() {
                    table = document.querySelector('#istifta_table');

                    if (!table) {
                        return;
                    }

                    initDatatable();
                    handleSearchDatatable();

                    $("#istifta_table_wrapper").find("div.row").addClass('px-15 pb-5');
                }
            };
        }();

        KTUtil.onDOMContentLoaded(function() {
            KategoriTables.init();
        });
    </script>

    <script>
        $(document).ready(function() {
            $('body').on('click', '.deleteButton, .pratinjauButton', function() {
                var id = $(this).data('id');
                var type = $(this).data('type');
                var title_pertanyaan = $(this).data('title_pertanyaan');
                var pengirim = $(this).data('pengirim');

                event.preventDefault();

                var title;
                var text = "Apakah anda ingin";
                var icon;
                var url = "";
                var method = 'POST';
                switch (type) {
                    case 'delete':
                        url = `{{ route('kelola-istifta.destroy', ['id' => ':id']) }}`;
                        method = 'DELETE';

                        title = "Hapus Pertanyaan";
                        text += ` menghapus pertanyaan <br /><strong>${title_pertanyaan}</strong>. <br />Dari <strong>${pengirim}</strong>`
                        icon = "warning";
                        break;
                    case 'pratinjau':
                        url = `{{ route('kelola-konten.preview', ['id' => ':id']) }}`;
                        url = url.replace(':id', id);
                        document.getElementById("preview-konten").src = url;
                }

                if (type != 'pratinjau') {
                    Swal.fire({
                        title: title,
                        html: text,
                        icon: icon,
                        showCancelButton: true,
                        showCloseButton: true
                    }).then((result) => {
                        if (result.isConfirmed) {
                            url = url.replace(':id', id);
                            $.ajax({
                                url: url,
                                type: 'POST',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    _method: method,
                                    type: type,
                                },
                                error: (response) => {
                                    let msg;
                                    msg = response.responseJSON.message

                                    Swal.fire({
                                        title: 'Perhatian',
                                        text: msg,
                                        icon: 'warning'
                                    })
                                },
                                success: (response) => {
                                    toastr.success(response.message)
                                    datatable.ajax.reload()
                                }
                            });
                        }
                    });
                }
            });
        });
    </script>
@endpush
