@extends('cms.layouts.master')
@section('title', 'Atur Posisi Iklan')

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-gray-900 fw-bold flex-column justify-content-center my-0">
                        Atur Posisi Iklan
                    </h1>
                    <!--end::Title-->

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-kategori.index') }}" class="text-muted text-hover-primary">
                                Kelola Iklan
                            </a>
                        </li>
                        <li class="breadcrumb-item"><span class="bullet bg-gray-500 w-5px h-2px"></span></li>
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted text-hover-primary">
                                Posisi Iklan
                            </a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->

        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <form id="set_iklan" method="post" action="{{ route('set-iklan.updateIklan') }}">
                    @csrf
                    <!--Insert Content Here-->
                    <div class="row g-5 g-xl-10 py-6">
                        <!--begin::Col-->
                        <div class="col">
                            <div class="card">
                                <div class="card-body d-flex flex-column">
                                    <!--begin::Accordion-->
                                    <div class="accordion" id="atur_iklan_accordion">
                                        @foreach ($advertisements_position as $key => $item)
                                            @php
                                                $slugTitle = Str::slug($item->title);
                                                $previewId = 'preview-' . $slugTitle;
                                                $selectId = 'select-' . $slugTitle;
                                            @endphp
                                            <div class="accordion-item">
                                                <h2 class="accordion-header border-bottom d-flex align-items-center" id="{{ $slugTitle }}">
                                                    <button class="accordion-button fs-4 fw-semibold bg-transparent shadow-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#{{ $slugTitle }}_body"
                                                        aria-controls="{{ $slugTitle }}_body">
                                                        {{ $item->title }}
                                                    </button>
                                                    @if ($item->title === 'Iklan Header' || $item->title === 'Iklan Footer')
                                                        <button type="button" class="btn btn-outline btn-outline-success w-25 me-5" onclick="addSection('{{ $slugTitle }}');">
                                                            <i class="ki-duotone ki-plus-square"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
                                                            Tambah {{ $item->title }}
                                                        </button>
                                                    @endif
                                                </h2>
                                                <div id="{{ $slugTitle }}_body" class="accordion-collapse collapse" aria-labelledby="{{ $slugTitle }}">
                                                    <div class="row g-5 p-5">
                                                        <div class="col-12 col-md-6">
                                                            <select class="form-select w-full ads-selections" data-control="select2" data-placeholder="Pilih {{ $item->title }}" data-hide-search="true" name="{{ $slugTitle }}[0]"
                                                                id="{{ $selectId }}" data-preview="{{ $previewId }}" onchange="previewImageChange('{{ $selectId }}', '{{ $previewId }}')">
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <div class="mh-150px">
                                                                <img class="w-auto mw-100 mh-150px rounded-2" src="{{ asset('assets/assets-landing/images/placeholder.png') }}" id="{{ $previewId }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <!--end::Accordion-->
                                </div>
                                <div class="card-footer d-flex justify-content-end">
                                    <button type="submit" class="btn btn-bd-primary" id="simpan_button">Simpan</button>
                                </div>
                            </div>
                        </div>
                        <!--end::Col-->
                    </div>
                </form>
            </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>
@endsection

@push('js')
    <script>
        const ads_set_select2_config = {
            allowClear: true,
            ajax: {
                url: "{{ route('kelola-iklan.set_ads') }}",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    var query = {
                        _token: '{{ csrf_token() }}',
                        search: params.term,
                        page: params.page || 1
                    };

                    return query;
                },
                processResults: function(data) {
                    // Iterate through the results and add a 'data-image' attribute
                    return {
                        results: data.results.map(function(item) {
                            return {
                                id: item.id,
                                text: item.text,
                                image_url: item.image // store image URL for use in option data
                            };
                        }),
                        pagination: {
                            more: data.pagination.more
                        }
                    };
                },
                cache: true
            }
        };

        $(document).ready(function() {
            $(".ads-selections").select2(ads_set_select2_config);
        });

        function createDynamicCounter(params) {
            let varName = params.replace('-', '_');
            if (typeof window[varName] === 'undefined') {
                window[varName] = 0;
            }
            window[varName] += 1;
            return window[varName];
        }

        function convertToTitleCase(str) {
            if (!str) {
                return ""
            }
            return str.toLowerCase().replace(/\b\w/g, s => s.toUpperCase());
        }

        function addSection(params) {
            var counterDynamic = createDynamicCounter(params);

            var item = convertToTitleCase(params.replace('-', ' '));
            var selectId = 'select-' + params + '-' + counterDynamic;
            var slugTitle = params;
            var previewId = 'preview-' + params + '-' + counterDynamic;

            const container = document.getElementById(params + '_body');
            const newContent = document.createElement('div');

            newContent.className = `row g-5 p-5`;
            newContent.innerHTML = `
                <div class="separator my-0"></div>
                <div class="col-12 col-md-6">
                    <select class="form-select w-full ads-selections" data-control="select2" data-placeholder="Pilih ${item}" data-hide-search="true" name="${slugTitle}[${counterDynamic}]"
                        id="${selectId}" data-preview="${previewId}" onchange="previewImageChange('${selectId}', '${previewId}')">
                        <option></option>
                    </select>
                </div>
                <div class="col-12 col-md-5 d-flex justify-content-center">
                    <div class="mh-150px">
                        <img class="w-auto mw-100 mh-150px rounded-2" src="{{ asset('assets/assets-landing/images/placeholder.png') }}" id="${previewId}">
                    </div>
                </div>
                <div class="col-12 col-md-1 d-flex justify-content-center">
                    <button class="btn btn-icon btn-light-danger" onclick="this.closest('.row').remove()">
                        <i class="ki-duotone ki-trash"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i>
                    </button>
                </div>
            `;

            container.appendChild(newContent);

            $(`#${selectId}`).select2(ads_set_select2_config);
        }

        $(".ads-selections").on('select2:select', function(e) {
            var selectedOption = e.params.data;
            var $option = $(this).find('option[value="' + selectedOption.id + '"]');
            $option.attr('data-image', selectedOption.image_url);
        });

        function previewImageChange(selectComponent, previewComponent) {
            var previewValue = previewComponent;
            var selectedOption = $(`#${selectComponent}`).select2('data');

            if (selectedOption && selectedOption.length > 0) {
                var imageValue = selectedOption[0].image_url;
            }

            var previewImg = document.getElementById(previewValue);
            if (imageValue) {
                previewImg.src = imageValue;
            } else {
                previewImg.src = '{{ asset('assets/assets-landing/images/placeholder.png') }}';
            }
        }

        document.getElementById('simpan_button').addEventListener('click', function() {
            document.getElementById('set_iklan').submit();
        });
    </script>
@endpush

{{-- Edit Section --}}
@push('js-1')
    <script>
        function convertToSlug(Text) {
            return Text.toLowerCase()
                .replace(/ /g, "-")
                .replace(/[^\w-]+/g, "");
        }

        const data = {!! json_encode(@$advertisements_position, JSON_HEX_TAG) !!};
        $(document).ready(function() {
            data.forEach(element_data => {
                const advertisements = element_data.advertisement;
                const slugTitle = convertToSlug(element_data.title);

                for (let index = 0; index < advertisements.length; index++) {
                    const element = advertisements[index];

                    let slugTitleComponent = slugTitle;
                    if (index > 0) {
                        slugTitleComponent += '-' + index;
                        addSection(slugTitle);
                    }

                    var $filter_ads = $("<option selected='selected'></option>")
                        .val(element.id)
                        .text(element.title);
                    $(`#select-${slugTitleComponent}`).append($filter_ads).trigger('change');

                    var previewImg = document.getElementById(`preview-${slugTitleComponent}`);
                    if (element.image) {
                        previewImg.src = element.image;
                    } else {
                        previewImg.src = '{{ asset('assets/assets-landing/images/placeholder.png') }}';
                    }
                }
            });
        });
    </script>
@endpush
