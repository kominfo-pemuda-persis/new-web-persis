@extends('cms.layouts.master')
@section('title', 'Daftar Iklan')

@push('css')
    <style>
        table.dataTable.no-margin {
            margin-top: 0 !important;
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-gray-900 fw-bold flex-column justify-content-center my-0">
                        Daftar Iklan
                    </h1>
                    <!--end::Title-->

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-kategori.index') }}" class="text-muted text-hover-primary">
                                Kelola Iklan
                            </a>
                        </li>
                        <li class="breadcrumb-item"><span class="bullet bg-gray-500 w-5px h-2px"></span></li>
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-kategori.index') }}" class="text-muted text-hover-primary">
                                Daftar Iklan
                            </a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->

                <!--begin::Actions-->
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <a href="#" class="btn btn-lg btn-bd-primary fw-bold addButton" data-bs-toggle="modal"
                        data-bs-target="#tambah-iklan-baru">
                        <i class="ki-duotone ki-plus-square">
                            <span class="path1"></span>
                            <span class="path2"></span>
                            <span class="path3"></span>
                        </i>
                        Tambah Baru
                    </a>
                </div>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->

        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <!--Insert Content Here-->
                <div class="row g-5 g-xl-10 py-6">
                    <!--begin::Col-->
                    <div class="col">
                        <div class="card">
                            <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                                <div class="card-title">
                                    <!--begin::Search-->
                                    <div class="d-flex align-items-center position-relative my-1">
                                        <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                                <path
                                                    d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                                            </svg>
                                        </span>
                                        <input type="text" id="search" data-kt-filter="search"
                                            class="form-control form-control-solid w-250px ps-14" placeholder="Cari"
                                            autocomplete="off" />
                                    </div>
                                </div>
                            </div>

                            <div class="card-body p-0 d-flex flex-column">
                                <table class="table table-row-bordered align-middle no-margin fs-6" id="iklan_table">
                                    <thead class="bg-light-secondary">
                                        <tr class="fw-bold fs-5 text-uppercase">
                                            <th class="py-5 text-center">No</th>
                                            <th class="py-5">Nama Iklan</th>
                                            <th class="py-5 text-center">Gambar</th>
                                            <th class="py-5">Klien</th>
                                            <th class="py-5">URL Target</th>
                                            <th class="py-5">Dilihat</th>
                                            <th class="py-5 text-center">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody class="fw-semibold text-gray-600">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--end::Col-->
                </div>
            </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>
@endsection
@include('cms.kelola_iklan.modal')
@push('js')
    <script>
        function handleImage(image) {
            image.onerror = "";
            image.src = '{{ asset('assets/assets-landing/images/placeholder.png') }}';
            return true;
        }

        let datatable;
        "use strict";

        var IklanTables = function() {
            var table;

            var initDatatable = function() {
                const tableRows = table.querySelectorAll('tbody tr');

                datatable = $(table).DataTable({
                    processing: true,
                    searching: true,
                    ordering: false,
                    bInfo: false,
                    layout: {
                        topStart: null,
                        topEnd: null,
                        bottomStart: 'pageLength',
                    },
                    language: {
                        emptyTable: 'Tidak Ada Data Konten'
                    },
                    ajax: {
                        url: "{{ route('kelola-iklan.index') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        },
                        data: function(d) {
                            d.type_form = "iklan_table";
                        },
                        error: function(req, err) {
                            language: {
                                emptyTable: 'Tidak Ada Data Iklan'
                            };
                            console.log('Table Error : ' + err);
                        }
                    },
                    dataType: 'JSON',
                    columns: [{
                            name: 'No.',
                            data: 'DT_RowIndex',
                            searchable: false,
                            className: 'ps-5 text-center',
                        },
                        {
                            name: 'title',
                            data: 'title',
                            defaultContent: '-',
                            render: function(data, type, row) {
                                var data;
                                data = '<div class="">'
                                data += '<h5 class="mb-2">' + row.title + '</h5>'
                                data += '<p class="mb-0">' + row.desc + '</p>'
                                data += '</div>'
                                return data;
                            },
                        },
                        {
                            name: 'image',
                            data: 'image',
                            defaultContent: '-',
                            render: function(data, type, row) {
                                var data;
                                data =
                                    '<div class="h-100px d-flex align-items-center justify-content-center">'
                                data += `<img class="w-auto mw-100 mh-100px rounded-2" src="` +
                                    row.image + `" onerror="handleImage(this)"/></img></div>`
                                return data;
                            },
                            className: ' w-25',
                        },
                        {
                            name: 'client.name',
                            data: 'client.name',
                            defaultContent: '-',
                            className: '',
                        },
                        {
                            name: 'url_ads',
                            data: 'url_ads',
                            defaultContent: '-',
                            render: function(data, type, row) {
                                var data;
                                data = '<a href="' + row.url_ads + '" target="_blank">'
                                data += row.url_ads
                                data += '</a>'
                                return data;
                            },
                            className: '',
                        },
                        {
                            name: 'clicked',
                            data: 'clicked',
                            defaultContent: '-',
                            className: '',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            searchable: false,
                            className: 'text-center',
                            render: function(data, type, row) {
                                var button
                                button =
                                    `<a href="#" data-toggle="tooltip" data-original-title="Edit"` +
                                    `data-id="` + row.id + `"` +
                                    `class="btn btn-icon btn-light-warning me-1 editButton"><i class="ki-duotone ki-notepad-edit"><span class="path1"></span><span class="path2"></span></i></a>`
                                button +=
                                    `<button class="btn btn-icon btn-light-danger deleteButton" data-id=` +
                                    row.id +
                                    `><i class="ki-duotone ki-trash"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i></button>`
                                return button
                            }
                        },
                    ],
                });
            }

            var handleSearchDatatable = () => {
                const filterSearch = document.querySelector('[data-kt-filter="search"]');
                filterSearch.addEventListener('keyup', function(e) {
                    datatable.search(e.target.value).draw();
                });
            }

            return {
                init: function() {
                    table = document.querySelector('#iklan_table');

                    if (!table) {
                        return;
                    }

                    initDatatable();
                    handleSearchDatatable();

                    datatable.on("draw", function() {
                        KTMenu.createInstances();
                    });

                    $("#iklan_table_wrapper").find("div.row").addClass('px-15 pb-5');
                }
            };
        }();

        KTUtil.onDOMContentLoaded(function() {
            IklanTables.init();
        });
    </script>

    <script>
        $(document).ready(function() {
            $('body').on('click', '.editButton', function() {
                var id = $(this).data('id');
                var url = `{{ route('kelola-iklan.edit', ['id' => ':id']) }}`;
                url = url.replace(':id', id);

                $.get(url, function(response) {
                    var clientId = response.client.id;
                    var clientName = response.client.name;
                    console.log(response);

                    $("#modal-title").html("Edit " + response.title);
                    $("#tambah-iklan-baru").modal("show");
                    $("#id").val(response.id);
                    $("#id").prop('disabled', false);
                    $("#title").val(response.title);
                    $("#desc").val(response.desc);

                    if (!$('#client_id option[value="' + clientId + '"]').length) {
                        var $selectedOption = $("<option selected='selected'></option>")
                            .val(clientId)
                            .text(clientName);
                        $("#client_id").append($selectedOption).trigger('change');
                    } else {
                        // Jika sudah ada, pilih opsi tersebut
                        $("#client_id").val(clientId).trigger('change');
                    }
                    $("#url_ads").val(response.url_ads);

                    if (response.image) {
                        $("#imagePreview").attr('src', response.image).removeClass(
                            'd-none');
                    } else {
                        $("#imagePreview").addClass('d-none');
                    }

                    // Menggunakan helper route untuk mendapatkan URL update
                    var updateUrl = `{{ route('kelola-iklan.update', ['id' => ':id']) }}`;
                    updateUrl = updateUrl.replace(':id', id);

                    $("#kelola_iklan").attr('action', updateUrl);
                });
            });


            $('body').on('click', '.addButton', function() {
                $("#modal-title").html("Tambah Iklan");

                // Mereset dan mengosongkan form
                var form = $('#kelola_iklan');
                form[0].reset(); // Reset form fields
                $('#imagePreview').addClass('d-none'); // Sembunyikan gambar preview
                $('#image').val(''); // Kosongkan input file

                // Jika ada elemen lain yang perlu direset, tambahkan di sini
            });


            $('body').on('click', '.deleteButton', function() {
                var id = $(this).data('id');
                event.preventDefault();

                Swal.fire({
                    title: `Hapus Data Iklan`,
                    text: "Apakah Anda yakin ingin menghapus data ini?",
                    icon: "warning",
                    showCancelButton: true,
                    showCloseButton: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        var url = `{{ route('kelola-iklan.destroy', ':id') }}`.replace(':id', id);

                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: {
                                _token: '{{ csrf_token() }}',
                                _method: 'DELETE'
                            },
                            error: (response) => {
                                let msg = response.responseJSON.message
                                if (!msg) msg =
                                    'Gagal mereset data. Terjadi kesalahan pada server'
                                Swal.fire({
                                    title: 'Perhatian',
                                    text: msg,
                                    icon: 'warning'
                                })
                            },
                            success: (response) => {
                                datatable.ajax.reload()
                                toastr.success("Data berhasil dihapus")
                            }
                        });
                    }
                });
            });

        });
    </script>
@endpush
