<div class="modal fade" tabindex="-1" id="tambah-iklan-baru" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="modal-title">
                    Tambah Iklan
                </h3>
                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                    aria-label="Close">
                    <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                </div>
                <!--end::Close-->
            </div>
            <div class="modal-body">
                <form action="{{ route('kelola-iklan.store') }}" method="post" id="kelola_iklan"
                    enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" class="form-control" name="id" id="id"
                        placeholder="Nama Kategori" />
                    <div class="row g-5">
                        <div class="col-12">
                            <label for="" class="form-label"><b>Nama Iklan</b></label>
                            <input type="text" class="form-control" name="title" id="title"
                                placeholder="Nama Iklan" required />
                        </div>
                        <div class="col-12">
                            <label for="" class="form-label"><b>Deskripsi</b></label>
                            <textarea name="desc" class="form-control" id="desc" placeholder="Deskripsi" data-kt-autosize="true"></textarea>
                        </div>

                        <div class="col-12" id="status_select_div">
                            <label for="" class="form-label"><b>Klien</b></label>
                            <select class="form-select" data-control="select2" data-placeholder="Pilih Klien"
                                data-dropdown-parent="#tambah-iklan-baru" name="client_id" id="client_id"
                                data-allow-clear="true" required>
                                <option></option>
                                @foreach ($clients as $client)
                                    <option value="{{ $client->id }}">{{ $client->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-12">
                            <label for="" class="form-label"><b>URL Target Iklan</b></label>
                            <input type="text" class="form-control" name="url_ads" id="url_ads"
                                placeholder="URL Target Iklan" />
                            <small class="form-text text-muted">( Contoh : https://www.toko.com )</small>
                        </div>
                        <div class="col-12">
                            <label for="" class="form-label"><b>Gambar</b></label>
                            <img id="imagePreview" class="img-fluid mb-3 d-none rounded-2"
                                onerror="handleImage(this)" />
                            <input type="file" class="d-none" id="image" name="image"
                                onchange="previewImage(event)">
                            <button type="button" class="w-100 btn btn-light-success"
                                onclick="document.getElementById('image').click()">
                                Pilih Gambar
                            </button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-bd-primary" form="kelola_iklan" id="simpan_button">Simpan</button>
            </div>
        </div>
    </div>
</div>

@push('js')
    <script>
        function previewImage(event) {
            const input = event.target;
            const preview = input.previousElementSibling; // Adjust based on your structure

            if (input.files && input.files[0]) {
                const reader = new FileReader();

                reader.onload = function (e) {
                    preview.src = e.target.result;
                    preview.classList.remove('d-none');
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function () {
            $('#client_id').select2({
                placeholder: "Pilih Klien",
                tags: true,
                allowClear: true,
                dropdownParent: $('#tambah-iklan-baru'),
                createTag: function (params) {
                    var term = $.trim(params.term);
                    if (term === '') {
                        return null;
                    }

                    // Create a new tag object
                    return {
                        id: 'new-' + term,
                        text: term,
                        newTag: true // Add a custom property to identify new tags
                    };
                }
            }).on('select2:select', function (e) {
                var data = e.params.data;
                if (data.newTag) {
                    // Handle the new tag input if necessary
                    $('#client_id').data('newClientName', data.text);
                } else {
                    $('#client_id').removeData('newClientName');
                }
            });
        });
    </script>

@endpush
