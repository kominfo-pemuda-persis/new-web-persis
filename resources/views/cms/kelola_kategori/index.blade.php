@extends('cms.layouts.master')
@section('title', 'Kelola Kategori')

@push('css')
    <style>
        table.dataTable.no-margin {
            margin-top: 0 !important;
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-gray-900 fw-bold flex-column justify-content-center my-0">
                        Kelola Kategori
                    </h1>
                    <!--end::Title-->

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-kategori.index') }}" class="text-muted text-hover-primary">
                                Kelola Kategori
                            </a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->

                <!--begin::Actions-->
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <a href="#" class="btn btn-lg btn-bd-primary fw-bold addButton" data-bs-toggle="modal"
                        data-bs-target="#tambah-baru-kategori">
                        <i class="ki-duotone ki-plus-square">
                            <span class="path1"></span>
                            <span class="path2"></span>
                            <span class="path3"></span>
                        </i>
                        Tambah Baru
                    </a>
                </div>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->

        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <!--Insert Content Here-->
                <div class="row g-5 g-xl-10 py-6">
                    <!--begin::Col-->
                    <div class="col">
                        <div class="card">
                            <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                                <div class="card-title">
                                    <!--begin::Search-->
                                    <div class="d-flex align-items-center position-relative my-1">
                                        <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                                <path
                                                    d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                                            </svg>
                                        </span>
                                        <input type="text" id="search" data-kt-filter="search"
                                            class="form-control form-control-solid w-250px ps-14" placeholder="Cari"
                                            autocomplete="off" />
                                    </div>
                                </div>
                            </div>

                            <div class="card-body p-0 d-flex flex-column">
                                <table class="table table-row-bordered align-middle no-margin fs-6" id="kategori_table">
                                    <thead class="bg-light-secondary">
                                        <tr class="fw-bold fs-5 text-uppercase">
                                            <th class="py-5 text-center">No</th>
                                            <th class="py-5">Nama Kategori</th>
                                            <th class="py-5 text-center">Status</th>
                                            <th class="py-5 text-center">Parent</th>
                                            <th class="py-5 text-center">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody class="fw-semibold text-gray-600">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--end::Col-->
                </div>
            </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>
@endsection
@include('cms.kelola_kategori.modal')
@push('js')
    <script>
        let datatable;
        "use strict";

        var KategoriTables = function() {
            var table;

            var initDatatable = function() {
                const tableRows = table.querySelectorAll('tbody tr');

                datatable = $(table).DataTable({
                    processing: true,
                    searching: true,
                    ordering: false,
                    bInfo: false,
                    layout: {
                        topStart: null,
                        topEnd: null,
                        bottomStart: 'pageLength',
                    },
                    language: {
                        emptyTable: 'Tidak Ada Data Kategori'
                    },
                    ajax: {
                        url: "{{ route('kelola-kategori.index') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        },
                        data: function(d) {
                            d.type_form = "kategori_table";
                        },
                        error: function(req, err) {
                            language: {
                                emptyTable: 'Tidak Ada Data Kategori'
                            };
                            console.log('Table Error : ' + err);
                        }
                    },
                    dataType: 'JSON',
                    rowGroup: {
                        className: "bg-secondary fs-5",
                        dataSrc: function(row) {
                            return '<div class="px-15 d-flex justify-content-between align-items-center">' +
                                '<div><span>' + row.content_categories_name + '</span>' +
                                (row.content_categories_status ?
                                    `<span class="badge badge-light-success fw-bold fs-8 px-2 py-1 ms-5">Aktif</span>` :
                                    `<span class="badge badge-light-danger fw-bold fs-8 px-2 py-1 ms-5">Tidak Aktif</span>`
                                ) +
                                '</div><div><span>' +
                                `<a href="#" data-toggle="tooltip" data-original-title="Edit"` +
                                `data-id="` + row.content_categories_id + `"` +
                                `class="btn btn-icon btn-light-warning me-1 editButton"><i class="ki-duotone ki-notepad-edit"><span class="path1"></span><span class="path2"></span></i></a>` +
                                `<button class="btn btn-icon btn-light-danger deleteButton" data-id=` +
                                row.content_categories_id +
                                `><i class="ki-duotone ki-trash"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i></button>` +
                                '</span></div>' +
                                '</div>'
                        }
                    },
                    columns: [{
                            name: 'No.',
                            data: 'DT_RowIndex',
                            searchable: false,
                            className: 'ps-5 text-center',
                            render: function(data, type, row) {
                                if (row.content_categories_sub_id) {
                                    return row.DT_RowIndex
                                } else {
                                    return '-'
                                }
                            },
                        },
                        {
                            name: 'content_categories_sub_name',
                            data: 'content_categories_sub_name',
                            defaultContent: '-',
                            render: function(data, type, row) {
                                if (row.content_categories_sub_id) {
                                    return row.content_categories_sub_name
                                } else {
                                    return '<div class="text-center">-</div>'
                                }
                            },
                        },
                        {
                            name: 'content_categories_sub_status',
                            data: 'content_categories_sub_status',
                            defaultContent: '-',
                            searchable: false,
                            className: 'text-center',
                            render: function(data, type, row) {
                                if (row.content_categories_sub_id) {
                                    if (row.content_categories_sub_status === 1) {
                                        return '<span class="badge badge-light-success fw-bold fs-8 px-2 py-1 ms-2">Aktif</span>'
                                    } else {
                                        return '<span class="badge badge-light-danger fw-bold fs-8 px-2 py-1 ms-2">Tidak Aktif</span>'
                                    };
                                } else {
                                    return '-'
                                }
                            },
                        },
                        {
                            name: 'content_categories_name',
                            data: 'content_categories_name',
                            defaultContent: '-',
                            className: 'text-center',
                            render: function(data, type, row) {
                                if (row.content_categories_sub_id) {
                                    return row.content_categories_name
                                } else {
                                    return '-'
                                }
                            },
                        },
                        {
                            data: 'action',
                            name: 'action',
                            searchable: false,
                            className: 'text-center',
                            render: function(data, type, row) {
                                if (row.content_categories_sub_id) {
                                    var button
                                    button =
                                        `<a href="#" data-toggle="tooltip" data-original-title="Edit"` +
                                        `data-id="` + row.content_categories_sub_id + `"` +
                                        `class="btn btn-icon btn-light-warning me-1 editButton"><i class="ki-duotone ki-notepad-edit"><span class="path1"></span><span class="path2"></span></i></a>`
                                    button +=
                                        `<button class="btn btn-icon btn-light-danger deleteButton" data-id=` +
                                        row.content_categories_sub_id +
                                        `><i class="ki-duotone ki-trash"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i></button>`

                                    return button
                                } else {
                                    return '-'
                                }
                            }
                        },
                    ],
                });
            }

            var handleSearchDatatable = () => {
                const filterSearch = document.querySelector('[data-kt-filter="search"]');
                filterSearch.addEventListener('keyup', function(e) {
                    datatable.search(e.target.value).draw();
                });
            }

            return {
                init: function() {
                    table = document.querySelector('#kategori_table');

                    if (!table) {
                        return;
                    }

                    initDatatable();
                    handleSearchDatatable();

                    $("#kategori_table_wrapper").find("div.row").addClass('px-15 pb-5');
                }
            };
        }();

        KTUtil.onDOMContentLoaded(function() {
            KategoriTables.init();
        });
    </script>

    <script>
        $(document).ready(function() {
            $('body').on('click', '.editButton', function() {
                var id = $(this).data('id');
                var url = `{{ route('kelola-kategori.edit', ['id' => ':id']) }}`;
                url = url.replace(':id', id);

                $.get(url, function(data) {
                    console.log(data)
                    $("#modal-title").html("Edit " + (data.category_sub_name ? "Sub" : "Parent") +
                        " Kategori");
                    $("#tambah-baru-kategori").modal("show");
                    $("#id").val(data.id);
                    $("#id").prop('disabled', false);
                    $("#nama_kategori").val(data.category_sub_name ? data.category_sub_name : data.category_name);
                    $("#desc_kategori").val(data.desc);
                    document.getElementsByClassName('ql-editor')[0].innerHTML = data.desc;

                    $("#status").val(data.status).trigger('change');

                    if (data.category_sub_name) {
                        var $selectedOption = $("<option selected='selected'></option>")
                            .val(data.content_categories.id)
                            .text(data.content_categories.category_name);

                        $("#parent_kategori").append($selectedOption).trigger('change');

                        // Subkategori
                        $('#check_subkategori').prop('checked', true);
                        $('#parent_kategori_div').show();
                        $('#parent_kategori').attr('disabled', false);
                        $('#parent_kategori').attr('required', true);
                    } else {
                        $('#check_subkategori').prop('checked', false);
                        $('#parent_kategori_div').hide();
                        $('#check_subkategori').prop('disabled', true);
                    };
                });
            });

            $('body').on('click', '.addButton', function() {
                $("#modal-title").html("Tambah Kategori");
                $("#id").val(null);
                $("#id").prop('disabled', true);
                $("#nama_kategori").val(null);
                $("#desc_kategori").val(null);

                document.getElementsByClassName('ql-editor')[0].innerHTML = "";
                $("#status").val(null).trigger('change');

                $('#check_subkategori').prop('checked', false);
                $('#check_subkategori').prop('disabled', false);
                $("#parent_kategori").val(null).trigger('change');
                $('#parent_kategori_div').hide();
                $('#parent_kategori').attr('disabled', true)
                $('#parent_kategori').attr('required', false)
            });

            $('body').on('click', '.deleteButton', function() {
                var id = $(this).data('id');
                event.preventDefault();

                Swal.fire({
                    title: `Hapus Data Kategori`,
                    text: "Apakah Anda yakin ingin menghapus data ini?",
                    icon: "warning",
                    showCancelButton: true,
                    showCloseButton: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        var url = `{{ route('kelola-kategori.destroy', ['id' => ':id']) }}`;
                        url = url.replace(':id', id);
                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: {
                                _token: '{{ csrf_token() }}',
                                _method: 'DELETE'
                            },
                            error: (response) => {
                                let msg;
                                if (response.responseJSON.message.includes(
                                        "SQLSTATE[23503]: Foreign key violation")) {
                                    msg =
                                        'Data memiliki sub-kategori atau konten, Maka Kategori ini tidak dapat dihapus.';
                                } else if (!msg) {
                                    msg =
                                        'Gagal menghapus data. Terjadi kesalahan pada server';
                                } else {
                                    msg = response.responseJSON.message
                                }

                                Swal.fire({
                                    title: 'Perhatian',
                                    text: msg,
                                    icon: 'warning'
                                })
                            },
                            success: (response) => {
                                datatable.ajax.reload()
                                toastr.success("Data berhasil dihapus")
                            }
                        });
                    }
                });
            });
        });
    </script>
@endpush
