@push('css')
    <style>
        .ql-container {
            height: calc(100% - 65px) !important;
        }
    </style>
@endpush
<div class="modal fade" tabindex="-1" id="tambah-baru-kategori" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="modal-title">
                    Tambah Kategori
                </h3>
                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                    aria-label="Close">
                    <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                </div>
                <!--end::Close-->
            </div>

            <div class="modal-body">
                <form action="{{ route('kelola-kategori.store') }}" method="post" id="kelola_kategori">
                    @csrf
                    <input type="hidden" class="form-control" name="id" id="id"
                        placeholder="Nama Kategori" />
                    <div class="row g-5">
                        <div class="col-12">
                            <h4>Nama Kategori</h4>
                            <input type="text" class="form-control" name="nama_kategori" id="nama_kategori"
                                placeholder="Nama Kategori" required />
                        </div>
                        <div class="col-12">
                            <h4>Deskripsi Kategori</h4>
                            <textarea name="desc_kategori" id="desc_kategori" class="hidden"></textarea>
                            <div id="quil_desc_kategori">
                            </div>
                        </div>
                        <div class="col-12" id="status_select_div">
                            <h4>Status Kategori</h4>
                            <select class="form-select" data-control="select2" data-placeholder="Pilih Status Kategori"
                                data-hide-search="true" name="status" id="status" required>
                                <option></option>
                                <option value="1">Aktif</option>
                                <option value="0">Tidak Aktif</option>
                            </select>
                        </div>
                        <div class="col-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="check_subkategori"
                                    id="check_subkategori" value="" />
                                <label class="form-check-label" for="flexCheckDefault">
                                    Ini adalah Subkategori
                                </label>
                            </div>
                        </div>
                        <div class="col-12 hidden" id="parent_kategori_div">
                            <h4>Parent Kategori</h4>
                            <select class="form-select" data-control="select2" data-placeholder="Pilih Parent Kategori"
                                name="parent_kategori" id="parent_kategori" disabled>
                            </select>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-bd-primary" form="kelola_kategori"
                    id="simpan_button">Simpan</button>
            </div>
        </div>
    </div>
</div>
@push('js')
    <script>
        $(document).ready(function() {
            $('#check_subkategori').change(function() {
                if (this.checked == true) {
                    $('#parent_kategori_div').show();
                    $('#parent_kategori').attr('disabled', false)
                    $('#parent_kategori').attr('required', true)
                } else {
                    $('#parent_kategori_div').hide();
                    $('#parent_kategori').attr('disabled', true)
                    $('#parent_kategori').attr('required', false)
                }
            });

            $("#parent_kategori").select2({
                dropdownParent: $('#tambah-baru-kategori'),
                ajax: {
                    url: "{{ route('kelola-kategori.index') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        var query = {
                            _token: '{{ csrf_token() }}',
                            search: params.term,
                            page: params.page || 1
                        };

                        return query;
                    },
                    cache: true
                }
            });

            var quill = new Quill('#quil_desc_kategori', {
                modules: {
                    toolbar: [
                        [{
                            header: [1, 2, false]
                        }],
                        ['bold', 'italic', 'underline'],
                    ]
                },
                placeholder: 'Deskripsi Kategori',
                theme: 'snow'
            });
            quill.root.innerHTML = '<p>' + document.getElementById("desc_kategori").value + '</p>';

            quill.on('text-change', function() {
                document.getElementById("desc_kategori").value = quill.root.innerHTML;
            });

        });
    </script>
@endpush
