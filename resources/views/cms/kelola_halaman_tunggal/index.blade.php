@extends('cms.layouts.master')
@section('title', 'Kelola Halaman Tunggal')

@push('css')
    <style>
        table.dataTable.no-margin {
            margin-top: 0 !important;
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-gray-900 fw-bold flex-column justify-content-center my-0">
                        Kelola Halaman Tunggal
                    </h1>
                    <!--end::Title-->

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-halaman-tunggal.index') }}" class="text-muted text-hover-primary">
                                Kelola Halaman Tunggal
                            </a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->

                <!--begin::Actions-->
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <a href="{{ route('kelola-halaman-tunggal.create') }}" class="btn btn-lg btn-bd-primary fw-bold">
                        <i class="ki-duotone ki-plus-square">
                            <span class="path1"></span>
                            <span class="path2"></span>
                            <span class="path3"></span>
                        </i>
                        Tambah Baru
                    </a>
                </div>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->

        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <!--Insert Content Here-->
                <div class="row g-5 g-xl-10 py-6">
                    <!--begin::Col-->
                    <div class="col">
                        <div class="card">
                            <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                                <div class="card-title">
                                    <!--begin::Search-->
                                    <div class="d-flex align-items-center position-relative my-1">
                                        <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                                            </svg>
                                        </span>
                                        <input type="text" id="search" data-kt-filter="search" class="form-control form-control-solid w-250px ps-14" placeholder="Cari" autocomplete="off" />
                                    </div>
                                </div>
                            </div>

                            <div class="card-body p-0 d-flex flex-column">
                                <table class="table table-row-bordered align-middle no-margin fs-6" id="halaman_tunggal_table">
                                    <thead class="bg-light-secondary">
                                        <tr class="fw-bold fs-5 text-capitalize">
                                            <th class="py-5 text-center">No</th>
                                            <th class="py-5 text-center">Nama Halaman</th>
                                            <th class="py-5 text-center">Slug</th>
                                            <th class="py-5 text-center">Dibuat Oleh</th>
                                            <th class="py-5 text-center">Terakhir Diperbaharui</th>
                                            <th class="py-5 text-center">Dilihat</th>
                                            <th class="py-5 text-center">Status</th>
                                            <th class="py-5 text-center">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody class="fw-semibold text-gray-600">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--end::Col-->
                </div>
            </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>
@endsection
@push('js')
    <script>
        let datatable;
        "use strict";

        var KategoriTables = function() {
            var table;

            var initDatatable = function() {
                const tableRows = table.querySelectorAll('tbody tr');

                datatable = $(table).DataTable({
                    processing: true,
                    searching: true,
                    ordering: false,
                    bInfo: false,
                    layout: {
                        topStart: null,
                        topEnd: null,
                        bottomStart: 'pageLength',
                    },
                    language: {
                        emptyTable: 'Tidak Ada Data Halaman Tunggal'
                    },
                    ajax: {
                        url: "{{ route('kelola-halaman-tunggal.index') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        },
                        data: function(d) {
                            d.type_form = "halaman_tunggal_table";
                        },
                        error: function(req, err) {
                            language: {
                                emptyTable: 'Tidak Ada Data Kategori'
                            };
                            console.log('Table Error : ' + err);
                        }
                    },
                    dataType: 'JSON',
                    columns: [{
                            name: 'No.',
                            data: 'DT_RowIndex',
                            searchable: false,
                            className: 'ps-5 text-center',
                        },
                        {
                            name: 'title',
                            data: 'title',
                            defaultContent: '-',
                            className: 'text-center',
                            render: function(data, type, row) {
                                if (row.title) {
                                    return row.title
                                } else {
                                    return '<div class="text-center">-</div>'
                                }
                            },
                        },
                        {
                            name: 'slug',
                            data: 'slug',
                            defaultContent: '-',
                            className: 'text-center',
                            render: function(data, type, row) {
                                if (row.slug) {
                                    return row.slug
                                } else {
                                    return '<div class="text-center">-</div>'
                                }
                            },
                        },
                        {
                            name: 'created_by',
                            data: 'created_by',
                            className: 'text-center',
                            defaultContent: '-',
                            render: function(data, type, row) {
                                if (row.created_by) {
                                    return row.creators.full_name
                                } else {
                                    return '<div class="text-center">-</div>'
                                }
                            },
                        },
                        {
                            name: 'updated_at',
                            data: 'updated_at',
                            defaultContent: '-',
                            className: 'text-center',
                            render: function(data, type, row) {
                                if (type === "sort" || type === "type") {
                                    return data;
                                }
                                return moment(data).format("DD-MM-YYYY HH:mm");
                            }
                        },
                        {
                            name: 'visited',
                            data: 'visited',
                            defaultContent: '-',
                            className: 'text-center',
                        },
                        {
                            name: 'status',
                            data: 'status',
                            defaultContent: '-',
                            className: 'text-center',
                            render: function(data, type, row) {
                                if (row.status) {
                                    return '<span class="badge badge-light-success fw-bold fs-8 px-2 py-1 ms-2">Aktif</span>'
                                } else {
                                    return '<span class="badge badge-light-warning fw-bold fs-8 px-2 py-1 ms-2">Draft</span>'
                                }
                            },
                        },

                        {
                            data: 'action',
                            name: 'action',
                            searchable: false,
                            className: 'text-center',
                            render: function(data, type, row) {
                                var button
                                button =
                                    `<a href="kelola-halaman-tunggal/edit/` + row.id + `" data-toggle="tooltip" data-original-title="Edit"` +
                                    `data-id="` + row.id + `"` +
                                    `class="btn btn-icon btn-light-warning me-1 editButton"><i class="ki-duotone ki-notepad-edit"><span class="path1"></span><span class="path2"></span></i></a>`
                                button +=
                                    `<button class="btn btn-icon btn-light-danger deleteButton" data-id=` + row.id +
                                    `><i class="ki-duotone ki-trash"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i></button>`

                                return button
                            }
                        },
                    ],
                });
            }

            var handleSearchDatatable = () => {
                const filterSearch = document.querySelector('[data-kt-filter="search"]');
                filterSearch.addEventListener('keyup', function(e) {
                    datatable.search(e.target.value).draw();
                });
            }

            return {
                init: function() {
                    table = document.querySelector('#halaman_tunggal_table');

                    if (!table) {
                        return;
                    }

                    initDatatable();
                    handleSearchDatatable();

                    $("#halaman_tunggal_table_wrapper").find("div.row").addClass('px-15 pb-5');
                }
            };
        }();

        KTUtil.onDOMContentLoaded(function() {
            KategoriTables.init();
        });
    </script>

    <script>
        $(document).ready(function() {
            $('body').on('click', '.deleteButton', function() {
                var id = $(this).data('id');
                event.preventDefault();

                Swal.fire({
                    title: `Hapus Data Halaman Tunggal`,
                    text: "Apakah Anda yakin ingin menghapus data ini?",
                    icon: "warning",
                    showCancelButton: true,
                    showCloseButton: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        var url = `{{ route('kelola-halaman-tunggal.destroy', ['id' => ':id']) }}`;
                        url = url.replace(':id', id);
                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: {
                                _token: '{{ csrf_token() }}',
                                _method: 'DELETE'
                            },
                            error: (response) => {
                                let msg;
                                if (response.responseJSON.message.includes(
                                        "SQLSTATE[23503]: Foreign key violation")) {
                                    msg =
                                        'Data memiliki sub-kategori atau konten, Maka Kategori ini tidak dapat dihapus.';
                                } else if (!msg) {
                                    msg =
                                        'Gagal menghapus data. Terjadi kesalahan pada server';
                                } else {
                                    msg = response.responseJSON.message
                                }

                                Swal.fire({
                                    title: 'Perhatian',
                                    text: msg,
                                    icon: 'warning'
                                })
                            },
                            success: (response) => {
                                datatable.ajax.reload()
                                toastr.success("Data berhasil dihapus")
                            }
                        });
                    }
                });
            });
        });
    </script>
@endpush
