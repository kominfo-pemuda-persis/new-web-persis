@extends('cms.layouts.master')
@section('title', 'Kelola Halaman Tunggal')

@push('css')
    <style>
        img[src="null"] {
            display: none;
        }

        .dropzone {
            border: 1px solid var(--bs-gray-300) !important;
            background-color: transparent !important;
        }

        .dz-preview {
            width: 100%;
            margin: 0 !important;
            height: 100%;
            padding: 15px;
            top: 0;
        }

        .dz-photo {
            height: 100%;
            width: 100%;
            overflow: hidden;
            border-radius: 12px;
            background: #eae7e2;
        }

        .dz-thumbnail {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .dz-image {
            width: 90px !important;
            height: 90px !important;
            border-radius: 6px !important;
        }

        .dz-remove {
            display: none !important;
        }

        .dz-delete {
            width: 24px;
            height: 24px;
            background: rgba(0, 0, 0, 0.57);
            position: absolute;
            opacity: 0;
            transition: all 0.2s ease;
            top: 30px;
            right: 30px;
            border-radius: 100px;
            z-index: 9999;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .dz-delete>svg {
            transform: scale(0.75);
            cursor: pointer;
        }

        .dz-preview:hover .dz-delete,
        .dz-preview:hover .dz-remove-image {
            opacity: 1;
        }

        .dz-message {
            height: 100%;
            margin: 0 !important;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .dropzone-drag-area {
            position: relative;
            padding: 0 !important;
        }
    </style>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Amiri:ital,wght@0,400;0,700;1,400;1,700&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Aref+Ruqaa:wght@400;700&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Changa:wght@200..800&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Courier+Prime:ital,wght@0,400;0,700;1,400;1,700&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Lateef:wght@200;300;400;500;600;700;800&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Mirza:wght@400;500;600;700&display=swap');

        @font-face {
            font-family: 'Akhbar MT';
            fonnt-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url('/assets/custom-fonts/Akhbar MT/Akhbar MT Regular.ttf') format("truetype");
            ;
        }

        @font-face {
            font-family: 'Akhbar MT';
            fonnt-style: normal;
            font-weight: 700;
            font-display: swap;
            src: url('/assets/custom-fonts/Akhbar MT/Akhbar MT Bold.ttf') format("truetype");
            ;
        }


        @font-face {
            font-family: 'Janna LT Bold';
            fonnt-style: normal;
            font-weight: 700;
            font-display: swap;
            src: url('/assets/custom-fonts/Janna LT/Janna LT Bold.ttf') format("truetype");
            ;
        }

        @font-face {
            font-family: 'KFGQPC Uthman Taha Naskh Regular';
            fonnt-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url('/assets/custom-fonts/KFGQPC Uthman Taha Naskh/KFGQPC Uthman Taha Naskh Regular.ttf') format("truetype");
            ;
        }

        .ql-container {
            height: 250px;
            max-height: 250px;
            overflow: auto;
        }

        /* Set droplist names - -item is the currently selected font, -label is the font's appearance in the droplist*/

        .ql-snow .ql-picker.ql-font .ql-picker-label::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item::before {
            content: "Inter";
            font-family: Inter, Helvetica, sans-serif;
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="tahoma"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="tahoma"]::before {
            content: "Tahoma";
            font-family: "Tahoma";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="georgia"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="georgia"]::before {
            content: "Georgia";
            font-family: "Georgia";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="book-antiqua"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="book-antiqua"]::before {
            content: "Book Antiqua";
            font-family: "Book Antiqua";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="arial"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="arial"]::before {
            content: "Arial";
            font-family: "Arial";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="trebuchet"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="trebuchet"]::before {
            content: "Trebuchet";
            font-family: "Trebuchet";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="verdana"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="verdana"]::before {
            content: "Verdana";
            font-family: "Verdana";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="times-new-roman"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="times-new-roman"]::before {
            content: "Times New Roman";
            font-family: "Times New Roman";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="wedings"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="wedings"]::before {
            content: "Wedings";
            font-family: "Wedings";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="britanic-bold"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="britanic-bold"]::before {
            content: "Britanic Bold";
            font-family: "Britanic Bold";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="janna-lt-bold"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="janna-lt-bold"]::before {
            content: "Janna LT Bold";
            font-family: "Janna LT Bold";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="kfgqpc-uthman-taha-naskh-regular"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="kfgqpc-uthman-taha-naskh-regular"]::before {
            content: "KFGQPC Uthman Taha Naskh Regular";
            font-family: "KFGQPC Uthman Taha Naskh Regular";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="akhbar-mt"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="akhbar-mt"]::before {
            content: "Akhbar MT";
            font-family: "Akhbar MT";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="changa"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="changa"]::before {
            content: "Changa";
            font-family: "Changa";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="amiri"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="amiri"]::before {
            content: "Amiri";
            font-family: "Amiri";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="lateef"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="lateef"]::before {
            content: "Lateef";
            font-family: "Lateef";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="aref-ruqaa"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="aref-ruqaa"]::before {
            content: "Aref Ruqaa";
            font-family: "Aref Ruqaa";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="mirza"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="mirza"]::before {
            content: "Mirza";
            font-family: "Mirza";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="courier-prime"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="courier-prime"]::before {
            content: "Courier Prime";
            font-family: "Courier Prime";
        }

        .ql-formats .ql-font.ql-picker {
            width: 140px !important;
        }

        .ql-font.ql-picker .ql-picker-label::before {
            width: 90%;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .ql-font.ql-picker .ql-picker-options {
            max-height: 200px;
            overflow: auto;
        }

        .ql-font-tahoma {
            font-family: "Tahoma" !important;
        }

        .ql-font-georgia {
            font-family: "Georgia" !important;
        }

        .ql-font-book-antiqua {
            font-family: "Book Antiqua" !important;
        }

        .ql-font-arial {
            font-family: "Arial" !important;
        }

        .ql-font-trebuchet {
            font-family: "Trebuchet" !important;
        }

        .ql-font-verdana {
            font-family: "Verdana" !important;
        }

        .ql-font-times-new-roman {
            font-family: "Times New Roman" !important;
        }

        .ql-font-wedings {
            font-family: "Wedings" !important;
        }

        .ql-font-britanic-bold {
            font-family: "Britanic Bold" !important;
        }

        .ql-font-janna-lt-bold {
            font-family: "Janna LT Bold" !important;
        }

        .ql-font-kfgqpc-uthman-taha-naskh-regular {
            font-family: "KFGQPC Uthman Taha Naskh Regular" !important;
        }

        .ql-font-akhbar-mt {
            font-family: "Akhbar MT" !important;
        }

        .ql-font-changa {
            font-family: "Changa" !important;
        }

        .ql-font-amiri {
            font-family: "Amiri" !important;
        }

        .ql-font-lateef {
            font-family: "Lateef" !important;
        }

        .ql-font-aref-ruqaa {
            font-family: "Aref Ruqaa" !important;
        }

        .ql-font-mirza {
            font-family: "Mirza" !important;
        }

        .ql-font-courier-prime {
            font-family: "Courier Prime" !important;
        }

        .ql-formats .ql-size.ql-picker {
            width: 56px !important;
        }

        .ql-size.ql-picker .ql-picker-options {
            max-height: 200px;
            overflow: auto;
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="8px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="8px"]::before {
            content: "8";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="9px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="9px"]::before {
            content: "9";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="10px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="10px"]::before {
            content: "10";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="12px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="12px"]::before {
            content: "12";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="14px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="14px"]::before {
            content: "14";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="16px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="16px"]::before {
            content: "16";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="18px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="18px"]::before {
            content: "18";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="20px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="20px"]::before {
            content: "20";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="24px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="24px"]::before {
            content: "24";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="32px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="32px"]::before {
            content: "32";
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-gray-900 fw-bold flex-column justify-content-center my-0">
                        {{ @$data ? 'Edit' : 'Buat' }} Halaman
                    </h1>
                    <!--end::Title-->

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-halaman-tunggal.index') }}" class="text-muted text-hover-primary">
                                Kelola Halaman Tunggal
                            </a>
                        </li>
                        <li class="breadcrumb-item"><span class="bullet bg-gray-500 w-5px h-2px"></span></li>
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-halaman-tunggal.create') }}" class="text-muted text-hover-primary">
                                {{ @$data ? 'Edit' : 'Buat' }} Halaman Tunggal
                            </a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <button type="submit" class="btn btn-outline btn-outline-success btn-color-success-400 btn-active-light-success" id="draft_button" form="tambah_konten_form">
                        Simpan ke Draf
                    </button>
                    <button type="submit" class="btn btn-lg btn-bd-primary fw-bold" id="publish_button" form="tambah_konten_form">
                        Simpan dan Publikasikan
                    </button>
                </div>
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->

        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <form action="{{ route('kelola-halaman-tunggal.store') }}" method="post" enctype="multipart/form-data" class="needs-validation" data-parsley-errors-messages-disabled id="tambah_konten_form">
                    @csrf
                    <input type="hidden" id="id" name="id" value={{ @$data->id ?? null }}>
                    <input type="hidden" id="created_by" name="created_by" value={{ @$data->created_by ?? null }}>
                    <input type="hidden" name="button_status" id="button_status" value="">
                    <input type="hidden" id="status" name="status">
                    <div class="row g-5 gx-xl-10 mb-5 mb-xl-10">
                        <!--begin::Primary Create-->
                        <div class="col-lg-8">
                            <div class="card card-flush">
                                <!--begin::Card body-->
                                <div class="card-body d-flex flex-column">
                                    <div class="row g-5">
                                        <div class="col-12">
                                            <h5 class="required">Judul</h5>
                                            <input type="text" class="form-control" placeholder="Judul" name="title" id="title" data-parsley-required value="{{ old('title') }}">
                                        </div>
                                        <div class="col-12">
                                            <h5 class="required">Slug</h5>
                                            <input type="text" class="form-control" placeholder="Slug" name="slug" id="slug" data-parsley-required value="{{ old('slug') }}">
                                        </div>
                                        <div class="col-12" id="gambar_sampul">
                                            <h5>Gambar Sampul</h5>
                                            <div class="dropzone" id="gambar_sampul_dropzone">
                                                <!--begin::Message-->
                                                <div class="dropzone-drag-area form-control form-control-transparent h-150px" id="previews">
                                                    <input type="file" id="featured_image" name="featured_image" style="display: none;" />
                                                    <div class="fallback">
                                                        <input name="file" type="file" />
                                                    </div>
                                                    <div class="dz-message">
                                                        <!--begin::Info-->
                                                        <div class="ms-4">
                                                            <span class="fs-5 fw-bold text-decoration-underline" style="color: #054c2d">
                                                                Unggah gambar
                                                            </span>
                                                            <span class="fs-7 fw-semibold text-gray-500">
                                                                atau seret gambar ke sini
                                                            </span>
                                                        </div>
                                                        <!--end::Info-->
                                                    </div>
                                                    <div class="d-none" id="dzPreviewContainer">
                                                        <div class="dz-preview dz-file-preview">
                                                            <div class="dz-photo">
                                                                <img class="dz-thumbnail" data-dz-thumbnail>
                                                            </div>
                                                            <button class="dz-delete border-0 p-0" type="button" data-dz-remove>
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" id="times">
                                                                    <path fill="#FFFFFF"
                                                                        d="M13.41,12l4.3-4.29a1,1,0,1,0-1.42-1.42L12,10.59,7.71,6.29A1,1,0,0,0,6.29,7.71L10.59,12l-4.3,4.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0L12,13.41l4.29,4.3a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42Z">
                                                                    </path>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="invalid-feedback fw-bold">
                                                    Tolong upload Gambar untuk sampul
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <h5>Masukkan Embed Code Youtube</h5>
                                            <textarea class="form-control" placeholder="Salin Embed Code disini" data-kt-autosize="true" name="embeded_string" id="embeded_string">{{ old('embeded_string') }}</textarea>
                                        </div>
                                        <div class="col-12">
                                            <h5 class="required">Konten</h5>
                                            <div>
                                                <div id="quill-halaman_tunggal" class="mb-3" style="height: 300px;"></div>
                                                <textarea rows="3" class="mb-3 d-none" name="content" id="quill-area-halaman_tunggal"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Card body-->
                            </div>
                        </div>
                        <!--end::Primary Create-->
                        <!--begin::Secondary Create-->
                        <div class="col-lg-4">
                            <div class="card card-flush mb-5">
                                <!--begin::Card body-->
                                <div class="card-body d-flex flex-column justify-content-end">
                                    <div class="row g-5">
                                        <div class="col-12">
                                            <h4 class="m-0">
                                                Atur SEO
                                            </h4>
                                        </div>
                                        <div class="separator"></div>
                                        {{-- <div class="col-12">
                                            <div class="form-check form-switch form-check-custom form-check-solid form-check-success justify-content-between align-items-center">
                                                <h4 class="m-0">
                                                    Sesuaikan Otomatis
                                                </h4>
                                                <input class="form-check-input" style="height: 24px; width: 42px;" type="checkbox" data-bs-target="#auto_generate" aria-expanded="false" aria-controls="published_at_div"
                                                    id="auto_generate" />
                                            </div>
                                        </div> --}}
                                        <div class="col-12">
                                            <h5 class="required">Deskripsi SEO</h5>
                                            <textarea class="form-control" name="desc_seo" id="desc_seo" placeholder="Deskripsi SEO" cols="30" rows="10" data-parsley-required>{{ old('desc_seo') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Card body-->
                            </div>
                        </div>
                        <!--end::Secondary Create-->
                    </div>
                </form>
            </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>
@endsection
@push('js')
    <script src="http://parsleyjs.org/dist/parsley.js"></script>

    <script>
        const fontSizeArr = ['8px', '9px', '10px', '12px', '14px', '16px', '18px', '20px', '24px', '32px'];

        const fontFamilyAttr = [
            '',
            'akhbar-mt',
            'amiri',
            'aref-ruqaa',
            'arial',
            'book-antiqua',
            // 'britanic-bold',
            'changa',
            'courier-prime',
            'georgia',
            'janna-lt-bold',
            'kfgqpc-uthman-taha-naskh-regular',
            'lateef',
            'mirza',
            'tahoma',
            'times-new-roman',
            'trebuchet',
            'verdana',
            // 'wedings'
        ];

        var Size = Quill.import('attributors/style/size');
        Size.whitelist = fontSizeArr;
        Quill.register(Size, true);

        var Font = Quill.import('attributors/class/font');
        Font.whitelist = fontFamilyAttr;
        Quill.register(fontFamilyAttr, true);

        const quil_toolbar = [
            [{
                'font': fontFamilyAttr
            }],
            [{
                'size': fontSizeArr
            }],
            // [{
            //     header: [1, 2, false]
            // }],
            ['bold', 'italic', 'underline', 'strike'],
            [{
                'list': 'ordered'
            }, {
                'list': 'bullet'
            }, {
                'indent': '-1'
            }, {
                'indent': '+1'
            }],
            [{
                'script': 'sub'
            }, {
                'script': 'super'
            }],
            [{
                'align': ''
            }, {
                'align': 'center'
            }, {
                'align': 'right'
            }, {
                'align': 'justify'
            }],
            ['link'],
            ['clean'],

        ];

        const quil_options = {
            modules: {
                toolbar: quil_toolbar,
            },
            theme: 'snow',
        };

        var quil_halaman_tunggal = document.getElementById('quill-halaman_tunggal');
        var quillEditor = document.getElementById('quill-area-halaman_tunggal');
        var editor = new Quill(quil_halaman_tunggal, quil_options);

        editor.on('text-change', function() {
            quillEditor.value = editor.root.innerHTML;
        });

        if ('{{ old('content') }}') {
            let realHTML = $('<textarea />').html('{{ old('content') }}').text();
            editor.clipboard.dangerouslyPasteHTML(realHTML);
        }

        document.getElementById('title').addEventListener('keyup', function(event) {
            let inputValue = event.target.value.toLowerCase();

            inputValue = inputValue.replace(/\s+/g, ' ');
            inputValue = inputValue.replace(/-+/g, '-');
            inputValue = inputValue.replace(/\s/g, '-');
            inputValue = inputValue.replace(/[^a-z0-9\s-]/g, '');

            document.getElementById('slug').value = inputValue;
        });

        var myDropzone = new Dropzone("#tambah_konten_form", {
            previewTemplate: $('#dzPreviewContainer').html(),
            previewsContainer: "#previews",
            autoProcessQueue: false,
            url: "#",
            acceptedFiles: 'image/*',
            maxFiles: 1,
            maxFilesize: 10,
            thumbnailWidth: 900,
            thumbnailHeight: 600,
            addRemoveLinks: true,
            init: function() {
                myDropzone = this;
                $('.dz-message').show();
                this.on('addedfile', function(file) {
                    $('.dz-message').hide();
                    $('#previews').removeClass('h-150px');
                    $('.dropzone-drag-area').removeClass('is-invalid').next('.invalid-feedback').hide();

                    document.getElementById('featured_image').file = file;
                });
                this.on('removedfile', function(file) {
                    $('.dz-message').show();
                    $('#previews').addClass('h-150px');
                });
            },
            success: function(file, response) {
                $('#tambah_konten_form').fadeOut(600);
                window.location.href = "{{ route('kelola-halaman-tunggal.index') }}?success=Data berhasil disimpan.";
            }
        });
    </script>

    <script>
        $('#draft_button, #publish_button').on('click', function(event) {
            event.preventDefault();
            var $this = $(this);

            let fileInput = document.createElement('input');
            fileInput.type = 'file';
            fileInput.name = 'featured_image';
            fileInput.classList.add('d-none');

            let storedFile = document.getElementById('featured_image').file;
            if (storedFile?.status === 'queued') {
                let dataTransfer = new DataTransfer();
                dataTransfer.items.add(storedFile);
                fileInput.files = dataTransfer.files;
            }
            let form = document.getElementById('tambah_konten_form'); // Replace 'myForm' with your form's ID
            form.appendChild(fileInput);

            $('#button_status').val($(this).attr('id'));
            form.submit();
        });
    </script>

    {{-- Edit Section --}}
    <script>
        const data = {!! json_encode(@$data, JSON_HEX_TAG) !!};
        if (data) {
            $('document').ready(function() {
                $("#status").val(data.status);
                $("#title").val(data.title);
                $("#slug").val(data.slug);
                $("#desc_seo").val(data.desc_seo);
                $("#embeded_string").val(data.embeded_string);

                document.getElementById("publish_button").innerHTML = "Simpan";

                // DropZone
                const featured_image_url = data.featured_image;
                const featured_image_name = featured_image_url.substring(featured_image_url.lastIndexOf('/') +
                    1);
                var mockFile = {
                    name: featured_image_name,
                    size: 12345,
                    type: 'image/jpeg',
                    status: Dropzone.ADDED,
                    url: featured_image_url,
                    accepted: true
                };

                myDropzone.emit('addedfile', mockFile);
                myDropzone.emit('thumbnail', mockFile, featured_image_url);
                myDropzone.emit('complete', mockFile);
                myDropzone.files.push(mockFile);
            });

            editor.clipboard.dangerouslyPasteHTML(data.content);
        }
    </script>
@endpush
