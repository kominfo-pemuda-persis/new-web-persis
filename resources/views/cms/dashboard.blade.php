    @extends('cms.layouts.master')
    @section('title', 'Dashboard')

    @push('css')
        <style>
            table.dataTable.no-footer {
                border-bottom: 0 !important;
            }

            #konten_terpopuler_table thead {
                display: none;
            }
        </style>
    @endpush

    @section('content')
        <div class="d-flex flex-column flex-column-fluid">
            <!--begin::Toolbar-->
            <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex text-gray-900 fw-bold flex-column justify-content-center my-0">
                            Dashboard
                        </h1>
                        <!--end::Title-->

                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('cms.index') }}" class="text-muted text-hover-primary">Home</a>
                            </li>
                            <li class="breadcrumb-item"><span class="bullet bg-gray-500 w-0.3125rem h-0.125rem"></span></li>
                            <li class="breadcrumb-item text-muted">Dashboards</li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page title-->
                </div>
                <!--end::Toolbar container-->
            </div>
            <!--end::Toolbar-->

            <!--begin::Content-->
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <!--begin::Content container-->
                <div id="kt_app_content_container" class="app-container container-fluid">
                    <div class="row g-5 gx-xl-10 mb-5 mb-xl-10">
                        <!--begin::Berita Hari Ini-->
                        <div class="col-lg col-sm-12">
                            <div class="card">
                                <div class="card-body d-flex flex-column">
                                    <div class="row align-items-center">
                                        <div class="col-2">
                                            <svg viewBox="0 0 51 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <rect x="0.5" y="0.5" width="50" height="50" rx="8" fill="#DFFFEA" />
                                                <rect x="13.5" y="14" width="24" height="24" rx="8" fill="#17C653" />
                                            </svg>
                                        </div>
                                        <div class="col">
                                            <p class="text-gray-600 fw-bold fs-4 m-0">Berita Hari Ini</p>
                                            <span class="fw-bold fs-2" data-kt-countup="true" data-kt-countup-value="{{ @$berita_hari_ini }}">0</span>
                                            <span class="fs-6">/{{ @$total_berita }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin::Berita Hari Ini-->
                        <!--end::Total Berita-->
                        <div class="col-lg col-sm-12">
                            <div class="card">
                                <div class="card-body d-flex flex-column">
                                    <div class="row align-items-center">
                                        <div class="col-2">
                                            <svg viewBox="0 0 51 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <rect x="0.166748" y="0.5" width="50" height="50" rx="8" fill="#F8F5FF" />
                                                <rect x="13.5002" y="14" width="24" height="24" rx="8" fill="#7239EA" />
                                            </svg>
                                        </div>
                                        <div class="col-10">
                                            <p class="text-gray-600 fw-bold fs-4 m-0">Total Berita</p>
                                            <span class="fw-bold fs-2" data-kt-countup="true" data-kt-countup-value="{{ @$total_berita }}">0</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Total Berita-->
                        <!--begin::Total Kontributor-->
                        <div class="col-lg col-sm-12">
                            <div class="card">
                                <div class="card-body d-flex flex-column">
                                    <div class="row align-items-center">
                                        <div class="col-2">
                                            <svg viewBox="0 0 51 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <rect x="0.833496" y="0.5" width="50" height="50" rx="8" fill="#FFF8DD" />
                                                <rect x="13.5" y="14" width="24" height="24" rx="8" fill="#F6C000" />
                                            </svg>
                                        </div>
                                        <div class="col">
                                            <p class="text-gray-600 fw-bold fs-4 m-0">Total Kontributor</p>
                                            <span class="fw-bold fs-2" data-kt-countup="true" data-kt-countup-value="{{ @$total_kontributor }}">0</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Total Kontributor-->
                    </div>
                    <div class="row g-5 gx-xl-10 mb-5 mb-xl-10">
                        <!--begin::Konten Terpopuler-->
                        <div class="col-lg-7">
                            <div class="card card-flush">
                                <div class="card-header pt-5">
                                    <div class="card-title d-flex align-items-center gap-2 gap-lg-3">
                                        <span class="fs-1 fw-bold text-gray-900 me-2 lh-1 ls-n2">Konten Terpopuler</span>
                                    </div>
                                    <div class="card-title d-flex align-items-center gap-2 gap-lg-3">
                                        <a href="{{ route('kelola-konten.index') }}">
                                            <span class="pt-1 fw-semibold fs-6" style="color: #065235">Lihat Semua Konten</span>
                                        </a>
                                    </div>
                                </div>

                                <!--begin::Card body-->
                                <div class="card-body d-flex flex-column">
                                    <div class="table-responsive">
                                        <table class="table table-row-bordered align-middle no-margin fs-6" id="konten_terpopuler_table">
                                            <thead class="bg-light-secondary">
                                                <tr class="fw-bold fs-5 text-uppercase">
                                                    <th class="py-5">Informasi</th>
                                                </tr>
                                            </thead>
                                            <tbody class="fw-semibold text-gray-600">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!--end::Card body-->
                            </div>
                        </div>
                        <!--end::Konten Terpopuler-->
                        <!--begin::Kontributor Terbanyak-->
                        <div class="col-lg-5">
                            <div class="card card-flush">
                                <div class="card-header pt-5">
                                    <div class="card-title d-flex align-items-center gap-2 gap-lg-3">
                                        <span class="fs-1 fw-bold text-gray-900 me-2 lh-1 ls-n2">Kontributor Terbanyak</span>
                                    </div>
                                    <div class="card-title d-flex align-items-center gap-2 gap-lg-3">
                                        <a href="#">
                                            <span class="pt-1 fw-semibold fs-6 hidden" style="color: #065235">
                                                Lihat Semua
                                            </span>
                                        </a>
                                    </div>
                                </div>

                                <!--begin::Card body-->
                                <div class="card-body d-flex flex-column justify-content-end">
                                    <div class="table-responsive">
                                        <table id="datatable_kontributor" class="table table-row-bordered no-footer gy-5 gs-7 fs-6">
                                            <thead class="bg-secondary">
                                                <tr class="fw-semibold fs-6">
                                                    <th>NAMA KONTRIBUTOR</th>
                                                    <th class="text-end">JUMLAH BERITA</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!--end::Card body-->
                            </div>
                        </div>
                        <!--end::Kontributor Terbanyak-->
                    </div>
                </div>
                <!--end::Content container-->
            </div>
            <!--end::Content-->
        </div>
    @endsection
    @push('js')
        <script>
            function handleImage(image) {
                image.onerror = "";
                image.src = '{{ asset('assets/assets-landing/images/placeholder.png') }}';
                return true;
            }

            $('document').ready(function() {
                $("#datatable_kontributor").DataTable({
                    scrollY: "31.25rem",
                    scrollCollapse: true,
                    paging: false,

                    processing: true,
                    bInfo: false,

                    searching: false,
                    ordering: false,

                    language: {
                        emptyTable: 'Tidak Ada Data Kontributor'
                    },
                    ajax: {
                        url: "{{ route('cms.index') }}",
                        data: function(d) {
                            d.type_form = "most_contributor";
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        },
                        error: function(req, err) {
                            language: {
                                emptyTable: 'Tidak Ada Data Kontributor'
                            };
                            console.log('Table Error : ' + err);
                        }
                    },
                    dataType: 'JSON',
                    columns: [{
                            name: 'full_name',
                            data: 'full_name',
                            render: function(data, type, row) {
                                // var nama_kontributor = "fullname_user";
                                var avatarUrl = row.photo ??
                                    "{{ Avatar::create('full_name')->toBase64() }}";
                                return '<img src="' + avatarUrl +
                                    '" class="rounded-3 me-6" width="25" height="25"/>' + row
                                    .full_name;
                            }
                        },
                        {
                            name: 'contents_count',
                            data: 'contents_count',
                            className: 'text-end',
                        },
                    ],
                });

                $("#konten_terpopuler_table").DataTable({
                    scrollX: false,
                    paging: false,

                    processing: true,
                    bInfo: false,

                    searching: false,
                    ordering: false,

                    language: {
                        emptyTable: 'Tidak Ada Data Kontributor'
                    },
                    ajax: {
                        url: "{{ route('cms.index') }}",
                        data: function(d) {
                            d.type_form = "konten_terpopuler";
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        },
                        error: function(req, err) {
                            language: {
                                emptyTable: 'Tidak Ada Data Kontributor'
                            };
                            console.log('Table Error : ' + err);
                        }
                    },
                    dataType: 'JSON',
                    columns: [{
                        name: 'full_name',
                        data: 'full_name',
                        render: function(data, type, row) {
                            var data;

                            function stripHtml(html) {
                                let doc = new DOMParser().parseFromString(html, 'text/html');
                                return doc.body.textContent || "";
                            }

                            function date_formater(date) {
                                var date = new Date(date);

                                const date_formatted = new Intl.DateTimeFormat('id', {
                                    day: '2-digit',
                                    month: 'short',
                                    year: 'numeric'
                                }).format(date);

                                const time_formatted = new Intl.DateTimeFormat('id', {
                                    hour12: false,
                                    hour: '2-digit',
                                    minute: '2-digit'
                                }).format(date);

                                return date_formatted + " | " + time_formatted;
                            }

                            var url =
                                `{{ route('portal.detail-content', ['category' => ':category', 'slug' => ':slug']) }}`;
                            url = url.replace(':category', row.content_category.category_name);
                            url = url.replace(':slug', row.slug);

                            data =
                                `<div class="d-flex align-items-center" role="button" onclick="location.href='` +
                                url +
                                `';"><div class="symbol symbol-75px me-3"><img class="object-fit-cover" src="` +
                                row.featured_image + `" onerror="handleImage(this)"/></div>`
                            data += `<div class="d-flex justify-content-start flex-column">`
                            data +=
                                `<div class="flex-fill"><p class="mb-2"><span class="badge badge-success rounded-pill">` +
                                row.content_category_sub.category_sub_name + `</span>&nbsp;<span>` +
                                date_formater(row.published_at) + `</span></p>` + `<h4>` +  stripHtml(row.title)+
                                `</h4></div></div>`
                            return data;
                        },
                    }],
                });
            });
        </script>
    @endpush
