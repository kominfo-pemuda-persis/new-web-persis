@extends('cms.layouts.master')
@section('title', 'Navigasi Footer')

@push('css')
    <style>
        .dragable {
            cursor: grab;
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-gray-900 fw-bold flex-column justify-content-center my-0">
                        Navigasi Footer
                    </h1>
                    <!--end::Title-->

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-konten.index') }}" class="text-muted text-hover-primary">
                                Kelola Portal
                            </a>
                        </li>
                        <li class="breadcrumb-item"><span class="bullet bg-gray-500 w-5px h-2px"></span></li>
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-konten.index') }}" class="text-muted text-hover-primary">
                                Navigasi Footer
                            </a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->

        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <!--Insert Content Here-->
                <div class="row g-5 g-xl-10 py-6">
                    <!--begin::Col-->
                    <div class="col">
                        <form action="{{ route('kelola-portal.navigasi-footer.update') }}" method="POST" id="kelola_portal_navigasi_footer">
                            @csrf
                            <div class="card">
                                <div class="card-header d-flex justify-content-between">
                                    <div class="card-title">
                                        <h3>Daftar Menu</h3>
                                    </div>
                                    <div class="card-toolbar">
                                        <button type="submit" id="btn_simpan" href="javascript:void(0);" class="btn btn-sm btn-bd-primary fw-bold me-4">
                                            Simpan Perubahan
                                        </button>
                                        <button type="button" id="tambah_menu" class="btn btn-sm btn-outline btn-outline-success">
                                            <i class="ki-duotone ki-plus-square fs-2">
                                                <span class="path1"></span>
                                                <span class="path2"></span>
                                                <span class="path3"></span>
                                            </i>
                                            Tambah Menu
                                        </button>
                                    </div>
                                </div>
                                <div class="card-body wadah-menu">
                                    @foreach ($menus as $menu)
                                        <div class="menu-item">
                                            <div class="row border border-1 rounded border-gray-300 d-flex align-items-center mt-1 bg-white">
                                                <div class="col-12 row d-flex justify-content-between p-0">
                                                    <div class="col d-flex align-items-center">
                                                        <div class="p-2 bg-light d-flex align-items-center rounded-start border-end border-1 border-gray-300 dragable draggable-menu">
                                                            <i class="ki-solid ki-burger-menu-3 fs-2"></i>
                                                        </div>
                                                        <h6 class="ms-5 mb-0">{{ $menu->menu_name }}</h6>
                                                    </div>
                                                    <div class="col-auto d-flex align-items-center text-end px-0">
                                                        {{-- <a href="javascript:void(0);" class="btn btn-sm py-1 px-0 mx-2 text-hover-info activeButton" data-id="{{ $menu->id }}" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-title="{{ $menu->status ? 'Nonaktifkan Menu' : 'Aktifkan Menu' }}">
                                                            @if ($menu->status)
                                                                <i class="ki-outline ki-eye-slash fs-2"></i>
                                                            @else
                                                                <i class="ki-outline ki-eye fs-2"></i>
                                                            @endif
                                                        </a> --}}
                                                        <a href="javascript:void(0);" class="btn btn-sm py-1 px-0 mx-2 text-hover-warning editButton" data-id="{{ $menu->id }}">
                                                            <i class="ki-outline ki-pencil fs-2"></i>
                                                        </a>
                                                        <a href="javascript:void(0);" class="btn btn-sm py-1 px-0 mx-2 text-hover-danger deleteButton" data-id="{{ $menu->id }}">
                                                            <i class="ki-outline ki-trash fs-2"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <input type="hidden" class="input-menu" name="data_menu[]" value="{{ $menu->id }}">
                                                <input type="hidden" class="input-order-menu" name="data_order_menu[{{ $menu->id }}]" value="{{ $loop->iteration }}">
                                            </div>
                                            <div class="wadah-submenu">
                                                @foreach (@$menu->children->sortBy('order') as $submenu)
                                                    <div class="row border border-1 rounded border-gray-300 d-flex align-items-center mt-1 ms-8 bg-white submenu-item">
                                                        <div class="col-12 row d-flex justify-content-between p-0">
                                                            <div class="col d-flex align-items-center">
                                                                <div class="p-2 bg-light d-flex align-items-center rounded-start border-end border-1 border-gray-300 dragable draggable-submenu">
                                                                    <i class="ki-solid ki-burger-menu-3 fs-2"></i>
                                                                </div>
                                                                <h6 class="ms-5 mb-0">{{ $submenu->menu_name }}</h6>
                                                            </div>
                                                            <div class="col-auto d-flex align-items-center text-end px-0">
                                                                {{-- <a href="javascript:void(0);" class="btn btn-sm py-1 px-0 mx-2 text-hover-info activeButton" data-id="{{ $submenu->id }}" data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-title="{{ $submenu->status ? 'Nonaktifkan Menu' : 'Aktifkan Menu' }}">
                                                                    @if ($submenu->status)
                                                                        <i class="ki-outline ki-eye-slash fs-2"></i>
                                                                    @else
                                                                        <i class="ki-outline ki-eye fs-2"></i>
                                                                    @endif
                                                                </a> --}}
                                                                <a href="javascript:void(0);" class="btn btn-sm py-1 px-0 mx-2 text-hover-warning editButton" data-id="{{ $submenu->id }}">
                                                                    <i class="ki-outline ki-pencil fs-2"></i>
                                                                </a>
                                                                <a href="javascript:void(0);" class="btn btn-sm py-1 px-0 mx-2 text-hover-danger deleteButton" data-id="{{ $submenu->id }}">
                                                                    <i class="ki-outline ki-trash fs-2"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" class="input-submenu" name="data_submenu[{{ $menu->id }}][]" value="{{ $submenu->id }}">
                                                        <input type="hidden" class="input-order-submenu" name="data_order_submenu[{{ $submenu->id }}]" value="{{ $loop->iteration }}">
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--end::Col-->
                </div>
            </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>

    <div class="modal fade" tabindex="-1" id="form-menu" style="display: none;">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="modal-title">
                        Tambah Menu
                    </h3>
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">
                    <form action="{{ route('kelola-portal.navigasi-footer.store') }}" method="POST" id="kelola_portal_navigasi_menu">
                        @csrf
                        <input type="hidden" class="form-control" name="id" id="input-id" />
                        <div class="row g-5">
                            <div class="col-12" id="status_select_div">
                                <label for="jenis_menu" class="form-label required">Jenis Menu</label>
                                <select class="form-select" data-control="select2" data-placeholder="Pilih Jenis Menu" data-hide-search="true" name="jenis_menu" id="input-jenis_menu" required>
                                    <option></option>
                                    <option value="Judul">Judul</option>
                                    <option value="Child Menu">Child Menu</option>
                                </select>
                            </div>
                            <div class="col-12 d-none item-jenis-menu-child" id="parent_menu_select_div">
                                <label for="parent_menu" class="form-label required">Pilih Parent Menu</label>
                                <select class="form-select" data-control="select2" data-allow-clear="false" data-placeholder="Pilih Parent Menu" name="parent_menu" id="input-parent_menu" required></select>
                            </div>
                            <div class="col-12 d-none item-jenis-menu-child" id="status_select_div">
                                <label for="sumber_data" class="form-label required">Sumber Data</label>
                                <select class="form-select" data-control="select2" data-placeholder="Pilih Sumber Data" data-hide-search="true" name="sumber_data" id="input-sumber_data" required>
                                    <option></option>
                                    <option value="Halaman Khusus">Halaman Khusus</option>
                                    <option value="Redirect Link">Redirect Link</option>
                                </select>
                            </div>
                            <div class="col-12 d-none" id="menu_name_select_div">
                                <label for="menu_name" class="form-label required">Masukkan Nama Menu</label>
                                <input type="text" class="form-control" placeholder="Masukkan Nama Menu" name="menu_name" id="input-menu_name" required>
                            </div>
                            <div class="col-12 d-none" id="item-sumber_data_type">
                                <label for="tautan" id="sumber_data_type" class="form-label required d-none">Masukkan Tautan</label>

                                <input type="text" class="form-control d-none" placeholder="https://" name="tautan" id="input-tautan" required>
                                <select class="form-select d-none" data-control="select2" data-allow-clear="false" data-placeholder="Pilih Halaman Khusus" name="halaman_khusus" id="input-halaman_khusus" required></select>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-bd-primary" form="kelola_kategori" id="simpan_button">Simpan</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('assets/assets-landing/libraries/jquery/jquery-ui.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#tambah_menu').on('click', function() {
                $('#modal-title').html('Tambah Menu');
                $('#simpan_button').html('Simpan');
                $('#form-menu').modal('show');
            });

            $('#simpan_button').on('click', function() {
                $('#kelola_portal_navigasi_menu').submit();
            });

            $('#form-menu').on('hidden.bs.modal', function() {
                $('#input-id').val('');
                $('#input-menu_name').val('');
                $('#input-jenis_menu').val('').trigger('change');
                $('#input-parent_menu').val('').trigger('change');
                $('#input-tautan').val('');
            });

            $('#input-jenis_menu').on('change', function() {
                if ($(this).val() == 'Child Menu') {
                    $('#menu_name_select_div').addClass('d-none');
                    $('.item-jenis-menu-child').removeClass('d-none');

                    $('#input-sumber_data').on('change', function() {
                        $('#item-sumber_data_type').show();
                        $('#item-sumber_data_type').removeClass('d-none');
                        $('#sumber_data_type').removeClass('d-none');
                        $('#menu_name_select_div').removeClass('d-none');

                        if ($(this).val() === 'Redirect Link') {
                            document.getElementById('sumber_data_type').innerHTML = 'Masukkan Tautan';
                            $('#input-tautan').removeClass('d-none');
                            $('#input-halaman_khusus').addClass('d-none');
                            if ($('#input-halaman_khusus').hasClass('select2-hidden-accessible')) {
                                $('#input-halaman_khusus').select2('destroy');
                            }
                        } else if ($(this).val() === 'Halaman Khusus') {
                            document.getElementById('sumber_data_type').innerHTML = 'Pilih Halaman Khusus';
                            $('#input-halaman_khusus').removeClass('d-none');
                            $('#input-tautan').addClass('d-none');

                            $('#input-halaman_khusus').select2({
                                placeholder: 'Pilih Halaman Khusus',
                                allowClear: false,
                                tags: false,
                                ajax: {
                                    url: "{{ route('kelola-portal.navigasi-footer.index') }}",
                                    dataType: 'json',
                                    delay: 250,
                                    data: function(params) {
                                        var query = {
                                            _token: '{{ csrf_token() }}',
                                            get_data: "halaman_tunggal",
                                            search: params.term,
                                            page: params.page || 1
                                        };

                                        return query;
                                    },
                                    cache: true
                                }
                            });
                            $('#input-halaman_khusus').on('change', function() {
                                $('#input-menu_name').val($(this).find(':selected').text());
                            });
                        } else {
                            $('#input-tautan').addClass('d-none');
                            $('#input-halaman_khusus').addClass('d-none');
                            $('#item-sumber_data_type').hide();
                            $('#item-sumber_data_type').addClass('d-none');
                        }
                    });

                    $("#input-parent_menu").select2({
                        dropdownParent: $('#form-menu'),
                        tags: true,
                        ajax: {
                            url: "{{ route('kelola-portal.navigasi-footer.index') }}",
                            dataType: 'json',
                            delay: 250,
                            minimumResultsForSearch: Infinity,
                            data: function(params) {
                                var query = {
                                    _token: '{{ csrf_token() }}',
                                    get_data: "menus",
                                    search: params.term,
                                    page: params.page || 1
                                };

                                return query;
                            },
                            processResults: function(data, params) {
                                params.page = params.page || 1;
                                return {
                                    results: data.results,
                                    pagination: {
                                        more: data.pagination.more
                                    }
                                };
                            },
                            templateResult: function(item) {
                                if (item.loading) {
                                    return item.text;
                                }
                                return item.text;
                            },
                            templateSelection: function(item) {
                                return item.text || item.id;
                            },
                            cache: true
                        }
                    });
                } else {
                    $('#item-sumber_data_type').hide();
                    $('#item-sumber_data_type').addClass('d-none');
                    $('#menu_name_select_div').removeClass('d-none');
                    $('.item-jenis-menu-child').addClass('d-none');

                    $('#input-parent_menu').val('').trigger('change');
                    $('#input-tautan').val('');
                    $('#input-sumber_data').val('').trigger('change');
                    $('#input-halaman_khusus').val('').trigger('change');
                    $('#input-halaman_khusus').addClass('d-none');
                    if ($('#input-halaman_khusus').hasClass('select2-hidden-accessible')) {
                        $('#input-halaman_khusus').select2('destroy');
                    }
                }
            });

            $('body').on('click', '.activeButton', function() {
                var id = $(this).data('id');
                event.preventDefault();

                Swal.fire({
                    title: `Status Data Menu`,
                    text: "Apakah Anda yakin ingin mengubah status data ini?",
                    icon: "warning",
                    showCancelButton: true,
                    showCloseButton: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        var url = `{{ route('kelola-portal.navigasi-menu.active', ':id') }}`.replace(':id', id);

                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: {
                                _token: '{{ csrf_token() }}',
                            },
                            error: (response) => {
                                let msg = response.responseJSON.message;
                                if (!msg) msg = 'Gagal mereset data. Terjadi kesalahan pada server';

                                Swal.fire({
                                    title: 'Perhatian',
                                    text: msg,
                                    icon: 'warning'
                                })
                            },
                            success: (response) => {
                                location.reload();
                                toastr.success("Data berhasil diupdate")
                            }
                        });
                    }
                });
            });

            $('body').on('click', '.deleteButton', function() {
                var id = $(this).data('id');
                event.preventDefault();

                Swal.fire({
                    title: `Hapus Data Menu`,
                    text: "Apakah Anda yakin ingin menghapus data ini?",
                    icon: "warning",
                    showCancelButton: true,
                    showCloseButton: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        var url = `{{ route('kelola-portal.navigasi-footer.destroy', ':id') }}`.replace(':id', id);

                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: {
                                _token: '{{ csrf_token() }}',
                                _method: 'DELETE'
                            },
                            error: (response) => {
                                let msg = response.responseJSON.message;
                                if (!msg) msg = 'Gagal mereset data. Terjadi kesalahan pada server';
                                Swal.fire({
                                    title: 'Perhatian',
                                    text: msg,
                                    icon: 'warning'
                                })
                            },
                            success: (response) => {
                                location.reload();
                                toastr.success("Data berhasil dihapus")
                            }
                        });
                    }
                });
            });

            $('body').on('click', '.editButton', function() {
                var id = $(this).data('id');

                $('#input-id').val(id);

                $.ajax({
                    url: "{{ route('kelola-portal.navigasi-footer.index') }}",
                    dataType: 'json',
                    data: {
                        id: id,
                        get_data: "menu",
                    },
                    success: (response) => {
                        const data = response.data;

                        if (data) {
                            $('#input-menu_name').val(data.menu_name);

                            if (data.parent) {
                                $('#input-jenis_menu').val('Child Menu').trigger('change');
                                $('#input-parent_menu').append(new Option(data.parent.menu_name, data.parent.id, true, true)).trigger('change');

                                $('#input-sumber_data').val(data.type).trigger('change');
                                $('#input-menu_name').val(data.menu_name);
                                $('#input-tautan').val(data.slug);
                                if (data.type === 'Halaman Khusus') {
                                    $('#input-halaman_khusus').append(new Option(data.halaman_tunggal.title, data.halaman_tunggal.id, true, true)).trigger('change');
                                }
                            } else {
                                $('#input-jenis_menu').val('Judul').trigger('change');
                            }
                        }
                    },
                    error: (response) => {
                        let msg = response.responseJSON.message;
                        if (!msg) msg = 'Gagal memuat data. Terjadi kesalahan pada server';

                        Swal.fire({
                            title: 'Perhatian',
                            text: msg,
                            icon: 'warning'
                        })
                    }
                });

                $('#modal-title').html('Ubah Menu');
                $('#simpan_button').html('Ubah');
                $('#form-menu').modal('show');
            });
        });

        $('.wadah-menu').sortable({
            connectWith: ".wadah-menu",
            handle: '.draggable-menu',
            tolerance: 'pointer',
            update: function(event, ui) {
                $('.wadah-menu').find(".menu-item").each(function(index) {
                    $(this).find('.input-order-menu').val(index + 1);
                });
            }
        });

        $('.wadah-submenu').sortable({
            // connectWith: ".wadah-submenu",
            handle: '.draggable-submenu',
            tolerance: 'pointer',
            update: function(event, ui) {
                $('.wadah-submenu').find(".submenu-item").each(function(index) {
                    $(this).find('.input-order-submenu').val(index + 1);
                });
            }
        });
    </script>
@endpush
