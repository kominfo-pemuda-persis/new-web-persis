@extends('cms.layouts.master')
@section('title', 'Konten Footer')

@push('css')
    <style>
        .dropzone {
            border: 0px solid !important;
            background-color: var(--bs-gray-100) !important;
        }

        .dz-preview {
            width: 100%;
            margin: 0 !important;
            height: 100%;
            padding: 15px;
            top: 0;
        }

        .dz-photo {
            height: 100%;
            width: 100%;
            overflow: hidden;
            border-radius: 12px;
            background: #eae7e2;
        }

        .dz-thumbnail {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .dz-image {
            width: 90px !important;
            height: 90px !important;
            border-radius: 6px !important;
        }

        .dz-remove {
            display: none !important;
        }

        .dz-delete {
            width: 24px;
            height: 24px;
            background: rgba(0, 0, 0, 0.57);
            position: absolute;
            opacity: 0;
            transition: all 0.2s ease;
            top: 30px;
            right: 30px;
            border-radius: 100px;
            z-index: 9999;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .dz-delete>svg {
            transform: scale(0.75);
            cursor: pointer;
        }

        .dz-preview:hover .dz-delete,
        .dz-preview:hover .dz-remove-image {
            opacity: 1;
        }

        .dz-message {
            height: 100%;
            margin: 0 !important;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .dropzone-drag-area {
            position: relative;
            padding: 0 !important;
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-gray-900 fw-bold flex-column justify-content-center my-0">
                        Konten Footer
                    </h1>
                    <!--end::Title-->

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-konten.index') }}" class="text-muted text-hover-primary">
                                Kelola Portal
                            </a>
                        </li>
                        <li class="breadcrumb-item"><span class="bullet bg-gray-500 w-5px h-2px"></span></li>
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-konten.index') }}" class="text-muted text-hover-primary">
                                Konten Footer
                            </a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->

                <!--begin::Actions-->
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <a id="btn_simpan" href="javascript:void(0);" class="btn btn-lg btn-bd-primary fw-bold">
                        Simpan Perubahan
                    </a>
                </div>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->

        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <!--Insert Content Here-->
                <div class="row g-5 g-xl-10 py-6">
                    <!--begin::Col-->
                    <div class="col">
                        <form id="form_konten_footer" action="{{ route('kelola-portal.konten-footer.store') }}" class="row" method="post" enctype="multipart/form-data" novalidate>
                            @csrf
                            <input type="hidden" id="id" name="id" value={{ @$data->id ?? null }}>

                            <div class="col-12 col-lg-6">
                                <div class="card">
                                    <div class="card-header d-flex justify-content-between">
                                        <div class="card-title">
                                            <h3>Branding Portal</h3>
                                        </div>
                                    </div>
                                    <div class="card-body row">
                                        <div class="col-12 mb-7">
                                            <label for="gambar" class="form-label fw-bolder">Gambar</label>
                                            <div class="dropzone mb-3" id="gambar_gambar">
                                                <div class="dropzone-drag-area form-control-transparent h-150px" id="previews_gambar">
                                                    <input type="file" id="gambar" name="web_logo_footer" class="d-none" />
                                                    <div class="fallback">
                                                        <input name="file" type="file" class="d-none" />
                                                    </div>
                                                    <div id="dzMessageGambar" class="dz-message">
                                                        <!--begin::Info-->
                                                        <div class="ms-4 text-center">
                                                            <svg width="39" height="34" viewBox="0 0 39 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M12.3999 0C11.7634 0 11.1529 0.252856 10.7028 0.702944C10.2528 1.15303 9.9999 1.76348 9.9999 2.4C9.9999 3.03652 10.2528 3.64697 10.7028 4.09706C11.1529 4.54714 11.7634 4.8 12.3999 4.8H26.7999C27.4364 4.8 28.0469 4.54714 28.497 4.09706C28.947 3.64697 29.1999 3.03652 29.1999 2.4C29.1999 1.76348 28.947 1.15303 28.497 0.702944C28.0469 0.252856 27.4364 0 26.7999 0H12.3999ZM5.1999 9.6C5.1999 8.96348 5.45276 8.35303 5.90285 7.90294C6.35293 7.45286 6.96338 7.2 7.5999 7.2H31.5999C32.2364 7.2 32.8469 7.45286 33.297 7.90294C33.747 8.35303 33.9999 8.96348 33.9999 9.6C33.9999 10.2365 33.747 10.847 33.297 11.2971C32.8469 11.7471 32.2364 12 31.5999 12H7.5999C6.96338 12 6.35293 11.7471 5.90285 11.2971C5.45276 10.847 5.1999 10.2365 5.1999 9.6ZM0.399902 19.2C0.399902 17.927 0.905615 16.7061 1.80579 15.8059C2.70596 14.9057 3.92686 14.4 5.1999 14.4H33.9999C35.2729 14.4 36.4938 14.9057 37.394 15.8059C38.2942 16.7061 38.7999 17.927 38.7999 19.2V28.8C38.7999 30.073 38.2942 31.2939 37.394 32.1941C36.4938 33.0943 35.2729 33.6 33.9999 33.6H5.1999C3.92686 33.6 2.70596 33.0943 1.80579 32.1941C0.905615 31.2939 0.399902 30.073 0.399902 28.8V19.2Z" fill="#ABC7C5"/>
                                                            </svg>
                                                            <p class="fs-7 fw-semibold text-gray-500 mb-0 mt-3">
                                                                Unggah gambar atau seret gambar ke sini
                                                            </p>
                                                        </div>
                                                        <!--end::Info-->
                                                    </div>
                                                    <div class="d-none" id="dzPreviewContainerGambar">
                                                        <div class="dz-preview dz-file-preview">
                                                            <div class="dz-photo">
                                                                <img class="dz-thumbnail" data-dz-thumbnail>
                                                            </div>
                                                            <button class="dz-delete border-0 p-0" type="button" data-dz-remove>
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" id="times">
                                                                    <path fill="#FFFFFF"
                                                                        d="M13.41,12l4.3-4.29a1,1,0,1,0-1.42-1.42L12,10.59,7.71,6.29A1,1,0,0,0,6.29,7.71L10.59,12l-4.3,4.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0L12,13.41l4.29,4.3a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42Z">
                                                                    </path>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <small class="form-text text-muted">Rekomendasi gambar berorientasi landscape. Serta ekstensi file yang diperbolehkan: svg, png, jpg, jpeg.</small>
                                        </div>
                                        <div class="col-12 mb-3">
                                            <label for="deskripsi" class="form-label fw-bolder">Deskripsi</label>
                                            <textarea class="form-control" name="web_footer_info" id="deskripsi" rows="5" cols="5" maxlength="120">{{ old('web_footer_info', @$data->web_footer_info) }}</textarea>
                                            <span class="fs-6 text-muted"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-lg-6">
                                <div class="card">
                                    <div class="card-header d-flex justify-content-between">
                                        <div class="card-title">
                                            <h3>Sosial Media</h3>
                                        </div>
                                    </div>
                                    <div class="card-body row">
                                        <div class="col-12 mb-7">
                                            <label for="input-url_socmed_fb" class="form-label fw-bolder">Tautan Facebook</label>
                                            <input type="text" class="form-control" value="{{ old('url_socmed_fb', @$data->url_socmed_fb) }}" placeholder="Tautan Facebook" name="url_socmed_fb" id="input-url_socmed_fb" required>
                                        </div>
                                        <div class="col-12 mb-7">
                                            <label for="url_socmed_ig" class="form-label fw-bolder">Tautan Instagram</label>
                                            <input type="text" class="form-control" value="{{ old('url_socmed_ig', @$data->url_socmed_ig) }}" placeholder="Tautan Instagram" name="url_socmed_ig" id="url_socmed_ig" required>
                                        </div>
                                        <div class="col-12 mb-7">
                                            <label for="url_socmed_x" class="form-label fw-bolder">Tautan X/Twitter</label>
                                            <input type="text" class="form-control" value="{{ old('url_socmed_x', @$data->url_socmed_x) }}" placeholder="Tautan X/Twitter" name="url_socmed_x" id="url_socmed_x" required>
                                        </div>
                                        <div class="col-12 mb-7">
                                            <label for="url_socmed_yt" class="form-label fw-bolder">Tautan Youtube</label>
                                            <input type="text" class="form-control" value="{{ old('url_socmed_yt', @$data->url_socmed_yt) }}" placeholder="Tautan Youtube" name="url_socmed_yt" id="url_socmed_yt" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--end::Col-->
                </div>
            </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>
@endsection

@push('js')
    <script>
        var myDropzone = new Dropzone("#form_konten_footer", {
            previewTemplate: $('#dzPreviewContainerGambar').html(),
            previewsContainer: "#previews_gambar",
            autoProcessQueue: false,
            url: "#",
            acceptedFiles: 'image/*',
            maxFiles: 1,
            maxFilesize: 10,
            thumbnailWidth: 900,
            thumbnailHeight: 600,
            addRemoveLinks: true,
            init: function() {
                myDropzone = this;

                const url_logo_footer = "{{ @$data->web_logo_footer ?? null }}";

                if (url_logo_footer) {
                    fetch(url_logo_footer)
                    .then(response => response.blob())
                    .then(blob => {
                        var mockFile = { name: "Logo Footer", size: blob.size };

                        myDropzone.emit("addedfile", mockFile);
                        myDropzone.emit("thumbnail", mockFile, url_logo_footer);

                        $('#dzMessageGambar').hide();
                        $('#previews_gambar').removeClass('h-150px');
                        $('#previews_gambar').removeClass('is-invalid').next('.invalid-feedback').hide();
                    })
                    .catch(error => console.error('Error fetching the image size:', error));
                }

                $('#dzMessageGambar').show();
                this.on('addedfile', function(file) {
                    $('#dzMessageGambar').hide();
                    $('#previews_gambar').removeClass('h-150px');
                    $('#previews_gambar').removeClass('is-invalid').next('.invalid-feedback').hide();

                    if (file instanceof File) {
                        var input = $('#gambar')[0];
                        var dataTransfer = new DataTransfer();
                        dataTransfer.items.add(file);
                        input.files = dataTransfer.files;
                    }
                });
                this.on('removedfile', function(file) {
                    $('#dzMessageGambar').show();
                    $('#previews_gambar').addClass('h-150px');

                    $('#gambar').val('');
                });
            },
            success: function(file, response) {
                $('#form_konten_footer').fadeOut(600);
                window.location.href = "{{ route('kelola-portal.pengaturan-portal.index') }}?success=Data berhasil disimpan.";
            }
        });

        $('#deskripsi').maxlength();

        $('#btn_simpan').on('click', function() {
            $('#form_konten_footer').submit();
        });
    </script>
@endpush
