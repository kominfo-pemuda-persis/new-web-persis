@extends('cms.layouts.master')
@section('title', 'Pengaturan Portal')

@push('css')
    <style>
        .dropzone {
            border: 0px solid !important;
            background-color: var(--bs-gray-100) !important;
        }

        .dz-preview {
            width: 100%;
            margin: 0 !important;
            height: 100%;
            padding: 15px;
            top: 0;
        }

        .dz-photo {
            height: 100%;
            width: 100%;
            overflow: hidden;
            border-radius: 12px;
            background: #eae7e2;
        }

        .dz-thumbnail {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .dz-image {
            width: 90px !important;
            height: 90px !important;
            border-radius: 6px !important;
        }

        .dz-remove {
            display: none !important;
        }

        .dz-delete {
            width: 24px;
            height: 24px;
            background: rgba(0, 0, 0, 0.57);
            position: absolute;
            opacity: 0;
            transition: all 0.2s ease;
            top: 30px;
            right: 30px;
            border-radius: 100px;
            z-index: 9999;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .dz-delete>svg {
            transform: scale(0.75);
            cursor: pointer;
        }

        .dz-preview:hover .dz-delete,
        .dz-preview:hover .dz-remove-image {
            opacity: 1;
        }

        .dz-message {
            height: 100%;
            margin: 0 !important;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .dropzone-drag-area {
            position: relative;
            padding: 0 !important;
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-column-fluid bg-light">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-gray-900 fw-bold flex-column justify-content-center my-0">
                        Pengaturan Portal
                    </h1>
                    <!--end::Title-->

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-konten.index') }}" class="text-muted text-hover-primary">
                                Kelola Portal
                            </a>
                        </li>
                        <li class="breadcrumb-item"><span class="bullet bg-gray-500 w-5px h-2px"></span></li>
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-konten.index') }}" class="text-muted text-hover-primary">
                                Pengaturan Portal
                            </a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->

                <!--begin::Actions-->
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <a id="btn_simpan" href="#" class="btn btn-lg btn-bd-primary fw-bold">
                        Perbarui Data
                    </a>
                </div>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->

        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <!--Insert Content Here-->
                <div class="row g-5 g-xl-10 py-6">
                    <!--begin::Col-->
                    <div class="col">
                        <div class="card">
                            <div class="card-body d-flex flex-column">
                                <form id="form_pengaturan_portal" action="{{ route('kelola-portal.pengaturan-portal.store') }}" class="row" method="post" enctype="multipart/form-data" novalidate>
                                    @csrf
                                    <input type="hidden" id="id" name="id" value={{ @$data->id ?? null }}>

                                    <div class="col-12 mb-7">
                                        <label for="web_title" class="form-label fw-bolder">Nama Website</label>
                                        <input type="text" class="form-control" value="{{ old('web_title', @$data->web_title) }}" placeholder="Nama Website" name="web_title" id="input-web_title" required>
                                    </div>

                                    <div class="col-12 mb-7">
                                        <label for="web_info_top" class="form-label fw-bolder">Deskripsi</label>
                                        <textarea class="form-control" placeholder="Deskripsi" name="web_info_top" id="input-web_info_top" rows="5">{{ old('web_info_top', @$data->web_info_top) }}</textarea>
                                    </div>

                                    <input type="file" id="logo_navbar" name="web_logo_top" class="d-none" />
                                    <input type="file" id="icon_website" name="web_icon" class="d-none" />
                                </form>

                                <div class="row">
                                    <form id="form_logo_navbar" action="{{ route('kelola-portal.pengaturan-portal.store') }}" class="col-lg-6 col-12" method="post" enctype="multipart/form-data" novalidate>
                                        <div>
                                            <label for="logo_navbar" class="form-label fw-bolder">Logo Navbar</label>
                                            <div class="dropzone mb-3" id="gambar_logo_navbar">
                                                <div class="dropzone-drag-area form-control-transparent h-150px" id="previews_logo_navbar">
                                                    <div class="fallback">
                                                        <input name="file" type="file" class="d-none" />
                                                    </div>
                                                    <div id="dzMessageLogoNavbar" class="dz-message">
                                                        <!--begin::Info-->
                                                        <div class="ms-4 text-center">
                                                            <svg width="39" height="34" viewBox="0 0 39 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M12.3999 0C11.7634 0 11.1529 0.252856 10.7028 0.702944C10.2528 1.15303 9.9999 1.76348 9.9999 2.4C9.9999 3.03652 10.2528 3.64697 10.7028 4.09706C11.1529 4.54714 11.7634 4.8 12.3999 4.8H26.7999C27.4364 4.8 28.0469 4.54714 28.497 4.09706C28.947 3.64697 29.1999 3.03652 29.1999 2.4C29.1999 1.76348 28.947 1.15303 28.497 0.702944C28.0469 0.252856 27.4364 0 26.7999 0H12.3999ZM5.1999 9.6C5.1999 8.96348 5.45276 8.35303 5.90285 7.90294C6.35293 7.45286 6.96338 7.2 7.5999 7.2H31.5999C32.2364 7.2 32.8469 7.45286 33.297 7.90294C33.747 8.35303 33.9999 8.96348 33.9999 9.6C33.9999 10.2365 33.747 10.847 33.297 11.2971C32.8469 11.7471 32.2364 12 31.5999 12H7.5999C6.96338 12 6.35293 11.7471 5.90285 11.2971C5.45276 10.847 5.1999 10.2365 5.1999 9.6ZM0.399902 19.2C0.399902 17.927 0.905615 16.7061 1.80579 15.8059C2.70596 14.9057 3.92686 14.4 5.1999 14.4H33.9999C35.2729 14.4 36.4938 14.9057 37.394 15.8059C38.2942 16.7061 38.7999 17.927 38.7999 19.2V28.8C38.7999 30.073 38.2942 31.2939 37.394 32.1941C36.4938 33.0943 35.2729 33.6 33.9999 33.6H5.1999C3.92686 33.6 2.70596 33.0943 1.80579 32.1941C0.905615 31.2939 0.399902 30.073 0.399902 28.8V19.2Z" fill="#ABC7C5"/>
                                                            </svg>
                                                            <p class="fs-7 fw-semibold text-gray-500 mb-0 mt-3">
                                                                Unggah gambar atau seret gambar ke sini
                                                            </p>
                                                        </div>
                                                        <!--end::Info-->
                                                    </div>
                                                    <div class="d-none" id="dzPreviewContainerLogoNavbar">
                                                        <div class="dz-preview dz-file-preview">
                                                            <div class="dz-photo">
                                                                <img class="dz-thumbnail" data-dz-thumbnail>
                                                            </div>
                                                            <button class="dz-delete border-0 p-0" type="button" data-dz-remove>
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" id="times">
                                                                    <path fill="#FFFFFF"
                                                                        d="M13.41,12l4.3-4.29a1,1,0,1,0-1.42-1.42L12,10.59,7.71,6.29A1,1,0,0,0,6.29,7.71L10.59,12l-4.3,4.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0L12,13.41l4.29,4.3a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42Z">
                                                                    </path>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <small class="form-text text-muted">Rekomendasi gambar berorientasi landscape. Serta ekstensi file yang diperbolehkan: svg, png, jpg, jpeg.</small>
                                        </div>
                                    </form>

                                    <form id="form_icon_website" action="{{ route('kelola-portal.pengaturan-portal.store') }}" class="col-lg-6 col-12" method="post" enctype="multipart/form-data" novalidate>
                                        <div>
                                            <label for="icon_website" class="form-label fw-bolder">Icon Website</label>
                                            <div class="dropzone mb-3" id="gambar_icon_website">
                                                <div class="dropzone-drag-area form-control-transparent h-150px" id="previews_icon_website">
                                                    <div class="fallback">
                                                        <input name="file" type="file" class="d-none" />
                                                    </div>
                                                    <div id="dzMessageIconWebsite" class="dz-message">
                                                        <!--begin::Info-->
                                                        <div class="ms-4 text-center">
                                                            <svg width="39" height="34" viewBox="0 0 39 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M12.3999 0C11.7634 0 11.1529 0.252856 10.7028 0.702944C10.2528 1.15303 9.9999 1.76348 9.9999 2.4C9.9999 3.03652 10.2528 3.64697 10.7028 4.09706C11.1529 4.54714 11.7634 4.8 12.3999 4.8H26.7999C27.4364 4.8 28.0469 4.54714 28.497 4.09706C28.947 3.64697 29.1999 3.03652 29.1999 2.4C29.1999 1.76348 28.947 1.15303 28.497 0.702944C28.0469 0.252856 27.4364 0 26.7999 0H12.3999ZM5.1999 9.6C5.1999 8.96348 5.45276 8.35303 5.90285 7.90294C6.35293 7.45286 6.96338 7.2 7.5999 7.2H31.5999C32.2364 7.2 32.8469 7.45286 33.297 7.90294C33.747 8.35303 33.9999 8.96348 33.9999 9.6C33.9999 10.2365 33.747 10.847 33.297 11.2971C32.8469 11.7471 32.2364 12 31.5999 12H7.5999C6.96338 12 6.35293 11.7471 5.90285 11.2971C5.45276 10.847 5.1999 10.2365 5.1999 9.6ZM0.399902 19.2C0.399902 17.927 0.905615 16.7061 1.80579 15.8059C2.70596 14.9057 3.92686 14.4 5.1999 14.4H33.9999C35.2729 14.4 36.4938 14.9057 37.394 15.8059C38.2942 16.7061 38.7999 17.927 38.7999 19.2V28.8C38.7999 30.073 38.2942 31.2939 37.394 32.1941C36.4938 33.0943 35.2729 33.6 33.9999 33.6H5.1999C3.92686 33.6 2.70596 33.0943 1.80579 32.1941C0.905615 31.2939 0.399902 30.073 0.399902 28.8V19.2Z" fill="#ABC7C5"/>
                                                            </svg>
                                                            <p class="fs-7 fw-semibold text-gray-500 mb-0 mt-3">
                                                                Unggah gambar atau seret gambar ke sini
                                                            </p>
                                                        </div>
                                                        <!--end::Info-->
                                                    </div>
                                                    <div class="d-none" id="dzPreviewContainerIconWebsite">
                                                        <div class="dz-preview dz-file-preview">
                                                            <div class="dz-photo">
                                                                <img class="dz-thumbnail" data-dz-thumbnail>
                                                            </div>
                                                            <button class="dz-delete border-0 p-0" type="button" data-dz-remove>
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" id="times">
                                                                    <path fill="#FFFFFF"
                                                                        d="M13.41,12l4.3-4.29a1,1,0,1,0-1.42-1.42L12,10.59,7.71,6.29A1,1,0,0,0,6.29,7.71L10.59,12l-4.3,4.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0L12,13.41l4.29,4.3a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42Z">
                                                                    </path>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <small class="form-text text-muted">Rekomendasi logo berskala 1:1, dengan ukuran 32x32piksel sampai 64x64piksel. Ekstensi file yang diperbolehkan: ico dan png.</small>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Col-->
                </div>
            </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>
@endsection

@push('js')
    <script>
        var myDropzone = new Dropzone("#form_logo_navbar", {
            previewTemplate: $('#dzPreviewContainerLogoNavbar').html(),
            previewsContainer: "#previews_logo_navbar",
            autoProcessQueue: false,
            url: "#",
            acceptedFiles: 'image/*',
            maxFiles: 1,
            maxFilesize: 10,
            thumbnailWidth: 900,
            thumbnailHeight: 600,
            addRemoveLinks: true,
            init: function() {
                myDropzone = this;

                const url_logo_navbar = "{{ @$data->web_logo_top ?? null }}";

                if (url_logo_navbar) {
                    fetch(url_logo_navbar)
                    .then(response => response.blob())
                    .then(blob => {
                        var mockFile = { name: "Logo Navbar", size: blob.size };

                        myDropzone.emit("addedfile", mockFile);
                        myDropzone.emit("thumbnail", mockFile, url_logo_navbar);

                        $('#dzMessageLogoNavbar').hide();
                        $('#previews_logo_navbar').removeClass('h-150px');
                        $('#previews_logo_navbar').removeClass('is-invalid').next('.invalid-feedback').hide();
                    })
                    .catch(error => console.error('Error fetching the image size:', error));
                }

                $('#dzMessageLogoNavbar').show();
                this.on('addedfile', function(file) {
                    $('#dzMessageLogoNavbar').hide();
                    $('#previews_logo_navbar').removeClass('h-150px');
                    $('#previews_logo_navbar').removeClass('is-invalid').next('.invalid-feedback').hide();

                    if (file instanceof File) {
                        var input = $('#logo_navbar')[0];
                        var dataTransfer = new DataTransfer();
                        dataTransfer.items.add(file);
                        input.files = dataTransfer.files;
                    }
                });
                this.on('removedfile', function(file) {
                    $('#dzMessageLogoNavbar').show();
                    $('#previews_logo_navbar').addClass('h-150px');

                    $('#logo_navbar').val('');
                });
            },
            success: function(file, response) {
                $('#form_logo_navbar').fadeOut(600);
                window.location.href = "{{ route('kelola-portal.pengaturan-portal.index') }}?success=Data berhasil disimpan.";
            }
        });

        var myDropzone2 = new Dropzone("#form_icon_website", {
            previewTemplate: $('#dzPreviewContainerIconWebsite').html(),
            previewsContainer: "#previews_icon_website",
            autoProcessQueue: false,
            url: "#",
            acceptedFiles: 'image/*',
            maxFiles: 1,
            maxFilesize: 10,
            thumbnailWidth: 900,
            thumbnailHeight: 600,
            addRemoveLinks: true,
            init: function() {
                myDropzone2 = this;

                const url_icon_website = "{{ @$data->web_icon ?? null }}";

                if (url_icon_website) {
                    fetch(url_icon_website)
                    .then(response => response.blob())
                    .then(blob => {
                        var mockFile = { name: "Icon Navbar", size: blob.size };

                        myDropzone2.emit("addedfile", mockFile);
                        myDropzone2.emit("thumbnail", mockFile, url_icon_website);

                        $('#dzMessageIconWebsite').hide();
                        $('#previews_icon_website').removeClass('h-150px');
                        $('#previews_icon_website').removeClass('is-invalid').next('.invalid-feedback').hide();
                    })
                    .catch(error => console.error('Error fetching the image size:', error));
                }

                $('#dzMessageIconWebsite').show();
                this.on('addedfile', function(file) {
                    $('#dzMessageIconWebsite').hide();
                    $('#previews_icon_website').removeClass('h-150px');
                    $('#previews_icon_website').removeClass('is-invalid').next('.invalid-feedback').hide();

                    if (file instanceof File) {
                        var input = $('#icon_website')[0];
                        var dataTransfer = new DataTransfer();
                        dataTransfer.items.add(file);
                        input.files = dataTransfer.files;
                    }
                });
                this.on('removedfile', function(file) {
                    $('#dzMessageIconWebsite').show();
                    $('#previews_icon_website').addClass('h-150px');

                    $('#icon_website').val('');
                });
            },
            success: function(file, response) {
                $('#form_icon_website').fadeOut(600);
                window.location.href = "{{ route('kelola-portal.pengaturan-portal.index') }}?success=Data berhasil disimpan.";
            }
        });

        $('#btn_simpan').on('click', function(e) {
            e.preventDefault();
            $('#form_pengaturan_portal').submit();
        });
    </script>
@endpush
