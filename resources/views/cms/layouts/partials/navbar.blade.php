<div class="app-navbar flex-shrink-0 ms-auto">
    <div class="app-navbar-item ms-1 ms-md-4" id="kt_header_user_menu_toggle">
        <div class="cursor-pointer symbol symbol-35px" data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
            <img src="{{ Auth::user()->photo ?? Avatar::create(@Auth::user()->full_name)->toBase64() }}" class="rounded-3" alt="user" />
            <span class="fs-6 ms-3">
                <span class="fw-semibold">
                    {{ Str::title(@Auth::user()->full_name) }}
                </span>
            </span>
        </div>
        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px" data-kt-menu="true">
            <div class="menu-item px-3">
                <div class="menu-content d-flex align-items-center px-3">
                    <div class="symbol symbol-50px me-5">
                        <img alt="Logo" src="{{ @Auth::user()->photo ?? Avatar::create(@Auth::user()->full_name)->toBase64() }}" />
                    </div>
                    <div class="d-flex flex-column">
                        <div class="fw-bold d-flex align-items-center fs-5">
                            {{ Str::title(@Auth::user()->full_name) }}
                        </div>
                        <a href="#" class="fw-semibold text-muted text-hover-primary fs-7">
                            {{ @Auth::user()->role_name->role_name }}
                        </a>
                    </div>
                </div>
            </div>

            <div class="separator my-2"></div>

            <div class="menu-item px-5">
                <a href="{{ route('user-management.atur-profile-password', ['value' => 'profile']) }}" class="menu-link px-5">
                    Atur Profile
                </a>
                <a href="{{ route('user-management.atur-profile-password', ['value' => 'password']) }}" class="menu-link px-5">
                    Ubah Kata Sandi
                </a>
            </div>

            <div class="separator my-2"></div>

            <div class="menu-item px-5">
                <a href="{{ route('logout') }}" class="menu-link px-5 text-danger">Keluar Akun</a>
            </div>
        </div>
    </div>
</div>
