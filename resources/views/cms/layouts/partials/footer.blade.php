<div id="kt_app_footer" class="app-footer">
    <div class="app-container container-fluid d-flex flex-column flex-md-row flex-center flex-md-stack py-3">
        <!--begin::Copyright-->
        <div class="text-gray-900 order-2 order-md-1">
            <span class="text-muted fw-semibold me-1">&copy; 2024</span>
            <a href="{{ url('') }}" target="_blank" class="text-gray-800 text-hover-primary">
                Pimpinan Persatuan Islam
            </a>
        </div>

        <div class="text-gray-900 order-2 order-md-1">
            <a href="https://someah.id" target="_blank" class="text-gray-800 text-hover-primary">
                <span class="text-muted fw-semibold me-1">Versi Aplikasi {{ env('BUILD_VERSION') }}</span> | Develop By.
                Someah Kreatif Nusantara
            </a>
        </div>
        <!--end::Copyright-->
    </div>
</div>
