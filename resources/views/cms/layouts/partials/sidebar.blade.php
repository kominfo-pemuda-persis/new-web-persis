<div class="app-sidebar-menu overflow-hidden flex-column-fluid">
    <!--begin::Menu wrapper-->
    <div id="kt_app_sidebar_menu_wrapper" class="app-sidebar-wrapper">
        <!--begin::Scroll wrapper-->
        <div id="kt_app_sidebar_menu_scroll" class="scroll-y my-5 mx-3" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_app_sidebar_logo, #kt_app_sidebar_footer"
            data-kt-scroll-wrappers="#kt_app_sidebar_menu" data-kt-scroll-offset="5px" data-kt-scroll-save-state="true">
            <!--begin::Menu-->
            <div class="menu menu-column menu-rounded menu-sub-indention fw-semibold fs-6" id="#kt_app_sidebar_menu" data-kt-menu="true" data-kt-menu-expand="false">
                <!--begin:Dashboard item-->
                <div class="menu-item">
                    <!--begin:Dashboard link-->
                    <a class="menu-link {{ request()->routeIs('cms.index') ? 'active bg-success bg-opacity-25' : '' }}" href="{{ route('cms.index') }}">
                        <span class="menu-icon">
                            <i class="ki-duotone ki-element-11 fs-2">
                                <span class="path1"></span>
                                <span class="path2"></span>
                                <span class="path3"></span>
                                <span class="path4"></span>
                            </i>
                        </span>
                        <span class="menu-title">Dasbor</span>
                    </a>
                    <!--end:Dashboard link-->
                </div>
                <!--end:Dashboard item-->

                <!--begin:Kunjungi Situs-->
                <div class="menu-item">
                    <a class="menu-link" href="{{ url('/')}}" target="_blank">
                        <span class="menu-icon">
                            <i class="ki-solid ki-eye fs-2">
                            </i>
                        </span>
                        <span class="menu-title">Kunjungi Situs</span>
                    </a>
                </div>
                <!--end:Kunjungi Situs-->

                <!--begin:Konten Section-->
                <div class="menu-item pt-5">
                    <div class="menu-content">
                        <span class="menu-heading fw-bold text-uppercase fs-7">Konten</span>
                    </div>
                </div>

                <!--begin:Kelola Kategori item-->
                @if (Auth::user()->role_name->role_name === 'Admin Pusat' || Auth::user()->role_name->role_name === 'Admin Otonom')
                    <div class="menu-item">
                        <a class="menu-link {{ request()->routeIs('kelola-kategori.*') ? 'active bg-success bg-opacity-25' : '' }}" href="{{ route('kelola-kategori.index') }}">
                            <span class="menu-icon">
                                <i class="ki-duotone ki-note fs-2">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                </i>
                            </span>
                            <span class="menu-title">Kelola Kategori</span>
                        </a>
                    </div>
                @endif
                <!--end:Kelola Kategori item-->

                <!--begin:Kelola Konten item-->
                @if (Auth::user()->role_name->role_name === 'Admin Pusat' || Auth::user()->role_name->role_name === 'Admin Otonom' || Auth::user()->role_name->role_name === 'Editor')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ request()->routeIs('kelola-konten.*') ? 'show' : '' }}">
                        <span class="menu-link">
                            <span class="menu-icon">
                                <i class="ki-duotone ki-note-2 fs-2">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                    <span class="path3"></span>
                                    <span class="path4"></span>
                                </i>
                            </span>
                            <span class="menu-title">Kelola Konten</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <!--begin:Menu sub-->
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Buat Konten item-->
                            <div class="menu-item">
                                <a class="menu-link {{ request()->routeIs('kelola-konten.create') ? 'active bg-success bg-opacity-25' : '' }}" href="{{ route('kelola-konten.create') }}">
                                    <span class="menu-bullet"><span class="bullet bullet-dot"></span></span>
                                    <span class="menu-title">Buat Konten</span>
                                </a>
                            </div>
                            <!--end:Buat Konten item-->
                            <!--begin:Daftar Konten item-->
                            <div class="menu-item">
                                <a class="menu-link {{ request()->routeIs('kelola-konten.index') ? 'active bg-success bg-opacity-25' : '' }}" href="{{ route('kelola-konten.index') }}">
                                    <span class="menu-bullet"><span class="bullet bullet-dot"></span></span>
                                    <span class="menu-title">Daftar Konten</span>
                                </a>
                            </div>
                            <!--end:Daftar Konten item-->
                        </div>
                        <!--end:Menu sub-->
                    </div>
                @endif
                <!--end:Kelola Konten item-->

                <!--begin:Kelola Iklan item-->
                @if (Auth::user()->role_name->role_name === 'Admin Pusat' || Auth::user()->role_name->role_name === 'Admin Otonom')
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ request()->routeIs('kelola-iklan.*') ? 'show' : '' }}">
                        <span class="menu-link">
                            <span class="menu-icon">
                                <i class="ki-duotone ki-picture fs-2">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                </i>
                            </span>
                            <span class="menu-title">Kelola Iklan</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <!--begin:Menu sub-->
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Buat Konten item-->
                            <div class="menu-item">
                                <a class="menu-link {{ request()->routeIs('kelola-iklan.index') ? 'active bg-success bg-opacity-25' : '' }}" href="{{ route('kelola-iklan.index') }}">
                                    <span class="menu-bullet"><span class="bullet bullet-dot"></span></span>
                                    <span class="menu-title">Daftar Iklan</span>
                                </a>
                            </div>
                            <!--end:Buat Konten item-->
                            <!--begin:Daftar Konten item-->
                            <div class="menu-item">
                                <a class="menu-link {{ request()->routeIs('kelola-iklan.set_ads') ? 'active bg-success bg-opacity-25' : '' }}" href="{{ route('kelola-iklan.set_ads') }}">
                                    <span class="menu-bullet"><span class="bullet bullet-dot"></span></span>
                                    <span class="menu-title">Atur Iklan</span>
                                </a>
                            </div>
                            <!--end:Daftar Konten item-->
                        </div>
                        <!--end:Menu sub-->
                    </div>
                @endif
                <!--end:Kelola Iklan item-->

                <!--begin:Kelola Halaman Tunggal item-->
                @if (Auth::user()->role_name->role_name === 'Admin Pusat' || Auth::user()->role_name->role_name === 'Admin Otonom')
                    <div class="menu-item">
                        <a class="menu-link" href="{{ route('kelola-halaman-tunggal.index') }}">
                            <span class="menu-icon">
                                <i class="ki-duotone ki-note fs-2">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                </i>
                            </span>
                            <span class="menu-title">Kelola Halaman Tunggal</span>
                        </a>
                    </div>
                @endif
                @if (Auth::user()->role_name->role_name === 'Admin Pusat' || Auth::user()->role_name->role_name === 'Admin Otonom')
                    <div class="menu-item">
                        <a class="menu-link" href="{{ route('kelola-istifta.index') }}">
                            <span class="menu-icon">
                                <i class="ki-duotone ki-note fs-2">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                </i>
                            </span>
                            <span class="menu-title">Istifta</span>
                        </a>
                    </div>
                @endif
                <!--end:Kelola Halaman Tunggal item-->
                <!--end:Konten Section-->

                <!--begin:Pengaturan Admin Section-->
                @if (Auth::user()->role_name->role_name === 'Admin Pusat' || Auth::user()->role_name->role_name === 'Admin Otonom')
                    <div class="menu-item pt-5">
                        <div class="menu-content">
                            <span class="menu-heading fw-bold text-uppercase fs-7">Pengaturan Admin</span>
                        </div>
                    </div>
                    <!--begin:Kelola Akun item-->
                    <div class="menu-item">
                        <a class="menu-link {{ request()->routeIs('user-management.index') ? 'active bg-success bg-opacity-25' : '' }}" href="{{ route('user-management.index') }}">
                            <span class="menu-icon">
                                <i class="ki-duotone ki-profile-circle fs-2">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                    <span class="path3"></span>
                                </i>
                            </span>
                            <span class="menu-title">Kelola Akun</span>
                        </a>
                    </div>
                    <!--end:Kelola Akun item-->

                    <!--begin:Kelola Halaman Otonom item-->
                    {{-- <div class="menu-item">
                        <a class="menu-link" href="pages/user-profile/overview.html">
                            <span class="menu-icon">
                                <i class="ki-duotone ki-profile-circle fs-2">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                    <span class="path3"></span>
                                </i>
                            </span>
                            <span class="menu-title">Kelola Halaman Otonom</span>
                        </a>
                    </div> --}}
                    <!--end:Kelola Halaman Otonom item-->

                    <!--begin:Kelola Kelola Portal item-->
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ request()->routeIs('kelola-portal.*') ? 'show' : '' }}">
                        <span class="menu-link">
                            <span class="menu-icon">
                                <i class="ki-duotone ki-setting-2 fs-2">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                </i>
                            </span>
                            <span class="menu-title">Kelola Portal</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <!--begin:Menu sub-->
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Pengaturan Portal item-->
                            <div class="menu-item">
                                <a class="menu-link {{ request()->routeIs('kelola-portal.pengaturan-portal.*') ? 'active bg-success bg-opacity-25' : '' }}" href="{{ route('kelola-portal.pengaturan-portal.index') }}">
                                    <span class="menu-bullet"><span class="bullet bullet-dot"></span></span>
                                    <span class="menu-title">Pengaturan Portal</span>
                                </a>
                            </div>
                            <!--end:Pengaturan Portal item-->
                            <!--begin:Navigasi Menu item-->
                            <div class="menu-item">
                                <a class="menu-link {{ request()->routeIs('kelola-portal.navigasi-menu.*') ? 'active bg-success bg-opacity-25' : '' }}" href="{{ route('kelola-portal.navigasi-menu.index') }}">
                                    <span class="menu-bullet"><span class="bullet bullet-dot"></span></span>
                                    <span class="menu-title">Navigasi Menu</span>
                                </a>
                            </div>
                            <!--end:Navigasi Menu item-->
                            <!--begin:Konten Footer item-->
                            <div class="menu-item">
                                <a class="menu-link {{ request()->routeIs('kelola-portal.konten-footer.*') ? 'active bg-success bg-opacity-25' : '' }}" href="{{ route('kelola-portal.konten-footer.index') }}">
                                    <span class="menu-bullet"><span class="bullet bullet-dot"></span></span>
                                    <span class="menu-title">Konten Footer</span>
                                </a>
                            </div>
                            <!--end:Konten Footer item-->
                            <!--begin:Navigasi Footer item-->
                            <div class="menu-item">
                                <a class="menu-link {{ request()->routeIs('kelola-portal.navigasi-footer.*') ? 'active bg-success bg-opacity-25' : '' }}" href="{{ route('kelola-portal.navigasi-footer.index') }}">
                                    <span class="menu-bullet"><span class="bullet bullet-dot"></span></span>
                                    <span class="menu-title">Navigasi Footer</span>
                                </a>
                            </div>
                            <!--end:Navigasi Footer item-->
                        </div>
                        <!--end:Menu sub-->
                    </div>
                    <!--end:Kelola Kelola Portal item-->
                @endif
                <!--end:Pengaturan Admin Section-->
            </div>
            <!--end::Menu-->
        </div>
        <!--end::Scroll wrapper-->
    </div>
    <!--end::Menu wrapper-->
</div>
