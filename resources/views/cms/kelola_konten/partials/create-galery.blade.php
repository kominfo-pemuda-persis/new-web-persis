@push('css')
@endpush
<div class="card card-flush mt-5 d-none" id="konten_gambar">
    <div class="card-header align-items-center pt-10">
        <div class="card-title">
            <h2>Unggah Galeri Gambar</h2>
        </div>
        <div class="card-toolbar flex-row-fluid justify-content-end">
            <button type="button" class="btn btn-outline btn-outline-success btn-color-success-400 btn-active-light-success" id="add_unggah_galeri_form">
                <i class="ki-duotone ki-plus-square">
                    <span class="path1"></span>
                    <span class="path2"></span>
                    <span class="path3"></span>
                </i>
                Tambah Gambar
            </button>
        </div>
    </div>
    <div class="card-body f-flex flex-column py-5 pb-10">
        <div id="konten_gambar_container">
            <div class="row g-5">
                <div class="col-4">
                    <h5 class="required">Gambar</h5>
                    <input type="hidden" name="content_pages_id[]" id="content_pages_id">
                    <img id="imagePreview_konten_gambar" class="img-fluid mt-3 d-none" />
                    <input type="file" class="d-none" accept="image/*" id="pilihGambar" name="pilihGambar[]" onchange="previewImage(event); kontenGambarGambarDescForm(this)">
                    <button type="button" class="w-100 btn btn-light-success" onclick="document.getElementById('pilihGambar').click()">
                        Pilih Gambar
                    </button>
                </div>
                <div class="col-8" id="deskripsi_gambar">
                    <h5 class="required">Caption Gambar</h5>
                    <div class="d-flex">
                        <input type="text" class="form-control" placeholder="Caption Gambar" name="caption[]" id="caption" value="{{ old('caption.0') }}" disabled>
                    </div>
                    <h5 class="mt-5 required">Deskripsi Gambar</h5>
                    <div class="d-flex">
                        <input type="text" class="form-control" placeholder="Deskripsi Gambar" name="desc_gambar[]" id="desc_gambar" value="{{ old('desc_gambar.0') }}" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('js-1')
    <script>
        var imagePagesCount = 1;
        var editImagePagesCount = 0;

        function addNewContentImageSection() {
            const imageUploadSection = `
                <div class="row g-5 pt-5">
                    <div class="mt-5 separator"></div>
                    <div class="col-4">
                        <h5 class="required">Gambar</h5>
                        <input type="hidden" name="content_pages_id[${imagePagesCount}]" id="content_pages_id_${imagePagesCount}">
                        <img id="imagePreview_${imagePagesCount}" class="w-100 img-fluid mt-3 d-none imagePreview" />
                        <input type="file" class="d-none pilihGambar" accept="image/*" data-desc-id="${imagePagesCount}" id="pilihGambar" name="pilihGambar[${imagePagesCount}]" onchange="previewImage(event); kontenGambarGambarDescForm(this)" />
                        <button type="button" class="w-100 btn btn-light-success" onclick="this.previousElementSibling.click()">Pilih Gambar</button>
                    </div>
                    <div class="col-7" id="deskripsi_gambar">
                        <h5 class="required">Caption Gambar</h5>
                        <div class="d-flex">
                            <input type="text" class="form-control" placeholder="Caption Gambar" name="caption[${imagePagesCount}]" id="caption_${imagePagesCount}" disabled />
                        </div>
                        <h5 class="mt-5 required">Deskripsi Gambar</h5>
                        <div class="d-flex">
                            <input type="text" class="form-control" placeholder="Deskripsi Gambar" name="desc_gambar[${imagePagesCount}]" id="desc_gambar_${imagePagesCount}" disabled />
                        </div>
                    </div>
                    <div class="col-1">
                        <h5 class="text-white">.</h5>
                        <button class="btn btn-icon btn-light-danger" onclick="this.closest('.row').remove()">
                            <i class="ki-duotone ki-trash"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i>
                        </button>
                    </div>
                </div>
            `;

            $('#konten_gambar_container').append(imageUploadSection);
            imagePagesCount += 1;
        }

        // Bind the click event to the "Tambah Halaman" button
        document.getElementById('add_unggah_galeri_form').addEventListener('click', addNewContentImageSection);

        function previewImage(event) {
            const input = event.target;
            const preview = input.previousElementSibling;

            if (input.files && input.files[0]) {
                const reader = new FileReader();

                reader.onload = function(e) {
                    preview.src = e.target.result;
                    preview.classList.remove('d-none');
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function kontenGambarGambarDescForm(value) {
            var caption = "#caption";
            var desc_gambar = "#desc_gambar";
            var id = $(value).data('desc-id') ? '_' + $(value).data('desc-id') : '';
            if ($(value).val()) {
                $(`${caption + id}`).prop('disabled', false);
                $(`${desc_gambar + id}`).prop('disabled', false);
            } else {
                $(`${caption + id}`).prop('disabled', true);
                $(`${desc_gambar + id}`).prop('disabled', true);
            }
        }
    </script>
    {{-- Edit Sections --}}
    <script>
        if (data && data.content_type === 'gambar') {
            $("#tambah_konten_form").append(`<input type="hidden" name="content_pages_id[0]" id="content_pages_id">`)
            document.getElementById("content_pages_id").value = data.content_pages[0].id;

            $('#imagePreview_konten_gambar').removeClass('d-none');
            document.getElementById('imagePreview_konten_gambar').src = data.content_pages[0].photo;
            document.getElementById("caption").value = data.content_pages[0].photo_caption;
            $("#caption").prop('disabled', false)
            document.getElementById("desc_gambar").value = data.content_pages[0].photo_desc;
            $("#desc_gambar").prop('disabled', false)

            if (data.content_pages.length > 1) {
                for (let index = 1; index < data.content_pages.length; index++) {
                    addNewContentImageSection();
                    document.getElementById(`content_pages_id_${index}`).value = data.content_pages[index].id;

                    $(`#imagePreview_${index}`).removeClass('d-none');
                    document.getElementById(`imagePreview_${index}`).src = data.content_pages[index].photo;
                    document.getElementById(`caption_${index}`).value = data.content_pages[index].photo_caption;
                    $(`#caption_${index}`).prop('disabled', false)
                    document.getElementById(`desc_gambar_${index}`).value = data.content_pages[index].photo_desc;
                    $(`#desc_gambar_${index}`).prop('disabled', false)
                }

                imagePagesCount = data.content_pages.length;
                editImagePagesCount = data.content_pages.length;
            }
        }
    </script>

    {{-- Old Sections --}}
    <script>
        if ('{{ old('total_konten') }}') {
            let oldCaptions = @json(old('caption', []));
            let oldDescriptions = @json(old('desc_gambar', []));

            var totalKonten = parseInt($('#total_konten').val());
            var old_imagePagesCount = editImagePagesCount ? (totalKonten - editImagePagesCount + 1) : totalKonten;
            var newIndex = editImagePagesCount > 0 ? editImagePagesCount - 1 : 0;

            for (let index = 1; index < old_imagePagesCount; index++) {
                addNewContentImageSection();

                document.getElementById(`caption_${newIndex + index}`).value = oldCaptions[newIndex + index];
                document.getElementById(`desc_gambar_${newIndex + index}`).value = oldDescriptions[newIndex + index];
            }
        }
    </script>
@endpush
