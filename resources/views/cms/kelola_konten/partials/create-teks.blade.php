@push('css')
    <style>
    </style>
@endpush
<div class="card card-flush mt-5 d-none" id="konten_text">
    <div class="card-header align-items-center pt-10">
        <div class="card-title">
            <h2>Isi Konten</h2>
        </div>
    </div>
    <div class="card-body f-flex flex-column py-5 pb-10">
        <div class="row g-5" id="teks-konten-container">
            <div class="accordion accordion-teks-konten-0" id="accordion-teks-konten-0">
                <div class="accordion-item">
                    <h2 class="accordion-header border-bottom d-flex align-items-center">
                        <button class="accordion-button bg-transparent shadow-none" type="button" data-bs-toggle="collapse" data-bs-target="#teks-konten-0" aria-expanded="true" aria-controls="teks-konten-0">
                            <h2 class="m-0">Halaman 1</h2>
                            <input type="hidden" name="content_pages_id[]" id="content_pages_id">
                        </button>
                    </h2>
                    <div class="accordion-collapse collapse show" id="teks-konten-0" data-bs-parent="#accordion-teks-konten-0">
                        <div class="accordion-body row g-5">
                            <div class="col-12" id="konten-teks-deskripsi">
                                <h5 class="required">Konten</h5>
                                <div>
                                    <textarea name="desc_konten_text[]" id="desc_konten_text" class="hidden"></textarea>
                                    <div id="quil_desc_konten_text"></div>
                                </div>
                            </div>
                            <div class="col-4" id="konten-teks-gambar">
                                <h5>Gambar</h5>
                                <img id="imagePreview_konten_teks" class="img-fluid mt-3 w-100 d-none" />
                                <input type="file" class="d-none" accept="image/*" id="content_text_image" name="content_text_image[]" onchange="previewImage(event); kontenTeksGambarDescForm(this)" multiple>
                                <button type="button" class="w-100 btn btn-light-success" onclick="document.getElementById('content_text_image').click()">
                                    Pilih Gambar
                                </button>
                            </div>
                            <div class="col-8" id="konten-teks-deskripsi-gambar">
                                <h5>Deskripsi Gambar</h5>
                                <input type="text" class="form-control" placeholder="Deskripsi" name="content_text_image_desc[]" id="content_text_image_desc" value="{{ old('content_text_image_desc.0') }}" disabled>
                            </div>
                            <div class="col-12" id="konten-teks-rekomendasi-konten">
                                <h5 class="required">Rekomendasi Konten</h5>
                                <select class="form-select related_content" data-control="select2" data-placeholder="Pilih Rekomendasi Konten" data-allow-clear="true" name="related_content[]" id="filter_related_content">
                                </select>
                            </div>
                            <div class="col-8">
                                <h5>Iklan</h5>
                                <select class="form-select filter_advertisement" data-control="select2" data-placeholder="Pilih Iklan" data-allow-clear="true" name="advertisement_id[]" id="filter_advertisement" onchange="adsPositionForm(this)">
                                </select>
                            </div>
                            <div class="col-4">
                                <h5>Posisi Iklan</h5>
                                <select class="form-select" data-control="select2" data-placeholder="Pilih Posisi Iklan" data-hide-search="true" name="advertisement_position[]" id="advertisement_position" disabled>
                                    <option></option>
                                    <option value="atas">Atas</option>
                                    <option value="tengah">Tengah</option>
                                    <option value="bawah">Bawah</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button type="button" class="col-12 mt-5 btn btn-outline btn-outline-success btn-color-success-400 btn-active-light-success" id="tambah_halaman_teks">
            <i class="ki-duotone ki-plus-square"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
            Tambah Halaman
        </button>
    </div>
</div>
@push('js-1')
    <script>
        const related_content_select2_config = {
            ajax: {
                url: "{{ route('kelola-konten.create') }}",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    var query = {
                        _token: '{{ csrf_token() }}',
                        type_form: "filter_select",
                        select_form: "filter_related_content",
                        search: params.term,
                        page: params.page || 1
                    };

                    return query;
                },
                cache: true
            }
        }

        const filter_advertisement_select2_config = {
            ajax: {
                url: "{{ route('kelola-konten.create') }}",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    var query = {
                        _token: '{{ csrf_token() }}',
                        type_form: "filter_select",
                        select_form: "filter_advertisement",
                        search: params.term,
                        page: params.page || 1
                    };

                    return query;
                },
                cache: true
            }
        }

        $('document').ready(function() {
            $(".related_content").select2(related_content_select2_config);
            $(".filter_advertisement").select2(filter_advertisement_select2_config);
        });

        function adsPositionForm(value) {
            var ads_position = "#advertisement_position";
            var ads_id = $(value).data('advertisement_id') ? '_' + $(value).data('advertisement_id') : '';

            if ($(value).val()) {
                $(`${ads_position + ads_id}`).prop('disabled', false);
            } else {
                $(`${ads_position + ads_id}`).prop('disabled', true);
            }
        }

        function kontenTeksGambarDescForm(value) {
            var content_text_image_desc = "#content_text_image_desc";
            var id = $(value).data('desc-id') ? '_' + $(value).data('desc-id') : '';
            if ($(value).val()) {
                $(`${content_text_image_desc + id}`).prop('disabled', false);
            } else {
                $(`${content_text_image_desc + id}`).prop('disabled', true);
            }
        }

        var desc_konten_text;
        $('document').ready(function() {
            const quil_desc_konten_text = document.getElementById('quil_desc_konten_text');
            desc_konten_text = new Quill(quil_desc_konten_text, quil_options, {
                placeholder: "Deskripsi Konten"
            });

            desc_konten_text.root.innerHTML = document.getElementById("desc_konten_text").value;

            desc_konten_text.on("text-change", function() {
                document.getElementById("desc_konten_text").value = desc_konten_text.root.innerHTML;
            });

            function previewImage(event) {
                const input = event.target;
                const preview = input.previousElementSibling; // Adjust based on your structure

                if (input.files) {
                    Array.from(input.files).forEach(file => {
                        const reader = new FileReader();

                        reader.onload = function(e) {
                            const img = document.createElement('img');
                            img.src = e.target.result;
                            img.classList.add('img-preview'); // Add your desired classes
                            previewContainer.appendChild(img);
                        };

                        reader.readAsDataURL(file);
                    });
                }
            }
        });

        // Function to add new content section
        var kontenPagesCount = 1;
        var editKontenPagesCount = 0;
        var kontenTextContent = [];

        function addNewContentSection() {
            // Get the container where new content will be added
            const container = document.getElementById('teks-konten-container');

            // Create a clone of the content structure
            const newContent = document.createElement('div');
            newContent.className = `accordion accordion-teks-konten-${kontenPagesCount}`;
            newContent.setAttribute("id", `accordion-teks-konten-${kontenPagesCount}`);
            newContent.innerHTML = `
                <div class="accordion-item">
                    <h2 class="accordion-header border-bottom d-flex align-items-center">
                        <button class="accordion-button bg-transparent shadow-none" type="button" data-bs-toggle="collapse" data-bs-target="#teks-konten-${kontenPagesCount}" aria-expanded="true" aria-controls="teks-konten-${kontenPagesCount}">
                            <h2 class="m-0">Halaman ${kontenPagesCount+1}</h2>
                        </button>
                        <button type="button" class="btn btn-icon btn-outline btn-outline-danger me-5" onclick="this.closest('.accordion').remove()" id="delete-konten-section">
                            <i class="ki-duotone ki-trash">
                                <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span>
                            </i>
                        </button>
                    </h2>
                    <div id="teks-konten-${kontenPagesCount}" class="accordion-collapse collapse show" data-bs-parent="#accordion-teks-konten-${kontenPagesCount}">
                        <div class="accordion-body row g-5">
                            <div class="col-12" id="konten-teks-deskripsi">
                                <h5 class="required">Konten</h5>
                                <input type="hidden" name="content_pages_id[${kontenPagesCount}]" id="content_pages_id_${kontenPagesCount}">
                                <div>
                                    <textarea name="desc_konten_text[]" id="desc_konten_text_${kontenPagesCount}" class="hidden"></textarea>
                                    <div id="quil_desc_konten_text_${kontenPagesCount}"></div>
                                </div>
                            </div>
                            <div class="col-4" id="konten-teks-gambar">
                                <h5>Gambar</h5>
                                <img id="imagePreview_${kontenPagesCount}" class="img-fluid mt-3 w-100 d-none" />
                                <input type="file" class="d-none" accept="image/*" data-desc-id="${kontenPagesCount}" id="content_text_image" name="content_text_image[]" onchange="previewImage(event); kontenTeksGambarDescForm(this)" multiple>
                                <button type="button" class="w-100 btn btn-light-success" onclick="this.previousElementSibling.click()">
                                    Pilih Gambar
                                </button>
                            </div>
                            <div class="col-8" id="konten-teks-deskripsi-gambar">
                                <h5>Deskripsi Gambar</h5>
                                <input type="text" class="form-control" placeholder="Deskripsi" name="content_text_image_desc[]" id="content_text_image_desc_${kontenPagesCount}" disabled>
                            </div>
                            <div class="col-12" id="konten-teks-rekomendasi-konten">
                                <h5 class="required">Rekomendasi Konten</h5>
                                <select class="form-select related_content" data-control="select2" data-placeholder="Pilih Rekomendasi Konten" data-allow-clear="true" name="related_content[]" id="filter_related_content_${kontenPagesCount}"></select>
                            </div>
                            <div class="col-8">
                                <h5>Iklan</h5>
                                <select class="form-select filter_advertisement" data-control="select2" data-placeholder="Pilih Iklan" data-allow-clear="true" data-advertisement_id="${kontenPagesCount}" onchange="adsPositionForm(this)" name="advertisement_id[]" id="filter_advertisement_${kontenPagesCount}"></select>
                            </div>
                            <div class="col-4">
                                <h5>Posisi Iklan</h5>
                                <select class="form-select" data-control="select2" data-placeholder="Pilih Posisi Iklan" data-hide-search="true" name="advertisement_position[]" id="advertisement_position_${kontenPagesCount}" disabled>
                                    <option></option>
                                    <option value="atas">Atas</option>
                                    <option value="tengah">Tengah</option>
                                    <option value="bawah">Bawah</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            `;

            // Append the new content to the container
            container.appendChild(newContent);

            // Reinitialize select2 container
            $(`#filter_related_content_${kontenPagesCount}`).select2(related_content_select2_config);
            $(`#filter_advertisement_${kontenPagesCount}`).select2(filter_advertisement_select2_config);
            $(`#advertisement_position_${kontenPagesCount}`).select2();

            // Reinitialize Quill Editor
            const newQuilEditor = document.getElementById(`quil_desc_konten_text_${kontenPagesCount}`);
            const newTextarea = document.getElementById(`desc_konten_text_${kontenPagesCount}`);

            if (newQuilEditor && newTextarea) {
                var newQuill = new Quill(newQuilEditor, quil_options, {
                    placeholder: "Deskripsi Kategori"
                });

                kontenTextContent.push(newQuill);

                newQuill.on("text-change", function() {
                    newTextarea.value = newQuill.root.innerHTML;
                });
            } else {
                console.error("Quill editor or textarea not found");
            }

            kontenPagesCount += 1;
        }

        // Bind the click event to the "Tambah Halaman" button
        document.getElementById('tambah_halaman_teks').addEventListener('click', addNewContentSection);
    </script>
    {{-- Tanya Jawab --}}
    <script>
        const istifta = {!! json_encode(@$istifta, JSON_HEX_TAG) !!};
        if (istifta) {
            $('document').ready(function() {
                $('#jenis_konten').val(istifta.content_type).trigger('change');

                filter_kategori(istifta.kategori.id, istifta.kategori.category_sub_name);
                filter_istifta(istifta.istifta.id, istifta.istifta.pertanyaan);
            });
        }

        $("#filter_istifta").on('change', function() {
            var id = $(this).find(":selected").val();
            $('#istifta_id').val(id);

            var url = `{{ route('kelola-istifta.create', ['id' => ':id']) }}`;
            url = url.replace(':id', id);
            $.get(url, function(data) {
                if (!data.content_id) {
                    pertanyaan_istifta(data)
                }
            });
        });
    </script>

    {{-- Edit Sections --}}
    <script>
        if (data && (data.content_type === 'teks' || data.content_type === 'tanya-jawab')) {
            // $("#tambah_konten_form").append(`<input type="hidden" name="content_pages_id[]" id="content_pages_id">`);
            // console.log(data.content_pages[0].id);
            if (data.istifta) {
                filter_istifta(data.istifta.id, data.istifta.pertanyaan);
            }
            document.getElementById("content_pages_id").value = data.content_pages[0].id;
            document.getElementById("desc_konten_text").value = data.content_pages[0].content;

            // Gambar
            $('#imagePreview_konten_teks').removeClass('d-none');
            document.getElementById('imagePreview_konten_teks').src = data.content_pages[0].photo;
            document.getElementById("content_text_image_desc").value = data.content_pages[0].photo_desc;
            $("#content_text_image_desc").prop('disabled', false)

            // Related Content
            var $filter_related_content = $("<option selected='selected'></option>")
                .val(data.content_pages[0].relateds.id)
                .text(data.content_pages[0].relateds.title);
            $("#filter_related_content").append($filter_related_content).trigger('change');

            if (data.content_pages[0].advertisement?.id) {
                var $advertisement_id = $("<option selected='selected'></option>")
                    .val(data.content_pages[0].advertisement.id)
                    .text(data.content_pages[0].advertisement.title);
                $("#filter_advertisement").append($advertisement_id).trigger('change');
            }

            $("#advertisement_position").val(data.content_pages[0].advertisement_position).trigger('change');

            if (data.content_pages.length > 1) {
                for (let index = 1; index < data.content_pages.length; index++) {
                    addNewContentSection();
                    document.getElementById(`content_pages_id_${index}`).value = data.content_pages[index].id;

                    kontenTextContent[index - 1].clipboard.dangerouslyPasteHTML(data.content_pages[index].content);

                    $(`#imagePreview_${index}`).removeClass('d-none');
                    document.getElementById(`imagePreview_${index}`).src = data.content_pages[index].photo;
                    $(`#content_text_image_desc_${index}`).prop('disabled', false)
                    document.getElementById(`content_text_image_desc_${index}`).value = data.content_pages[index]
                        .photo_desc;

                    let $related_content = $("<option selected='selected'></option>")
                        .val(data.content_pages[index].relateds.id)
                        .text(data.content_pages[index].relateds.title);
                    $(`#filter_related_content_${index}`).append($related_content).trigger('change');

                    if (data.content_pages[index].advertisement?.id) {
                        let $advertisement_id = $("<option selected='selected'></option>")
                            .val(data.content_pages[index].advertisement.id)
                            .text(data.content_pages[index].advertisement.title);
                        $(`#filter_advertisement_${index}`).append($advertisement_id).trigger('change');
                    }

                    $(`#advertisement_position_${index}`).val(data.content_pages[index].advertisement_position).trigger(
                        'change');
                }

                kontenPagesCount = data.content_pages.length;
                editKontenPagesCount = data.content_pages.length;
            }
        }
    </script>

    {{-- Old Sections --}}
    <script>
        if ('{{ old('total_konten') }}') {
            if ('{{ old('desc_konten_text.0') }}') {
                let realHTML = $('<textarea />').html('{{ old('desc_konten_text.0') }}').text();
                document.getElementById("desc_konten_text").value = realHTML;
            }

            $('document').ready(function() {
                if ('{{ old('related_content.0') }}') {
                    var $filter_related_content = $("<option selected='selected'></option>")
                        .val('{{ old('related_content.0') }}')
                        .text('{{ @\App\Models\Content::find(old('related_content.0'))->title }}');
                    $("#filter_related_content").append($filter_related_content).trigger('change');
                }

                if ('{{ old('advertisement_id.0') }}') {
                    var $advertisement_id = $("<option selected='selected'></option>")
                        .val('{{ old('advertisement_id.0') }}')
                        .text('{{ @\App\Models\Advertisements::find(old('advertisement_id.0'))->title }}');
                    $("#filter_advertisement").append($advertisement_id).trigger('change');
                }

                if ('{{ old('advertisement_position.0') }}') {
                    $("#advertisement_position").val('{{ old('advertisement_position.0') }}').trigger('change');
                }

                let oldDescKontenText = @json(old('desc_konten_text', []));
                let oldTextImageDesc = @json(old('content_text_image_desc', []));
                let oldRelatedContent = @json(old('related_content', []));
                let oldAdvertisementID = @json(old('advertisement_id', []));
                let oldAdvertisementPosition = @json(old('advertisement_position', []));

                var totalKonten = parseInt($('#total_konten').val());
                var old_editKontenPagesCount = editKontenPagesCount ? (totalKonten - editKontenPagesCount + 1) :
                    totalKonten;
                var newIndex = editKontenPagesCount > 0 ? editKontenPagesCount - 1 : 0;


                $(`#filter_advertisement_${kontenPagesCount}`).select2(filter_advertisement_select2_config);

                for (let index = 1; index < old_editKontenPagesCount; index++) {
                    addNewContentSection();

                    kontenTextContent[newIndex + index - 1].clipboard.dangerouslyPasteHTML(oldDescKontenText[
                        newIndex + index]);
                    document.getElementById(`content_text_image_desc_${newIndex + index}`).value = oldTextImageDesc[
                        newIndex + index] ?? null;
                    // Related Content
                    if (oldRelatedContent[newIndex + index]) {
                        $.ajax({
                            type: 'GET',
                            url: (`{{ route('kelola-konten.getRelatedContent', ['id' => ':id']) }}`)
                                .replace(':id', oldRelatedContent[newIndex + index]),
                            delay: 250
                        }).then(function(response) {
                            var option = new Option(response.name, response.id, true, true);
                            $(`#filter_related_content_${newIndex + index}`).append(option).trigger(
                                'change');

                            $(`#filter_related_content_${newIndex + index}`).trigger({
                                type: 'select2:select',
                                params: {
                                    results: response
                                }
                            });
                        });
                    }
                    // Advertisement
                    if (oldAdvertisementID[newIndex + index]) {
                        $.ajax({
                            type: 'GET',
                            url: (`{{ route('kelola-konten.getAdvertisementContent', ['id' => ':id']) }}`)
                                .replace(':id', oldAdvertisementID[newIndex + index]),
                            delay: 250
                        }).then(function(response) {
                            var option = new Option(response.name, response.id, true, true);
                            $(`#filter_advertisement_${newIndex + index}`).append(option).trigger('change');

                            $(`#filter_advertisement_${newIndex + index}`).trigger({
                                type: 'select2:select',
                                params: {
                                    results: response
                                }
                            });
                        });
                    }
                    // Advertisement Position
                    $(`#advertisement_position_${newIndex + index}`).val(oldAdvertisementPosition[newIndex + index])
                        .trigger('change');
                }
            });
        }
    </script>
@endpush
