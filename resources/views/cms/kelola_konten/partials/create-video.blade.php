@push('css')
@endpush
<div class="card card-flush mt-5 d-none" id="konten_video">
    <div class="card-header align-items-center pt-5 pt-10">
        <div class="card-title">
            <h2>Embed Video</h2>
        </div>
    </div>
    <div class="card-body f-flex flex-column py-5 pb-10">
        <div class="row g-5">
            <div class="col-12">
                <h5 class="required">Masukkan Embed Code Youtube</h5>
                <textarea class="form-control" placeholder="Salin Embed Code disini" data-kt-autosize="true" name="embed_video" id="embed_video">{{ old('embed_video') }}</textarea>
            </div>
        </div>
    </div>
</div>
@push('js-1')
    {{-- Edit Sections --}}
    <script>
        if (data && data.content_type === 'video') {
            $("#tambah_konten_form").append(`<input type="hidden" name="content_pages_id[]" id="content_pages_id">`)
            document.getElementById("content_pages_id").value = data.content_pages[0].id;
            document.getElementById("embed_video").value = data.content_pages[0].embeded_string;
        }
    </script>
@endpush
