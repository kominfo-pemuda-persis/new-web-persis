@extends('cms.layouts.master')
@section('title', 'Kelola Konten')

@push('css')
    <style>
        img[src="null"] {
            display: none;
        }

        .dropzone {
            border: 1px solid var(--bs-gray-300) !important;
            background-color: transparent !important;
        }

        .dz-preview {
            width: 100%;
            margin: 0 !important;
            height: 100%;
            padding: 15px;
            top: 0;
        }

        .dz-photo {
            height: 100%;
            width: 100%;
            overflow: hidden;
            border-radius: 12px;
            background: #eae7e2;
        }

        .dz-thumbnail {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .dz-image {
            width: 90px !important;
            height: 90px !important;
            border-radius: 6px !important;
        }

        .dz-remove {
            display: none !important;
        }

        .dz-delete {
            width: 24px;
            height: 24px;
            background: rgba(0, 0, 0, 0.57);
            position: absolute;
            opacity: 0;
            transition: all 0.2s ease;
            top: 30px;
            right: 30px;
            border-radius: 100px;
            z-index: 9999;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .dz-delete>svg {
            transform: scale(0.75);
            cursor: pointer;
        }

        .dz-preview:hover .dz-delete,
        .dz-preview:hover .dz-remove-image {
            opacity: 1;
        }

        .dz-message {
            height: 100%;
            margin: 0 !important;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .dropzone-drag-area {
            position: relative;
            padding: 0 !important;
        }
    </style>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Amiri:ital,wght@0,400;0,700;1,400;1,700&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Aref+Ruqaa:wght@400;700&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Changa:wght@200..800&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Courier+Prime:ital,wght@0,400;0,700;1,400;1,700&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Lateef:wght@200;300;400;500;600;700;800&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Mirza:wght@400;500;600;700&display=swap');

        @font-face {
            font-family: 'Akhbar MT';
            fonnt-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url('/assets/custom-fonts/Akhbar MT/Akhbar MT Regular.ttf') format("truetype");
            ;
        }

        @font-face {
            font-family: 'Akhbar MT';
            fonnt-style: normal;
            font-weight: 700;
            font-display: swap;
            src: url('/assets/custom-fonts/Akhbar MT/Akhbar MT Bold.ttf') format("truetype");
            ;
        }


        @font-face {
            font-family: 'Janna LT Bold';
            fonnt-style: normal;
            font-weight: 700;
            font-display: swap;
            src: url('/assets/custom-fonts/Janna LT/Janna LT Bold.ttf') format("truetype");
            ;
        }

        @font-face {
            font-family: 'KFGQPC Uthman Taha Naskh Regular';
            fonnt-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url('/assets/custom-fonts/KFGQPC Uthman Taha Naskh/KFGQPC Uthman Taha Naskh Regular.ttf') format("truetype");
            ;
        }

        .ql-container {
            height: 250px;
            max-height: 250px;
            overflow: auto;
        }

        /* Set droplist names - -item is the currently selected font, -label is the font's appearance in the droplist*/

        .ql-snow .ql-picker.ql-font .ql-picker-label::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item::before {
            content: "Inter";
            font-family: Inter, Helvetica, sans-serif;
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="tahoma"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="tahoma"]::before {
            content: "Tahoma";
            font-family: "Tahoma";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="georgia"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="georgia"]::before {
            content: "Georgia";
            font-family: "Georgia";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="book-antiqua"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="book-antiqua"]::before {
            content: "Book Antiqua";
            font-family: "Book Antiqua";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="arial"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="arial"]::before {
            content: "Arial";
            font-family: "Arial";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="trebuchet"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="trebuchet"]::before {
            content: "Trebuchet";
            font-family: "Trebuchet";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="verdana"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="verdana"]::before {
            content: "Verdana";
            font-family: "Verdana";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="times-new-roman"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="times-new-roman"]::before {
            content: "Times New Roman";
            font-family: "Times New Roman";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="wedings"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="wedings"]::before {
            content: "Wedings";
            font-family: "Wedings";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="britanic-bold"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="britanic-bold"]::before {
            content: "Britanic Bold";
            font-family: "Britanic Bold";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="janna-lt-bold"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="janna-lt-bold"]::before {
            content: "Janna LT Bold";
            font-family: "Janna LT Bold";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="kfgqpc-uthman-taha-naskh-regular"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="kfgqpc-uthman-taha-naskh-regular"]::before {
            content: "KFGQPC Uthman Taha Naskh Regular";
            font-family: "KFGQPC Uthman Taha Naskh Regular";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="akhbar-mt"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="akhbar-mt"]::before {
            content: "Akhbar MT";
            font-family: "Akhbar MT";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="changa"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="changa"]::before {
            content: "Changa";
            font-family: "Changa";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="amiri"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="amiri"]::before {
            content: "Amiri";
            font-family: "Amiri";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="lateef"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="lateef"]::before {
            content: "Lateef";
            font-family: "Lateef";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="aref-ruqaa"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="aref-ruqaa"]::before {
            content: "Aref Ruqaa";
            font-family: "Aref Ruqaa";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="mirza"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="mirza"]::before {
            content: "Mirza";
            font-family: "Mirza";
        }

        .ql-snow .ql-picker.ql-font .ql-picker-label[data-value="courier-prime"]::before,
        .ql-snow .ql-picker.ql-font .ql-picker-item[data-value="courier-prime"]::before {
            content: "Courier Prime";
            font-family: "Courier Prime";
        }

        .ql-formats .ql-font.ql-picker {
            width: 140px !important;
        }

        .ql-font.ql-picker .ql-picker-label::before {
            width: 90%;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .ql-font.ql-picker .ql-picker-options {
            max-height: 200px;
            overflow: auto;
        }

        .ql-font-tahoma {
            font-family: "Tahoma" !important;
        }

        .ql-font-georgia {
            font-family: "Georgia" !important;
        }

        .ql-font-book-antiqua {
            font-family: "Book Antiqua" !important;
        }

        .ql-font-arial {
            font-family: "Arial" !important;
        }

        .ql-font-trebuchet {
            font-family: "Trebuchet" !important;
        }

        .ql-font-verdana {
            font-family: "Verdana" !important;
        }

        .ql-font-times-new-roman {
            font-family: "Times New Roman" !important;
        }

        .ql-font-wedings {
            font-family: "Wedings" !important;
        }

        .ql-font-britanic-bold {
            font-family: "Britanic Bold" !important;
        }

        .ql-font-janna-lt-bold {
            font-family: "Janna LT Bold" !important;
        }

        .ql-font-kfgqpc-uthman-taha-naskh-regular {
            font-family: "KFGQPC Uthman Taha Naskh Regular" !important;
        }

        .ql-font-akhbar-mt {
            font-family: "Akhbar MT" !important;
        }

        .ql-font-changa {
            font-family: "Changa" !important;
        }

        .ql-font-amiri {
            font-family: "Amiri" !important;
        }

        .ql-font-lateef {
            font-family: "Lateef" !important;
        }

        .ql-font-aref-ruqaa {
            font-family: "Aref Ruqaa" !important;
        }

        .ql-font-mirza {
            font-family: "Mirza" !important;
        }

        .ql-font-courier-prime {
            font-family: "Courier Prime" !important;
        }

        .ql-formats .ql-size.ql-picker {
            width: 56px !important;
        }

        .ql-size.ql-picker .ql-picker-options {
            max-height: 200px;
            overflow: auto;
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="8px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="8px"]::before {
            content: "8";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="9px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="9px"]::before {
            content: "9";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="10px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="10px"]::before {
            content: "10";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="12px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="12px"]::before {
            content: "12";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="14px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="14px"]::before {
            content: "14";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="16px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="16px"]::before {
            content: "16";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="18px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="18px"]::before {
            content: "18";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="20px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="20px"]::before {
            content: "20";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="24px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="24px"]::before {
            content: "24";
        }

        .ql-snow .ql-picker.ql-size .ql-picker-label[data-value="32px"]::before,
        .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="32px"]::before {
            content: "32";
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-gray-900 fw-bold flex-column justify-content-center my-0">
                        {{ @$data ? 'Edit' : 'Buat' }} Konten
                    </h1>
                    <!--end::Title-->

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-konten.index') }}" class="text-muted text-hover-primary">
                                Kelola Kategori
                            </a>
                        </li>
                        <li class="breadcrumb-item"><span class="bullet bg-gray-500 w-5px h-2px"></span></li>
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-konten.create') }}" class="text-muted text-hover-primary">
                                {{ @$data ? 'Edit' : 'Buat' }} Konten
                            </a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->

        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <form action="{{ route('kelola-konten.store') }}" method="post" enctype="multipart/form-data" class="needs-validation" data-parsley-errors-messages-disabled id="tambah_konten_form">
                    @csrf
                    <input type="hidden" id="content_id" name="content_id" value={{ @$data->id ?? null }}>
                    <input type="hidden" id="istifta_id" name="istifta_id" value={{ @$istifta['istifta']->id ?? null }}>
                    <input type="hidden" id="created_by" name="created_by" value={{ @$data->created_by ?? null }}>
                    <input type="hidden" id="total_konten" name="total_konten" value="{{ old('total_konten') }}">
                    <input type="hidden" id="content_type" name="content_type">
                    <input type="hidden" id="status" name="status">
                    <div class="row g-5 gx-xl-10 mb-5 mb-xl-10">
                        <!--begin::Primary Create-->
                        <div class="col-lg-8">
                            <div class="card card-flush">
                                <!--begin::Card body-->
                                <div class="card-body d-flex flex-column">
                                    <div class="row g-5">
                                        <div class="col-12">
                                            <h5 class="required">Jenis Konten</h5>
                                            <select class="form-control form-select w-50" data-control="select2" data-placeholder="Pilih Jenis Konten" data-hide-search="true" name="jenis_konten" id="jenis_konten" data-parsley-trigger="change">
                                                <option></option>
                                                <option value="teks">Teks</option>
                                                <option value="gambar">Gambar</option>
                                                <option value="video">Video</option>
                                                <option value="tanya-jawab">Tanya Jawab</option>
                                            </select>
                                        </div>
                                        <div class="col-12 d-none" id="istiftaSelect">
                                            <h5 class="required">Pilih Pertanyaan</h5>
                                            <select class="form-control form-select w-50" data-control="select2" data-placeholder="Pilih Pertanyaan" data-hide-search="true" name="filter_istifta" id="filter_istifta" data-parsley-trigger="change"
                                                disabled>
                                                @if (old('filter_istifta'))
                                                    <option value={{ old('filter_istifta') }}>
                                                        {{ \App\Models\Istifta::find(old('filter_istifta'))->pertanyaan }}
                                                    </option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-12">
                                            <h5 class="required">Judul Konten</h5>
                                            {{-- <input type="text" class="form-control" placeholder="Judul Konten" name="title" id="title" data-parsley-required value="{{ old('title') }}"> --}}
                                            <div>
                                                <div id="quill-judul_konten" class="mb-3" style="height: 50px;"></div>
                                                <textarea rows="1" class="mb-3 d-none" name="title" id="quill-area-judul_konten"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <h5 class="required">Slug</h5>
                                            <input type="text" class="form-control" placeholder="Slug" name="slug" id="slug" data-parsley-required value="{{ old('slug') }}">
                                        </div>
                                        <div class="col-12 d-none" id="gambar_sampul">
                                            <h5 class="required">Gambar Sampul</h5>
                                            <div class="dropzone" id="gambar_sampul_dropzone">
                                                <!--begin::Message-->
                                                <div class="dropzone-drag-area form-control form-control-transparent h-150px" id="previews">
                                                    <input type="file" id="featured_image" name="featured_image" style="display: none;" />
                                                    <div class="fallback">
                                                        <input name="file" type="file" />
                                                    </div>
                                                    <div class="dz-message">
                                                        <!--begin::Info-->
                                                        <div class="ms-4">
                                                            <span class="fs-5 fw-bold text-decoration-underline" style="color: #054c2d">
                                                                Unggah gambar
                                                            </span>
                                                            <span class="fs-7 fw-semibold text-gray-500">
                                                                atau seret gambar ke sini
                                                            </span>
                                                        </div>
                                                        <!--end::Info-->
                                                    </div>
                                                    <div class="d-none" id="dzPreviewContainer">
                                                        <div class="dz-preview dz-file-preview">
                                                            <div class="dz-photo">
                                                                <img class="dz-thumbnail" data-dz-thumbnail>
                                                            </div>
                                                            <button class="dz-delete border-0 p-0" type="button" data-dz-remove>
                                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" id="times">
                                                                    <path fill="#FFFFFF"
                                                                        d="M13.41,12l4.3-4.29a1,1,0,1,0-1.42-1.42L12,10.59,7.71,6.29A1,1,0,0,0,6.29,7.71L10.59,12l-4.3,4.29a1,1,0,0,0,0,1.42,1,1,0,0,0,1.42,0L12,13.41l4.29,4.3a1,1,0,0,0,1.42,0,1,1,0,0,0,0-1.42Z">
                                                                    </path>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="invalid-feedback fw-bold">
                                                    Tolong upload Gambar untuk sampul berita
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 d-none" id="desc_gambar_sampul">
                                            <h5 class="required">Deskripsi Gambar Sampul</h5>
                                            <input type="text" class="form-control" placeholder="Deskripsi Gambar Sampul" name="desc_featured_image" id="desc_featured_image" value="{{ old('desc_featured_image') }}">
                                        </div>
                                        <div class="col-12 d-none" id="deskripsi">
                                            <h5 class="required">Deskripsi</h5>
                                            <div>
                                                <div id="quill-desc_kategori" class="mb-3" style="height: 300px;"></div>
                                                <textarea rows="3" class="mb-3 d-none" name="desc_kategori" id="quill-area-desc_kategori"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Card body-->
                            </div>

                            <!--begin::Card Embed Video-->
                            @include('cms.kelola_konten.partials.create-video')
                            <!--end::Card Embed Video-->

                            <!--begin::Card Konten Text-->
                            @include('cms.kelola_konten.partials.create-teks')
                            <!--end::Card Konten Text-->

                            <!--begin::Card Gambar-->
                            @include('cms.kelola_konten.partials.create-galery')
                            <!--end::Card Gambar-->

                        </div>
                        <!--end::Primary Create-->
                        <!--begin::Secondary Create-->
                        <div class="col-lg-4">
                            <div class="card card-flush mb-5">
                                <!--begin::Card body-->
                                <div class="card-body d-flex flex-column justify-content-end">
                                    <div class="row g-5">
                                        <div class="col-12">
                                            <h5 class="required">Reporter</h5>
                                            <select class="form-control form-select" data-control="select2" data-placeholder="Pilih Reporter" data-allow-clear="true" name="reporter" id="reporter">
                                                @if (old('reporter'))
                                                    <option value={{ old('reporter') }}>
                                                        {{ Str::isUuid(old('reporter')) ? \App\Models\Reporter::find(old('reporter'))->reporter_name : old('reporter') }}
                                                    </option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-12">
                                            <h5 class="required">Kategori</h5>
                                            <select class="form-control form-select" data-control="select2" data-placeholder="Pilih Kategori" data-allow-clear="true" name="content_category_id" id="filter_kategori">
                                                @if (old('content_category_id'))
                                                    <option value={{ old('content_category_id') }}>
                                                        {{ \App\Models\ContentCategoriesSub::find(old('content_category_id'))->category_sub_name }}
                                                    </option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-12">
                                            <h5 class="required">Tag</h5>
                                            <select class="form-control form-select" data-control="select2" data-tags="true" data-placeholder="Tambahkan Tag" data-allow-clear="true" name="filter_tag[]" id="filter_tag" multiple="multiple">
                                                @if (is_array(old('filter_tag')))
                                                    @foreach (old('filter_tag') as $tag)
                                                        <option value={{ $tag }} selected>
                                                            {{ Str::isUuid($tag) ? \App\Models\Tag::find($tag)->title : $tag }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="separator"></div>
                                        <div class="col-12">
                                            <h5 class="required">Deskripsi SEO</h5>
                                            <textarea class="form-control" name="desc_seo" id="desc_seo" placeholder="Deskripsi SEO" cols="30" rows="10" data-parsley-required>{{ old('desc_seo') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Card body-->
                            </div>
                            <div class="card card-flush mb-5">
                                <!--begin::Card body-->
                                <div class="card-body d-flex flex-column justify-content-end">
                                    <div class="row g-5">
                                        <div class="col-12">
                                            <div class="form-check form-switch form-check-custom form-check-solid form-check-success justify-content-between align-items-center">
                                                <h4 class="m-0" for="is_headline">
                                                    Penjadwalan
                                                </h4>
                                                <input class="form-check-input" style="height: 24px; width: 42px;" type="checkbox" data-bs-toggle="collapse" data-bs-target="#published_at_div" aria-expanded="false" aria-controls="published_at_div"
                                                    id="published_at_switch" />
                                            </div>
                                        </div>
                                        <div class="col-12 mt-0">
                                            <div class="collapse" id="published_at_div">
                                                <h5 class="mt-5 required">Tanggal Publikasi</h5>
                                                <div class="input-group">
                                                    <span class="input-group-text bg-transparent border-end-0">
                                                        <i class="ki-duotone ki-calendar fs-2">
                                                            <span class="path1"></span>
                                                            <span class="path2"></span>
                                                        </i>
                                                    </span>
                                                    <input class="form-control border-start-0" placeholder="Tanggal Publikasi" name="published_at" id="published_at" disabled />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="separator"></div>
                                        @if ($is_headline <= 5)
                                            <div class="col-12">
                                                <div class="form-check form-switch form-check-custom form-check-solid form-check-success justify-content-between align-items-center">
                                                    <h4 class="m-0" for="is_headline">
                                                        Jadikan Heading
                                                    </h4>
                                                    <input class="form-check-input" style="height: 24px; width: 42px;" type="checkbox" name="is_headline" id="is_headline" />
                                                </div>
                                            </div>
                                            <div class="separator"></div>
                                        @endif
                                        <div class="col-12">
                                            <input type="hidden" name="button_status" id="button_status" value="">
                                            <button type="submit" class="w-100 btn btn-bd-primary mb-5" id="publish_button" form="tambah_konten_form">
                                                Publikasikan Sekarang
                                            </button>
                                            <button type="button" class="w-100 btn btn-outline btn-outline-success btn-color-success-400 btn-active-light-success" id="draft_button" form="tambah_konten_form">
                                                Simpan ke Draft
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Card body-->
                            </div>
                        </div>
                        <!--end::Secondary Create-->
                    </div>
                </form>
            </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>
@endsection
@push('js')
    <script src="http://parsleyjs.org/dist/parsley.js"></script>
    <script>
        function filter_kategori(id, text) {
            var $filter_kategori = $("<option selected='selected'></option>").val(id).text(text);
            $("#filter_kategori").append($filter_kategori).trigger('change');
        }

        function filter_istifta(id, text) {
            var $filter_istifta = $("<option selected='selected'></option>").val(id).text(text);
            $("#filter_istifta").append($filter_istifta).trigger('change');
        }

        function pertanyaan_istifta(data) {
            var pengirim_pertanyaan = data.show_name ? data.full_name : 'Hamba Allah';
            var pertanyaan = `<p><strong>Pertanyaan dari : </strong>${pengirim_pertanyaan}</p>`
            pertanyaan += `<p><strong>Pertanyaan : </strong>${data.pertanyaan}</p><br><br><strong>Jawaban :</strong> `

            let realHTML = $('<textarea />').html(pertanyaan).text()
            desc_konten_text.clipboard.dangerouslyPasteHTML(realHTML);
        }
    </script>
    <script>
        var jenis_konten_filter;
        $("#jenis_konten").on('change', function() {
            jenis_konten_filter = $("#jenis_konten").find(":selected").text();

            switch (jenis_konten_filter) {
                case "Teks":
                case "Tanya Jawab":
                    if (jenis_konten_filter == "Tanya Jawab") {
                        $('#istiftaSelect').removeClass('d-none');
                        $('#filter_istifta').prop('disabled', false);
                        $('#content_type').attr('value', 'tanya-jawab');

                        filter_kategori("{{ @$kategori_istifta->id }}", "{{ @$kategori_istifta->text }}");
                    } else {
                        $('#istiftaSelect').addClass('d-none');
                        $('#filter_istifta').prop('disabled', true);
                        $('#content_type').attr('value', 'teks');
                        $('#istifta_id').val(null);
                        $("#filter_kategori").empty().trigger('change')
                    }
                    $('#desc_gambar_sampul').removeClass('d-none');
                    $('#gambar_sampul').removeClass('d-none');
                    $('#konten_gambar').addClass('d-none');
                    $('#konten_video').addClass('d-none');

                    $('#konten_text').removeClass('d-none');
                    $('#deskripsi').addClass('d-none');
                    break;
                case "Gambar":
                    $('#gambar_sampul').addClass('d-none');
                    $('#desc_gambar_sampul').addClass('d-none');
                    $('#deskripsi').addClass('d-none');
                    $('#konten_text').addClass('d-none');
                    $('#konten_video').addClass('d-none');
                    $('#istiftaSelect').addClass('d-none');

                    $('#konten_gambar').removeClass('d-none');
                    $('#content_type').attr('value', 'gambar');
                    break;
                case "Video":
                    $('#desc_gambar_sampul').addClass('d-none');
                    $('#konten_text').addClass('d-none');
                    $('#konten_gambar').addClass('d-none');
                    $('#istiftaSelect').addClass('d-none');

                    $('#gambar_sampul').removeClass('d-none');
                    $('#deskripsi').removeClass('d-none');
                    $('#konten_video').removeClass('d-none');
                    $('#content_type').attr('value', 'video');
                    break;
                default:
                    $('#gambar_sampul').addClass('d-none');
                    $('#desc_gambar_sampul').addClass('d-none');
                    $('#deskripsi').addClass('d-none');
                    $('#istiftaSelect').addClass('d-none');

                    $('#konten_text').addClass('d-none');
                    $('#konten_video').addClass('d-none');
                    $('#konten_gambar').addClass('d-none');

                    $('#content_type').attr('value', null);
                    break;
            }
        });

        // Select2 Initialize
        $('document').ready(function() {
            const selectFields = [{
                    id: '#filter_kategori',
                    oldValue: '{{ old('content_category_id') }}',
                    form_select: 'filter_kategori',
                    tags: false
                },
                {
                    id: '#reporter',
                    oldValue: '{{ old('reporter') }}',
                    form_select: 'filter_reporter',
                },
                {
                    id: '#filter_tag',
                    oldValue: @json(old('filter_tag', [])),
                    form_select: 'filter_tag',
                },
                {
                    id: '#filter_istifta',
                    oldValue: '{{ old('filter_istifta') }}',
                    form_select: 'filter_istifta',
                    tags: false
                }
            ];

            function initFilterSelect2(field) {
                $(field.id).select2({
                    tags: field.tags ?? true,
                    multiple: Array.isArray(field.oldValue),
                    ajax: {
                        url: "{{ route('kelola-konten.create') }}",
                        dataType: 'json',
                        delay: 250,
                        data: function(params) {
                            var query = {
                                _token: '{{ csrf_token() }}',
                                type_form: "filter_select",
                                select_form: field.form_select,
                                search: params.term,
                                page: params.page || 1
                            };

                            return query;
                        },
                        cache: true
                    }
                });
            }

            selectFields.forEach(initFilterSelect2);
        });

        // Old Value
        $('document').ready(function() {
            $('#jenis_konten').val('{{ old('jenis_konten') }}').trigger('change');
        });
    </script>

    <script>
        const fontSizeArr = ['8px', '9px', '10px', '12px', '14px', '16px', '18px', '20px', '24px', '32px'];

        const fontFamilyAttr = [
            '',
            'akhbar-mt',
            'amiri',
            'aref-ruqaa',
            'arial',
            'book-antiqua',
            // 'britanic-bold',
            'changa',
            'courier-prime',
            'georgia',
            'janna-lt-bold',
            'kfgqpc-uthman-taha-naskh-regular',
            'lateef',
            'mirza',
            'tahoma',
            'times-new-roman',
            'trebuchet',
            'verdana',
            // 'wedings'
        ];

        var Size = Quill.import('attributors/style/size');
        Size.whitelist = fontSizeArr;
        Quill.register(Size, true);

        var Font = Quill.import('attributors/class/font');
        Font.whitelist = fontFamilyAttr;
        Quill.register(fontFamilyAttr, true);

        const quil_toolbar = [
            [{
                'font': fontFamilyAttr
            }],
            [{
                'size': fontSizeArr
            }],
            // [{
            //     header: [1, 2, false]
            // }],
            ['bold', 'italic', 'underline', 'strike'],
            [{
                'list': 'ordered'
            }, {
                'list': 'bullet'
            }, {
                'indent': '-1'
            }, {
                'indent': '+1'
            }],
            [{
                'script': 'sub'
            }, {
                'script': 'super'
            }],
            [{
                'align': ''
            }, {
                'align': 'center'
            }, {
                'align': 'right'
            }, {
                'align': 'justify'
            }],
            ['link'],
            ['clean'],

        ];

        const quil_options = {
            modules: {
                toolbar: quil_toolbar,
            },
            theme: 'snow',
        };

        var quil_desc_kategori = document.getElementById('quill-desc_kategori');
        var quillEditor = document.getElementById('quill-area-desc_kategori');
        var editor = new Quill(quil_desc_kategori, quil_options);

        editor.on('text-change', function() {
            quillEditor.value = editor.root.innerHTML;
        });

        if ('{{ old('desc_kategori') }}') {
            let realHTML = $('<textarea />').html('{{ old('desc_kategori') }}').text();
            editor.clipboard.dangerouslyPasteHTML(realHTML);
        }

        var quilJudulKonten = document.getElementById('quill-judul_konten');
        var quillJudulKontenEditor = document.getElementById('quill-area-judul_konten');
        var editorJudulKonten = new Quill(quilJudulKonten, quil_options);

        editorJudulKonten.on('text-change', function() {
            quillJudulKontenEditor.value = editorJudulKonten.root.innerHTML;
            let inputValue = (editorJudulKonten.root.innerText).toLowerCase();

            inputValue = inputValue.replace(/\s+/g, ' ');
            inputValue = inputValue.replace(/-+/g, '-');
            inputValue = inputValue.replace(/\s/g, '-');
            inputValue = inputValue.replace(/[^a-z0-9\s-]/g, '');

            document.getElementById('slug').value = inputValue;
        });

        if ('{{ old('title') }}') {
            let realHTML = $('<textarea />').html('{{ old('title') }}').text();
            editorJudulKonten.clipboard.dangerouslyPasteHTML(realHTML);
        }

        $('#published_at_switch').click(function() {
            if ($(this).is(':checked')) {
                document.getElementById("published_at").disabled = false;
            } else {
                document.getElementById("published_at").disabled = true;
            }
        });

        const now = new Date();
        var tanggalPublikasi = new tempusDominus.TempusDominus(document.getElementById("published_at"), {
            display: {
                sideBySide: false
            },
            localization: {
                format: 'dd/MM/yyyy HH:mm',
                hourCycle: 'h24'
            },
            allowInputToggle: true,
            promptTimeOnDateChange: true
        });


        document.getElementById('published_at').addEventListener("change.td", function() {
            if (moment(document.getElementById('published_at').value, 'DD/MM/YYYY HH:mm:ss').toDate() <=
                new Date()) {
                document.getElementById("publish_button").innerHTML = "Publikasikan Sekarang";
            } else {
                document.getElementById("publish_button").innerHTML = "Jadwalkan";
            }
        });

        // document.getElementById('title').addEventListener('keyup', function(event) {
        //     let inputValue = event.target.value.toLowerCase();

        //     inputValue = inputValue.replace(/\s+/g, ' ');
        //     inputValue = inputValue.replace(/-+/g, '-');
        //     inputValue = inputValue.replace(/\s/g, '-');
        //     inputValue = inputValue.replace(/[^a-z0-9\s-]/g, '');

        //     document.getElementById('slug').value = inputValue;
        // });

        var myDropzone = new Dropzone("#tambah_konten_form", {
            previewTemplate: $('#dzPreviewContainer').html(),
            previewsContainer: "#previews",
            autoProcessQueue: false,
            url: "#",
            acceptedFiles: 'image/*',
            maxFiles: 1,
            maxFilesize: 10,
            thumbnailWidth: 900,
            thumbnailHeight: 600,
            addRemoveLinks: true,
            init: function() {
                myDropzone = this;
                $('.dz-message').show();
                this.on('addedfile', function(file) {
                    $('.dz-message').hide();
                    $('#previews').removeClass('h-150px');
                    $('.dropzone-drag-area').removeClass('is-invalid').next('.invalid-feedback').hide();

                    document.getElementById('featured_image').file = file;
                });
                this.on('removedfile', function(file) {
                    $('.dz-message').show();
                    $('#previews').addClass('h-150px');
                });
            },
            success: function(file, response) {
                $('#tambah_konten_form').fadeOut(600);
                window.location.href = "{{ route('kelola-konten.index') }}?success=Data berhasil disimpan.";
            }
        });
    </script>

    <script>
        $('#draft_button, #publish_button').on('click', function(event) {
            event.preventDefault();
            var $this = $(this);

            if (jenis_konten_filter != "gambar") {
                let fileInput = document.createElement('input');
                fileInput.type = 'file';
                fileInput.name = 'featured_image';
                fileInput.classList.add('d-none');

                let storedFile = document.getElementById('featured_image').file;
                if (storedFile?.status === 'queued') {
                    let dataTransfer = new DataTransfer();
                    dataTransfer.items.add(storedFile);
                    fileInput.files = dataTransfer.files;
                }
                this.appendChild(fileInput);
            }

            $('#button_status').val($(this).attr('id'));
            $('#total_konten').val(jenis_konten_filter == 'Teks' || jenis_konten_filter == 'teks' ? parseInt(
                kontenPagesCount) : parseInt(imagePagesCount));
            document.getElementById("tambah_konten_form").submit();
        });
    </script>

    {{-- Edit Section --}}
    <script>
        const data = {!! json_encode(@$data, JSON_HEX_TAG) !!};
        if (data) {
            $('document').ready(function() {
                $("#jenis_konten").val(data.content_type).trigger('change');
                jenis_konten_filter = data.content_type;
                $("#jenis_konten").select2({
                    disabled: 'readonly'
                });
                $("#status").val(data.status);
                // $("#title").val(data.title);
                // document.getElementById("quill-area-judul_konten").value = data.title;
                // if ('{{ old('title') }}') {
                //     let realHTML = $('<textarea />').html('{{ old('title') }}').text();
                editorJudulKonten.clipboard.dangerouslyPasteHTML(data.title);
                // }

                $("#slug").val(data.slug);
                $("#desc_seo").val(data.desc_seo);
                tanggalPublikasi.dates.setValue(tempusDominus.DateTime.convert(moment(data.published_at,
                    'YYYY-MM-DD HH:mm:ss').toDate()));

                filter_kategori(data.content_category_sub.id, data.content_category_sub.category_sub_name)

                var $filter_reporter = $("<option selected='selected'></option>")
                    .val(data.reporters.id)
                    .text(data.reporters.reporter_name);
                $("#reporter").append($filter_reporter).trigger('change');

                var $tags = $(document.createDocumentFragment());
                data.tags.forEach(function(item) {
                    var $tags_data = $("<option selected='selected'></option>")
                        .val(item.id)
                        .text(item.title);
                    $tags.append($tags_data);
                });
                $('#filter_tag').val(null).trigger('change');
                $("#filter_tag").append($tags).trigger('change');

                if (data.is_headline === 1) {
                    document.getElementById("is_headline").checked = true;
                }

                document.getElementById("publish_button").innerHTML = "Simpan";
                document.getElementById("published_at_switch").checked = true;
                $("#published_at_div").addClass('show');
                $("#published_at").prop('disabled', false);
                if (data.status === 1) {
                    $('#draft_button').addClass('d-none');
                }

                // DropZone
                if (data.content_type != 'gambar') {
                    const featured_image_url = data.featured_image;
                    const featured_image_name = featured_image_url.substring(featured_image_url.lastIndexOf('/') +
                        1);
                    var mockFile = {
                        name: featured_image_name,
                        size: 12345,
                        type: 'image/jpeg',
                        status: Dropzone.ADDED,
                        url: featured_image_url,
                        accepted: true
                    };

                    // Call the default addedfile event handler
                    myDropzone.emit('addedfile', mockFile);
                    //show the thumbnail of the file:
                    myDropzone.emit('thumbnail', mockFile, featured_image_url);
                    myDropzone.emit('complete', mockFile);
                    myDropzone.files.push(mockFile);
                }

                // // // Deskripsi Gambar Sampul
                document.getElementById("desc_featured_image").value = data.desc_featured_image;
            });

            if (data.content_type === 'video') {
                editor.clipboard.dangerouslyPasteHTML(data.content_pages[0].content);
            }
        }
    </script>
@endpush
