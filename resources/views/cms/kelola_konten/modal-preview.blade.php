@push('css')
    <style>
        .ql-container {
            height: calc(100% - 65px) !important;
        }
    </style>
@endpush
<div class="modal fade" tabindex="-1" id="preview_konten" style="display: none;">
    <div class="modal-dialog modal-fullscreen modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="modal-title">
                    Pratinjau Konten
                </h3>
                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                    <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                </div>
                <!--end::Close-->
            </div>

            <div class="modal-body">
                <iframe id="preview-konten" class="w-100 h-100 lozad" src="{{ asset('assets') }}/media/misc/spinner.gif" frameborder="0"></iframe>
            </div>
        </div>
    </div>
</div>
@push('js')
@endpush
