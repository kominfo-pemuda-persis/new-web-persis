@extends('cms.layouts.master')
@section('title', 'Daftar Konten')

@push('css')
    <style>
        table.dataTable.no-margin {
            margin-top: 0 !important;
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-gray-900 fw-bold flex-column justify-content-center my-0">
                        Daftar Konten
                    </h1>
                    <!--end::Title-->

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-konten.index') }}" class="text-muted text-hover-primary">
                                Kelola Kategori
                            </a>
                        </li>
                        <li class="breadcrumb-item"><span class="bullet bg-gray-500 w-5px h-2px"></span></li>
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-konten.index') }}" class="text-muted text-hover-primary">
                                Daftar Konten
                            </a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->

                <!--begin::Actions-->
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <a href="{{ route('kelola-konten.create') }}" class="btn btn-lg btn-bd-primary fw-bold">
                        <i class="ki-duotone ki-plus-square">
                            <span class="path1"></span>
                            <span class="path2"></span>
                            <span class="path3"></span>
                        </i>
                        Tambah Baru
                    </a>
                </div>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->

        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <!--Insert Content Here-->
                <div class="row g-5 g-xl-10 py-6">
                    <!--begin::Col-->
                    <div class="col">
                        <div class="card">
                            <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                                <div class="card-title">
                                    <!--begin::Search-->
                                    <div class="d-flex align-items-center position-relative my-1">
                                        <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                                            </svg>
                                        </span>
                                        <input type="text" id="search" data-kt-filter="search" class="form-control form-control-solid w-250px ps-14" placeholder="Cari" autocomplete="off" />
                                    </div>
                                </div>
                                <div class="card-toolbar flex-row-fluid justify-content-end">
                                    <div class="row w-75">
                                        <div class="col-2">
                                            <select class="form-select" data-control="select2" data-placeholder="Jenis" data-hide-search="true" data-allow-clear="true" name="filter_jenis" id="filter_jenis">
                                                <option></option>
                                                <option value="Video">Video</option>
                                                <option value="Gambar">Gambar</option>
                                                <option value="Teks">Teks</option>
                                            </select>
                                        </div>
                                        <div class="col-3">
                                            <select class="form-select" data-control="select2" data-placeholder="Kategori" data-allow-clear="true" name="filter_kategori" id="filter_kategori">
                                            </select>
                                        </div>
                                        <div class="col-3">
                                            <select class="form-select" data-control="select2" data-placeholder="Penulis" data-allow-clear="true" name="filter_penulis" id="filter_penulis">
                                            </select>
                                        </div>
                                        <div class="col-2">
                                            <select class="form-select" data-control="select2" data-placeholder="Status" data-hide-search="true" data-allow-clear="true" name="filter_status" id="filter_status">
                                                <option></option>
                                                <option value="4">Takedown</option>
                                                <option value="3">Scheduled</option>
                                                <option value="2">Archived</option>
                                                <option value="1">Publish</option>
                                                <option value="0">Draft</option>
                                            </select>
                                        </div>
                                        <div class="col-2">
                                            <select class="form-select" data-control="select2" data-placeholder="Urutan Berita" data-hide-search="true" data-allow-clear="true" name="urutan_berita" id="urutan_berita">
                                                <option></option>
                                                <option value="desc">Terbaru</option>
                                                <option value="asc">Terlama</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="card-body p-0 d-flex flex-column">
                                <table class="table table-row-bordered align-middle no-margin fs-6" id="kategori_table">
                                    <thead class="bg-light-secondary">
                                        <tr class="fw-bold fs-5">
                                            <th class="py-5 text-center">No</th>
                                            <th class="py-5">Informasi</th>
                                            <th class="py-5">Jenis</th>
                                            <th class="py-5">Tanggal Pembuatan</th>
                                            <th class="py-5">Tanggal Publikasi</th>
                                            <th class="py-5">Kategori</th>
                                            <th class="py-5">Penulis</th>
                                            <th class="py-5">Dilihat</th>
                                            <th class="py-5">Status</th>
                                            <th class="py-5 text-center">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody class="fw-semibold text-gray-600">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--end::Col-->
                </div>
            </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>
@endsection
@include('cms.kelola_konten.modal-preview')
@push('js')
    <script>
        let datatable;
        "use strict";

        var KategoriTables = function() {
            var table;

            var initDatatable = function() {
                const tableRows = table.querySelectorAll('tbody tr');

                datatable = $(table).DataTable({
                    processing: true,
                    searching: true,
                    serverSide: true,
                    bInfo: false,
                    layout: {
                        topStart: null,
                        topEnd: null,
                        bottomStart: 'pageLength',
                    },
                    language: {
                        emptyTable: 'Tidak Ada Data Konten'
                    },
                    ajax: {
                        url: "{{ route('kelola-konten.index') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        },
                        data: function(d) {
                            d.type_form = "konten_table";
                        },
                        error: function(req, err) {
                            language: {
                                emptyTable: 'Tidak Ada Data Kategori'
                            };
                            console.log('Table Error : ' + err);
                        }
                    },
                    dataType: 'JSON',
                    columnDefs: [{
                        orderable: false,
                        targets: 0
                    }],
                    order: [
                        [10, 'desc'],
                        [3, 'desc'],
                        [11, 'desc'],
                    ],
                    columns: [
                        // 0 - Number
                        {
                            name: 'DT_RowIndex',
                            data: 'DT_RowIndex',
                            searchable: false,
                            ordering: false,
                            className: 'ps-5 text-center',
                        },
                        // 1 - Title
                        {
                            name: 'title',
                            data: 'title',
                            defaultContent: '-',
                            render: function(data, type, row) {
                                url = `{{ route('kelola-konten.preview', ['id' => ':id']) }}`;
                                url = url.replace(':id', row.id);

                                var data;
                                data =
                                    `<div class="d-flex align-items-center"><div class="symbol symbol-75px me-3"><img class="object-fit-cover" src="` +
                                    row.featured_image + `" onerror="handleImage(this)"/></div>`
                                data += `<div class="d-flex justify-content-start flex-column">`
                                // if (row.is_headline === 1) {
                                //     data +=
                                //         `<p class="mb-2"><span class="badge badge-warning text-uppercase p-3"><i class="ki-duotone ki-star"></i>&nbsp;Headline</span></p>`
                                // }
                                data +=
                                    `<a href="` + url + `" target="_blank" class="text-gray-800 fw-bold mb-1 fs-6">` + row.title +
                                    `</a>`
                                data += `</div></div>`
                                return data;
                            },
                        },
                        // 2 - Content Type
                        {
                            name: 'content_type',
                            data: 'content_type',
                            defaultContent: '-',
                            className: 'text-center text-capitalize',
                        },
                        // 3 - Created At
                        {
                            name: 'created_at',
                            data: 'created_at',
                            defaultContent: '-',
                            className: 'text-center text-capitalize',
                            render: function(data, type, row) {
                                if (type === "sort" || type === "type") {
                                    return data;
                                }
                                return moment(data).format("DD-MM-YYYY HH:mm");
                            }
                        },
                        // 4 - Published At
                        {
                            name: 'published_at',
                            data: 'published_at',
                            defaultContent: '-',
                            className: 'text-center text-capitalize',
                            render: function(data, type, row) {
                                if (type === "sort" || type === "type") {
                                    return data;
                                }
                                return moment(data).format("DD-MM-YYYY HH:mm");
                            }
                        },
                        // 5 - Kategori
                        {
                            name: 'content_category_sub.category_sub_name',
                            data: 'content_category_sub.category_sub_name',
                            defaultContent: '-',
                            className: 'text-center w-100px',
                        },
                        // 6 - Penulis
                        {
                            name: 'creators.full_name',
                            data: 'creators.full_name',
                            defaultContent: '-',
                            className: 'text-center',
                        },
                        // 7 - Dilihat
                        {
                            name: 'visited',
                            data: 'visited',
                            defaultContent: '-',
                            className: 'text-center',
                        },
                        // 8 - Status
                        {
                            name: 'status',
                            data: 'status',
                            defaultContent: '-',
                            className: 'text-center',
                            render: function(data, type, row) {
                                switch (row.status) {
                                    case 1:
                                        return '<span class="badge badge-light-success fw-bold fs-8 px-2 py-1 ms-2">Publish</span>'
                                        break;
                                    case 2:
                                        return '<span class="badge badge-light-warning fw-bold fs-8 px-2 py-1 ms-2">Archived</span>'
                                        break;
                                    case 3:
                                        return '<span class="badge badge-light-info fw-bold fs-8 px-2 py-1 ms-2">Scheduled</span>'
                                        break;
                                    case 4:
                                        return '<span class="badge badge-light-danger fw-bold fs-8 px-2 py-1 ms-2">Takedown</span>'
                                        break;
                                    default:
                                        return '<span class="badge badge-light-danger fw-bold fs-8 px-2 py-1 ms-2">Draft</span>'
                                        break;
                                }
                            },
                        },
                        // 9 - Action
                        {
                            data: 'action',
                            name: 'action',
                            searchable: false,
                            className: 'pe-5 text-center',
                            render: function(data, type, row) {
                                var button
                                button =
                                    `<div class="menu menu-column menu-gray-600 menu-active-primary menu-hover-primary menu-here-primary menu-show-primary btn btn-secondary btn-icon" data-kt-menu="true"><div class="menu-item here" data-kt-menu-trigger="click" data-kt-menu-placement="left-start"><a href="#" class="menu-link"><span><i class="ki-duotone ki-dots-square-vertical"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i></span></a><div class="menu-sub menu-sub-dropdown py-2">`
                                // Pratinjau
                                button +=
                                    `<div class="menu-item"><a href="#" class="menu-link pratinjauButton" data-id="` + row.id + `"
                                    data-type="pratinjau" data-bs-toggle="modal" data-bs-target="#preview_konten">`
                                button +=
                                    `<span class="menu-icon"><i class="ki-duotone ki-eye"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i></span><span class="menu-title">Pratinjau</span></a></div>`

                                // Edit
                                button +=
                                    `<div class="menu-item"><a href="kelola-konten/edit/` + row.id +
                                    `" class="menu-link editButton" data-id="` +
                                    row.id + `">`
                                button +=
                                    `<span class="menu-icon"><i class="ki-duotone ki-pencil"><span class="path1"></span><span class="path2"></span></i></span><span class="menu-title">Edit</span></a></div>`

                                // Copy Link
                                if (row.status === 1) {
                                    button += `
                                        <div class="menu-item">
                                            <a href="#" class="menu-link editButton" onclick="copyToClipboard(encodeURIComponent('${row.content_category.category_name.toLowerCase()}'), encodeURIComponent('${row.slug}')); return false;">
                                                <span class="menu-icon"><i class="ki-duotone ki-star"></i></span>
                                                <span class="menu-title">Copy Link</span>
                                            </a>
                                        </div>`;
                                }

                                // Headline Section
                                // if (row.is_headline === 1) {
                                //     // Hapus Headline
                                //     button +=
                                //         `<div class="menu-item"><a href="#" class="menu-link changeStatus"
                            //         data-id="` + row.id +
                                //         `" data-type="headline" data-status='0' data-title_berita="` +
                                //         row.title + `">`
                                //     button +=
                                //         `<span class="menu-icon"><i class="ki-duotone ki-star"></i></span><span class="menu-title">Hapus Headline</span></a></div>`
                                // } else if (row.status === 1) {
                                //     if ({{ $headline_count }} < 4) {
                                //         // Jadikan Headline
                                //         button +=
                                //             `<div class="menu-item"><a href="#" class="menu-link changeStatus"
                            //         data-id="` + row.id + `" data-type="headline" data-status='1' data-title_berita="` +
                                //             row.title + `">`
                                //         button +=
                                //             `<span class="menu-icon"><i class="ki-duotone ki-star"></i></span><span class="menu-title">Jadikan Headline</span></a></div>`
                                //     }
                                // }
                                // Arsipkan
                                button +=
                                    `<div class="menu-item"><a href="#" class="menu-link changeStatus"
                                        data-id="` + row.id +
                                    `" data-type="status" data-status='2' data-title_berita="` +
                                    row.title + `">`
                                button +=
                                    `<span class="menu-icon"><i class="ki-duotone ki-archive"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i></span><span class="menu-title">Arsipkan</span></a></div>`

                                // Takedown
                                button +=
                                    `<div class="menu-item"><a href="#" class="menu-link changeStatus"
                                        data-id="` + row.id +
                                    `" data-type="status" data-status='4' data-title_berita="` +
                                    row.title + `">`
                                button +=
                                    `<span class="menu-icon"><i class="ki-duotone ki-information-5"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i></span><span class="menu-title">Takedown</span></a></div>`

                                // Hapus
                                if (row.status != 1) {
                                    button +=
                                        `<div class="menu-item"><a href="#" class="menu-link deleteButton"
                                        data-id="` + row.id + `" data-type="delete" data-title_berita="` + row.title +
                                        `">`
                                    button +=
                                        `<span class="menu-icon"><i class="ki-duotone ki-trash"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i></span><span class="menu-title">Hapus</span></a></div>`
                                }

                                button += `</div></div></div>`
                                return button
                            }
                        },
                        // 10 - Headline
                        {
                            name: 'is_headline',
                            data: 'is_headline',
                            visible: false,
                            searchable: 'false',
                            defaultContent: '-',
                            className: 'text-center',
                        },
                        // 11 - Updated At
                        {
                            name: 'contents.updated_at',
                            data: 'contents.updated_at',
                            visible: false,
                            searchable: 'false',
                            defaultContent: '-',
                            className: 'text-center',
                        },
                    ],
                });
            }

            var handleSearchDatatable = () => {
                const filterSearch = document.querySelector('[data-kt-filter="search"]');
                filterSearch.addEventListener('keyup', function(e) {
                    datatable.search(e.target.value).draw();
                });
            }

            return {
                init: function() {
                    table = document.querySelector('#kategori_table');

                    if (!table) {
                        return;
                    }

                    initDatatable();
                    handleSearchDatatable();

                    datatable.on("draw", function() {
                        KTMenu.createInstances();
                    });

                    $("#kategori_table_wrapper").find("div.row").addClass('px-15 pb-5');
                }
            };
        }();

        KTUtil.onDOMContentLoaded(function() {
            KategoriTables.init();
        });
    </script>

    <script>
        $(document).ready(function() {
            $("#filter_kategori").select2({
                ajax: {
                    url: "{{ route('kelola-konten.index') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        var query = {
                            _token: '{{ csrf_token() }}',
                            type_form: "filter_select",
                            select_form: "filter_kategori",
                            search: params.term,
                            page: params.page || 1
                        };

                        return query;
                    },
                    cache: true
                }
            });
            $("#filter_penulis").select2({
                ajax: {
                    url: "{{ route('kelola-konten.index') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        var query = {
                            _token: '{{ csrf_token() }}',
                            type_form: "filter_select",
                            select_form: "filter_penulis",
                            search: params.term,
                            page: params.page || 1
                        };

                        return query;
                    },
                    cache: true
                }
            });
        });
        $("#urutan_berita").on('change', function() {
            filter = $("#urutan_berita").find(":selected").val();
            datatable.order([4, filter]).draw();
        });

        $("#filter_jenis").on('change', function() {
            filter = $("#filter_jenis").find(":selected").text();
            datatable.columns(2).search(filter).draw();
        });

        $("#filter_kategori").on('change', function() {
            filter = $("#filter_kategori").find(":selected").text();
            datatable.columns(5).search('^' + filter + '$', true, true, false).draw();
        });

        $("#filter_penulis").on('change', function() {
            filter = $("#filter_penulis").find(":selected").text();
            datatable.column(6).search(filter).draw();
        });

        $("#filter_status").on('change', function() {
            filter = $("#filter_status").find(":selected").val();
            datatable.columns(8).search(filter).draw();
        });

        function copyToClipboard(category, slug) {
            category = decodeURIComponent(category);
            slug = decodeURIComponent(slug);

            var link = `{!! route('portal.detail-content', ['category' => '__CATEGORY__', 'slug' => '__SLUG__']) !!}`;
            link = link.replace('__CATEGORY__', category.toLowerCase());
            link = link.replace('__SLUG__', slug);

            navigator.clipboard.writeText(link).then(() => {
                toastr.success("Link berhasil dicopy")
                // Swal.fire({
                //     icon: 'success',
                //     title: 'Copied!',
                //     text: 'The link has been copied to clipboard.',
                //     toast: true,
                //     position: 'top-end',
                //     showConfirmButton: false,
                //     timer: 2000
                // });
            }).catch(err => {
                toastr.warning("Link gagal dicopy")
                console.error('Failed to copy: ', err);
            });
        }
    </script>

    <script>
        $(document).ready(function() {
            $('body').on('click', '.changeStatus, .deleteButton, .pratinjauButton', function() {
                var id = $(this).data('id');
                var type = $(this).data('type');
                var status = $(this).data('status');
                var title_berita = $(this).data('title_berita');

                event.preventDefault();

                var title;
                var text = "Apakah anda ingin";
                var icon;
                var url = `{{ route('kelola-konten.change-status', ['id' => ':id']) }}`;
                var method = 'POST';
                switch (type) {
                    case 'delete':
                        url = `{{ route('kelola-konten.destroy', ['id' => ':id']) }}`;
                        method = 'DELETE';

                        title = "Hapus Konten";
                        text += " menghapus konten <br /><strong>" + title_berita + "</strong>."
                        icon = "warning";
                        break;
                    case 'headline':
                        if (status) {
                            title = "Jadikan ";
                            text = text + " menjadikan <strong>" + title_berita +
                                "</strong><br /> sebagai headline."
                            icon = "question";
                        } else {
                            title = "Hapus ";
                            text += " menghapus <strong>" + title_berita + "</strong><br /> dari headline."
                            icon = "warning";
                        }

                        title += "Headline";
                        break;
                    case 'status':
                        switch (status) {
                            case 2:
                                title = "Arsipkan ";
                                text = text + " mengarsipkan konten <strong>" + title_berita + "</strong>"
                                icon = "question";
                                break;
                            case 4:
                                title = "Takedown ";
                                text = text + " mentakedown konten <strong>" + title_berita + "</strong>"
                                icon = "warning";
                                break;
                        }
                        title += "Konten";
                        break;
                    case 'pratinjau':
                        url = `{{ route('kelola-konten.preview', ['id' => ':id']) }}`;
                        url = url.replace(':id', id);
                        document.getElementById("preview-konten").src = url;
                }

                if (type != 'pratinjau') {
                    Swal.fire({
                        title: title,
                        html: text,
                        icon: icon,
                        showCancelButton: true,
                        showCloseButton: true
                    }).then((result) => {
                        if (result.isConfirmed) {
                            url = url.replace(':id', id);
                            $.ajax({
                                url: url,
                                type: 'POST',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    _method: method,
                                    type: type,
                                    status: status,
                                },
                                error: (response) => {
                                    let msg;
                                    msg = response.responseJSON.message

                                    Swal.fire({
                                        title: 'Perhatian',
                                        text: msg,
                                        icon: 'warning'
                                    })
                                },
                                success: (response) => {
                                    toastr.success(response.message)
                                    datatable.ajax.reload()
                                }
                            });
                        }
                    });
                }
            });
        });
    </script>
@endpush
