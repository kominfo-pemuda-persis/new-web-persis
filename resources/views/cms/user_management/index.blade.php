@extends('cms.layouts.master')
@section('title', 'Kelola Akun')

@push('css')
    <style>
        table.dataTable.no-margin {
            margin-top: 0 !important;
        }
    </style>
@endpush

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-gray-900 fw-bold flex-column justify-content-center my-0">
                        Kelola Akun
                    </h1>
                    <!--end::Title-->

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('kelola-kategori.index') }}" class="text-muted text-hover-primary">
                                Kelola Akun
                            </a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->

                <!--begin::Actions-->
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <a href="#" class="btn btn-lg btn-bd-primary fw-bold addButton" data-bs-toggle="modal"
                        data-bs-target="#tambah-akun">
                        <i class="ki-duotone ki-plus-square">
                            <span class="path1"></span>
                            <span class="path2"></span>
                            <span class="path3"></span>
                        </i>
                        Tambah Baru
                    </a>
                </div>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->

        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <!--Insert Content Here-->
                <div class="row g-5 g-xl-10 py-6">
                    <!--begin::Col-->
                    <div class="col">
                        <div class="card">
                            <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                                <div class="card-title">
                                    <!--begin::Search-->
                                    <div class="d-flex align-items-center position-relative my-1">
                                        <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                                <path
                                                    d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                                            </svg>
                                        </span>
                                        <input type="text" id="search" data-kt-filter="search"
                                            class="form-control form-control-solid w-250px ps-14" placeholder="Cari"
                                            autocomplete="off" />
                                    </div>
                                </div>
                            </div>

                            <div class="card-body p-0 d-flex flex-column">
                                <table class="table table-row-bordered align-middle no-margin fs-6" id="kategori_table">
                                    <thead class="bg-light-secondary">
                                        <tr class="fw-bold fs-5 text-uppercase">
                                            <th class="py-5 text-center">No</th>
                                            <th class="py-5">Nama Akun</th>
                                            <th class="py-5 text-center">Kontak</th>
                                            <th class="py-5 text-center">Role</th>
                                            <th class="py-5 text-center">Status</th>
                                            <th class="py-5 text-center">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody class="fw-semibold text-gray-600">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--end::Col-->
                </div>
            </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>
@endsection
@include('cms.user_management.modal')
@push('js')
    <script>
        let datatable;
        "use strict";

        var KategoriTables = function() {
            var table;

            var initDatatable = function() {
                const tableRows = table.querySelectorAll('tbody tr');

                datatable = $(table).DataTable({
                    processing: true,
                    searching: true,
                    ordering: false,
                    bInfo: false,
                    layout: {
                        topStart: null,
                        topEnd: null,
                        bottomStart: 'pageLength',
                    },
                    language: {
                        emptyTable: 'Tidak Ada Data Users'
                    },
                    ajax: {
                        url: "{{ route('user-management.index') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        },
                        data: function(d) {
                            d.type_form = "management_user_table";
                        },
                        error: function(req, err) {
                            language: {
                                emptyTable: 'Tidak Ada Data Users'
                            };
                            console.log('Table Error : ' + err);
                        }
                    },
                    dataType: 'JSON',
                    // rowGroup: {
                    //     className: "bg-secondary fs-5",
                    //     dataSrc: function(row) {
                    //         return '<div class="px-15 d-flex justify-content-between align-items-center">' +
                    //             '<div><span>' + row.role_name.role_name + '</span></div>'
                    //     }
                    // },
                    columns: [{
                            name: 'No.',
                            data: 'DT_RowIndex',
                            searchable: false,
                            className: 'ps-5 text-center',
                            render: function(data, type, row) {
                                if (row.role_name) {
                                    return row.DT_RowIndex;
                                } else {
                                    return '-';
                                }
                            },
                        },
                        {
                            name: 'full_name',
                            data: 'full_name',
                            defaultContent: '-',
                            render: function(data, type, row) {
                                if (row.role_name) {
                                    return row.full_name;
                                } else {
                                    return '<div class="text-center">-</div>';
                                }
                            },
                        },
                        {
                            name: 'email',
                            data: 'email',
                            defaultContent: '-',
                            searchable: false,
                            render: function(data, type, row) {
                                if (row.role_name) {
                                    var data;
                                    data = '<span><i class="bi bi-envelope-fill me-2"></i>' +
                                        row.email + '</span><br/>'
                                    if (row.phone) {
                                        data += '<span><i class="bi bi-telephone-fill me-2"></i>' +
                                            row.phone + '</span><br/>'
                                    }
                                    return data
                                } else {
                                    return '-'
                                }
                            },
                        },
                        {
                            name: 'role_name.role_name',
                            data: 'role_name.role_name',
                            defaultContent: '-',
                            searchable: false,
                            className: 'text-center',
                            render: function(data, type, row) {
                                if (row.role_name) {
                                    return row.role_name.role_name
                                } else {
                                    return '-'
                                }
                            },
                        },
                        {
                            name: 'status',
                            data: 'status',
                            defaultContent: '-',
                            searchable: false,
                            className: 'text-center',
                            render: function(data, type, row) {
                                if (row.role_name) {
                                    if (row.status === 2) {
                                        return '<span class="badge badge-light-danger fw-bold fs-8 px-2 py-1 ms-2">Banned</span>'
                                    } else if (row.status === 1) {
                                        return '<span class="badge badge-light-success fw-bold fs-8 px-2 py-1 ms-2">Aktif</span>'
                                    } else {
                                        return '<span class="badge badge-light-secondary fw-bold fs-8 px-2 py-1 ms-2">Non-Aktif</span>'
                                    };
                                } else {
                                    return '-'
                                }
                            },
                        },
                        {
                            data: 'action',
                            name: 'action',
                            searchable: false,
                            className: 'text-center',
                            render: function(data, type, row) {
                                if (row.role_name) {
                                    var button
                                    button =
                                        `<a href="#" data-toggle="tooltip" data-original-title="Edit"` +
                                        `data-id="` + row.id + `"` +
                                        `class="btn btn-icon btn-light-warning me-1 editButton"><i class="ki-duotone ki-notepad-edit"><span class="path1"></span><span class="path2"></span></i></a>`
                                    button +=
                                        `<button class="btn btn-icon btn-light-info resetPassButton" data-id=` +
                                        row.id +
                                        ` data-name="` + row.full_name +
                                        `"><i class="ki-duotone ki-key"><span class="path1"></span><span class="path2"></span></i></button>`
                                    if ({!! json_encode(@Auth::user()->id, JSON_HEX_TAG) !!} != row.id) {
                                        button +=
                                            `<button class="btn btn-icon btn-light-danger deleteButton" data-id=` +
                                            row.id +
                                            `><i class="ki-duotone ki-trash"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i></button>`
                                    }

                                    return button
                                } else {
                                    return '-'
                                }
                            }
                        },
                    ],
                });
            }

            var handleSearchDatatable = () => {
                const filterSearch = document.querySelector('[data-kt-filter="search"]');
                filterSearch.addEventListener('keyup', function(e) {
                    datatable.search(e.target.value).draw();
                });
            }

            return {
                init: function() {
                    table = document.querySelector('#kategori_table');

                    if (!table) {
                        return;
                    }

                    initDatatable();
                    handleSearchDatatable();

                    $("#kategori_table_wrapper").find("div.row").addClass('px-15 pb-5');
                }
            };
        }();

        KTUtil.onDOMContentLoaded(function() {
            KategoriTables.init();
        });
    </script>

    <script>
        $(document).ready(function() {
            $('body').on('click', '.editButton', function() {
                var id = $(this).data('id');
                var url = `{{ route('user-management.edit', ['id' => ':id']) }}`;
                url = url.replace(':id', id);

                $.get(url, function(data) {
                    $("#modal-title").html("Edit Akun");
                    $("#tambah-akun").modal("show");
                    $("#id").val(data.id);

                    $("#full_name").val(data.full_name);
                    $("#username").val(data.username);
                    $('#username').attr('readonly', true)
                    document.getElementById('username').classList.add("form-control-solid");
                    $("#email").val(data.email);
                    $('#email').attr('readonly', true)
                    document.getElementById('email').classList.add("form-control-solid");
                    $("#phone").val(data.phone);

                    $("#status").val(data.status).trigger('change');
                    var $selectedRoleOption = $("<option selected='selected'></option>")
                        .val(data.role_name.id)
                        .text(data.role_name.role_name);

                    $("#role").append($selectedRoleOption).trigger('change');
                });
            });

            $('body').on('click', '.addButton', function() {
                $("#modal-title").html("Tambah Akun");
                $("#id").val(null);

                $("#full_name").val(null);
                $("#username").val(null);
                $('#username').attr('readonly', false)
                document.getElementById('username').classList.remove("form-control-solid");
                $("#email").val(null);
                $('#email').attr('readonly', false)
                document.getElementById('email').classList.remove("form-control-solid");
                $("#phone").val(null);

                $("#role").val(null).trigger('change');
                $("#status").val(null).trigger('change');
            });

            $('body').on('click', '.deleteButton', function() {
                var id = $(this).data('id');
                event.preventDefault();

                Swal.fire({
                    title: `Hapus Data Akun`,
                    text: "Apakah Anda yakin ingin menghapus akun ini?",
                    icon: "warning",
                    showCancelButton: true,
                    showCloseButton: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        var url = `{{ route('user-management.destroy', ['id' => ':id']) }}`;
                        url = url.replace(':id', id);
                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: {
                                _token: '{{ csrf_token() }}',
                                _method: 'DELETE'
                            },
                            error: (response) => {
                                let msg;
                                if (response.responseJSON.message.includes(
                                        "SQLSTATE[23503]: Foreign key violation")) {
                                    msg =
                                        'Data memiliki Kategori, Sub-Kategori atau konten, Maka User ini tidak dapat dihapus.';
                                } else if (!msg) {
                                    msg =
                                        'Gagal menghapus data. Terjadi kesalahan pada server';
                                } else {
                                    msg = response.responseJSON.message
                                }

                                Swal.fire({
                                    title: 'Perhatian',
                                    text: msg,
                                    icon: 'warning'
                                })
                            },
                            success: (response) => {
                                datatable.ajax.reload()
                                toastr.success("Data berhasil dihapus")
                            }
                        });
                    }
                });
            });

            $('body').on('click', '.resetPassButton', function() {
                var id = $(this).data('id');
                var name = $(this).data('name');
                event.preventDefault();

                Swal.fire({
                    title: `Reset Kata Sandi`,
                    html: "Apakah Anda yakin untuk mereset kata sandi akun milik <strong>" + name +
                        "</strong> menjadi bawaan sistem?",
                    icon: "warning",
                    showCancelButton: true,
                    showCloseButton: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        var url = `{{ route('user-management.reset-password', ['id' => ':id']) }}`;
                        url = url.replace(':id', id);
                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: {
                                _token: '{{ csrf_token() }}',
                            },
                            error: (response) => {
                                let msg = response.responseJSON.message
                                if (!msg) msg =
                                    'Gagal mereset data. Terjadi kesalahan pada server'
                                Swal.fire({
                                    title: 'Perhatian',
                                    text: msg,
                                    icon: 'warning'
                                })
                            },
                            success: (response) => {
                                if (response.status === true) {
                                    swal.fire("Kata Sandi Berhasi Direset", response
                                        .message,
                                        "success");
                                } else {
                                    swal.fire("Error!", response.message, "error");
                                }
                            }
                        });
                    }
                });
            });

            @if (Session::has('success-create-user'))
                Swal.fire({
                    title: `Akun Berhasil Dibuat`,
                    html: "Anda berhasil membuat akun baru <strong>" +
                        {!! json_encode(session('success-create-user'), JSON_HEX_TAG) !!} +
                        "</strong> dengan kata sandi <strong>jamiyyahIslamiyyah</strong>",
                    icon: "success",
                    showCloseButton: true
                });
            @endif
        });
    </script>
@endpush
