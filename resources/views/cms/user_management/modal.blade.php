<div class="modal fade" tabindex="-1" id="tambah-akun" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="modal-title">
                    Tambah Akun
                </h3>
                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                    aria-label="Close">
                    <i class="ki-duotone ki-cross fs-1"><span class="path1"></span><span class="path2"></span></i>
                </div>
                <!--end::Close-->
            </div>

            <div class="modal-body">
                <form action="{{ route('user-management.store') }}" method="post" id="kelola_akun">
                    @csrf
                    <input type="hidden" class="form-control" id="id" name="id" />
                    <input type="hidden" name="type" value="createedit_modal_user" />
                    <div class="row g-5">
                        <div class="col-12">
                            <h4>Nama Akun</h4>
                            <input type="text" class="form-control" name="full_name" id="full_name"
                                placeholder="Nama Akun" required />
                        </div>
                        <div class="col-12">
                            <h4>Username</h4>
                            <input type="text" class="form-control" name="username" id="username"
                                placeholder="Username" required />
                        </div>
                        <div class="col-12 fv-row">
                            <h4>Email</h4>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email"
                                required />
                        </div>
                        <div class="col-12">
                            <h4>Nomor Telepon</h4>
                            <input type="tel" class="form-control" name="phone" id="phone"
                                placeholder="Nomor Telepon" />
                        </div>
                        <div class="separator mt-5"></div>
                        {{-- <div class="col-12">
                            <h4>Password</h4>
                        <input type="password" class="form-control" name="password" id="password"
                            placeholder="Password" />
                        </div> --}}
                        <div class="col-12">
                            <h4>Role</h4>
                            <select class="form-select" data-control="select2" data-placeholder="Pilih Role"
                                name="role" id="role">
                            </select>
                        </div>
                        <div class="col-12">
                            <h4>Status Akun</h4>
                            {{-- <select class="form-select" data-control="select2" data-placeholder="Pilih Status"
                                data-hide-search="true" name="status" id="status" required>
                                <option value="2">Banned</option>
                                <option value="1">Aktif</option>
                                <option value="0">Tidak Aktif</option>
                            </select> --}}
                            <select class="form-select" data-control="select2" data-placeholder="Pilih Status"
                                data-hide-search="true" name="status">
                                <option></option>
                                <option value="2">Banned</option>
                                <option value="1" selected>Aktif</option>
                                <option value="0">Tidak Aktif</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-bd-primary" form="kelola_akun" id="simpan_button">Simpan</button>
            </div>
        </div>
    </div>
</div>
@push('js')
    <script>
        $(document).ready(function() {
            $("#role").select2({
                dropdownParent: $('#tambah-akun'),
                ajax: {
                    url: "{{ route('user-management.index') }}",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        var query = {
                            _token: '{{ csrf_token() }}',
                            search: params.term,
                            page: params.page || 1
                        };

                        return query;
                    },
                    cache: true
                }
            });

            $(function() {
                const form = document.getElementById('kelola_akun');
                var validator = FormValidation.formValidation(
                    form, {
                        fields: {
                            'email': {
                                validators: {
                                    emailAddress: {
                                        message: 'Bukan merupakan alamat surel yang valid'
                                    },
                                    notEmpty: {
                                        message: 'Alamat Surel dibutuhkan'
                                    }
                                }
                            },
                        },

                        plugins: {
                            trigger: new FormValidation.plugins.Trigger(),
                            bootstrap: new FormValidation.plugins.Bootstrap5({
                                rowSelector: '.fv-row',
                                eleInvalidClass: '',
                                eleValidClass: ''
                            })
                        }
                    }
                );
            });

            $(function() {
                $("input[type='tel']").on('input', function(e) {
                    $(this).val($(this).val().replace(/[^0-9]/g, ''));
                });
            });
        });
    </script>
@endpush
