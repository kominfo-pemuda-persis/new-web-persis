@extends('cms.layouts.master')
@section('title', 'Atur Profil')

@push('css')
@endpush

@section('content')
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-gray-900 fw-bold flex-column justify-content-center my-0">
                        Atur Profile
                    </h1>
                    <!--end::Title-->

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('cms.index') }}" class="text-muted text-hover-primary">Home</a>
                        </li>
                        <li class="breadcrumb-item"><span class="bullet bg-gray-500 w-5px h-2px"></span></li>
                        <li class="breadcrumb-item text-muted">Kelola Profil</li>
                        <li class="breadcrumb-item"><span class="bullet bg-gray-500 w-5px h-2px"></span></li>
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('user-management.atur-profile-password', ['value' => @$value]) }}"
                                class="text-muted text-hover-primary">
                                Atur Profil
                            </a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->

        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <!--Insert Content Here-->
                <!--begin::Row-->
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('user-management.store') }}" method="post" id="kelola_akun"
                            enctype="multipart/form-data" novalidate>
                            @csrf
                            <input type="hidden" name="id" value="{{ Auth::user()->id }}" />
                            <input type="hidden" name="type" value="{{ @$value }}" />
                            @if (@$value === 'profile')
                                <div class="row g-5">
                                    <div class="col-2">
                                        <h6>
                                            Foto Profil
                                        </h6>
                                        <img id="imagePreview" class="img-fluid img-thumbnail w-100"
                                            src="{{ Auth::user()->photo ?? Avatar::create(@Auth::user()->full_name)->toBase64() }}"
                                            alt="" />
                                        <input type="file" class="d-none" accept="image/*" id="photo" name="photo"
                                            onchange="previewImage(event)">
                                        <button type="button" class="w-100 btn btn-light-success"
                                            onclick="document.getElementById('photo').click()">
                                            Ubah Profile Picture
                                        </button>
                                    </div>
                                    <div class="col-10">
                                        <div class="row g-5">
                                            <div class="col-12">
                                                <h6>Nama Akun</h6>
                                                <input type="text" class="form-control" name="full_name" id="full_name"
                                                    placeholder="Nama Akun" value="{{ Auth::user()->full_name }}"
                                                    required />
                                            </div>
                                            <div class="col-12">
                                                <h6>Username</h6>
                                                <input type="text" class="form-control" name="username" id="username"
                                                    placeholder="Username" value="{{ Auth::user()->username }}" required />
                                            </div>
                                            <div class="col-12 fv-row">
                                                <h6>Email</h6>
                                                <input type="email" class="form-control" name="email" id="email"
                                                    placeholder="Email" value="{{ Auth::user()->email }}" required />
                                            </div>
                                            <div class="col-12">
                                                <h6>Nomor Telepon</h6>
                                                <input type="tel" class="form-control" name="phone" id="phone"
                                                    placeholder="Nomor Telepon" value="{{ Auth::user()->phone }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="mb-10">
                                    <h1>Ubah Kata Sandi</h1>
                                    <h4 class="text-gray-500">
                                        Untuk keamanan akun Anda, mohon untuk tidak menyebarkan kata sandi Anda ke orang
                                        lain.
                                    </h4>
                                </div>
                                <div class="row g-5">
                                    <div class="col-12">
                                        <h6>Kata Sandi Lama</h6>
                                        <input type="password" class="form-control" name="old_password" id="old_password"
                                            placeholder="Kata Sandi Lama" required />
                                    </div>
                                    <div class="col-12 fv-row">
                                        <h6>Kata Sandi Baru</h6>
                                        <div class="fv-row" data-kt-password-meter="true">
                                            <div class="mb-1">
                                                <div class="position-relative mb-3">
                                                    <input class="form-control bg-transparent" type="password"
                                                        placeholder="Kata Sandi Baru" name="password" id="password"
                                                        autocomplete="off" required />
                                                    <span
                                                        class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2"
                                                        data-kt-password-meter-control="visibility">
                                                        <i class="bi bi-eye-slash fs-2"></i>
                                                        <i class="bi bi-eye fs-2 d-none"></i>
                                                    </span>
                                                </div>
                                                <div class="d-flex align-items-center mb-3"
                                                    data-kt-password-meter-control="highlight">
                                                    <div
                                                        class="flex-grow-1 bg-secondary bg-active-danger rounded h-5px me-2">
                                                    </div>
                                                    <div
                                                        class="flex-grow-1 bg-secondary bg-active-warning rounded h-5px me-2">
                                                    </div>
                                                    <div
                                                        class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2">
                                                    </div>
                                                    <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-muted">
                                                Gunakan 8 karakter atau lebih dengan campuran huruf, angka, dan simbol.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <h6>Ulangi Kata Sandi Baru</h6>
                                        <div class="fv-row mb-8">
                                            <input type="password" placeholder="Ulangi Kata Sandi Baru"
                                                name="confirm_password" id="confirm_password" autocomplete="off"
                                                class="form-control bg-transparent" required />
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </form>
                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <a href="{{ URL::previous() }}" class="btn btn-light me-5">Kembali</a>
                        <button type="submit" class="btn btn-bd-primary" form="kelola_akun" id="simpan_button"
                            data-id="{{ $value }}">Simpan</button>
                    </div>
                </div>
                <!--end::Row-->
            </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>
@endsection
@push('js')
    <script>
        function previewImage(event) {
            const input = event.target;
            const preview = input.previousElementSibling; // Adjust based on your structure

            if (input.files && input.files[0]) {
                const reader = new FileReader();

                reader.onload = function(e) {
                    preview.src = e.target.result;
                };

                reader.readAsDataURL(input.files[0]);
            }
        };

        $(document).ready(function() {
            $(function() {
                const form = document.getElementById('kelola_akun');
                var validator = FormValidation.formValidation(
                    form, {
                        fields: {
                            'email': {
                                validators: {
                                    emailAddress: {
                                        message: 'Bukan merupakan alamat surel yang valid'
                                    },
                                    notEmpty: {
                                        message: 'Alamat Surel dibutuhkan'
                                    }
                                }
                            },
                        },

                        plugins: {
                            trigger: new FormValidation.plugins.Trigger(),
                            bootstrap: new FormValidation.plugins.Bootstrap5({
                                rowSelector: '.fv-row',
                                eleInvalidClass: '',
                                eleValidClass: ''
                            })
                        }
                    }
                );
            });

            $(function() {
                $("input[type='tel']").on('input', function(e) {
                    $(this).val($(this).val().replace(/[^0-9]/g, ''));
                });
            });

            $(function() {
                const form = document.getElementById('kelola_akun');
                var validator = FormValidation.formValidation(
                    form, {
                        fields: {
                            'password': {
                                validators: {
                                    notEmpty: {
                                        message: 'Kata Sandi Diperlukan'
                                    },
                                    callback: {
                                        message: 'Tolong Masukan Katasandi yang sesuai',
                                        callback: function(input) {
                                            if (input.value.length > 0) {
                                                return validatePassword();
                                            }
                                        }
                                    }
                                }
                            },
                            'confirm-password': {
                                validators: {
                                    notEmpty: {
                                        message: 'Kolom Ulangi Kata Sandi Diperlukan'
                                    },
                                    identical: {
                                        compare: function() {
                                            return form.querySelector('[name="password"]').value;
                                        },
                                        message: 'Kolom Kata Sandi Baru dan Ulangi Kata Sandi tidak sama'
                                    }
                                }
                            },
                        },
                        plugins: {
                            trigger: new FormValidation.plugins.Trigger({
                                event: {
                                    password: false
                                }
                            }),
                            bootstrap: new FormValidation.plugins.Bootstrap5({
                                rowSelector: '.fv-row',
                                eleInvalidClass: '', // comment to enable invalid state icons
                                eleValidClass: '' // comment to enable valid state icons
                            })
                        }
                    }
                );
            });
        });

        $('body').on('click', '#simpan_button', function() {
            var id = $(this).data('id');
            const form = document.getElementById("kelola_akun");
            event.preventDefault();
            if (form.checkValidity()) {
                Swal.fire({
                    title: "Simpan data {{ $value }} baru",
                    html: "Apakah anda yakin untuk menyimpan perubahan <strong>{{ $value }}</strong>?",
                    icon: "warning",
                    showCancelButton: true,
                    showCloseButton: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        console.log()
                        if ($('[name=type]').val() === 'password') {
                            if ($('#password').val() === $('#confirm_password').val()) {
                                return form.submit();
                            } else {
                                return Swal.fire({
                                    title: "Kata Sandi Baru tidak sesuai",
                                    text: "Kata sandi baru dan ulangi kata sandi baru tidak sesuai!",
                                    icon: "error"
                                });
                            }
                        }
                        form.submit();
                    }
                });
            }
        });
    </script>
@endpush
