<!DOCTYPE html>
<html lang="en">

<head>
    <base href="/" />
    <title>Login | CMS {{ config('app.name')() }}</title>
    <meta charset="utf-8" />
    <meta name="description" content="Pemuda Persatuan Islam" />
    <meta name="keywords" content="pemuda persatuan islam, persis, pemuda persis, someah" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <meta property="og:locale" content="id" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Pemuda Persatuan Islam" />

    <meta property="og:url" content="{{ url('') }}" />
    <meta property="og:site_name" content="{{ url('') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="canonical" href="{{ url('') }}" />

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets') }}/media/icon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets') }}/media/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets') }}/media/icon/favicon-16x16.png">
    <link rel="manifest" href="{{ asset('assets') }}/media/icon/site.webmanifest">
    <link rel="shortcut icon" href="{{ asset('assets') }}/media/icon/favicon.ico">

    <!--begin::Fonts(mandatory for all pages)-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
    <!--end::Fonts-->

    <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
    <link href="{{ asset('assets') }}/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets') }}/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->

    <style>
        body {
            background-color: #F7F8FA;
            background-image: url("{{ asset('assets') }}/media/backgrounds/bg-login.png");
            background-size: cover;
        }

        .btn-bd-primary {
            --bs-btn-font-weight: 600;
            --bs-btn-color: var(--bs-white);
            --bs-btn-bg: #065235;
            --bs-btn-border-color: #065235;
            --bs-btn-border-radius: .5rem;
            --bs-btn-hover-color: var(--bs-white);
            --bs-btn-hover-bg: #054c2d;
            --bs-btn-hover-border-color: #054c2d;
            --bs-btn-focus-shadow-rgb: var(--bd-violet-rgb);
            --bs-btn-active-color: var(--bs-btn-hover-color);
            --bs-btn-active-bg: #043e24;
            --bs-btn-active-border-color: #043e24;
        }
    </style>
</head>

<body id="kt_body">
    <div class="d-flex flex-column flex-root" id="kt_app_root">
        <div class="d-flex flex-column flex-lg-row flex-column-fluid">
            <div class="d-flex flex-column flex-lg-row-fluid w-lg-50 p-10 order-2 order-lg-1">
                <div class="d-flex flex-center flex-column flex-lg-row-fluid">
                    <div class="col-12 text-center mt-5 mb-10">
                        <img src="{{ asset('assets') }}/assets-landing/images/logo-web-persis-light.svg" class="rounded mh-100px">
                    </div>
                    <div class="w-lg-650px p-10 bg-white rounded">
                        <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" data-kt-redirect-url="{{ route('cms.index') }}" action="{{ route('login') }}" method="POST">
                            @csrf
                            <center>
                                <h1 class="fw-bolder text-gray-900 mb-3">
                                    Masuk ke CMS {{ config('app.name')() }}
                                </h1>
                                <div class="fw-semibold fs-6 text-gray-500 mb-10">
                                    Silakan masukkan email dan kata sandi untuk melanjutkan.
                                </div>
                            </center>
                            <span class="fw-bold fs-5 text-gray-600">Email</span>
                            <div class="fv-row mb-10 mt-1">
                                <input type="text" placeholder="Masukan Email" name="email" autocomplete="off" class="form-control bg-transparent" />
                            </div>
                            <span class="fw-bold fs-5 text-gray-600">Kata Sandi</span>
                            <div class="fv-row mb-9 mt-1 input-group">
                                <!--begin::Password-->
                                <input type="password" placeholder="Masukan Kata Sandi" name="password" id="password" autocomplete="off" class="form-control bg-transparent border-right-0" />
                                <span class="input-group-text bg-transparent border-left-0">
                                    <i class="fs-2 bi bi-eye" id="togglePassword" style="cursor: pointer"></i>
                                </span>
                                <!--end::Password-->
                            </div>
                            <!--end::Input group=-->
                            <!--begin::Submit button-->
                            <div class="d-grid mb-10">
                                <button type="submit" id="kt_sign_in_submit" class="btn btn-bd-primary">
                                    <!--begin::Indicator label-->
                                    <span class="indicator-label">Sign In</span>
                                    <!--end::Indicator label-->
                                    <!--begin::Indicator progress-->
                                    <span class="indicator-progress">Please wait...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    <!--end::Indicator progress-->
                                </button>
                            </div>
                            <!--end::Submit button-->
                        </form>
                        <!--end::Form-->
                        <div class="text-gray-500 text-center fw-semibold fs-6">
                            Untuk mendaftar, silakan Hubungi
                            <a href="#" class="text-decoration-underline" style="color: #09734A">Admin IT - PP PERSIS</a>
                        </div>
                        <div class="text-gray-500 text-center fw-semibold fs-9 mt-5">
                            Versi Aplikasi {{ env('BUILD_VERSION') }}
                        </div>
                    </div>
                    <div class="text-gray-500 text-center fw-semibold fs-6 mt-10">
                        ©2024 - Pimpinan Pusat Persatuan Islam
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Form-->
            </div>
            <!--end::Body-->
        </div>
    </div>

    <script>
        var hostUrl = "{{ asset('assets') }}/";
    </script>

    <script src="{{ asset('assets') }}/plugins/global/plugins.bundle.js"></script>
    <script src="{{ asset('assets') }}/js/scripts.bundle.js"></script>

    <script src="{{ asset('assets') }}/js/custom/authentication/sign-in/general.js"></script>
    <script>
        const togglePassword = document.querySelector("#togglePassword");
        const password = document.querySelector("#password");

        togglePassword.addEventListener("click", function() {
            const type = password.getAttribute("type") === "password" ? "text" : "password";
            password.setAttribute("type", type);
            // toggle the eye icon
            this.classList.toggle('bi-eye');
            this.classList.toggle('bi-eye-slash');
        });
    </script>
</body>

</html>
