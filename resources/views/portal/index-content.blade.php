@extends('portal.layouts.portal')

@section('index-content')
    @if ($latest_video_content->count() > 0)
        <section class="py-4 py-lg-5" id="video-content">
            <div class="container">
                <div class="section-title mb-4">
                    <h3>KONTEN VIDEO</h3>
                    <div class="line">
                        <div class="yellow-line"></div>
                        <div class="base-line"></div>
                    </div>
                </div>
                <div class="section-content row row-gap-4">
                    @foreach ($latest_video_content as $video)
                        <div class="col-12 col-sm-4">
                            <div class="card-content">
                                <div class="card-image">
                                    <a class=""
                                        href="{{ route('portal.detail-content', ['category' => $video->content_category_sub->content_categories->slug, 'slug' => $video->slug]) }}">
                                        <img class="object-fit-cover h-100" src="{{ $video->featured_image }}"
                                            onerror="handleImage(this)" />
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="card-info mb-2">
                                        <div>
                                            <span
                                                class="badge rounded-pill bg-brand text-white">{{ $video->content_category_sub->category_sub_name }}</span>
                                        </div>
                                        <p class="text-secondary">
                                            {{ \Carbon\Carbon::parse($video->published_at)->isoFormat('DD MMM YYYY | HH:mm') }}
                                        </p>
                                    </div>
                                    <a
                                        href="{{ route('portal.detail-content', ['category' => $video->content_category_sub->content_categories->slug, 'slug' => $video->slug]) }}">
                                        <h3 class="card-title">
                                            {{ $video->title }}
                                        </h3>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    @if ($latest_image_content->count() > 0)
        <section class="py-4 py-lg-5" id="image-content">
            <div class="container">
                <div>
                    <div class="section-title mb-4">
                        <h3>KONTEN GAMBAR</h3>
                        <div class="line">
                            <div class="yellow-line"></div>
                            <div class="base-line"></div>
                        </div>
                    </div>
                    <div class="section-content row row-gap-3 row-gap-md-4 row-cols-2 row-cols-lg-4">
                        @foreach ($latest_image_content as $images)
                            <div class="col">
                                <div class="content-image">
                                    <a class=""
                                        href="{{ route('portal.detail-content', ['category' => $images->content_category_sub->content_categories->slug, 'slug' => $images->slug]) }}">
                                        <img class="object-fit-cover h-100" src="{{ $images->featured_image }}"
                                            onerror="handleImage(this)" />
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection
