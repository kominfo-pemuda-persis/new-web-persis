@extends('portal.layouts.master')
@section('title', strip_tags($content->title))

@section('extras-meta')
    <meta name="title" content="{{ strip_tags($content->title) }}" />
    <meta name="description" content="{{ $content->desc_seo }}" />
    @php
        $tagNames = $tags->pluck('title')->toArray();
    @endphp
    <meta name="keywords" content="{{ implode(', ', $tagNames) }}" />

    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:site_name" content="{{ config('app.name')() }}" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:title" content="{{ strip_tags($content->title) }}" />
    <meta property="og:description" content="{{ $content->desc_seo }}" />
    <meta property="og:image" content="{{ $content->featured_image }}" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="300" />
    <meta property="og:image:height" content="300" />
@endsection

@push('css')
    @if ($content->content_type == 'gambar')
        <link rel="stylesheet" href="{{ asset('assets') }}/assets-landing/styles/carousel.css" />
    @endif
@endpush

@section('content')
    <nav class="list-breadcrumb mb-4" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('portal') }}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('portal.category', ['category' => $content->content_category_sub->content_categories->slug]) }}">
                    {{ $content->content_category_sub->content_categories->category_name }} </a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">
                <a href="{{ route('portal.sub-category', ['category' => $content->content_category_sub->content_categories->slug, 'categorySub' => $content->content_category_sub->slug]) }}">
                    {{ $content->content_category_sub->category_sub_name }} </a>
            </li>
        </ol>
    </nav>
    <div class="content-detail mb-4">
        <h1>
            {!! $content->title !!}
        </h1>
        <span class="d-flex flex-wrap gap-2">
            <p>
                oleh <a class="no-click" onclick="return false">{{ $content->reporters->reporter_name }}</a>
            </p>
            •
            <p>{{ \Carbon\Carbon::parse($content->published_at)->isoFormat('DD MMMM YYYY | HH:mm') }}</p>
            </sp>
    </div>

    @if ($content->content_type == 'teks' || $content->content_type == 'tanya-jawab')
        <div class="thumbnail mb-4">
            <figure>
                <img src="{{ $content->featured_image }}" class="w-100" alt="" onerror="handleImage(this)" />
                <figcaption>
                    {{ $content->desc_featured_image }}
                </figcaption>
            </figure>
        </div>

        @foreach ($content_pages as $page)
            @if (!empty($page->advertisement_id) && $page->advertisement_position == 'top')
                <div class="w-100 mb-4 widget-content rounded">
                    <a href="{{ $page->advertisement->url_ads }}" target="_blank">
                        <img class="" src="{{ $page->advertisement->image }}" alt="{{ $page->advertisement->desc }}">
                    </a>
                </div>
            @endif

            <div class="text-content ql-editor p-0 border-0" data-gramm="false" contenteditable="false">

                {!! $page->content !!}

                @if (!empty($page->advertisement_id) && $page->advertisement_position == 'middle')
                    <div class="w-100 my-4 widget-content rounded">
                        <a href="{{ $page->advertisement->url_ads }}" target="_blank">
                            <img class="" src="{{ $page->advertisement->image }}" alt="{{ $page->advertisement->desc }}">
                        </a>
                    </div>
                @endif

                @if (!empty($page->related_content))
                    <blockquote>
                        <span>BACA JUGA:</span>
                        <a href="{{ route('portal.detail-content', ['category' => $page->relateds->content_category_sub->content_categories->slug, 'slug' => $page->relateds->slug]) }}">
                            {!! $page->relateds->title !!}
                        </a>
                    </blockquote>
                @endif

                @if (!empty($page->photo))
                    <figure class="mb-4">
                        <img src="{{ $page->photo }}" class="w-100" alt="" onerror="handleImage(this)" />
                        <figcaption>
                            {{ $page->photo_desc }}
                        </figcaption>
                    </figure>
                @endif
            </div>

            @if (!empty($page->advertisement_id) && $page->advertisement_position == 'bottom')
                <div class="w-100 mb-4 widget-content rounded">
                    <a href="{{ $page->advertisement->url_ads }}" target="_blank">
                        <img class="" src="{{ $page->advertisement->image }}" alt="{{ $page->advertisement->desc }}">
                    </a>
                </div>
            @endif
        @endforeach

        @if ($content->display == 'pages')
            <div class="d-flex justify-content-center pagination-area mb-4">
                {{ $content_pages->links('pagination::bootstrap-4') }}
            </div>
        @endif
    @endif

    @if ($content->content_type == 'gambar')
        <div class="area-image">
            <div class="carousel carousel-main" data-flickity='{"pageDots": false }'>
                @foreach ($content_pages as $page)
                    <div class="carousel-cell">
                        <img src="{{ $page->photo }}" />
                        <div class="caption">
                            {!! $page->content !!}
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="carousel carousel-nav" data-flickity='{ "asNavFor": ".carousel-main", "contain": true, "pageDots": false }'>
                @foreach ($content_pages as $page)
                    <div class="carousel-cell">
                        <img src="{{ $page->photo }}" />
                    </div>
                @endforeach
            </div>
        </div>
    @endif

    @if ($content->content_type == 'video')
        @foreach ($content_pages as $page)
            <div class="area-video mb-4">
                {!! $page->embeded_string !!}
            </div>

            <div class="text-content ql-editor p-0 border-0" data-gramm="false" contenteditable="false">
                {!! $page->content !!}
            </div>
        @endforeach
    @endif

    <div class="contributor-area">
        <span>Reporter: <a class="no-click" onclick="return false">{{ $content->reporters->reporter_name }}</a></span>
        <span>Editor: <a class="no-click" onclick="return false">{{ $content->creators->full_name }}</a></span>
    </div>

    <div class="share-area mb-4 gap-2">
        <p class="flex-grow-1">Bagikan:</p>
        <div class="share-socmed">
            <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current()) }}" class="btn  btn-box rounded btn-facebook" target="_blank" rel="noopener noreferrer">
                <i class="bi bi-facebook"></i>
            </a>
            <a href="https://twitter.com/intent/tweet?url={{ urlencode(url()->current()) }}" class="btn  btn-box rounded btn-x" target="_blank" rel="noopener noreferrer">
                <i class="bi bi-twitter-x"></i>
            </a>
            <a href="https://api.whatsapp.com/send?text={{ urlencode(url()->current()) }}" class="btn  btn-box rounded btn-whatsapp" target="_blank" rel="noopener noreferrer">
                <i class="bi bi-whatsapp"></i>
            </a>
            <a href="#" class="btn rounded btn-clipboard" id="copyLinkButton">
                <i class="bi bi-link-45deg"></i>
                <span class="d-none d-sm-inline">Bagikan Tautan</span>
            </a>

            <div class="toast toast-copy position-absolute m-3 bottom-0 start-50 translate-middle-x align-items-center border-0 z-3" id="copyToast" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="d-flex">
                    <div class="toast-body">
                        Tautan telah disalin!
                    </div>
                    <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
            </div>
        </div>
    </div>

    <div class="tags-area mb-4">
        <a class="mb-2">Tags</a>
        <div class="d-flex flex-wrap gap-2 row-gap-2">
            @foreach ($tags as $item)
                <a class="btn btn-tags" href="{{ route('portal.tags', ['slug' => $item->slug]) }}">{{ $item->title }}</a>
            @endforeach
        </div>
    </div>

    @if ($content_relevant->count() > 0)
        <section class="related-area">
            <div class="section-title mb-4">
                <h3>BERITA TERKAIT</h3>
                <div class="line">
                    <div class="yellow-line"></div>
                    <div class="base-line"></div>
                </div>
            </div>
            <div class="section-content">
                @foreach ($content_relevant as $relevant)
                    <article class="news-content">
                        <div class="news-image">
                            <a href="{{ route('portal.detail-content', ['category' => $relevant->content_category_sub->content_categories->slug, 'slug' => $relevant->slug]) }}">
                                <img class="object-fit-cover h-100 w-100" src="{{ $relevant->featured_image }}" onerror="handleImage(this)" />
                            </a>
                        </div>
                        <div class="news-detail">
                            <div class="news-info mb-2">
                                <div>
                                    <span class="badge rounded-pill bg-brand text-white">{{ $relevant->content_category_sub->category_sub_name }}</span>
                                </div>
                                <p class="text-secondary">
                                    {{ \Carbon\Carbon::parse($relevant->published_at)->isoFormat('DD MMMM YYYY | HH:mm') }}
                                </p>
                            </div>
                            <a href="{{ route('portal.detail-content', ['category' => $relevant->content_category_sub->content_categories->slug, 'slug' => $relevant->slug]) }}">
                                <h3 class="news-title">
                                    {!! $relevant->title !!}
                                </h3>
                            </a>
                        </div>
                    </article>
                @endforeach
            </div>
        </section>
    @endif
@endsection

@push('bottom-js')
    <script>
        $(document).ready(function() {
            $("#copyLinkButton").click(function() {
                event.preventDefault();
                var link = window.location.href;

                navigator.clipboard.writeText(link);
                $("#copyToast").toast("show");
            });
        });
    </script>

    @if (!$iklanPopUp->advertisement->isEmpty())
        @include('portal.layouts.partials.pop-up-ads')
        <script>
            $(window).on('load', function() {
                $('#pop-up').modal('show');
            });
        </script>
    @endif
@endpush
