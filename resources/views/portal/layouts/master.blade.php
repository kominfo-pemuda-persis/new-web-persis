<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />

    <title>@yield('title') - {{ config('app.name')() }}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1" />

    @yield('extras-meta')

    {{-- <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets') }}/media/icon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets') }}/media/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets') }}/media/icon/favicon-16x16.png">
    <link rel="manifest" href="{{ asset('assets') }}/media/icon/site.webmanifest"> --}}
    <link rel="shortcut icon" href="{{ $webInfos->web_icon }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/assets-landing/libraries/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/assets-landing/libraries/bootstrap-icon/bootstrap-icons.min.css" />

    <link rel="stylesheet" href="{{ asset('assets') }}/assets-landing/styles/quill.snow.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/assets-landing/styles/main.css?v=1.0.0" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/assets-landing/libraries/flickity/flickity.min.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/assets-landing/styles/carousel-widget.css" />

    @stack('css')

    <script>
        function handleImage(image) {
            image.onerror = "";
            image.src = '{{ asset('assets/assets-landing/images/placeholder.png') }}';
            return true;
        }
    </script>

    <script async src="https://www.googletagmanager.com/gtag/js?id={{ config('services.google_analytics.measurement_id') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', '{{ config('services.google_analytics.measurement_id') }}');
    </script>

    @stack('js')

</head>

<body>
    <div class="content-area">
        @include('portal.layouts.partials.navbar')

        <div class="body-content">
            <div>
                <div class="portal-sticky">
                    <!-- Area Iklan Sticky  Kiri -->
                    <div class="sticky-widget d-none d-xxl-block">
                        @if ($iklanStickyKiri && $iklanStickyKiri->advertisement && $iklanStickyKiri->advertisement->isNotEmpty())
                            @foreach ($iklanStickyKiri->advertisement as $advertisement)
                                <a class="w-100 d-block" href="{{ $advertisement->url_ads }}" target="_blank">
                                    <img class="w-100 object-fit-cover rounded-2" src="{{ $advertisement->image }}" alt="{{ $advertisement->desc }}" />
                                </a>
                            @endforeach
                        @endif
                    </div>

                    <div class="flex-fill">
                        <!-- Area Iklan Atas -->
                        @if ($iklanHeader && $iklanHeader->advertisement && $iklanHeader->advertisement->isNotEmpty())
                            <div class="container widget pt-3 pt-lg-4 text-center">
                                <div class="carousel carousel-widget">
                                    @foreach ($iklanHeader->advertisement as $advertisement)
                                        <div class="carousel-cell">
                                            <a class="w-100 d-block" href="{{ $advertisement->url_ads }}" target="_blank">
                                                <img class="rounded-2" src="{{ $advertisement->image }}" alt="{{ $advertisement->desc }}" />
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                        <!-- Konten Area dengan Aside -->
                        <main class="container py-3 py-lg-4" id="content">
                            <div class="row row-gap-4">
                                <div class="col">
                                    @yield('content')
                                </div>

                                <!-- Section: Aside -->
                                @include('portal.layouts.partials.aside')
                            </div>

                        </main>
                    </div>

                    <!-- Area Iklan Sticky Kanan -->
                    <div class="sticky-widget d-none d-xxl-block">
                        @if ($iklanStickyKanan && $iklanStickyKanan->advertisement && $iklanStickyKanan->advertisement->isNotEmpty())
                            @foreach ($iklanStickyKanan->advertisement as $advertisement)
                                <a class="w-100 d-block" href="{{ $advertisement->url_ads }}" target="_blank">
                                    <img class="w-100 object-fit-cover rounded-2" src="{{ $advertisement->image }}" alt="{{ $advertisement->desc }}" />
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>

                <!-- Iklan Bawah -->
                @if ($iklanFooter && $iklanFooter->advertisement && $iklanFooter->advertisement->isNotEmpty())
                    <div class="container widget pb-3 pb-lg-4 text-center">
                        <div class="carousel carousel-widget">
                            @foreach ($iklanFooter->advertisement as $advertisement)
                                <div class="carousel-cell">
                                    <a class="w-100 d-block" href="{{ $advertisement->url_ads }}" target="_blank">
                                        <img class="rounded-2" src="{{ $advertisement->image }}" alt="{{ $advertisement->desc }}" />
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif

                <!-- Footer -->
                @include('portal.layouts.partials.footer')

            </div>
        </div>

        <script src="{{ asset('assets') }}/assets-landing/libraries/jquery/jquery-3.7.1.min.js"></script>
        <script src="{{ asset('assets') }}/assets-landing/libraries/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="{{ asset('assets') }}/assets-landing/scripts/main.js"></script>
        <script src="{{ asset('assets') }}/assets-landing/libraries/flickity/flickity.pkgd.min.js"></script>

        <script>
            var $carousel = $('.carousel.carousel-widget').flickity({
                pageDots: false,
                prevNextButtons: false,
                autoPlay: true,
                pauseAutoPlayOnHover: false
            });

            var flkty = $carousel.data('flickity');

            $carousel.on('click', function(event, pointer) {
                $carousel.flickity('playPlayer');
            });

            $carousel.on('dragend', function(event, pointer) {
                $carousel.flickity('playPlayer');
            });

            function truncateText(element, maxLines) {
                const lineHeight = parseFloat(window.getComputedStyle(element).lineHeight);
                const maxHeight = lineHeight * maxLines;

                if (element.scrollHeight > maxHeight) {
                    let words = element.textContent.split(' ');
                    let truncated = '';

                    element.textContent = '';
                    for (let word of words) {
                        element.textContent += word + ' ';
                        if (element.scrollHeight >= maxHeight) {
                            element.textContent = truncated.trim() + '...';
                            break;
                        }
                        truncated = element.textContent;
                    }
                }
            }

            //potong title
            document.querySelectorAll('.simple-title').forEach(el => truncateText(el, 4));
        </script>

        @stack('bottom-js')
</body>
