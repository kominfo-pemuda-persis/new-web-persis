<footer>
    <div class="container">
        <div class="row py-4 py-lg-5">
            <div class="col-12 col-md-6 col-lg-4 mb-5 mb-md-0">
                <div class="">
                    <div class="brand-footer">
                        <a href="{{ route('portal') }}" class="image-footer">
                            <img src="{{ $webInfos->web_logo_footer }}" alt="Logo Persis" />
                        </a>
                        <p>
                            {{ $webInfos->web_footer_info }}
                        </p>
                    </div>
                    <div class="list-socmed mb-4">
                        @if (!empty($webInfos->url_socmed_fb))
                            <a href="{{ $webInfos->url_socmed_fb }}" class="btn rounded btn-facebook">
                                <i class="bi bi-facebook"></i>
                            </a>
                        @endif
                        @if (!empty($webInfos->url_socmed_ig))
                            <a href="{{ $webInfos->url_socmed_ig }}" class="btn rounded btn-instagram">
                                <i class="bi bi-instagram"></i>
                            </a>
                        @endif
                        @if (!empty($webInfos->url_socmed_x))
                            <a href="{{ $webInfos->url_socmed_x }}" class="btn rounded btn-x">
                                <i class="bi bi-twitter-x"></i>
                            </a>
                        @endif
                        @if (!empty($webInfos->url_socmed_yt))
                            <a href="{{ $webInfos->url_socmed_yt }}" class="btn rounded btn-youtube">
                                <i class="bi bi-youtube"></i>
                            </a>
                        @endif
                    </div>
                    {{-- <div class="subscribe-email">
                        <p>
                            Dapatkan informasi dan insight
                            pilihan redaksi dari Portal Persis
                        </p>
                        <form class="form-subscribe" action="{{ $webInfos->url_mail }}">
                            <input class="form-control" type="e-mail" placeholder="Masukkan email Anda" />
                            <button class="btn" type="submit">
                                KIRIM
                            </button>
                        </form>
                    </div> --}}
                </div>
                <div></div>
            </div>
            <div class="col-12 col-md-6 col-lg-8">
                @foreach ($footerMenuGrouped as $menu)
                    <div class="group-footer">
                        <h2 class="mb-3">{{ $menu['title'] }}</h2>
                        <div>
                            <ul class="list-footer">
                                @foreach ($menu['items'] as $item)
                                    <li>
                                        <a href="{{ url($item->slug) }}" target="_blank">{{ $item->menu_name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="second-footer">
        <div class="container">
            <p>Copyright © 2024 Pimpinan Persatuan Islam.</p>
        </div>
    </div>
</footer>
