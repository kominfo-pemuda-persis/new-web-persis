@push('css')
    <style>
        .ask-button {
            position: fixed;
            bottom: 20px;
            right: 20px;
            background-color: #09734a;
            color: white;
            border-radius: 50%;
            width: 60px;
            height: 60px;
            display: flex;
            align-items: center;
            justify-content: center;
            box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.1);
            cursor: pointer;
            z-index: 1000;
        }

        .ask-modal {
            display: none;
            position: fixed;
            bottom: 90px;
            right: 20px;
            width: 400px;
            background: white;
            border-radius: 10px;
            box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.2);
            z-index: 1000;
            max-height: 80%;
            overflow-y: auto;
            opacity: 0;
            transform: translateY(20px);
            transition: opacity 0.3s ease, transform 0.3s ease;
        }

        .ask-modal.show {
            display: block;
            opacity: 1;
            transform: translateY(0);
        }

        .ask-modal-header {
            background: #09734a;
            color: white;
            padding: 10px;
            border-radius: 10px 10px 0 0;
            text-align: center;
        }

        .ask-modal-body {
            padding: 20px;
            max-height: 60vh;
            overflow-y: auto;
        }

        .ask-modal-footer {
            background: white;
            padding: 10px 20px;
            border-top: 1px solid #ddd;
            text-align: right;
            position: sticky;
            bottom: 0;
        }

        .form-check-input:checked {
            background-color: #09734a;
            border-color: #09734a;
        }
    </style>
@endpush

<div class="ask-button" id="askButton">
    <span class="svg-icon">
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path opacity="0.3" d="M2 4V16C2 16.6 2.4 17 3 17H13L16.6 20.6C17.1 21.1 18 20.8 18 20V17H21C21.6 17 22 16.6 22 16V4C22 3.4 21.6 3 21 3H3C2.4 3 2 3.4 2 4Z" fill="currentColor" />
            <path d="M18 9H6C5.4 9 5 8.6 5 8C5 7.4 5.4 7 6 7H18C18.6 7 19 7.4 19 8C19 8.6 18.6 9 18 9ZM16 12C16 11.4 15.6 11 15 11H6C5.4 11 5 11.4 5 12C5 12.6 5.4 13 6 13H15C15.6 13 16 12.6 16 12Z" fill="currentColor" />
        </svg>
    </span>
</div>

<div class="ask-modal" id="askModal">
    <div class="ask-modal-header">
        <span class="fw-bold">Ajukan Pertanyaan untuk Rubrik Istifta</span>
        <button type="button" class="btn-close btn-close-white float-end" aria-label="Close" id="CloseAskModal"></button>
    </div>
    <form action="{{ route('kelola-istifta.store') }}" method="post" id="askForm" class="needs-validation" novalidate>
        <div class="ask-modal-body">
            @csrf
            <div class="mb-3">
                <label for="namaLengkap" class="form-label required">Nama Lengkap*</label>
                <input type="text" class="form-control" id="namaLengkap" name="full_name" placeholder="Nama Anda" value="{{ old('full_name') }}" required>
                <div class="form-check mt-2">
                    <input type="checkbox" class="form-check-input shadow-none" name="show_name" id="tampilkanNama">
                    <label class="form-check-label" for="tampilkanNama">Tampilkan nama anda di website</label>
                </div>
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email*</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Alamat Email" value="{{ old('email') }}" required>
                <div class="form-check mt-2">
                    <input type="checkbox" class="form-check-input shadow-none" name="send_to_email" id="kirimNotifikasi">
                    <label class="form-check-label" for="kirimNotifikasi">Kirim notifikasi jawaban ke email</label>
                </div>
            </div>
            <div class="mb-3">
                <label for="topikPertanyaan" class="form-label">Topik Pertanyaan*</label>
                <select class="form-select" id="topikPertanyaan" name="topik_pertanyaan" required>
                    <option value="" selected disabled>Pilih Topik Pertanyaan</option>
                    <option>Fiqh</option>
                    <option>Akhlak</option>
                    <option>Ibadah</option>
                    <option>Muamalah</option>
                    <option>Aqidah</option>
                    <option>Lainnya</option>
                </select>
            </div>
            <div class="mb-3">
                <label for="pertanyaan" class="form-label">Pertanyaan*</label>
                <textarea class="form-control" id="pertanyaan" name="pertanyaan" rows="3" required>{{ old('pertanyaan') }}</textarea>
            </div>
            {{-- <div class="g-recaptcha" data-sitekey={{ config('services.recaptcha.key') }} id="recaptcha-container"></div> --}}
        </div>
        <div class="ask-modal-footer">
            <button type="submit" class="btn btn-success w-100 g-recaptcha" data-sitekey={{ config('services.recaptcha.key') }} data-callback='onSubmit' data-action='submit'>Kirim</button>
            {{-- <button type="submit" class="btn btn-success w-100">Kirim</button> --}}
        </div>
    </form>
</div>

@push('bottom-js')
    <script>
        const askButton = document.getElementById('askButton');
        const askModal = document.getElementById('askModal');

        askButton.addEventListener('click', () => {
            if (askModal.classList.contains('show')) {
                askModal.classList.remove('show');
                setTimeout(() => {
                    askModal.style.display = 'none';
                }, 300);
            } else {
                askModal.style.display = 'block';
                setTimeout(() => {
                    askModal.classList.add('show');
                }, 10);
            }
        });

        const closeModal = document.getElementById('CloseAskModal');
        closeModal.addEventListener('click', () => {
            askModal.classList.remove('show');
            setTimeout(() => {
                askModal.style.display = 'none';
            }, 300);
        });

        $(document).ready(function() {
            $('#topikPertanyaan').select2({
                theme: 'bootstrap-5',
                placeholder: 'Pilih Topik Pertanyaan',
                minimumResultsForSearch: -1,
                dropdownParent: $('#askModal')
            });
        });

        (function() {
            'use strict';

            document.addEventListener('DOMContentLoaded', function() {
                var form = document.getElementById("askForm");
                var modalBody = document.querySelector('.ask-modal-body');
                var originalModalContent = modalBody.innerHTML; // Store original modal content

                function fadeOut(element, callback) {
                    element.style.opacity = 1;
                    let fadeEffect = setInterval(() => {
                        if (element.style.opacity > 0) {
                            element.style.opacity -= 0.1;
                        } else {
                            clearInterval(fadeEffect);
                            if (callback) callback();
                        }
                    }, 50);
                }

                function fadeIn(element, content, callback) {
                    element.style.opacity = 0;
                    element.innerHTML = content;
                    let fadeEffect = setInterval(() => {
                        if (element.style.opacity < 1) {
                            element.style.opacity = parseFloat(element.style.opacity) + 0.1;
                        } else {
                            clearInterval(fadeEffect);
                            if (callback) callback();
                        }
                    }, 50);
                }

                function restoreOriginalForm() {
                    fadeOut(modalBody, function() {
                        fadeIn(modalBody, originalModalContent, function() {
                            // Delay the reCAPTCHA reset and rendering
                            // setTimeout(function() {
                            //     grecaptcha.render('recaptcha-container', { // Re-render reCAPTCHA
                            //         'sitekey': `{{ config('services.recaptcha.key') }}`
                            //     });
                            // }, 500); // A short delay to ensure DOM updates before rendering reCAPTCHA
                        });
                    });
                }

                if (form) {
                    form.addEventListener('submit', function(event) {
                        event.preventDefault(); // Prevent default form submission
                        event.stopPropagation();

                        var captcha = grecaptcha.getResponse();

                        if (!form.checkValidity() || !captcha) {
                            grecaptcha.reset();
                            form.classList.add('was-validated');
                            return;
                        }

                        // Serialize form data
                        var formData = new FormData(form);
                        formData.append('g-recaptcha-response', captcha);

                        // Fade out current form and show loading
                        fadeOut(modalBody, function() {
                            fadeIn(modalBody, '<div class="text-center p-3"><strong>Memproses...</strong></div>');
                        });

                        // Send AJAX request
                        fetch(form.action, {
                                method: 'POST',
                                body: formData,
                                headers: {
                                    'X-Requested-With': 'XMLHttpRequest',
                                    'X-CSRF-TOKEN': document.querySelector('input[name="_token"]').value
                                }
                            })
                            .then(response => response.json())
                            .then(data => {
                                setTimeout(() => {
                                    if (data.success) {
                                        fadeOut(modalBody, function() {
                                            fadeIn(modalBody, '<div class="alert alert-success text-center">✅ Pertanyaan berhasil dikirim</div>');
                                        });
                                    } else if (data.errors) {
                                        let errorMessages = Object.values(data.errors).map(err => `<li>${err}</li>`).join('');
                                        fadeOut(modalBody, function() {
                                            fadeIn(modalBody, `<div class="alert alert-danger text-center"><ul>${errorMessages}</ul></div>`);
                                        });
                                    } else {
                                        fadeOut(modalBody, function() {
                                            fadeIn(modalBody, '<div class="alert alert-danger text-center">❌ Pertanyaan gagal dikirim</div>');
                                        });
                                    }

                                    // Restore the original form after 3 seconds
                                    setTimeout(() => restoreOriginalForm(), 3000);
                                }, 1500);
                            })
                            .catch(error => {
                                console.error("Error:", error);
                                setTimeout(() => {
                                    fadeOut(modalBody, function() {
                                        fadeIn(modalBody, '<div class="alert alert-danger text-center">❌ Terjadi kesalahan. Coba lagi nanti.</div>');
                                    });

                                    // Restore the form after 3 seconds
                                    setTimeout(() => restoreOriginalForm(), 3000);
                                }, 1500);
                            });
                    }, false);
                }
            });
        })();

        function onSubmit(token) {
            var form = document.getElementById("askForm");

            var event = new Event("submit", {
                bubbles: true,
                cancelable: true
            });
            form.dispatchEvent(event);
        }
    </script>
@endpush
