<div class="modal fade" tabindex="-1" id="pop-up" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content bg-transparent border-0">
            <div class="modal-header border-0 p-0">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                    style="position: absolute !important; margin-right: -20px !important; margin-top: -15px !important; background-color: white !important; border-radius: 15px !important; width: 30px !important; height: 30px !important; opacity: 1 !important; left: 92%; z-index: 1000;"></button>
            </div>
            <div class="modal-body bg-white rounded-3 d-flex justify-content-center">
                <a class="w-100" href="{{ @$iklanPopUp->advertisement[0]->url_ads }}">
                    <img class="w-100 lozad" src="{{ @$iklanPopUp->advertisement[0]->image }}">
                </a>
            </div>
        </div>
    </div>
</div>
