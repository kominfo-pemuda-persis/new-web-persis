<header>
    <!-- Desktop & Tab Navbar -->
    <div class="bg-navbar">
        <div class="ornament ornament-left d-none d-xxl-block">
            <img class="h-100" src="{{ asset('assets') }}/assets-landing/images/ornament-left.png"" alt="">
        </div>

        <div class="ornament ornament-right d-none d-xxl-block">
            <img class="h-100" src="{{ asset('assets') }}/assets-landing/images/ornament-right.png"" alt="">
        </div>
        <div class="navbar top-navbar d-flex d-none d-md-block">
            <div class="container">
                <div>
                    <a class="navbar-brand" href="{{ route('portal') }}">
                        <img src="{{ $webInfos->web_logo_top }}" alt="Logo Persatuan Islam" />
                    </a>
                </div>
                <form action="{{ route('portal.search') }}" method="GET" class="search-button">
                    <div class="input-group">
                        <input class="form-control border-start-0 border-bottom-0 border" type="search"
                            placeholder="Cari tokoh, topik atau peristiwa..." id="example-search-input"
                            name="q" />
                        <button class="btn btn-outline-secondary bg-white border-start-0 border-bottom-0 border ms-n5"
                            type="button">
                            <i class="bi bi-search"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <nav class="bg-brand-dark d-none d-md-block">
            <div class="container">
                <div class="navbar py-1 flex-nowrap">
                    <div class="text-white px-2 flex-shrink-0">
                        {{ \Carbon\Carbon::now()->translatedFormat('l, j F Y') }}
                    </div>
                    <span class="text-white px-2"> | </span>
                    <div class="navbar-menu flex-grow-1" id="navbarNav">
                        <ul class="navbar-nav navbar-dark me-auto text-white" id="nav">
                            {{-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                                    data-bs-auto-close="outside" aria-expanded="false" id="dropdownMenuButton1">
                                    Berita
                                </a>
                                <ul class="dropdown-menu dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item" href="#">Berita Jam'iyyah</a></li>
                                    <li><a class="dropdown-item" href="#">Berita Nasional</a></li>
                                    <li><a class="dropdown-item" href="#">Berita Internasional</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                                    data-bs-auto-close="outside" aria-expanded="false" id="dropdownMenuButton2">
                                    Artikel
                                </a>
                                <ul class="dropdown-menu dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                    <li><a class="dropdown-item" href="#">Tsaqofah Islamiyyah</a></li>
                                    <li><a class="dropdown-item" href="#">Kejam'iyyahan</a></li>
                                    <li><a class="dropdown-item" href="#">Pemikiran</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                                    data-bs-auto-close="outside" aria-expanded="false" id="dropdownMenuButton3">
                                    Kajian
                                </a>
                                <ul class="dropdown-menu dropdown-menu" aria-labelledby="dropdownMenuButton3">
                                    <li><a class="dropdown-item" href="#">Fiqh</a></li>
                                    <li><a class="dropdown-item" href="#">Akhlaq</a></li>
                                    <li><a class="dropdown-item" href="#">Hadits</a></li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Konten Multimedia</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://www.youtube.com/@PERSISTVCHANNEL/streams"
                                    target="_blank">Live</a>
                            </li> --}}
                            @foreach ($navMenuGrouped as $parentId => $parentMenu)
                                @if (empty($parentMenu['items']))
                                    <!-- Parent menu tanpa child -->
                                    <li class="nav-item">
                                        <a class="nav-link"
                                            href="{{ url($parentMenu['slug']) }}">{{ $parentMenu['title'] }}</a>
                                    </li>
                                @else
                                    <!-- Parent menu dengan child -->
                                    @php
                                        $dropdownId = 'dropdownMenu' . $parentId;
                                    @endphp
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" role="button"
                                            data-bs-toggle="dropdown" aria-expanded="false" id="{{ $dropdownId }}">
                                            {{ $parentMenu['title'] }}
                                        </a>
                                        <ul class="dropdown-menu" aria-labelledby="{{ $dropdownId }}">
                                            @foreach ($parentMenu['items'] as $childMenu)
                                                <li><a class="dropdown-item"
                                                        href="{{ url($childMenu['slug']) }}">{{ $childMenu['title'] }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endif
                            @endforeach
                            <li class="nav-item dropdown d-none">
                                <a class="nav-link dropdown-toggle" href="#" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false" id="navbarDropdownMenu">
                                    Lainnya
                                </a>
                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdownMenu"></ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <!-- Mobile Navbar  -->
    <div class="navbar navbar-mobile d-flex bg-navbar d-block d-md-none">
        <div class="container" id="navMobile">
            <button class="btn" type="button" data-bs-toggle="collapse" data-bs-target="#navbarMobile"
                aria-controls="navbarMobile" aria-expanded="false" aria-label="Toggle Navigation">
                <i class="bi bi-list collapsed"></i>
                <i class="bi bi-x-lg expanded"></i>
            </button>
            <div class="collapse nav-mobile" id="navbarMobile" data-bs-parent="#navMobile">
                <div class="nav-content">
                    <div class="date-item">
                        <div class="container">
                            {{ \Carbon\Carbon::now()->translatedFormat('l, j F Y') }}
                        </div>
                    </div>
                    <nav class="menu-mobile container">
                        <ul class="navbar-nav">
                            @foreach ($navMenuGrouped as $parentId => $parentMenu)
                                @if (empty($parentMenu['items']))
                                    <!-- Parent menu tanpa child -->
                                    <li class="nav-item">
                                        <a class="nav-link"
                                            href="{{ url($parentMenu['slug']) }}">{{ $parentMenu['title'] }}</a>
                                    </li>
                                @else
                                    <!-- Parent menu dengan child -->
                                    @php
                                        $dropdownId = 'dropdownMenu' . $parentId;
                                    @endphp
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" role="button"
                                            data-bs-toggle="dropdown" aria-expanded="false" id="{{ $dropdownId }}">
                                            {{ $parentMenu['title'] }}
                                        </a>
                                        <ul class="dropdown-menu" aria-labelledby="{{ $dropdownId }}">
                                            @foreach ($parentMenu['items'] as $childMenu)
                                                <li><a class="dropdown-item"
                                                        href="{{ url($childMenu['slug']) }}">{{ $childMenu['title'] }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </nav>
                </div>
            </div>
            <div>
                <a class="navbar-brand nav-brand-mobile" href="{{ route('portal') }}">
                    <img src="{{ $webInfos->web_logo_top }}" alt="Logo Persatuan Islam" />
                </a>
            </div>
            <button class="btn" type="button" data-bs-toggle="collapse" data-bs-target="#searchCollapse"
                aria-controls="searchCollapse" aria-expanded="false" aria-label="Toggle Search Collapse">
                <i class="bi bi-search collapsed"></i>
                <i class="bi bi-x-lg expanded"></i>
            </button>
            <div class="collapse nav-mobile nav-search" id="searchCollapse" data-bs-parent="#navMobile">
                <div class="container">
                    <form action="{{ route('portal.search') }}" method="GET" class="search-button w-100">
                        <div class="input-group">
                            <input class="form-control border" type="search"
                                placeholder="Cari tokoh, topik atau peristiwa..." id="example-search-input"
                                name="q" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>
