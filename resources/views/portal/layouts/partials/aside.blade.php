<aside class="col-md-12">
    @if ($ketua_content->count() > 0)
        <section class="pojok-ketua mb-4">
            <div class="radial-gradient-circle top"></div>
            <div class="pojok-title">
                <div class="d-flex justify-content-between">
                    <h3>POJOK KETUA</h3>
                    <a href="{{ route('portal.tags', ['slug' => 'pojok-ketua']) }}"> Lihat Selengkapnya </a>
                </div>
            </div>
            <div class="pojok-content row row-gap-2">
                @foreach ($ketua_content as $widget)
                    <article>
                        <div class="pojok-news">
                            <a href="{{ route('portal.detail-content', ['category' => $widget->content_category_sub->content_categories->category_name, 'slug' => $widget->slug]) }}">
                                <h3 class="news-title">
                                    {!! $widget->title !!}
                                </h3>
                                <p class="text-secondary">
                                    {{ \Carbon\Carbon::parse($widget->published_at)->isoFormat('DD MMM YYYY | HH:mm') }}
                                </p>
                            </a>
                        </div>
                    </article>
                @endforeach
            </div>

            <div class="pojok-footer">
                <h5>KH Jeje Zaenuddin</h5>
                <p>
                    Ketua Umum Persis <br />
                    Masa Jiihad 2022-2027
                </p>
                <div class="foto-ketua">
                    <img src="{{ asset('assets') }}/assets-landing/images/ustaz-jeje-zaenudin.png" alt="" />
                </div>
            </div>
            <div class="radial-gradient-circle bottom"></div>
        </section>
    @endif

    @if (@$iklanSamping1 && @$iklanSamping1->advertisement && $iklanSamping1->advertisement->isNotEmpty())
        @foreach ($iklanSamping1->advertisement as $advertisement)
            <section class="aside-widget text-center mb-4">
                <a class="w-100" href="{{ $advertisement->url_ads }}">
                    <img class="rounded-2 img-fluid" src="{{ $advertisement->image }}" alt="{{ $advertisement->desc }}" />
                </a>
            </section>
        @endforeach
    @endif

    {{-- Terpopuler --}}
    <section class="mb-4">
        <div class="section-title mb-3">
            <h3>TERPOPULER</h3>
            <div class="line">
                <div class="yellow-line"></div>
                <div class="base-line"></div>
            </div>
        </div>
        <div class="aside-list">

            @foreach ($popular_tags as $index => $tag)
                <a class="mostItem" href="{{ route('portal.detail-content', ['category' => $tag->content_category_sub->category_sub_name, 'slug' => $tag->slug]) }}">
                    <div class="mostItem-count">{{ $index + 1 }}</div>
                    <div class="mostItem-box">
                        <h5 class="mostItem-title simple-title">{{ strip_tags($tag->title) }}</h5>
                    </div>
                    <div class="mostItem-img">
                        <img src="{{ $tag->featured_image }}" onerror="handleImage(this)" />
                    </div>
                </a>
            @endforeach

    </section>

    {{-- Istifta --}}
    @if (@$istifta)
        <section class="mb-4">
            <div class="section-title mb-3">
                <h3>ISTIFTA</h3>
                <div class="line">
                    <div class="yellow-line"></div>
                    <div class="base-line"></div>
                </div>
            </div>
            <div class="aside-list">

                @foreach ($istifta as $index => $tag)
                    <a class="mostItem align-items-center" href="{{ route('portal.detail-content', ['category' => $tag->content_category_sub->category_sub_name, 'slug' => $tag->slug]) }}">
                        <div class="mostItem-box">
                            <h5 class="mostItem-title simple-title">{{ strip_tags($tag->title) }}</h5>
                        </div>
                        <div class="mostItem-img">
                            <img src="{{ $tag->featured_image }}" onerror="handleImage(this)" />
                        </div>
                    </a>
                @endforeach
        </section>
    @endif

    @if (@$iklanSamping2 && @$iklanSamping2->advertisement && $iklanSamping2->advertisement->isNotEmpty())
        @foreach ($iklanSamping2->advertisement as $advertisement)
            <section class="aside-widget text-center mb-4">
                <a class="w-100" href="{{ $advertisement->url_ads }}">
                    <img class="rounded-2 img-fluid" src="{{ $advertisement->image }}" alt="{{ $advertisement->desc }}" />
                </a>
            </section>
        @endforeach
    @endif
</aside>
