@extends('portal.layouts.master')
@section('title', $page->title)

@section('extras-meta')
    <meta name="title" content="{{ $page->title }}" />
    <meta name="description" content="{{ $page->desc_seo }}" />

    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:site_name" content="Portal Persatuan Islam" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:title" content="{{ $page->title }}" />
    <meta property="og:description" content="{{ $page->desc_seo }}" />
    @if (!empty($page->featured_image))
        <meta property="og:image" content="{{ $page->featured_image }}" />
        <meta property="og:image:type" content="image/png" />
        <meta property="og:image:width" content="300" />
        <meta property="og:image:height" content="300" />
    @else
        <meta property="og:image" content="{{ asset('assets/assets-landing/images/placeholder.png') }}" />
        <meta property="og:image:type" content="image/png" />
        <meta property="og:image:width" content="300" />
        <meta property="og:image:height" content="300" />
    @endif
@endsection

@section('content')
    @if (!empty($page->advertisement_id) && $page->advertisement_position == 'top')
        <div class="w-100 mb-4 widget-content rounded">
            <a href="{{ $page->advertisement->url_ads }}" target="_blank">
                <img class="" src="{{ $page->advertisement->image }}" alt="{{ $page->advertisement->desc }}">
            </a>
        </div>
    @endif

    <nav class="list-breadcrumb mb-4" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('portal') }}">Home</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">
                {{ $page->title }}
            </li>
        </ol>
    </nav>

    {{-- Judul --}}
    <div class="content-detail mb-4">
        <h1>
            {{ $page->title }}
        </h1>
    </div>

    {{-- Featured Image --}}
    @if ($page->featured_image)
        <div class="thumbnail mb-4">
            <figure>
                <img src="{{ $page->featured_image }}" class="w-100" alt="" onerror="handleImage(this)" />
                <figcaption>
                    {{ @$page->desc_featured_image }}
                </figcaption>
            </figure>
        </div>
    @endif

    {{-- Embeded Content --}}
    @if ($page->embeded_string)
        <div class="area-video mb-4">
            {!! $page->embeded_string !!}
        </div>
    @endif

    {{-- Content --}}
    <div class="text-content ql-editor p-0 border-0 mb-4" data-gramm="false" contenteditable="false">
        @if (!empty($page->advertisement_id) && $page->advertisement_position == 'top')
            <div class="w-100 mb-4 widget-content rounded">
                <a href="{{ $page->advertisement->url_ads }}" target="_blank">
                    <img class="" src="{{ $page->advertisement->image }}" alt="{{ $page->advertisement->desc }}">
                </a>
            </div>
        @endif

        {!! $page->content !!}

        @if (!empty($page->advertisement_id) && $page->advertisement_position == 'middle')
            <div class="w-100 my-4 widget-content rounded">
                <a href="{{ $page->advertisement->url_ads }}" target="_blank">
                    <img class="" src="{{ $page->advertisement->image }}" alt="{{ $page->advertisement->desc }}">
                </a>
            </div>
        @endif
    </div>


    {{-- Share Area --}}
    <div class="share-area mb-4 gap-2">
        <p class="flex-grow-1">Bagikan:</p>
        <div class="share-socmed">
            <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current()) }}" class="btn  btn-box rounded btn-facebook" target="_blank" rel="noopener noreferrer">
                <i class="bi bi-facebook"></i>
            </a>
            <a href="https://twitter.com/intent/tweet?url={{ urlencode(url()->current()) }}" class="btn  btn-box rounded btn-x" target="_blank" rel="noopener noreferrer">
                <i class="bi bi-twitter-x"></i>
            </a>
            <a href="https://api.whatsapp.com/send?text={{ urlencode(url()->current()) }}" class="btn  btn-box rounded btn-whatsapp" target="_blank" rel="noopener noreferrer">
                <i class="bi bi-whatsapp"></i>
            </a>
            <a href="#" class="btn rounded btn-clipboard" id="copyLinkButton">
                <i class="bi bi-link-45deg"></i>
                <span class="d-none d-sm-inline">Bagikan Tautan</span>
            </a>

            <div class="toast toast-copy position-absolute m-3 bottom-0 start-50 translate-middle-x align-items-center border-0 z-3" id="copyToast" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="d-flex">
                    <div class="toast-body">
                        Tautan telah disalin!
                    </div>
                    <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
            </div>
        </div>
    </div>

    {{-- Creator --}}
    <div class="contributor-area p-2" style="background-color: #F9F9F9;">
        <div class="d-flex align-items-center">
            <div>
                <img alt="Logo" style="width: 50px; height: 50px; border-radius: 50%;" src="{{ $page->creators->photo ?? Avatar::create($page->creators->full_name)->toBase64() }}" />
            </div>
            <div class="d-flex flex-column ms-3">
                <span><a class="no-click" onclick="return false">{{ $page->creators->full_name }}</a></span>
                <span>{{ \Carbon\Carbon::parse($page->updated_at)->translatedFormat('l, j F Y') }}</span>
            </div>
        </div>
    </div>

    {{-- <section class="page-area">
        <div class="section-title mb-4">
            <h1>{{ $page->title }}</h1>
            <div class="line">
                <div class="yellow-line"></div>
                <div class="base-line"></div>
            </div>
        </div>

        @if (!empty($page->featured_image))
            <div class="thumbnail mb-4">
                <figure>
                    <img src="{{ $page->featured_image }}" class="w-100" alt="" onerror="handleImage(this)" />
                </figure>
            </div>
        @endif

        @if (!empty($page->embeded_string))
            <div class="area-video mb-4">
                {!! $page->embeded_string !!}
            </div>
        @endif

        <div class="text-content ql-editor p-0 border-0" data-gramm="false" contenteditable="false">
            {!! $page->content !!}
        </div>

        <div class="share-area mb-4 gap-2">
            <p class="flex-grow-1">Bagikan:</p>
            <div class="share-socmed">
                <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current()) }}" class="btn  btn-box rounded btn-facebook" target="_blank" rel="noopener noreferrer">
                    <i class="bi bi-facebook"></i>
                </a>
                <a href="https://twitter.com/intent/tweet?url={{ urlencode(url()->current()) }}" class="btn  btn-box rounded btn-x" target="_blank" rel="noopener noreferrer">
                    <i class="bi bi-twitter-x"></i>
                </a>
                <a href="https://api.whatsapp.com/send?text={{ urlencode(url()->current()) }}" class="btn  btn-box rounded btn-whatsapp" target="_blank" rel="noopener noreferrer">
                    <i class="bi bi-whatsapp"></i>
                </a>
                <a href="#" class="btn rounded btn-clipboard" id="copyLinkButton">
                    <i class="bi bi-link-45deg"></i>
                    <span class="d-none d-sm-inline">Bagikan Tautan</span>
                </a>

                <div class="toast toast-copy position-absolute m-3 bottom-0 start-50 translate-middle-x align-items-center border-0 z-3" id="copyToast" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="d-flex">
                        <div class="toast-body">
                            Tautan telah disalin!
                        </div>
                        <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}
@endsection
@push('bottom-js')
    <script>
        $(document).ready(function() {
            $("#copyLinkButton").click(function() {
                event.preventDefault();
                var link = window.location.href;

                navigator.clipboard.writeText(link);
                $("#copyToast").toast("show");
            });
        });
    </script>

    @if (!$iklanPopUp->advertisement->isEmpty())
        @include('portal.layouts.partials.pop-up-ads')
        <script>
            $(window).on('load', function() {
                $('#pop-up').modal('show');
            });
        </script>
    @endif
@endpush
