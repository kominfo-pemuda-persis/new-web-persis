@extends('portal.layouts.master')
@section('title')
    @if (isset($tag_content))
        Konten #{{ $tag_content->title }} Terbaru
    @elseif(isset($query))
        Hasil Pencarian untuk "{{ $query }}"
    @elseif(isset($category))
        Konten "{{ $category->category_name }}" Terbaru
    @elseif(isset($category_sub))
        Konten "{{ $category_sub->category_sub_name }}" Terbaru
    @endif
@endsection

@section('extras-meta')
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:site_name" content="{{ config('app.name')() }}" />

    @if (isset($tag_content))
        <meta name="title" content="Konten #{{ $tag_content->title }} Terbaru" />
        <meta name="description" content="Kumpulan Konten  #{{ $tag_content->title }} Terbaru" />
        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:title" content="Konten #{{ $tag_content->title }} Terbaru" />
        <meta property="og:description" content="Kumpulan Konten  #{{ $tag_content->title }} Terbaru" />
    @elseif(isset($query))
        <meta name="title" content='Hasil Pencarian untuk "{{ $query }}"' />
        <meta name="description" content='Kumpulan Konten tentang "{{ $query }}"' />
        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:title" content='Hasil Pencarian untuk "{{ $query }}"' />
        <meta property="og:description" content='Kumpulan Konten tentang "{{ $query }}"' />
    @elseif(isset($category))
        <meta name="title" content='Hasil Pencarian untuk "{{ $category->category_name }}" Terbaru' />
        <meta name="description" content='Kumpulan Konten tentang "{{ $category->category_name }}" Terbaru' />
        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:title" content='Hasil Pencarian untuk "{{ $category->category_name }}" Terbaru' />
        <meta property="og:description" content='Kumpulan Konten tentang "{{ $category->category_name }}" Terbaru' />
    @elseif(isset($category_sub))
        <meta name="title" content='Hasil Pencarian untuk "{{ $category_sub->category_sub_name }}" Terbaru' />
        <meta name="description" content='Kumpulan Konten tentang "{{ $category_sub->category_sub_name }}" Terbaru' />
        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:title" content='Hasil Pencarian untuk "{{ $category_sub->category_sub_name }}" Terbaru' />
        <meta property="og:description"
            content='Kumpulan Konten tentang "{{ $category_sub->category_sub_name }}" Terbaru' />
    @endif

    <meta property="og:image" content="{{ asset('assets/assets-landing/images/placeholder.png') }}" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="300" />
    <meta property="og:image:height" content="300" />
@endsection

@section('content')
    <nav class="list-breadcrumb mb-4" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('portal') }}">Home</a>
            </li>
            @if (isset($tag_content))
                <li class="breadcrumb-item active">
                    Tag
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    {{ $tag_content->title }}
                </li>
            @elseif(isset($query))
                <li class="breadcrumb-item active" aria-current="page">
                    Pencarian
                </li>
            @elseif(isset($category))
                <li class="breadcrumb-item active" aria-current="page">
                    {{ $category->category_name }}
                </li>
            @elseif(isset($category_sub))
                <li class="breadcrumb-item" aria-current="page">
                    <a href="{{ route('portal.category', ['category' => $category_sub->content_categories->slug]) }}">
                        {{ $category_sub->content_categories->category_name }}
                    </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    {{ $category_sub->category_sub_name }}
                </li>
            @endif
        </ol>
    </nav>

    <section class="result-area">
        <div class="section-title mb-4">
            @if (isset($query))
                <h3>Hasil Pencarian "{{ $query }}"</h3>
                <div class="line">
                    <div class="yellow-line"></div>
                    <div class="base-line"></div>
                </div>
                <p>
                    Menampilkan hasil {{ $contents->total() }} konten untuk "{{ $query }}"
                </p>
            @elseif(isset($tag_content))
                <h3>#{{ $tag_content->title }}</h3>
                <div class="line">
                    <div class="yellow-line"></div>
                    <div class="base-line"></div>
                </div>
                <p>
                    Menampilkan hasil {{ $contents->total() }} konten untuk "#{{ $tag_content->title }}"
                </p>
            @elseif(isset($category))
                <h3>{{ $category->category_name }}</h3>
                <div class="line">
                    <div class="yellow-line"></div>
                    <div class="base-line"></div>
                </div>
                <p>
                    Menampilkan hasil {{ $contents->total() }} konten untuk "{{ $category->category_name }}"
                </p>
            @elseif(isset($category_sub))
                <h3>{{ $category_sub->category_sub_name }}</h3>
                <div class="line">
                    <div class="yellow-line"></div>
                    <div class="base-line"></div>
                </div>
                <p>
                    Menampilkan hasil {{ $contents->total() }} konten untuk "{{ $category_sub->category_sub_name }}"
                </p>
            @endif
        </div>
        <div class="section-content">
            @foreach ($contents as $content)
                <article class="news-content">
                    <div class="news-image">
                        <a
                            href="{{ route('portal.detail-content', ['category' => $content->content_category_sub->content_categories->category_name, 'slug' => $content->slug]) }}">
                            <img class="object-fit-cover h-100 w-100" src="{{ $content->featured_image }}"
                                onerror="handleImage(this)" />
                        </a>
                    </div>
                    <div class="news-detail">
                        <div class="news-info mb-2">
                            <div>
                                <span
                                    class="badge rounded-pill bg-brand text-white">{{ $content->content_category_sub->category_sub_name }}</span>
                            </div>
                            <p class="text-secondary">
                                {{ \Carbon\Carbon::parse($content->published_at)->isoFormat('DD MMM YYYY | HH:mm') }}
                            </p>
                        </div>
                        <a
                            href="{{ route('portal.detail-content', ['category' => $content->content_category_sub->content_categories->slug, 'slug' => $content->slug]) }}">
                            <h3 class="news-title">
                                {!! $content->title !!}
                            </h3>
                        </a>
                        <p class="description-news text-secondary">
                            {{ $content->desc_seo }}
                        </p>
                    </div>
                </article>
            @endforeach
        </div>

        <div class="d-flex justify-content-center">
            @if (isset($query))
                {{ $contents->onEachSide(1)->appends(['q' => $query])->links('pagination::bootstrap-4') }}
            @else
                {{ $contents->onEachSide(1)->links('pagination::bootstrap-4') }}
            @endif
        </div>
    </section>
@endsection
