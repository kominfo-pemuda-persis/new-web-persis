@extends('portal.layouts.portal')
@section('title', 'Beranda')

@section('headline-content')
    <section class="py-4 py-lg-5" id="headline-content">
        <div class="container">
            <div class="row row-gap-4">
                <main class="col">
                    <section id="headline">
                        <div class="align-items-stretch">
                            <a class="card h-100" href="{{ route('portal.detail-content', ['category' => $main_headline->content_category_sub->content_categories->slug, 'slug' => $main_headline->slug]) }}">
                                <img class="card-img object-fit-cover h-100" src="{{ $main_headline->featured_image }}" onerror="handleImage(this)" />
                                <div class="card-img-overlay d-flex flex-column justify-content-end bg-overlay-dark">
                                    <div class="news-info mb-2">
                                        <p class="text-secondary text-white-50">
                                            {{ \Carbon\Carbon::parse($main_headline->published_at)->isoFormat('DD MMM YYYY | HH:mm') }}
                                        </p>
                                    </div>
                                    <h2 class="headline-title mb-0">
                                        {!! $main_headline->title !!}
                                    </h2>
                                </div>
                            </a>
                        </div>

                        @if ($headline_content->count() > 0)
                            <div class="row">
                                @foreach ($headline_content as $headline)
                                    <article class="col-12 col-sm-3 headline">
                                        <div class="card-headline rounded">
                                            <div class="news-image">
                                                <a href="{{ route('portal.detail-content', ['category' => $headline->content_category_sub->content_categories->slug, 'slug' => $headline->slug]) }}">
                                                    <img class="object-fit-cover h-100 w-100" src="{{ $headline->featured_image }}" onerror="handleImage(this)" />
                                                </a>
                                            </div>
                                            <div class="news-info">
                                                {{-- <div>
                                                    <span
                                                        class="badge rounded-pill bg-brand text-white">{{ $headline->content_category_sub->category_sub_name }}</span>
                                                </div> --}}
                                                <p class="text-secondary">
                                                    {{ \Carbon\Carbon::parse($headline->published_at)->isoFormat('DD MMM YYYY | HH:mm') }}
                                                </p>
                                                <a href="{{ route('portal.detail-content', ['category' => $headline->content_category_sub->content_categories->slug, 'slug' => $headline->slug]) }}">
                                                    <h3 class="news-title simple-title">
                                                        {!! $headline->title !!}
                                                    </h3>
                                                </a>
                                            </div>
                                    </article>
                                @endforeach

                            </div>
                        @endif
                    </section>
                    <!-- @if ($latest_content->count() > 0)
                                                                                                                                                                                                <section id="latestNews" class="pt-4">
                                                                                                                                                                                                    <div class="section-title mb-3">
                                                                                                                                                                                                        <h3>TERKINI</h3>
                                                                                                                                                                                                        <div class="line">
                                                                                                                                                                                                            <div class="yellow-line"></div>
                                                                                                                                                                                                            <div class="base-line"></div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                    </div>
                                                                                                                                                                                                    <div class="section-content section-content-readmore">
                                                                                                                                                                                                        @foreach ($latest_content as $latest)
    <article class="news-content">
                                                                                                                                                                                                                <div class="news-image">
                                                                                                                                                                                                                    <a href="{{ route('portal.detail-content', ['category' => $latest->content_category_sub->content_categories->slug, 'slug' => $latest->slug]) }}">
                                                                                                                                                                                                                        <img class="object-fit-cover h-100 w-100" src="{{ $latest->featured_image }}" onerror="handleImage(this)" />
                                                                                                                                                                                                                    </a>
                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                <div class="news-detail">
                                                                                                                                                                                                                    <div class="news-info mb-2">
                                                                                                                                                                                                                        {{-- <div>
                                                    <span
                                                        class="badge rounded-pill bg-brand text-white">{{ $latest->content_category_sub->category_sub_name }}</span>
                                                </div> --}}
                                                                                                                                                                                                                        <p class="text-secondary">
                                                                                                                                                                                                                            {{ \Carbon\Carbon::parse($latest->published_at)->isoFormat('DD MMM YYYY | HH:mm') }}
                                                                                                                                                                                                                        </p>
                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                    <a href="{{ route('portal.detail-content', ['category' => $latest->content_category_sub->content_categories->slug, 'slug' => $latest->slug]) }}">
                                                                                                                                                                                                                        <h3 class="news-title">
                                                                                                                                                                                                                            {!! $latest->title !!}
                                                                                                                                                                                                                        </h3>
                                                                                                                                                                                                                    </a>
                                                                                                                                                                                                                    <p class="description-news text-secondary">
                                                                                                                                                                                                                        {{ $latest->desc_seo }}
                                                                                                                                                                                                                    </p>
                                                                                                                                                                                                                </div>
                                                                                                                                                                                                            </article>
    @endforeach

                                                                                                                                                                                                    </div>
                                                                                                                                                                                            @endif -->
                    <!-- </section> -->

                    <section id="latestNews" class="pt-4">
                        <div class="section-title mb-3">
                            <h3>TERKINI</h3>
                            <div class="line">
                                <div class="yellow-line"></div>
                                <div class="base-line"></div>
                            </div>
                        </div>
                        <div class="section-content section-content-readmore">
                            @foreach ($latest_content as $latest)
                                <article class="news-content">
                                    <div class="news-image">
                                        <a href="{{ route('portal.detail-content', ['category' => $latest->content_category_sub->content_categories->slug, 'slug' => $latest->slug]) }}">
                                            <img class="object-fit-cover h-100 w-100" src="{{ $latest->featured_image }}" onerror="handleImage(this)" />
                                        </a>
                                    </div>
                                    <div class="news-detail">
                                        <div class="news-info mb-2">
                                            {{-- <div>
                                                <span
                                                    class="badge rounded-pill bg-brand text-white">{{ $latest->content_category_sub->category_sub_name }}</span>
                                            </div> --}}
                                            <p class="text-secondary">
                                                {{ \Carbon\Carbon::parse($latest->published_at)->isoFormat('DD MMM YYYY | HH:mm') }}
                                            </p>
                                        </div>
                                        <a href="{{ route('portal.detail-content', ['category' => $latest->content_category_sub->content_categories->slug, 'slug' => $latest->slug]) }}">
                                            <h3 class="news-title">
                                                {!! $latest->title !!}
                                            </h3>
                                        </a>
                                        <p class="description-news text-secondary">
                                            {{ $latest->desc_seo }}
                                        </p>
                                    </div>
                                </article>
                            @endforeach
                        </div>
                        @if (count($latest_content) < $content_count)
                            <div class="text-center">
                                <button class="mt-2 btn btn-outline btn-outline-success btn-color-success-400 btn-active-light-success" id="show_more">Show More</button>
                            </div>
                            <div id="loader" class="text-center mt-2" style="display: none;">
                                <div class="spinner-border text-success" role="status">
                                    <span class="sr-only"> </span>
                                </div>
                            </div>
                        @endif
                    </section>
                </main>

                <!-- Section: Aside Content -->
                @include('portal.layouts.partials.aside')
            </div>
        </div>
    </section>
@endsection

@section('index-content')
    @if ($latest_video_content->count() > 0)
        <section class="py-4 py-lg-5" id="video-content">
            <div class="container">
                <div class="section-title mb-4">
                    <h3>KONTEN VIDEO</h3>
                    <div class="line">
                        <div class="yellow-line"></div>
                        <div class="base-line"></div>
                    </div>
                </div>
                <div class="section-content row row-gap-4">
                    @foreach ($latest_video_content as $video)
                        <div class="col-12 col-sm-4">
                            <div class="card-content">
                                <div class="card-image">
                                    <a class="" href="{{ route('portal.detail-content', ['category' => $video->content_category_sub->content_categories->slug, 'slug' => $video->slug]) }}">
                                        <img class="object-fit-cover h-100" src="{{ $video->featured_image }}" onerror="handleImage(this)" />
                                    </a>
                                </div>
                                <div class="card-detail">
                                    <div class="card-info mb-2">
                                        {{-- <div>
                                            <span
                                                class="badge rounded-pill bg-brand text-white">{{ $video->content_category_sub->category_sub_name }}</span>
                                        </div> --}}
                                        <p class="text-secondary">
                                            {{ \Carbon\Carbon::parse($video->published_at)->isoFormat('DD MMM YYYY | HH:mm') }}
                                        </p>
                                    </div>
                                    <a href="{{ route('portal.detail-content', ['category' => $video->content_category_sub->content_categories->slug, 'slug' => $video->slug]) }}">
                                        <h3 class="card-title">
                                            {!! $video->title !!}
                                        </h3>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    @if ($latest_image_content->count() > 0)
        <section class="py-4 py-lg-5" id="image-content">
            <div class="container">
                <div>
                    <div class="section-title mb-4">
                        <h3>KONTEN GAMBAR</h3>
                        <div class="line">
                            <div class="yellow-line"></div>
                            <div class="base-line"></div>
                        </div>
                    </div>
                    <div class="section-content row row-gap-3 row-gap-md-4 row-cols-2 row-cols-lg-4">
                        @foreach ($latest_image_content as $images)
                            <div class="col">
                                <div class="content-image">
                                    <a class="" href="{{ route('portal.detail-content', ['category' => $images->content_category_sub->content_categories->slug, 'slug' => $images->slug]) }}">
                                        <img class="object-fit-cover h-100" src="{{ $images->featured_image }}" onerror="handleImage(this)" />
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif

    @include('portal.layouts.partials.ask')
@endsection

@push('bottom-js')
    <script>
        let offset = 15;
        var latest_content = {{ count($latest_content) }}
        var content_count = {{ $content_count }}

        if (latest_content < content_count) {
            document.getElementById('show_more').addEventListener('click', function() {

                const showMoreButton = this;
                const loader = document.getElementById('loader');

                // Hide the "Show More" button and show the loader
                showMoreButton.style.display = 'none';
                loader.style.display = 'block';

                fetch(`{{ route('portal') }}?offset=${offset}`, {
                        method: 'GET',
                        headers: {
                            'X-Requested-With': 'XMLHttpRequest',
                        },
                    })
                    .then(response => response.json())
                    .then(data => {
                        if (data.contents.length > 0) {
                            let sectionContent = document.querySelector('.section-content');
                            data.contents.forEach(content => {
                                let article = `
                                    <article class="news-content">
                                        <div class="news-image">
                                            <a href="/${content.content_category_sub.content_categories.slug}/read/${content.slug}">
                                                <img class="object-fit-cover h-100 w-100" src="${content.featured_image}" onerror="handleImage(this)" />
                                            </a>
                                        </div>
                                        <div class="news-detail">
                                            <div class="news-info mb-2">
                                                <p class="text-secondary">
                                                    ${new Date(content.published_at).toLocaleString('id-ID', {
                                                        day: '2-digit',
                                                        month: 'short',
                                                        year: 'numeric',
                                                        hour: '2-digit',
                                                        minute: '2-digit',
                                                    }).replace(',', ' |')}
                                                </p>
                                            </div>
                                            <a href="/${content.content_category_sub.content_categories.slug}/read/${content.slug}">
                                                <h3 class="news-title">
                                                    ${content.title}
                                                </h3>
                                            </a>
                                            <p class="description-news text-secondary">
                                                ${content.desc_seo}
                                            </p>
                                        </div>
                                    </article>`;
                                sectionContent.insertAdjacentHTML('beforeend', article);
                            });
                            offset += 10;
                            showMoreButton.style.display = 'inline'; // Show the button again
                        } else {
                            showMoreButton.remove(); // Remove the button if no more content
                        }
                    })
                    .catch(error => console.error('Error loading more content:', error))
                    .finally(() => {
                        loader.style.display = 'none'; // Hide the loader
                    });
            });
        }
    </script>
@endpush
