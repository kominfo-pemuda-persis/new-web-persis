"use strict";
var KTSigninGeneral = (function () {
    var t, e, r;
    return {
        init: function () {
            (t = document.querySelector("#kt_sign_in_form")),
                (e = document.querySelector("#kt_sign_in_submit")),
                (r = FormValidation.formValidation(t, {
                    fields: {
                        email: {
                            validators: {
                                regexp: {
                                    regexp: /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
                                    message:
                                        "Harus berupa alamat surel yang valid.",
                                },
                                notEmpty: {
                                    message: "Email tidak boleh kosong",
                                },
                            },
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: "Password tidak boleh kosong",
                                },
                            },
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: ".fv-row",
                            eleInvalidClass: "",
                            eleValidClass: "",
                        }),
                    },
                })),
                !(function (t) {
                    try {
                        return new URL(t), !0;
                    } catch (t) {
                        return !1;
                    }
                })(e.closest("form").getAttribute("action"));
            e.addEventListener("click", function (i) {
                i.preventDefault(),
                    r.validate().then(function (r) {
                        "Valid" == r
                            ? (e.setAttribute("data-kt-indicator", "on"),
                              (e.disabled = !0),
                              axios
                                  .post(
                                      e.closest("form").getAttribute("action"),
                                      new FormData(t)
                                  )
                                  .then(function (e) {
                                      console.log("log Disni", e.data.status);
                                      if (e.data.status) {
                                          t.reset(),
                                              Swal.fire({
                                                  text: "Berhasil Login!",
                                                  icon: "success",
                                                  buttonsStyling: !1,
                                                  confirmButtonText: "Ok!",
                                                  customClass: {
                                                      confirmButton:
                                                          "btn btn-primary",
                                                  },
                                              });
                                          const e = t.getAttribute(
                                              "data-kt-redirect-url"
                                          );
                                          e && (location.href = e);
                                      } else
                                          Swal.fire({
                                              text: e.data.message,
                                              icon: "error",
                                              buttonsStyling: !1,
                                              confirmButtonText: "Ok!",
                                              customClass: {
                                                  confirmButton:
                                                      "btn btn-primary",
                                              },
                                          });
                                  })
                                  .catch(function (t) {
                                      Swal.fire({
                                          text: "Maaf, Terjadi Kesalahan pada Aplikasi, Mohon hubungi Administrator.",
                                          icon: "error",
                                          buttonsStyling: !1,
                                          confirmButtonText: "Ok!",
                                          customClass: {
                                              confirmButton: "btn btn-primary",
                                          },
                                      });
                                  })
                                  .then(() => {
                                      e.removeAttribute("data-kt-indicator"),
                                          (e.disabled = !1);
                                  }))
                            : Swal.fire({
                                  text: "Email/Kata Sandi tidak boleh kosong!",
                                  icon: "error",
                                  buttonsStyling: !1,
                                  confirmButtonText: "Ok!",
                                  customClass: {
                                      confirmButton: "btn btn-primary",
                                  },
                              });
                    });
            });
        },
    };
})();

KTUtil.onDOMContentLoaded(function () {
    KTSigninGeneral.init();
});
