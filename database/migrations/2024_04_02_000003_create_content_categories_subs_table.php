<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('content_categories_subs', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->string('category_sub_name');
            $table->string('slug');
            $table->string('desc')->nullable();

            $table->foreignUuid('content_categories_id');
            $table->foreign('content_categories_id')
                ->references('id')
                ->on('content_categories');

            $table->tinyInteger('status')->default('1');

            $table->foreignUuid('created_by');
            $table->foreign('created_by')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('content_categories_subs');
    }
};
