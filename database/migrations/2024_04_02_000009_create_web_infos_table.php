<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('web_infos', function (Blueprint $table) {
            $table->string('web_home', 100);
            $table->string('web_title', 100);
            $table->string('web_icon', 100);
            $table->string('web_logo_top', 255);
            $table->text('web_info_top')->nullable();
            $table->string('web_logo_footer', 255);
            $table->text('web_footer_info')->nullable();
            $table->string('url_socmed_fb', 100)->nullable();
            $table->string('url_socmed_ig', 100)->nullable();
            $table->string('url_socmed_x', 100)->nullable();
            $table->string('url_mail', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('web_infos');
    }
};
