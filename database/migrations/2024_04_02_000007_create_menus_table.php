<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->string('menu_name');
            $table->string('desc');
            $table->text('slug');

            $table->foreignUuid('parent')->nullable();
            $table->tinyInteger('status');
            $table->tinyInteger('can_navigate');
            $table->tinyInteger('order');

            $table->foreignUuid('created_by');
            $table->foreign('created_by')
                ->references('id')
                ->on('users');

            $table->timestamps();
        });

        Schema::table('menus', function (Blueprint $table) {
            $table->foreign('parent')
                ->references('id')
                ->on('menus');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('menus');
    }
};
