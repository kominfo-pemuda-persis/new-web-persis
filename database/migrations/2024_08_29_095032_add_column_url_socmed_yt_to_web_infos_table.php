<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('web_infos', function (Blueprint $table) {
            $table->string('url_socmed_yt', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('web_infos', function (Blueprint $table) {
            $table->dropColumn('url_socmed_yt');
        });
    }
};
