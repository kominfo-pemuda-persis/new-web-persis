<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('set_iklan_list', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('ads_position');
            $table->foreign('ads_position')
                ->references('id')
                ->on('set_iklan');

            $table->uuid('ads_id')->nullable();
            $table->foreign('ads_id')->references('id')->on('advertisements')->onDelete('cascade');

            $table->integer('order')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('set_iklan_list');
    }
};
