<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('istifta', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->string('full_name');
            $table->boolean('show_name')->default(0);
            $table->string('email');
            $table->boolean('send_to_email')->default(0);
            $table->string('topik_pertanyaan');
            $table->text('pertanyaan');

            $table->foreignUuid('content_id')->nullable();
            $table->foreign('content_id')
                ->references('id')
                ->on('contents');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('istifta');
    }
};
