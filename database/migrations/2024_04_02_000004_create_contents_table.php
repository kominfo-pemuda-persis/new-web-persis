<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->string('title');
            $table->text('featured_image');
            $table->text('desc_seo');
            $table->text('slug');
            $table->text('content_type');

            $table->foreignUuid('content_category_sub_id');
            $table->foreign('content_category_sub_id')
                ->references('id')
                ->on('content_categories_subs');

            $table->tinyInteger('is_headline')->default(0);
            $table->enum('display', ['all', 'pages']);
            $table->bigInteger('visited');
            $table->tinyInteger('status')->default(0);
            $table->dateTime('scheduled_date')->nullable();

            $table->foreignUuid('created_by');
            $table->foreign('created_by')
                ->references('id')
                ->on('users');

            $table->dateTime('published_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('contents');
    }
};
