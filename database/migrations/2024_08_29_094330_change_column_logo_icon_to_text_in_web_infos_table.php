<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('web_infos', function (Blueprint $table) {
            $table->text('web_icon')->nullable()->change();
            $table->text('web_logo_top')->nullable()->change();
            $table->text('web_logo_footer')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('web_infos', function (Blueprint $table) {
            $table->string('web_icon', 100)->nullable()->change();
            $table->string('web_logo_top', 255)->nullable()->change();
            $table->string('web_logo_footer', 255)->nullable()->change();
        });
    }
};
