<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('advertisements', function (Blueprint $table) {
            // Ubah nama kolom client menjadi client_id
            $table->renameColumn('client', 'client_id');
        });

        // Ubah tipe kolom client_id menjadi UUID dengan menggunakan klausa USING
        DB::statement('ALTER TABLE advertisements ALTER COLUMN client_id TYPE uuid USING client_id::uuid');

        Schema::table('advertisements', function (Blueprint $table) {
            // Tambahkan foreign key constraint
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('advertisements', function (Blueprint $table) {
            // Drop foreign key constraint
            $table->dropForeign(['client_id']);
        });

        // Ubah tipe kolom client_id kembali ke string dengan menggunakan klausa USING
        DB::statement('ALTER TABLE advertisements ALTER COLUMN client_id TYPE varchar USING client_id::varchar');

        Schema::table('advertisements', function (Blueprint $table) {
            // Ubah nama kolom client_id menjadi client
            $table->renameColumn('client_id', 'client');
        });
    }
};
