<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('content_pages', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->foreignUuid('content_id');
            $table->foreign('content_id')
                ->references('id')
                ->on('contents');

            $table->text('content');
            $table->text('embeded_string')->nullable();
            $table->text('photo')->nullable();
            $table->text('photo_desc')->nullable();

            $table->foreignUuid('related_content')->nullable();
            $table->foreign('related_content')
                ->references('id')
                ->on('contents');

            $table->foreignUuid('advertisement_id')->nullable();
            $table->foreign('advertisement_id')
                ->references('id')
                ->on('advertisements');

            $table->enum('advertisement_position', ['atas', 'tengah', 'bawah'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('content_pages');
    }
};
