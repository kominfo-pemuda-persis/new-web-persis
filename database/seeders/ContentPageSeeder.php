<?php

namespace Database\Seeders;

use App\Models\Advertisements;
use App\Models\Content;
use App\Models\ContentPage;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ContentPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $positions = ['atas', 'tengah', 'bawah'];

        $content_satu = Content::where('slug', 'persis-dan-persistri-kota-tangerang-siap-tebar-1000-paket-sembako-selama-ramadan-tahun-ini')->firstorfail()->id;
        $content_dua = Content::where('slug', 'persis-dukung-penyelenggaraan-sidang-isbat-demi-minimalisasi-dampak-perbedaan-di-kalangan-umat-islam-indonesia')->firstorfail()->id;
        $content_tiga = Content::where('slug', 'dukung-fatwa-mui-persis-dorong-umat-islam-tak-menggunakan-produk-pro-israel')->firstorfail()->id;
        $content_empat = Content::where('slug', 'semakin-tangguh-di-era-digital')->firstorfail()->id;
        $content_lima = Content::where('slug', 'serba-serbi-haji-2024')->firstorfail()->id;
        $content_enam = Content::where('slug', 'praktek-manasik-haji-khusus-karya-Imtaq-persatuan-islam')->firstorfail()->id;

        $data = [
            [
                'content_id' => $content_satu,
                'content' =>
                "<p>Kota Tangerang, persis.or.id - Pimpinan Daerah Persatuan Islam (PERSIS) dan Persatuan Islam Istri (PERSISTRI) Kota Tangerang kembali berkolaborasi mengajak ikhwatu iman untuk melakukan aksi sosial di Bulan Suci Ramadan 1445 H. Ajakan aksi sosial ini dituangkan dalam Surat Edaran yang ditanda tangani oleh Ketua PERSIS Ustaz Budiman. SE, dan Ketua PERSISTRI Kota Tangerang Ir.&nbsp; Mardiah Rahman Alimar di Kota Tangerang, Ahad 24 Maret 2024.</p><p>Ketua PD PERSIS Kota Tangerang Ustaz Budiman SE, menjelaskan, aksi sosial berbagi sembako sudah menjadi agenda rutin tahunan keluarga besar PERSIS dan PERSISTRI Kota Tangerang.</p>p>&ldquo;Melalui PD PERSISTRI Kota Tangerang, setiap bulan Ramadhan melakukan aksi sosial berupa pasar murah. Kami juga mengadakan Ta'jil&nbsp; on the road. Hal ini bertujuan untuk meringkankan beban masyarakat di tengah sulitnya ekonomi dan mahalnya harga kebutuhan sembako,&rdquo; kata Ustaz Budiman ketika dimintai keterangannya, Kamis (7</p>p>Panitia mentargetkan 1.000 paket yang rencananya akan didistribusikan kepada masyarakat yang membutuhkan khususnya di wilayah sekitar kantor PD PERSIS dan PERSISTRI Kota Tangerang serta Pondok Tahfidz Wadil Quran (PTWQ)</p>p>&ldquo;Untuk merealisasikan target tersebut, kami memohon dukungan dan aksi nyata para donatur sekalian atas kegiatan ini,&rdquo; ungkapnya.&nbsp;</p>p>Senada dengan Ustaz Budiman, Ketua PERSISTRI Kota Tangerang, Ustazah Ir.&nbsp; Mardiah Rahman Alimar menambahkan, aksi sosial ini memiliki tujuan untuk merekatkan tali silaturahim terhadap sesama manusia.</p>p>Selain itu, lanjut Ustadzah Mardiah, hal ini terlaksana sebagai perwujudan salah satu program kerja Bidang Garapan Sosial yakni menumbuhkan kepedulian sosial, serta menguatkan peran dan fungsi Jam'iyyah dalam bermasyarakat.</p>p>&ldquo;Dengan harapan, Jam'iyyah ini dapat memberikan manfaat sebesar- besarnya untuk masyarakat sekitar,&rdquo; ucapnya.</p>p>Ustazah Mardiah pun menerangkan, adapun tema yang kami angkat pada aksi sosial Ramadan 1445 H adalah Membangkitkan Semangat Meraih Taqwa Dalam Menghidupkan Keutamaan Bulan Ramadhan.</p>p>Peserta kegiatan Ramadhan Berkah 1445 H adalah Anggota PD PERSIS dan PERSISTRI Kota Tangerang, Santri Wadil Qur'an, dan Masyarakat Umum di sekitar kantor PD PERSIS Kota Tangerang.</p>p>&ldquo;In syaa Allah aksi sosial akan kami selenggarakan di Klinik drg. Rina Rahmatun pada Ahad 24 Maret 2024, di Jalan Empu Gandring No 34, Perumnas 2, Kota Tangerang,&rdquo; jelasnya.</p>",
                'embeded_string' => null,
                'photo' => null,
                'photo_desc' => null,
                'related_content' => $content_dua,
                'advertisement_id' => Advertisements::first()->id,
                'advertisement_position' => $positions[array_rand($positions)],
            ],
            [
                'content_id' => $content_dua,
                'content' =>
                '"<p>Jakarta, Persis.or.id - Pimpinan Pusat Persatuan Islam (PP Persis) menilai pelaksanaan Sidang Isbat memberikan manfaat yang besar bagi umat Islam Indonesian. Pasalnya, Indonesia terdiri dari banyak ormas yang memiliki lembaga-lembaga hisab dan rukyat yang berbeda-beda metoda dan standar yang dipakainya. Hal ini, menanggapi wacana dan usulan peniadaan sidang isbat. demi efisiensi anggaran.&nbsp;</p> <p>PP Persis secara resmi mengirim pandangan tersebut dalam Surat Rekomendasi untuk Menteri Agama Republik Indonesia (Cq. Direktur Jenderal Bimbingan Masyarakat Islam) yang ditandatangani oleh Ketua Umum PP Persatuan Islam Ustadz Jeje Zaenudin dan Sekretaris Umum Dr Haris Muslim di Bandung, Kamis 7 Maret 2023.</p> <p>""Maka dengan keberadaan forum sidang isbat di bawah Kementerian Agama sangat besar manfaatnya dalam meminimalisir dampak negatif dari adanya perbedaan awal Ramadhan, Idulfitri, dan Iduladha,"" kata Ketua Umum PP Persis, Ustaz Jeje Zaenudin dalam keterangannya, Ahad (10/3/2024).</p> <p>Meski penanggalan bulan hijriyah setiap bulan sudah dapat diketahui dan ditetapkan melalui penghitungan hisab dan standar imkanurrukyat yang disepakati, namun untuk Ramadhan, Idulfitri, dan Iduladha, forum sidang isbat tetap dibutuhkan sebagai proses konfirmasi dan penegasan terhadap kebenaran hasil penghitungan hisab.</p> <p>""Sehingga memberi kekuatan dan kepastian hukum bagi seluruh kelompok masyarakat muslim Indonesia, baik yang mau mengikuti penetapan pemerintah ataupun yang mau mengikuti penetapan ormas yang diikutinya,"" ujarnya.</p> <p>Menurutnya, Persis mendukung pemerintah untuk melanjutkan keberadaan forum Sidang Isbat sebagai pelaksanaan ketentuan pasal 52A Undang-Undang Nomor 3 Tahun 2006. Namun, bentuk dan formatnya bisa disesuaikan dengan situasi, kondisi, dan kebutuhan.</p> <p>&nbsp;</p>"',
                'embeded_string' => null,
                'photo' => null,
                'photo_desc' => null,
                'related_content' => $content_satu,
                'advertisement_id' => Advertisements::first()->id,
                'advertisement_position' => $positions[array_rand($positions)],
            ],
            [
                'content_id' => $content_tiga,
                'content' =>
                '<p><span style="font-family:  geneva, sans-serif;">Bandung, persis.or.id - Pimpinan Pusat Persatuan Islam sangat mendukung fatwa Majelis Ulama Indonesia yang mengharamkan produk dari perusahaan yang berpihak kepada Israel, termasuk buah kurma yang identik pada bulan suci Ramadhan.</span><br /><br /><span style="font-family:  geneva, sans-serif;">Hal ini dikatakan oleh Ketua Bidang Dakwah Pimpinan Pusat Persatuan Islam Drs KH. Uus Muhammad Ruhiat, ketika dimintai keterangannya, Senin (11/3/2024).</span><br /><br /><span style="font-family:  geneva, sans-serif;">Selain itu, Kiai Uus pun mengapresiasi Ketum PP Persis Dr. KH. Jeje Zaenudin yang nampak hadir bersama Ketua Ormas Islam lainnya ketika mendeklarasikan fatwa tersebut.</span><br /><br /><span style="font-family:  geneva, sans-serif;">&ldquo;Sudah seharusnya umat Islam tidak membeli dan mengkonsumsi semua jenis produk apapun dari Israel,&rdquo; tegas Kiai Uus.</span><br /><br /><span style="font-family:  geneva, sans-serif;">Hal ini karena kekejaman Israel kepada rakya Palestina.</span><br /><br /><span style="font-family:  geneva, sans-serif;">Kiai Uus memandang, pengharaman itu didasarkan atas dasar saddudzariah atau tindakan preventif. </span><span style="font-family: geneva, sans-serif;">Sampai saat ini, rakyat Palestina melawan Israel dengan nyawa.</span><br /><br /><span style="font-family:  geneva, sans-serif;">&ldquo;Dan sebaliknya, kita harus terus melawan Israel dengan membangkrutkan perusahaan-perusahaan mereka,&rdquo; ungkapnya.</span>',
                'embeded_string' => null,
                'photo' => null,
                'photo_desc' => null,
                'related_content' => $content_dua,
                'advertisement_id' => Advertisements::first()->id,
                'advertisement_position' => $positions[array_rand($positions)],
            ],
            [
                'content_id' => $content_tiga,
                'content' =>
                '<span style="font-family:  geneva, sans-serif;">Apapun yang kita beli dan kita konsumsi dari Israel, akan mengingatkan kita betapa kejamnya serangan Israel kepada rakyat Palestina.</span><br /><br /><span style="font-family:  geneva, sans-serif;">&ldquo;Hal itu tentu akan menyedihkan kita,&rdquo; tandasnya.</span><br /><br /><span style="font-family:  geneva, sans-serif;">Terakhir, Kiai Uus berpesan, membeli produk mereka berarti melanggengkan penjajahan terhadap warga Palestina dan tidak membelinya berarti melemahkan kedigdayaan mereka sebagai penjajah</span><br /><br /><span style="font-family:  geneva, sans-serif;">Diketahui Majelis Ulama Indonesia mengeluarkan fatwa haram tersebut disampaikan Ketua MUI Bidang Hubungan Luar Negeri dan Kerja Sama Internasional Prof Sudarnoto Abdul Hakim dalam acara launching kegiatan Safari Ramadhan bertemakan membasuh luka Palestina di kantor MUI, Jakarta Pusat, dikutip dari inews.id, Ahad (10/3/2024). (/HL)</span></p>',
                'embeded_string' => null,
                'photo' => null,
                'photo_desc' => null,
                'related_content' => $content_satu,
                'advertisement_id' => Advertisements::first()->id,
                'advertisement_position' => $positions[array_rand($positions)],
            ],
            [
                'content_id' => $content_empat,
                'content' =>
                '"<p class=""""MsoNormal"""" style=""""margin-bottom: 0cm; text-align: center;"""" align=""""center""""><span style=""""font-family:  sans-serif; font-size: 12pt;"""">oleh Silma Kaffah Millati, S.Psi*</span></p><p class=""""MsoNormal"""" style=""""margin-bottom: 0cm; text-align: justify;"""" align=""""center""""><span style=""""font-family:  sans-serif; font-size: 12pt; text-align: justify;"""">Sejak pertama manusia diciptakan, manusia hidup berdampingan dengan berbagai macam tantangan dan permasalahan. Sampai hari ini, kita hidup dalam kemajuan teknologi yang sangat pesat, arus informasi yang sangat cepat dan acak-acakan, kehidupan bisa kita nikmati melalui genggaman, saling tersenyum dengan sahabat menggunakan sebidang layar kecil dan kemudahan lainnya. </span></p><p class=""""MsoNormal"""" style=""""margin-bottom: 0cm; text-align: justify;"""" align=""""center""""><span style=""""font-family:  sans-serif; font-size: 12pt; text-align: justify;"""">Problematika kehidupan satu abad lalu bahkan belasan tahun lalu saja dengan saat ini jauh berbeda. </span><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1;"""">Di samping segala macam kemudahan yang dihadirkan oleh teknologi, ternyata banyak juga tantangan dan permasalahan baru yang harus manusia hadapi.</span></p><p class=""""MsoNormal"""" style=""""margin-bottom: 0cm; text-align: justify;"""" align=""""center""""><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1;""""> Terutama tantangan dalam aspek psikologis, manusia yang hari ini sangat dekat dengan digitalisasi khususnya generasi milenial dan generasi Z terdampak banyak oleh digitalisasi, baik dampak positif maupun dampak negatifnya.</span></p><p class=""""MsoNormal"""" style=""""text-align: justify;""""><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1;"""">Dampak positif yang ditimbulkan tentu berbagai macam kemudahan, mulai dari kemudahan komunikasi, informasi, transportasi, sampai kemudahan transaksi.</span></p>"',
                'embeded_string' => null,
                'photo' => null,
                'photo_desc' => null,
                'related_content' => $content_satu,
                'advertisement_id' => Advertisements::first()->id,
                'advertisement_position' => $positions[array_rand($positions)],
            ],
            [
                'content_id' => $content_empat,
                'content' =>
                '"<p class=""""MsoNormal"""" style=""""text-align: justify;""""><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1;"""">Namun, tidak dapat menutup mata, dampak negatif yang selanjutnya menjadi tantangan yang harus kita hadapi di era digital ini adalah derasnya arus informasi yang tidak bisa kita bendung, kekerasan yang berbasis digital <em>(cyber bullying, cyber crime, cyber sexual),</em> mentalitas &ldquo;instan&rdquo; yang secara langsung ataupun tidak, misalnya, fenomena <em>Fearing Of Missing Out</em> (FOMO) dan masih banyak permasalahan lainnya yang hadir.</span></p><p class=""""MsoNormal"""" style=""""text-align: justify;""""><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1;"""">Dalam menghadapi berbagai macam dinamika kehidupan, Allah telah menganugerahkan kepada setiap manusia <em style=""""mso-bidi-font-style: normal;"""">self recilience, </em>yaitu mekanisme dinamis yang menyertakan peran beragam faktor individual, sosial, dan lingkungan yang menggambarkan kapasitas dan ketangguhan seseorang untuk bangkit dari permasalahan emosional negatif ketika menghadapi situasi yang menekan atau mengandung kendala yang signifikan (Hendriani, 2018)<a style=""""mso-footnote-id: ftn1;"""" title="""""""" href=""""file:///D:/PP%20Pemudi/Senbud/Buletin%20Annisa/edisi%20kesmen/edit.%20Buletin%20Anisa%20(Silma%20Kaffah%20Millati).docx#_ftn1"""" name=""""_ftnref1""""><span class=""""MsoFootnoteReference""""><span style=""""mso-special-character: footnote;""""><!-- [if !supportFootnotes]--><span class=""""MsoFootnoteReference""""><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;"""">[1]</span></span><!--[endif]--></span></span></a>.</span></p><p class=""""MsoNormal"""" style=""""text-align: justify;""""><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1;"""">Secara alamiah manusia memiliki ketangguhan, kecenderungan untuk bangkit dari keterpurukan, kemampuan beradaptasi dengan situasi yang menyulitkan.</span></p>"',
                'embeded_string' => null,
                'photo' => null,
                'photo_desc' => null,
                'related_content' => $content_dua,
                'advertisement_id' => Advertisements::first()->id,
                'advertisement_position' => $positions[array_rand($positions)],
            ],
            [
                'content_id' => $content_empat,
                'content' =>
                ' "<p class=""""MsoNormal"""" style=""""text-align: justify;""""><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1;"""">Penelitian yang dilakukan oleh Reivich dan Shatte (2002) selama kurang lebih 15 tahun di Universitas Pennsylvania mengungkapkan bahwa resiliensi memegang peranan penting dalam hidup individu, yang mana resiliensi merupakan hal yang esensial bagi kesuksesan dan kebahagiaan<a style=""""mso-footnote-id: ftn2;"""" title="""""""" href=""""file:///D:/PP%20Pemudi/Senbud/Buletin%20Annisa/edisi%20kesmen/edit.%20Buletin%20Anisa%20(Silma%20Kaffah%20Millati).docx#_ftn2"""" name=""""_ftnref2""""><span class=""""MsoFootnoteReference""""><span style=""""mso-special-character: footnote;""""><!-- [if !supportFootnotes]--><span class=""""MsoFootnoteReference""""><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;"""">[2]</span></span><!--[endif]--></span></span></a>.</span></p> <p class=""""MsoNormal"""" style=""""text-align: justify;""""><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1;"""">Bahkan jauh sebelum teori-teori barat dan penelitian tentang <em style=""""mso-bidi-font-style: normal;"""">self recilience </em>ini, Allah sudah mengabarkan bagaimana kita harus bersabar dalam menghadapi ujian melalui firman-Nya dalam QS Furqan ayat 75.</span></p> <p class=""""MsoNormal"""" style=""""text-align: justify;""""><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1;""""><em style=""""mso-bidi-font-style: normal;"""">&rdquo;<em><span style=""""font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; background: white;"""">Mereka itulah orang yang dibalasi dengan martabat yang tinggi (dalam surga) karena kesabaran mereka dan mereka disambut dengan penghormatan dan ucapan selamat di dalamnya.&rdquo;</span></em></em></span></p> <p class=""""MsoNormal"""" style=""""text-align: justify;""""><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1;""""><em><span style=""""font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; background: white; font-style: normal; mso-bidi-font-style: italic;"""">Sebab bersabar dalam ujian adalah fondasi awal </span></em><em><span style=""""font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; background: white;"""">self recilience </span></em><em><span style=""""font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; background: white; font-style: normal; mso-bidi-font-style: italic;"""">menuju langkah-langkah selanjutnya untuk bangkit.</span></em></span></p> <p class=""""MsoNormal"""" style=""""text-align: justify;""""><em><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; background: white; font-style: normal; mso-bidi-font-style: italic;"""">Memang terdengar klasik di telinga kita, dan orang pada umumnya sudah mengetahui bahwa ketika diuji harus dihadapi dengan kesabaran. Namun sering kali dalam realitanya bersabar adalah sesuatu yang sangat sulit saat dijalani. Oleh sebab itu lah Allah membalas kesabaran kita di dunia dengan surga, </span></em><em><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; background: white; font-style: normal;"""">Insyaallah.</span></em></p> <p class=""""MsoNormal"""" style=""""text-align: justify;""""><em><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; background: white; font-style: normal; mso-bidi-font-style: italic;"""">Apalagi di era digital ini, permasalahan dan ujian bisa datang kapan saja dan dengan mudahnya dapat mengubah kondisi emosional kita. Secara pribadi, saya ingin mengajak kepada seluruh pembaca untuk sama-sama lebih tangguh dalam menjalani kehidupan bagaimanapun kondisinya.</span></em></p> <p class=""""MsoNormal"""" style=""""text-align: justify;""""><em><span style=""""font-size: 12.0pt; line-height: 107%; font-family: sans-serif; mso-bidi-font-family: Calibri; mso-bidi-theme-font: minor-latin; color: black; mso-themecolor: text1; border: none windowtext 1.0pt; mso-border-alt: none windowtext 0cm; padding: 0cm; background: white; font-style: normal; mso-bidi-font-style: italic;"""">Percayalah pada Allah Swt. bahwa Dia akan menolongmu, percayalah pada diri sendiri bahwa kita mampu melewati ujian ini dengan baik, percayalah pada masa depan yang lebih baik jika kita mampu bersabar hari ini.</span></em></p> <p class=""""MsoNormal"""">&nbsp;</p> <div style=""""mso-element: footnote-list;""""><hr align=""""left"""" size=""""1"""" width=""""33%"""" /><!--[endif]--> <div id=""""ftn1"""" style=""""mso-element: footnote;""""> <p class=""""MsoFootnoteText""""><span class=""""MsoFootnoteReference""""><strong><span style=""""font-size: 12pt; line-height: 17.12px; font-family:  sans-serif;"""">*Bidgar Advokasi PC Pemudi PERSIS Batununggal</span></strong></span></p> <p class=""""MsoFootnoteText""""><a style=""""mso-footnote-id: ftn1;"""" title="""""""" href=""""file:///D:/PP%20Pemudi/Senbud/Buletin%20Annisa/edisi%20kesmen/edit.%20Buletin%20Anisa%20(Silma%20Kaffah%20Millati).docx#_ftnref1"""" name=""""_ftn1""""><span class=""""MsoFootnoteReference""""><span style=""""mso-special-character: footnote;""""><!-- [if !supportFootnotes]--><span class=""""MsoFootnoteReference""""><span style=""""font-size: 10.0pt; line-height: 107%; font-family: sans-serif; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;"""">[1]</span></span><!--[endif]--></span></span></a> Dianesti Dewi. A, Taufik. (2022) Resiliensi Anak yang Pernah Berhadapan dengan Hukuman. <em style=""""mso-bidi-font-style: normal;"""">Social Work Jurnal.</em> Vol : XII. No. 01: 33-34</p> </div> <div id=""""ftn2"""" style=""""mso-element: footnote;""""> <p class=""""MsoFootnoteText""""><a style=""""mso-footnote-id: ftn2;"""" title="""""""" href=""""file:///D:/PP%20Pemudi/Senbud/Buletin%20Annisa/edisi%20kesmen/edit.%20Buletin%20Anisa%20(Silma%20Kaffah%20Millati).docx#_ftnref2"""" name=""""_ftn2""""><span class=""""MsoFootnoteReference""""><span style=""""mso-special-character: footnote;""""><!-- [if !supportFootnotes]--><span class=""""MsoFootnoteReference""""><span style=""""font-size: 10.0pt; line-height: 107%; font-family: sans-serif; mso-ascii-theme-font: minor-latin; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin;  mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;"""">[2]</span></span><!--[endif]--></span></span></a> Ibid</p> </div> </div>"',
                'embeded_string' => null,
                'photo' => null,
                'photo_desc' => null,
                'related_content' => $content_tiga,
                'advertisement_id' => Advertisements::first()->id,
                'advertisement_position' => $positions[array_rand($positions)],
            ],
            [
                'content_id' => $content_lima,
                'content' => '"<p>Muliakan Tamu Allah, Direktur Bina Haji Kemenag RI Dorong Petugas PPIH Ramah Lansia</p>"',
                'embeded_string' => null,
                'photo' => 'https://i.ibb.co/LQCJ5p7/Gambar-Whats-App-2024-03-20-pukul-11-40-17-a8fff919.jpg',
                'photo_desc' => null,
                'related_content' => null,
                'advertisement_id' => Advertisements::first()->id,
                'advertisement_position' => $positions[array_rand($positions)],
            ],
            [
                'content_id' => $content_lima,
                'content' => '<p>Wamenag RI Tegaskan Kesiapan Petugas PPIH 2024 agar Pelayanan Prima kepada Para Jemaah Haji</p>',
                'embeded_string' => null,
                'photo' => 'https://i.ibb.co/LQCJ5p7/Gambar-Whats-App-2024-03-20-pukul-11-40-17-a8fff919.jpg',
                'photo_desc' => null,
                'related_content' => null,
                'advertisement_id' => Advertisements::first()->id,
                'advertisement_position' => $positions[array_rand($positions)],
            ],
            [
                'content_id' => $content_lima,
                'content' => '<p>Melalui Beberapa Kali Tes, Kader Kader Terbaik PERSIS Terpilih sebagai Petugas PPIH Arab Saudi 1445 H</p>',
                'embeded_string' => null,
                'photo' => 'https://i.ibb.co/LQCJ5p7/Gambar-Whats-App-2024-03-20-pukul-11-40-17-a8fff919.jpg',
                'photo_desc' => null,
                'related_content' => null,
                'advertisement_id' => Advertisements::first()->id,
                'advertisement_position' => $positions[array_rand($positions)],
            ],
            [
                'content_id' => $content_lima,
                'content' => '<p>Miliki Tanggung jawab Besar Selama Bertugas, Bernilai Ibadah bagi Petugas PPIH Arab Saudi</p>',
                'embeded_string' => null,
                'photo' => 'https://i.ibb.co/LQCJ5p7/Gambar-Whats-App-2024-03-20-pukul-11-40-17-a8fff919.jpg',
                'photo_desc' => null,
                'related_content' => null,
                'advertisement_id' => Advertisements::first()->id,
                'advertisement_position' => $positions[array_rand($positions)],
            ],
            [
                'content_id' => $content_enam,
                'content' => '<p></p>',
                'embeded_string' => '<iframe src="https://www.youtube.com/embed/pBzeowaFpYE?si=U_pXoDhcIEfLhVlu" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>',
                'photo' => null,
                'photo_desc' => null,
                'related_content' => null,
                'advertisement_id' => Advertisements::first()->id,
                'advertisement_position' => $positions[array_rand($positions)],
            ],
        ];

        foreach ($data as $value) {
            if (!ContentPage::where('content', $value['content'])->first()) {
                ContentPage::create($value);
            }
        }
    }
}
