<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\ContentTag;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([RoleSeeder::class, UserSeeder::class, ReporterSeeder::class, ContentCategoriesSeeder::class, ContentCategoriesSubSeeder::class, ContentSeeder::class, ClientSeeder::class, AdvertisementSeeder::class, ContentPageSeeder::class, TagSeeder::class, ContentTagSeeder::class, SetIklanSeeder::class, WebInfoSeeder::class]);
    }
}
