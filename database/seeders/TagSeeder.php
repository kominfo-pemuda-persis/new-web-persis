<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $editor_role = Role::where('role_name', 'Editor')->firstorfail()->id;
        $editor_id = User::where('role', $editor_role)->firstorfail()->id;

        $data = [
            [
                'title' => 'Sidang Isbat',
                'desc' => 'Seputar Sidang Isbat Kemenag',
                'slug' => 'tag-sidang-isbat',
                'created_by' => $editor_id,
            ],
            [
                'title' => 'Sembako',
                'desc' => 'Seputar Sembako',
                'slug' => 'tag-sembako',
                'created_by' => $editor_id,
            ],
            [
                'title' => 'Kalender',
                'desc' => 'Seputar Penentuan Kalender',
                'slug' => 'tag-kalender',
                'created_by' => $editor_id,
            ],
            [
                'title' => 'Hijriyyah',
                'desc' => 'Seputar Kalender Hijriyyah',
                'slug' => 'tag-hijriyyah',
                'created_by' => $editor_id,
            ],
            [
                'title' => 'Ramadhan',
                'desc' => 'Seputar Ramadhan',
                'slug' => 'tag-ramadhan',
                'created_by' => $editor_id,
            ],
            [
                'title' => 'Palestina',
                'desc' => 'Seputar Palestina',
                'slug' => 'tag-palestina',
                'created_by' => $editor_id,
            ],
            [
                'title' => 'Boikot',
                'desc' => 'Seputar Boikot Israhell',
                'slug' => 'tag-boikot',
                'created_by' => $editor_id,
            ],
            [
                'title' => 'Haji',
                'desc' => 'Seputar Haji',
                'slug' => 'tag-haji',
                'created_by' => $editor_id,
            ],
            [
                'title' => 'Digital',
                'desc' => 'Seputar Informasi Digital',
                'slug' => 'tag-digital',
                'created_by' => $editor_id,
            ],
            [
                'title' => 'Pojok Ketua',
                'desc' => 'Seputar Informasi dari Ketua PP PERSIS',
                'slug' => 'tag-pojok-ketua',
                'created_by' => $editor_id,
            ],
        ];

        foreach ($data as $value) {
            if (!Tag::where('title', $value['title'])->first()) {
                Tag::create($value);
            }
        }
    }
}
