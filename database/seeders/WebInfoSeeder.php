<?php

namespace Database\Seeders;

use App\Models\WebInfo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class WebInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        WebInfo::truncate();
        WebInfo::create([
            'web_home' => env('APP_URL', url()),
            'web_title' => 'Portal Persis',
            'web_icon' => asset('assets/media/icon/favicon.ico'),
            'web_logo_top' => asset('assets/assets-landing/images/logo-web-persis-dark.svg'),
            'web_info_top' => 'Persatuan Islam adalah sebuah organisasi Islam di Indonesia. Persis didirikan pada 12 September 1923 di Bandung oleh sekelompok Islam yang berminat dalam pendidikan dan aktivitas keagamaan yang dipimpin oleh Mohamad Zamzam dan Muhammad Yunus.',
            'web_logo_footer' => asset('assets/assets-landing/images/logo-web-persis-light.svg'),
            'web_footer_info' => 'Pimpinan Pusat Persatuan Islam - Cahaya Islam Berkemajuan.',
            'url_socmed_fb' => 'https://www.facebook.com/infopersis',
            'url_socmed_ig' => 'https://www.instagram.com/infopersis',
            'url_socmed_x' => 'https://twitter.com/info_persis',
            'url_socmed_yt' => 'https://www.youtube.com/c/PERSISTVCHANNEL',
            'url_mail' => 'mailto:info@persis.or.id',
        ]);
    }
}
