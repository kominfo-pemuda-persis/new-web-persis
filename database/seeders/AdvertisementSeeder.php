<?php

namespace Database\Seeders;

use App\Models\Advertisements;
use App\Models\Client;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AdvertisementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $editor_role = Role::where('role_name', 'Admin Pusat')->firstorfail()->id;
        $admin_pusat_id = User::where('role', $editor_role)->firstorfail()->id;

        // if (App::environment('local') || App::environment('development')) {
        //     $data = [
        //         [
        //             'title' => 'Iklan TGM',
        //             'desc' => 'Iklan TGM Cup',
        //             'image' => '/tgm.png',
        //             'width' => '300',
        //             'height' => '300',
        //             'url_ads' => 'https://tgm.id',
        //             'clicked' => 0,
        //             'client_id' => Client::where('name', 'TGM')->firstorfail()->id,
        //             'ads_start' => null,
        //             'ads_end' => null,
        //             'created_by' => $admin_pusat_id,
        //         ],
        //         [
        //             'title' => 'Lazpersis',
        //             'desc' => 'Percobaan Iklan dari Website Lazpersis',
        //             'image' => 'https://pzu.catatzakat.com/storage/image/pusat-zakat-umat/IR2uasohvJXP38tyW9mvD52vxYFXrktQVNPrI9go.jpg.webp',
        //             'width' => '0',
        //             'height' => '0',
        //             'url_ads' => 'https://lazpersis.or.id/',
        //             'clicked' => 0,
        //             'client_id' => Client::where('name', 'Lazpersis')->firstorfail()->id,
        //             'ads_start' => null,
        //             'ads_end' => null,
        //             'created_by' => $admin_pusat_id,
        //         ],
        //     ];
        // } else {
        $data = [
            [
                'title' => 'Iklan Tersedia Kotak',
                'desc' => 'Area Iklan Tersedia untuk Iklan berbentuk Persegi',
                'image' => 'https://placehold.co/300x300',
                'width' => '300',
                'height' => '300',
                'url_ads' => '#',
                'clicked' => 0,
                'client_id' => Client::where('name', 'PP PERSIS')->firstorfail()->id,
                'ads_start' => null,
                'ads_end' => null,
                'created_by' => $admin_pusat_id,
            ],
            [
                'title' => 'Iklan Tersedia Panjang',
                'desc' => 'Area Iklan Tersedia untuk Iklan berbentuk Persegi Horizontal',
                'image' => 'https://placehold.co/1100x130',
                'width' => '1100',
                'height' => '130',
                'url_ads' => '#',
                'clicked' => 0,
                'client_id' => Client::where('name', 'PP PERSIS')->firstorfail()->id,
                'ads_start' => null,
                'ads_end' => null,
                'created_by' => $admin_pusat_id,
            ],
            [
                'title' => 'Iklan Tersedia Sticky',
                'desc' => 'Area Iklan Tersedia untuk Iklan berbentuk Persegi Panjang Vertical',
                'image' => 'https://placehold.co/160x600',
                'width' => '160',
                'height' => '600',
                'url_ads' => '#',
                'clicked' => 0,
                'client_id' => Client::where('name', 'PP PERSIS')->firstorfail()->id,
                'ads_start' => null,
                'ads_end' => null,
                'created_by' => $admin_pusat_id,
            ],
        ];
        // }

        foreach ($data as $value) {
            if (!Advertisements::where('title', $value['title'])->first()) {
                Advertisements::create($value);
            }
        }
    }
}
