<?php

namespace Database\Seeders;

use App\Models\ContentCategories;
use App\Models\ContentCategoriesSub;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class ContentCategoriesSubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $editor_role = Role::where('role_name', 'Editor')->firstorfail()->id;
        $data = [
            [
                'category_sub_name' => "Berita Jam'iyyah",
                'slug' => \Str::slug("Berita Jam'iyyah"),
                'desc' => "Berita seputar jam'iyyah",
                'content_categories_id' => ContentCategories::where('category_name', 'Berita')->firstorfail()->id,
                'status' => 1,
                'created_by' => User::where('role', $editor_role)->firstorfail()->id,
            ],
            [
                'category_sub_name' => 'Berita Nasional',
                'slug' => \Str::slug('Berita Nasional'),
                'desc' => "Berita Wilayah Indonesia",
                'content_categories_id' => ContentCategories::where('category_name', 'Berita')->firstorfail()->id,
                'status' => 1,
                'created_by' => User::where('role', $editor_role)->firstorfail()->id,
            ],
            [
                'category_sub_name' => 'Berita Internasional',
                'slug' => \Str::slug('Berita Internasional'),
                'desc' => "Berita Luar Negeri",
                'content_categories_id' => ContentCategories::where('category_name', 'Berita')->firstorfail()->id,
                'status' => 1,
                'created_by' => User::where('role', $editor_role)->firstorfail()->id,
            ],
            [
                'category_sub_name' => 'Tsaqofah Islamiyyah',
                'slug' => \Str::slug('Tsaqofah Islamiyyah'),
                'desc' => "Artikel tentang ketahuidan",
                'content_categories_id' => ContentCategories::where('category_name', 'Artikel')->firstorfail()->id,
                'status' => 1,
                'created_by' => User::where('role', $editor_role)->firstorfail()->id,
            ],
            [
                'category_sub_name' => "Kejam'iyyahan",
                'slug' => \Str::slug("Kejam'iyyahan"),
                'desc' => "Artikel tentang Kejam'iyyahan",
                'content_categories_id' => ContentCategories::where('category_name', 'Artikel')->firstorfail()->id,
                'status' => 1,
                'created_by' => User::where('role', $editor_role)->firstorfail()->id,
            ],
            [
                'category_sub_name' => 'Pemikiran',
                'slug' => \Str::slug('Pemikiran'),
                'desc' => 'Artikel tentang isu-isu terkini',
                'content_categories_id' => ContentCategories::where('category_name', 'Artikel')->firstorfail()->id,
                'status' => 1,
                'created_by' => User::where('role', $editor_role)->firstorfail()->id,
            ],
            [
                'category_sub_name' => 'Fiqh',
                'slug' => \Str::slug('Fiqh'),
                'desc' => 'Kajian tentang Fiqh',
                'content_categories_id' => ContentCategories::where('category_name', 'Kajian')->firstorfail()->id,
                'status' => 1,
                'created_by' => User::where('role', $editor_role)->firstorfail()->id,
            ],
            [
                'category_sub_name' => 'Akhlaq',
                'slug' => \Str::slug('Akhlaq'),
                'desc' => 'Kajian tentang Akhlaq',
                'content_categories_id' => ContentCategories::where('category_name', 'Kajian')->firstorfail()->id,
                'status' => 1,
                'created_by' => User::where('role', $editor_role)->firstorfail()->id,
            ],
            [
                'category_sub_name' => 'Hadits',
                'slug' => \Str::slug('Hadits'),
                'desc' => 'Kajian tentang ilmu Hadits',
                'content_categories_id' => ContentCategories::where('category_name', 'Kajian')->firstorfail()->id,
                'status' => 1,
                'created_by' => User::where('role', $editor_role)->firstorfail()->id,
            ],
            [
                'category_sub_name' => "Bingkai Jam'iyyah",
                'slug' => \Str::slug("Bingkai Jam'iyyah"),
                'desc' => "Galeri Foto kegiatan Jam'iyyah",
                'content_categories_id' => ContentCategories::where('category_name', 'Konten Multimedia')->firstorfail()->id,
                'status' => 1,
                'created_by' => User::where('role', $editor_role)->firstorfail()->id,
            ],
        ];
        foreach ($data as $value) {
            if (!ContentCategoriesSub::where('category_sub_name', $value['category_sub_name'])->first()) {
                ContentCategoriesSub::create($value);
            }
        }
    }
}
