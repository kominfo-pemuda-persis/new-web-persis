<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (App::environment('local') || App::environment('development')) {
            $data = [
                [
                    'full_name' => 'Marco Simanalagi',
                    'username' => 'marcothehash',
                    'email' => 'marco.simanalagi@gmail.com',
                    'phone' => '081224388153',
                    'photo' => null,
                    'password' => 'marcothehash',
                    'role' => Role::where('role_name', 'Admin Pusat')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Ujang Hernandes',
                    'username' => 'ujanghernandes',
                    'email' => 'ujang.hernandes@gmail.com',
                    'phone' => '081224388151',
                    'photo' => null,
                    'password' => 'ujanghernandes',
                    'role' => Role::where('role_name', 'Admin Otonom')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Lord Uhe',
                    'username' => 'lorduhe',
                    'email' => 'lord.uhe@gmail.com',
                    'phone' => '081224388150',
                    'photo' => null,
                    'password' => 'uheuhe',
                    'role' => Role::where('role_name', 'Editor')->firstorfail()->id,
                    'status' => 1,
                ],
            ];
        } else {
            $data = [
                [
                    'full_name' => 'Admin Pusat',
                    'username' => 'adminpusat',
                    'email' => 'adminpusat@persis.or.id',
                    'phone' => '-',
                    'photo' => null,
                    'password' => '4dminPusatPersis',
                    'role' => Role::where('role_name', 'Admin Pusat')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Admin Otonom',
                    'username' => 'adminotonom',
                    'email' => 'adminotonom@persis.or.id',
                    'phone' => '-',
                    'photo' => null,
                    'password' => '4dminOtonomPersis',
                    'role' => Role::where('role_name', 'Admin Otonom')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Editor',
                    'username' => 'editor',
                    'email' => 'editor@persis.or.id',
                    'phone' => '-',
                    'photo' => null,
                    'password' => '4EditorPersis',
                    'role' => Role::where('role_name', 'Editor')->firstorfail()->id,
                    'status' => 1,
                ],
            ];
        }

        foreach ($data as $value) {
            if (!User::where('role', $value['role'])->first()) {
                User::create($value);
                dump(
                    'Role : ' . Role::where('id', $value['role'])->first()->role_name,
                    'Email : ' . $value['email'] . ' - Password : ' . $value['password']
                );
            }
        }
    }
}
