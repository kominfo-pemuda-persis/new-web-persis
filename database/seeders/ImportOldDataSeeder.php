<?php

namespace Database\Seeders;

use App\Models\Content;
use App\Models\ContentCategories;
use App\Models\ContentCategoriesSub;
use App\Models\ContentPage;
use App\Models\ContentTag;
use App\Models\Reporter;
use App\Models\Role;
use App\Models\Tag;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ImportOldDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (file_exists(database_path('seeders/posts.json'))) {
            $role_data = [
                [
                    'role_name' => 'Admin Pusat',
                    'desc' => 'User untuk Admin Pusat',
                    'status' => 1,
                ],
                [
                    'role_name' => 'Admin Otonom',
                    'desc' => 'User Untuk Admin Otonom',
                    'status' => 1,
                ],
                [
                    'role_name' => 'Editor',
                    'desc' => '',
                    'status' => 1,
                ],
                [
                    'role_name' => 'Pengguna',
                    'desc' => 'User untuk pengguna',
                    'status' => 0,
                ],
            ];
            foreach ($role_data as $value) {
                if (!Role::where('role_name', $value['role_name'])->first()) {
                    Role::create($value);
                }
            }

            $user_data = [
                [
                    'full_name' => 'Hendi Santika',
                    'username' => 'hendisantika',
                    'email' => 'hendisantika@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Admin Pusat')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'rizki',
                    'username' => 'rizki',
                    'email' => 'rizkiheryandi@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Admin Pusat')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Hasan Nasrulloh',
                    'username' => 'hasannasrulloh',
                    'email' => 'jauhar.jundi@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Admin Pusat')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Ismail Fajar',
                    'username' => 'ismailfajar81',
                    'email' => 'ismailfajar81@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Admin Pusat')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'admin',
                    'username' => 'admin-test',
                    'email' => 'admin@test.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Admin Pusat')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Irwan',
                    'username' => 'irwan',
                    'email' => 'irwan@yopmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Admin Pusat')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Muslim Nurdin',
                    'username' => 'muslimnurdin',
                    'email' => 'muslimnurdin1980@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Editor')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Dhanyawan - Editor',
                    'username' => 'dhanyawan',
                    'email' => 'dhany.haflah@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Editor')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Henry Lukmanul Hakim',
                    'username' => 'henrylukmanulhakim',
                    'email' => 'henrylookman1971@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Editor')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Asep Sufyan Nurdin',
                    'username' => 'asepsufyannurdin',
                    'email' => 'asepsofyannurdin@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Editor')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Andri Ridwan Fauzi',
                    'username' => 'andriridwanfauzi',
                    'email' => 'ridwan.fauzi@ayomedia.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Editor')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Fia Afifah',
                    'username' => 'FiaAfifah',
                    'email' => 'fiafifahrahmah@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Editor')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Ilmi Fadillah',
                    'username' => 'imifadhil',
                    'email' => 'imi3sidik@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Editor')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Agus Nurputra',
                    'username' => 'agusnurputra',
                    'email' => 'agusnurputra23@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Editor')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Taufik Ginanjar',
                    'username' => 'taufikginanjar23',
                    'email' => 'taufikginanjar23@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Editor')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Taufiqul Hakim',
                    'username' => 'iqul1999',
                    'email' => 'thakim1999@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Editor')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Isman Rahmani Yusron',
                    'username' => 'isman',
                    'email' => 'rahmaniyusron@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Editor')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Dila Fadhilah Fathir',
                    'username' => 'dilafathir16',
                    'email' => 'dilafathir16@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Editor')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Sami Kalammallah',
                    'username' => 'sami',
                    'email' => 'skalexsong@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Editor')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'jackerx',
                    'username' => 'jackerx',
                    'email' => 'jackerx@mail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Pengguna')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'abbuqu',
                    'username' => 'abbuqu',
                    'email' => 'fikrigo67@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Pengguna')->firstorfail()->id,
                    'status' => 1,
                ],
                [
                    'full_name' => 'Gicky Tamimi',
                    'username' => 'gickytamimi',
                    'email' => 'gickytamimi@gmail.com',
                    'password' => 'TempPassword#2024Persis',
                    'role' => Role::where('role_name', 'Pengguna')->firstorfail()->id,
                    'status' => 1,
                ],
            ];
            foreach ($user_data as $value) {
                if (!User::where('email', $value['email'])->first() && !User::where('username', $value['username'])->first()) {
                    $result = User::create($value);
                }
            }

            $default_user_editor = User::where('full_name', 'admin')->first()->id;
            $default_reporter_editor = Reporter::where('reporter_name', 'Reporter')->first()?->id;
            if (!$default_reporter_editor) {
                $default_reporter_editor = Reporter::create(['reporter_name' => 'Reporter', 'added_by' => $default_user_editor])->first()->id;
            }

            $tags_data = [
                [
                    "title" => "Covid-19",
                    "slug" => "covid-19",
                    "desc" => ""
                ],
                [
                    "title" => "Pesantren PERSIS",
                    "slug" => "pesantren-persis",
                    "desc" => ""
                ],
                [
                    "title" => "PP PERSIS",
                    "slug" => "pp-persis",
                    "desc" => ""
                ],
                [
                    "title" => "Pimpinan Wilayah",
                    "slug" => "pimpinan-wilayah",
                    "desc" => "Tag untuk berita-berita dari kontributor tingkat PW"
                ],
                [
                    "title" => "Persada 8",
                    "slug" => "persada-8",
                    "desc" => "Persada kedelapan PD Pemuda Persis Kabupaten Bandung 2021"
                ],
                [
                    "title" => "PERSIS",
                    "slug" => "persis",
                    "desc" => ""
                ],
                [
                    "title" => "Pimpinan Daerah",
                    "slug" => "pimpinan-daerah",
                    "desc" => "Tanda untuk berita tingkat PD Persis dan otonom"
                ],
                [
                    "title" => "Aceng Zakaria",
                    "slug" => "aceng-zakaria",
                    "desc" => "Tanda untuk ketua umum"
                ],
                [
                    "title" => "Ketua Umum Persis",
                    "slug" => "ketua-umum-persis",
                    "desc" => ""
                ],
                [
                    "title" => "Karya Imtaq",
                    "slug" => "karya-imtaq",
                    "desc" => ""
                ],
                [
                    "title" => "Pimpinan Cabang",
                    "slug" => "pimpinan-cabang",
                    "desc" => "Pimpinan Cabang Persis dan otonom"
                ],
                [
                    "title" => "Pendidikan Anak",
                    "slug" => "pendidikan-anak",
                    "desc" => ""
                ],
                [
                    "title" => "STAI PERSIS",
                    "slug" => "stai-persis",
                    "desc" => ""
                ],
                [
                    "title" => "Palestina",
                    "slug" => "palestina",
                    "desc" => ""
                ],
                [
                    "title" => "Pusat Zakat Umat",
                    "slug" => "pusat-zakat-umat",
                    "desc" => ""
                ],
                [
                    "title" => "Komunisme",
                    "slug" => "komunisme",
                    "desc" => ""
                ],
                [
                    "title" => "HIMA PERSIS",
                    "slug" => "hima-persis",
                    "desc" => ""
                ],
                [
                    "title" => "Risalah",
                    "slug" => "risalah",
                    "desc" => ""
                ],
                [
                    "title" => "Waketum PERSIS",
                    "slug" => "waketum-persis",
                    "desc" => ""
                ],
                [
                    "title" => "Pimpinan Pusat",
                    "slug" => "pimpinan-pusat",
                    "desc" => "Pengurus persis dan otonom tingkat pusat"
                ],
                [
                    "title" => "Pemudi Persis",
                    "slug" => "pemudi-persis",
                    "desc" => ""
                ],
                [
                    "title" => "Pemurtadan",
                    "slug" => "pemurtadan",
                    "desc" => ""
                ],
                [
                    "title" => "Perguruan Tinggi",
                    "slug" => "perguruan-tinggi",
                    "desc" => ""
                ],
                [
                    "title" => "Vaksinasi",
                    "slug" => "vaksinasi",
                    "desc" => ""
                ],
                [
                    "title" => "PERSISTRI",
                    "slug" => "persistri",
                    "desc" => ""
                ],
                [
                    "title" => "Santri Berprestasi",
                    "slug" => "santri-berprestasi",
                    "desc" => ""
                ],
                [
                    "title" => "Satgas PERSIS Respon Covid-19",
                    "slug" => "satgas-persis-respon-covid-19",
                    "desc" => ""
                ],
                [
                    "title" => "Dewam Hisbah",
                    "slug" => "dewam-hisbah",
                    "desc" => ""
                ],
                [
                    "title" => "Pemuda Persis",
                    "slug" => "pemuda-persis",
                    "desc" => ""
                ],
                [
                    "title" => "Gerhana",
                    "slug" => "gerhana",
                    "desc" => ""
                ],
                [
                    "title" => "Dewan Hisab dan Rukyat",
                    "slug" => "dewan-hisab-dan-rukyat",
                    "desc" => ""
                ],
                [
                    "title" => "Safari Dakwah",
                    "slug" => "safari-dakwah",
                    "desc" => ""
                ],
                [
                    "title" => "Pinjaman Online",
                    "slug" => "pinjaman-online",
                    "desc" => ""
                ],
                [
                    "title" => "Bidgar Ekonomi",
                    "slug" => "bidgar-ekonomi",
                    "desc" => ""
                ],
                [
                    "title" => "UNIPI",
                    "slug" => "unipi",
                    "desc" => ""
                ],
                [
                    "title" => "Pimpinan Jamaah",
                    "slug" => "pimpinan-jamaah",
                    "desc" => ""
                ],
                [
                    "title" => "Nahdlatul Ulama",
                    "slug" => "nahdlatul-ulama",
                    "desc" => ""
                ],
                [
                    "title" => "Muhammadiyah",
                    "slug" => "muhammadiyah",
                    "desc" => ""
                ],
                [
                    "title" => "Menteri Agama",
                    "slug" => "menteri-agama",
                    "desc" => ""
                ],
                [
                    "title" => "Dakwah Pelosok Negeri",
                    "slug" => "dakwah-pelosok-negeri",
                    "desc" => ""
                ],
                [
                    "title" => "Kafilah Du'at",
                    "slug" => "kafilah-duat",
                    "desc" => ""
                ],
                [
                    "title" => "HIMI PERSIS",
                    "slug" => "himi-persis",
                    "desc" => ""
                ],
                [
                    "title" => "Sigab PERSIS",
                    "slug" => "sigab-persis",
                    "desc" => ""
                ],
                [
                    "title" => "Wakaf",
                    "slug" => "wakaf",
                    "desc" => ""
                ],
                [
                    "title" => "Shurulkhan",
                    "slug" => "shurulkhan",
                    "desc" => ""
                ],
                [
                    "title" => "HIPPI",
                    "slug" => "hippi",
                    "desc" => "Himpunan Pengusaha Persatuan Islam"
                ],
                [
                    "title" => "Brigade PERSIS",
                    "slug" => "brigade-persis",
                    "desc" => ""
                ],
                [
                    "title" => "KKBH",
                    "slug" => "kkbh",
                    "desc" => "Kantor Konsultasi Bantuan Hukum"
                ],
                [
                    "title" => "Muktamar PERSIS",
                    "slug" => "muktamar-persis",
                    "desc" => ""
                ],
                [
                    "title" => "Muktamar Hima PERSIS",
                    "slug" => "muktamar-hima-persis",
                    "desc" => ""
                ],
                [
                    "title" => "IPTK",
                    "slug" => "iptk",
                    "desc" => ""
                ],
                [
                    "title" => "Dewan Hisbah",
                    "slug" => "dewan-hisbah",
                    "desc" => ""
                ],
                [
                    "title" => "Muktamar",
                    "slug" => "muktamar",
                    "desc" => ""
                ],
                [
                    "title" => "Muktamar Pemudi",
                    "slug" => "muktamar-pemudi",
                    "desc" => ""
                ],
                [
                    "title" => "KBIHU PERSIS",
                    "slug" => "kbihu-persis",
                    "desc" => ""
                ],
                [
                    "title" => "Muktamar XVI",
                    "slug" => "muktamar-xvi",
                    "desc" => ""
                ],
                [
                    "title" => "IPP",
                    "slug" => "ipp",
                    "desc" => ""
                ],
                [
                    "title" => "IPPi",
                    "slug" => "ippi",
                    "desc" => ""
                ],
                [
                    "title" => "bencana",
                    "slug" => "bencana",
                    "desc" => "bencana"
                ],
                [
                    "title" => "Taujiihaat",
                    "slug" => "taujiihaat",
                    "desc" => "Arahan pekanan dari Ketua Umum PP Persis"
                ],
                [
                    "title" => "Piala Dunia",
                    "slug" => "piala-dunia",
                    "desc" => ""
                ],
                [
                    "title" => "Respon PERSIS",
                    "slug" => "respon-persis",
                    "desc" => "Respon PERSIS"
                ],
                [
                    "title" => "LAZ PERSIS",
                    "slug" => "laz-persis",
                    "desc" => "LAZ PERSIS"
                ],
                [
                    "title" => "PTWQ",
                    "slug" => "ptwq",
                    "desc" => "Pondok Tahfizh Wadil Quran"
                ],
                [
                    "title" => "santri",
                    "slug" => "santri",
                    "desc" => "santri"
                ],
                [
                    "title" => "Haji",
                    "slug" => "haji",
                    "desc" => "Haji"
                ],
                [
                    "title" => "Haji & Umrah",
                    "slug" => "haji-umrah",
                    "desc" => ""
                ],
                [
                    "title" => "Dakwah",
                    "slug" => "dakwah",
                    "desc" => ""
                ],
                [
                    "title" => "Pendidikan",
                    "slug" => "pendidikan",
                    "desc" => ""
                ],
                [
                    "title" => "IAI PERSIS Bandung",
                    "slug" => "iai-persis-bandung",
                    "desc" => "IAI PERSIS Bandung"
                ],
                [
                    "title" => "Pemilu",
                    "slug" => "pemilu",
                    "desc" => ""
                ],
                [
                    "title" => "Advertorial",
                    "slug" => "advertorial",
                    "desc" => ""
                ]
            ];
            dump('Clean Tags Table');
            Tag::truncate();
            foreach ($tags_data as $value) {
                if (!Tag::where('title', $value['title'])->first()) {
                    $value["created_by"] = $default_user_editor;
                    $value["created_at"] = Carbon::now();
                    $value["updated_at"] = Carbon::now();

                    Tag::create($value);
                }
            }

            $category_data = [
                [
                    'category_name' => 'News',
                    'slug' => \Str::slug('News'),
                    'desc' => '',
                ],
                [
                    'category_name' => 'Kajian',
                    'slug' => \Str::slug('Kajian'),
                    'desc' => '',
                ],
                [
                    'category_name' => 'Istifta',
                    'slug' => \Str::slug('Istifta'),
                    'desc' => '',
                ],
                [
                    'category_name' => 'Tsaqofah',
                    'slug' => \Str::slug('Tsaqofah'),
                    'desc' => '',
                ],
            ];

            ContentCategories::truncate();
            dump('Clean Content Categories Table');
            foreach ($category_data as $value) {
                if (!ContentCategories::where('category_name', $value['category_name'])->first()) {
                    $value['status'] = 1;
                    $value['created_by'] = $default_user_editor;
                    ContentCategories::create($value);
                }
            }

            $category_sub_data = [
                // News
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'News')->firstorfail()->id,
                    'category_sub_name' => "Internasional",
                    'slug' => \Str::slug("Internasional"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'News')->firstorfail()->id,
                    'category_sub_name' => "Nasional",
                    'slug' => \Str::slug("Nasional"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'News')->firstorfail()->id,
                    'category_sub_name' => "Ja'miyyah",
                    'slug' => \Str::slug("Ja'miyyah"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'News')->firstorfail()->id,
                    'category_sub_name' => "Persistri",
                    'slug' => \Str::slug("Persistri"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'News')->firstorfail()->id,
                    'category_sub_name' => "Pemuda Persis",
                    'slug' => \Str::slug("Pemuda Persis"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'News')->firstorfail()->id,
                    'category_sub_name' => "Hima Persis",
                    'slug' => \Str::slug("Hima Persis"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'News')->firstorfail()->id,
                    'category_sub_name' => "Himi Persis",
                    'slug' => \Str::slug("Himi Persis"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'News')->firstorfail()->id,
                    'category_sub_name' => "IPP",
                    'slug' => \Str::slug("IPP"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'News')->firstorfail()->id,
                    'category_sub_name' => "IPPI",
                    'slug' => \Str::slug("IPPI"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'News')->firstorfail()->id,
                    'category_sub_name' => "Persis Pers",
                    'slug' => \Str::slug("Persis Pers"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'News')->firstorfail()->id,
                    'category_sub_name' => "Kepesantrenan",
                    'slug' => \Str::slug("Kepesantrenan"),
                    'desc' => ""
                ],

                // Kajian
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'Kajian')->firstorfail()->id,
                    'category_sub_name' => "Akidah",
                    'slug' => \Str::slug("Akidah"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'Kajian')->firstorfail()->id,
                    'category_sub_name' => "Dakwah",
                    'slug' => \Str::slug("Dakwah"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'Kajian')->firstorfail()->id,
                    'category_sub_name' => "Muamalah",
                    'slug' => \Str::slug("Muamalah"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'Kajian')->firstorfail()->id,
                    'category_sub_name' => "Ibadah",
                    'slug' => \Str::slug("Ibadah"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'Kajian')->firstorfail()->id,
                    'category_sub_name' => "Siyasah",
                    'slug' => \Str::slug("Siyasah"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'Kajian')->firstorfail()->id,
                    'category_sub_name' => "Hisab Rukyat",
                    'slug' => \Str::slug("Hisab Rukyat"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'Kajian')->firstorfail()->id,
                    'category_sub_name' => "Tarbiyyah",
                    'slug' => \Str::slug("Tarbiyyah"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'Kajian')->firstorfail()->id,
                    'category_sub_name' => "Risalah",
                    'slug' => \Str::slug("Risalah"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'Kajian')->firstorfail()->id,
                    'category_sub_name' => "Taujihat",
                    'slug' => \Str::slug("Taujihat"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'Kajian')->firstorfail()->id,
                    'category_sub_name' => "Istifta",
                    'slug' => \Str::slug("Istifta"),
                    'desc' => ""
                ],

                // Tsaqofah
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'Tsaqofah')->firstorfail()->id,
                    'category_sub_name' => "Khazanah",
                    'slug' => \Str::slug("Khazanah"),
                    'desc' => ""
                ],
                [
                    'content_categories_id' => ContentCategories::where('category_name', 'Tsaqofah')->firstorfail()->id,
                    'category_sub_name' => "Advertorial",
                    'slug' => \Str::slug("Advertorial"),
                    'desc' => ""
                ],
            ];

            ContentCategoriesSub::truncate();
            dump('Clean Content Categories Sub Table');
            foreach ($category_sub_data as $value) {
                if (!ContentCategoriesSub::where('category_sub_name', $value['category_sub_name'])->first()) {
                    $value['status'] = 1;
                    $value['created_by'] = $default_user_editor;
                    ContentCategoriesSub::create($value);
                }
            }

            Content::truncate();
            ContentPage::truncate();
            dump('Clean Content and Content Page Table');
            $posts = json_decode(file_get_contents(database_path('seeders/posts.json')), true);

            if ($posts) {
                foreach ($posts as $post) {
                    $thumbnail = $post['thumbnail'];
                    if (!empty($thumbnail) && !Str::isUrl($thumbnail)) {
                        $thumbnail = Storage::disk('s3')->url($thumbnail);
                        dump($thumbnail);
                    }

                    dump('Import ' . $post['title']);
                    $content_data = [
                        'featured_image' => $thumbnail,
                        'title' => $post['title'],
                        'desc_seo' => $post['meta_description'] ?? $post['title'],
                        'slug' => $post['slug'],
                        'content_type' => 'teks',
                        'content_category_sub_id' => $post['categories_slug'] ? ContentCategoriesSub::where('slug', $post['categories_slug'])->first()->id : ContentCategoriesSub::where('slug', 'nasional')->first()->id,
                        'status' => $post['published'],
                        'created_by' => $default_user_editor,
                        'reporter_id' => $default_reporter_editor,
                        'published_at' => $post['published_at'] ? Carbon::createFromFormat('d/n/Y H:i:s', $post['published_at'])->format('Y-m-d H:i:s') : now(),
                        'created_at' => $post['created_at'] ? Carbon::createFromFormat('d/n/Y H:i:s', $post['created_at'])->format('Y-m-d H:i:s') : $post['created_at'],
                        'updated_at' => $post['updated_at'] ? Carbon::createFromFormat('d/n/Y H:i:s', $post['updated_at'])->format('Y-m-d H:i:s') : $post['updated_at'],
                        'is_headline' => 0,
                        'display' => 'all'
                    ];

                    if (!Content::where('title', $post['title'])->where('desc_seo', $content_data['desc_seo'])->where('slug', $content_data['slug'])->first()) {
                        $content = Content::create($content_data);
                    }

                    $content_page_data = [
                        'content_id' => $content->id,
                        'content' => $post['content'],
                        'photo' => $thumbnail,
                        'created_at' => $post['created_at'] ? Carbon::createFromFormat('d/n/Y H:i:s', $post['created_at'])->format('Y-m-d H:i:s') : $post['created_at'],
                        'updated_at' => $post['updated_at'] ? Carbon::createFromFormat('d/n/Y H:i:s', $post['updated_at'])->format('Y-m-d H:i:s') : $post['updated_at'],
                        'page_order' => 0,
                    ];
                    if (!ContentPage::where('content', $post['content'])->first()) {
                        ContentPage::create($content_page_data);
                    }
                }
            }

            ContentTag::truncate();
            dump('Clean Content Tag Table');
            $posts_tag = json_decode(file_get_contents(database_path('seeders/posts_tags.json')), true);
            if ($posts_tag) {
                foreach ($posts_tag as $tag) {
                    $content_id = Content::where('slug', $tag['post_slug'])->first()->id;
                    $tags_id = Tag::where('slug', $tag['tag_slug'])->first()->id;

                    if (!ContentTag::where('content_id', $content_id)->where('tag_id', $tags_id)->first()) {
                        ContentTag::create(['content_id' => $content_id, 'tag_id' => $tags_id]);
                    }
                }
            }

            dump('Creating Headline');
            DB::transaction(function () {
                $latestRecords = Content::where('status', 1)->orderBy('published_at', 'desc')->take(5)->get();

                foreach ($latestRecords as $record) {
                    $record->is_headline = 1;
                    $record->save();
                }
            });
        } else {
            dump("file posts.json is not found");
        }
    }
}
