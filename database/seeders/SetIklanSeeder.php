<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\SetIklan;
use App\Models\Advertisements;

class SetIklanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $data = [
            [
                'title' => 'Iklan Pop-up',
                'order' => 0
            ],
            [
                'title' => 'Iklan Header',
                'order' => 1
            ],
            [
                'title' => 'Iklan Footer',
                'order' => 2
            ],
            [
                'title' => 'Iklan Samping 1',
                'order' => 3
            ],
            [
                'title' => 'Iklan Samping 2',
                'order' => 4
            ],
            [
                'title' => 'Iklan Sticky Kiri',
                'order' => 5
            ],
            [
                'title' => 'Iklan Sticky Kanan',
                'order' => 6
            ],
        ];

        foreach ($data as $value) {
            if (!SetIklan::where('title', $value['title'])->first()) {
                SetIklan::create($value);
            }
        }
    }
}
