<?php

namespace Database\Seeders;

use App\Models\Reporter;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ReporterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $added_by = User::firstorfail()->id;
        Reporter::create([
            'reporter_name' => 'Reporter Seeder',
            'added_by' => $added_by
        ]);
    }
}
