<?php

namespace Database\Seeders;

use App\Models\Content;
use App\Models\ContentTag;
use App\Models\Tag;
use Illuminate\Database\Seeder;

class ContentTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $content_satu = Content::where('slug', 'persis-dan-persistri-kota-tangerang-siap-tebar-1000-paket-sembako-selama-ramadan-tahun-ini')->firstorfail()->id;
        $content_dua = Content::where('slug', 'persis-dukung-penyelenggaraan-sidang-isbat-demi-minimalisasi-dampak-perbedaan-di-kalangan-umat-islam-indonesia')->firstorfail()->id;
        $content_tiga = Content::where('slug', 'dukung-fatwa-mui-persis-dorong-umat-islam-tak-menggunakan-produk-pro-israel')->firstorfail()->id;
        $content_empat = Content::where('slug', 'semakin-tangguh-di-era-digital')->firstorfail()->id;
        $content_lima = Content::where('slug', 'serba-serbi-haji-2024')->firstorfail()->id;
        $content_enam = Content::where('slug', 'praktek-manasik-haji-khusus-karya-Imtaq-persatuan-islam')->firstorfail()->id;

        $data = [
            [
                'content_id' => $content_satu,
                'tag_id' => Tag::where('title', 'Sembako')->firstorfail()->id
            ],            [
                'content_id' => $content_satu,
                'tag_id' => Tag::where('title', 'Ramadhan')->firstorfail()->id
            ],            [
                'content_id' => $content_dua,
                'tag_id' => Tag::where('title', 'Sidang Isbat')->firstorfail()->id
            ],            [
                'content_id' => $content_dua,
                'tag_id' => Tag::where('title', 'Kalender')->firstorfail()->id
            ],            [
                'content_id' => $content_dua,
                'tag_id' => Tag::where('title', 'Hijriyyah')->firstorfail()->id
            ],            [
                'content_id' =>$content_dua,
                'tag_id' => Tag::where('title', 'Ramadhan')->firstorfail()->id
            ],            [
                'content_id' =>$content_tiga,
                'tag_id' => Tag::where('title', 'Palestina')->firstorfail()->id
            ],            [
                'content_id' =>$content_tiga,
                'tag_id' => Tag::where('title', 'Boikot')->firstorfail()->id
            ],            [
                'content_id' => $content_empat,
                'tag_id' => Tag::where('title', 'Digital')->firstorfail()->id
            ],            [
                'content_id' => $content_lima,
                'tag_id' => Tag::where('title', 'Haji')->firstorfail()->id
            ],            [
                'content_id' => $content_enam,
                'tag_id' => Tag::where('title', 'Haji')->firstorfail()->id
            ],
        ];

        ContentTag::truncate();
        foreach ($data as $value) {
            ContentTag::create($value);
        }
    }
}
