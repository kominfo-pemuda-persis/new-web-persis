<?php

namespace Database\Seeders;

use App\Models\ContentCategories;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class ContentCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $editor_role = Role::where('role_name', 'Editor')->firstorfail()->id;
        $data = [
            [
                'category_name' => 'Berita',
                'slug' => \Str::slug('Berita'),
                'desc' => 'Konten Berita',
                'status' => 1,
                'created_by' => User::where('role', $editor_role)->firstorfail()->id,
            ],
            [
                'category_name' => 'Artikel',
                'slug' => \Str::slug('Artikel'),
                'desc' => 'Konten Artikel',
                'status' => 1,
                'created_by' => User::where('role', $editor_role)->firstorfail()->id,
            ],
            [
                'category_name' => 'Kajian',
                'slug' => \Str::slug('Kajian'),
                'desc' => 'Konten Kajian',
                'status' => 1,
                'created_by' => User::where('role', $editor_role)->firstorfail()->id,
            ],
            [
                'category_name' => 'Konten Multimedia',
                'slug' => \Str::slug('Konten Multimedia'),
                'desc' => 'Konten Berupa Gambar atau Video',
                'status' => 1,
                'created_by' => User::where('role', $editor_role)->firstorfail()->id,
            ],
        ];
        foreach ($data as $value) {
            if (!ContentCategories::where('category_name', $value['category_name'])->first()) {
                ContentCategories::create($value);
            }
        }
    }
}
