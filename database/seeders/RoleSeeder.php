<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'role_name' => 'Admin Pusat',
                'desc' => 'User untuk Admin Pusat',
                'status' => 1,
            ],
            [
                'role_name' => 'Admin Otonom',
                'desc' => 'User Untuk Admin Otonom',
                'status' => 1,
            ],
            [
                'role_name' => 'Editor',
                'desc' => '',
                'status' => 1,
            ],
            [
                'role_name' => 'Pengguna',
                'desc' => 'User untuk pengguna',
                'status' => 0,
            ],
        ];
        foreach ($data as $value) {
            if (!Role::where('role_name', $value['role_name'])->first()) {
                Role::create($value);
            }
        }
    }
}
