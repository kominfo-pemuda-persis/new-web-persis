<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'name' => 'TGM',
            ],
            [
                'name' => 'Lazpersis',
            ],
            [
                'name' => 'PP PERSIS',
            ],
        ];

        foreach ($data as $value) {
            if (!Client::where('name', $value['name'])->first()) {
                Client::create($value);
            }
        }
    }
}
