<?php

namespace Database\Seeders;

use App\Models\Content;
use App\Models\ContentCategoriesSub;
use App\Models\Reporter;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $editor_role = Role::where('role_name', 'Editor')->firstorfail()->id;
        $editor_id = User::where('role', $editor_role)->firstorfail()->id;
        $reporter_id = Reporter::first()->id;
        $data = [
            [
                'title' => 'Persis dan Persistri Kota Tangerang Siap Tebar 1.000 Paket Sembako Selama Ramadan Tahun Ini',
                'featured_image' => 'https://i.ibb.co/NxZxTh7/Whats-App-Image-2024-03-07-at-18-08-27.jpg[/img][/url]',
                'desc_seo' => 'Persis dan Persistri Kota Tangerang Siap Tebar 1.000 Paket Sembako Selama Ramadan Tahun Ini',
                'slug' => 'persis-dan-persistri-kota-tangerang-siap-tebar-1000-paket-sembako-selama-ramadan-tahun-ini',
                'content_category_sub_id' => ContentCategoriesSub::where('category_sub_name', "Berita Jam'iyyah")->firstorfail()->id,
                'content_type' => 'teks',
                'is_headline' => 1,
                'display' => 'all',
                'visited' => 0,
                'status' => 1,
                'scheduled_date' => null,
                'created_by' => $editor_id,
                'published_at' => now(),
                'reporter_id' => $reporter_id,
            ],
            [
                'title' => 'Persis Dukung Penyelenggaraan Sidang Isbat Demi Minimalisasi Dampak Perbedaan di Kalangan Umat Islam Indonesia',
                'featured_image' => 'https://i.ibb.co/LYQMrf9/IMG-20240310-WA0020.jpg[/img][/url]',
                'desc_seo' => 'Persis Dukung Penyelenggaraan Sidang Isbat Demi Minimalisasi Dampak Perbedaan di Kalangan Umat Islam Indonesia',
                'slug' => 'persis-dukung-penyelenggaraan-sidang-isbat-demi-minimalisasi-dampak-perbedaan-di-kalangan-umat-islam-indonesia',
                'content_category_sub_id' => ContentCategoriesSub::where('category_sub_name', "Berita Jam'iyyah")->firstorfail()->id,
                'content_type' => 'teks',
                'is_headline' => 1,
                'display' => 'all',
                'visited' => 0,
                'status' => 1,
                'scheduled_date' => null,
                'created_by' => $editor_id,
                'published_at' => now(),
                'reporter_id' => $reporter_id,
            ],
            [
                'title' => 'Dukung Fatwa MUI, PERSIS Dorong Umat Islam Tak Menggunakan Produk Pro-Israel',
                'featured_image' => 'https://i.ibb.co/BwmvRfV/Gambar-Whats-App-2024-03-11-pukul-11-39-16-000b94da.jpg',
                'desc_seo' => 'Dukung Fatwa MUI, PERSIS Dorong Umat Islam Tak Menggunakan Produk Pro-Israel',
                'slug' => 'dukung-fatwa-mui-persis-dorong-umat-islam-tak-menggunakan-produk-pro-israel',
                'content_category_sub_id' => ContentCategoriesSub::where('category_sub_name', 'Berita Internasional')->firstorfail()->id,
                'content_type' => 'teks',
                'is_headline' => 1,
                'display' => 'pages',
                'visited' => 0,
                'status' => 1,
                'scheduled_date' => null,
                'created_by' => $editor_id,
                'published_at' => now(),
                'reporter_id' => $reporter_id,
            ],
            [
                'title' => 'Semakin Tangguh di Era Digital',
                'featured_image' => 'https://pasteboard.co/gPseheuTwHOx.jpg',
                'desc_seo' => 'Secara alamiah manusia memiliki ketangguhan, kecenderungan untuk bangkit dari keterpurukan, kemampuan beradaptasi dengan situasi yang menyulitkan.',
                'slug' => 'semakin-tangguh-di-era-digital',
                'content_category_sub_id' => ContentCategoriesSub::where('category_sub_name', 'Pemikiran')->firstorfail()->id,
                'content_type' => 'teks',
                'is_headline' => 0,
                'display' => 'all',
                'visited' => 0,
                'status' => 1,
                'scheduled_date' => null,
                'created_by' => $editor_id,
                'published_at' => now(),
                'reporter_id' => $reporter_id,
            ],
            [
                'title' => 'Serba Serbi Haji 2024',
                'featured_image' => 'https://i.ibb.co/Zgj2DsT/Untitled.jpg',
                'desc_seo' => 'Serba Serbi Haji 2024',
                'slug' => 'serba-serbi-haji-2024',
                'content_category_sub_id' => ContentCategoriesSub::where('category_sub_name', "Bingkai Jam'iyyah")->firstorfail()->id,
                'content_type' => 'gambar',
                'is_headline' => 0,
                'display' => 'pages',
                'visited' => 0,
                'status' => 1,
                'scheduled_date' => null,
                'created_by' => $editor_id,
                'published_at' => now(),
                'reporter_id' => $reporter_id,
            ],
            [
                'title' => 'Praktek Manasik Haji Khusus Karya Imtaq Persatuan Islam',
                'featured_image' => 'https://i.ibb.co/MZMS7mk/Whats-App-Image-2024-04-05-at-16-43-07.jpg[/img][/url]',
                'desc_seo' => 'Praktek Manasik Haji Khusus Karya Imtaq Persatuan Islam',
                'slug' => 'praktek-manasik-haji-khusus-karya-Imtaq-persatuan-islam',
                'content_category_sub_id' => ContentCategoriesSub::where('category_sub_name', "Kejam'iyyahan")->firstorfail()->id,
                'content_type' => 'video',
                'is_headline' => 0,
                'display' => 'all',
                'visited' => 0,
                'status' => 1,
                'scheduled_date' => null,
                'created_by' => $editor_id,
                'published_at' => now(),
                'reporter_id' => $reporter_id,
            ],
        ];

        foreach ($data as $value) {
            if (!Content::where('title', $value['title'])->first()) {
                Content::create($value);
            }
        }
    }
}
